const colors = {
  primary: "#084c8d",
  background: "#fff",
  primaryLight: "#019fc6",
  white: "#fff",
  black: "#000",
  orange: "#eda13a",
  green: "#5ca110",
  greenDark: "#77b300",
  ratingBorder: "#979797",
  ratingBackground: "#d8d8d8",
  danger: "#f00",
  transparent: "transparent",
  greyStar: "#9b9b9b",
  grey: "#dfdfdf",
  searchBg: "#f6f6f6",
  magento: "#830065",
  textBlue: "#019fc6",
  cyan: "#00a7b5",
  filterBg: "#00a7b5",
  blue: "#084c8d",
  textPicker: "#3f403e",
  text: "#63666a",
  textDark: "#3f403e",
  textGrey: "#4a4a4a",
  textSuggestion: "#878787",
  textHeader: "#3d3935",
  border: "#cfcfcf",
  tickColor: "#addc91",
  iconColor: "#009dd8",

  requestContainerBg: "#78baca",
  estimateContainerBg: "#019fc6",
  estimateBg: "#6fa6b4",
  requestBg: "#f5a623",
  requestDarkBg: "#d48a12",
  placeHolderSuggestion: "#8c8c8c",
  errorColor: "#a8071a"
};

export default colors;
