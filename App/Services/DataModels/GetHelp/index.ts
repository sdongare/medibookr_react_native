interface DrugResponse {
  Score: string;
  DisplayName: string;
  ID: string;
  SEOName: string;
  NDC: string;
}

interface PriceResponse {
  Address: PriceAddress;
  Distance: number;
  Nabp: string;
  Name: string;
  NcpdpChaincode: string;
  Price: PriceObject[];
}

interface PriceObject {
  Price: number;
  UsualAndCustomary: number;
}

interface PriceAddress {
  Address1: string;
  Address2: string;
  City: string;
  Latitude: number;
  Longitude: number;
  PostalCode: string;
  State: string;
}

interface InfoMessage {
  key: string;
  title: string;
  message: string;
}