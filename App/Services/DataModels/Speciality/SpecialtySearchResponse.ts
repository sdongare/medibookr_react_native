interface SpecialtySearchResponse {
  count: number;
  limit: number;
  results: ResultsSpecialtyItem[];
}
interface ResultsSpecialtyItem {
  specialtyId: string;
  specialty: Specialty;
}
interface Specialty {
  id: number;
  specialtyId: string;
  level_0: string;
  level_1: string;
  level_2: string;
  level_3: null;
}
