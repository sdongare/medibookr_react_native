interface PatientsInfo {
  fullName: string;
  id: number;
  userId: number;
  providerId: string;
  firstName: string;
  lastName: string;
  email: string;
  clientId: string;
  dob: string;
  address: string;
  phone: string;
  city: string;
  state: string;
  zipcode: string;
  insurance: Insurance;
}

interface Insurer {
  id: number;
  name: string;
  state: string;
  pokitdokTradingPartnerId: string;
}

interface Insurance {
  insurerId: string;
  patientId: number;
  policyNumber: string;
  groupNumber: string;
  planId: string;
  planType: null;
  insurerPlan: InsurerPlan;
}

interface InsurerPlan {
  planId: string;
  name: string;
  pokitdokTradingPartnerId: string;
}