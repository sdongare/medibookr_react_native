interface PCPResponse {
  message: string;
  addressLine1: string;
  addressLine2: string;
  age: string;
  city: string;
  credentials: string;
  description: string;
  email: string;
  facilityFlag: null;
  gender: string;
  inPracticeSince: string;
  inTheMedibookrFlag: null;
  latitude: string;
  logo: null;
  longitude: string;
  name: string;
  npi: null;
  phone: string;
  practice: null;
  preferredFlag: boolean;
  providerId: string;
  specialty: string;
  state: string;
  status: string;
  zipcode: string;
}
