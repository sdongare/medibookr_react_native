interface BenefitsResponse {
  meta: Meta;
  data: Data;
}
interface Meta {
  processing_time: number;
  application_mode: string;
  credits_billed: number;
  credits_remaining: number;
  rate_limit_cap: number;
  rate_limit_reset: number;
  rate_limit_amount: number;
  activity_id: string;
}
interface Data {
  client_id: string;
  correlation_id: string;
  coverage: Coverage;
  payer: Payer;
  provider: Provider;
  subscriber: Subscriber;
  valid_request: boolean;
  trading_partner_id: string;
  service_types: string[];
  service_type_codes: string[];
  summary: Summary;
  pharmacy: Pharmacy;
  originating_company_id: string;
  trace_number: string;
  benefit_related_entities: BenefitRelatedEntitiesItem[];
}
interface Coverage {
  eligibility_begin_date: string;
  plan_begin_date: string;
  service_date: string;
  insurance_type: string;
  plan_description: string;
  active: boolean;
  plan_number: string;
  group_number: string;
  group_description: string;
  coinsurance: CoinsuranceItem[];
  contacts: ContactsItem[];
  copay: CopayItem[];
  deductibles: DeductiblesItem[];
  out_of_pocket: OutOfPocketItem[];
  limitations: LimitationsItem[];
  service_types: string[];
  service_type_codes: string[];
  level: string;
  plans: PlansItem[];
}
interface CoinsuranceItem {
  messages: MessagesItem[];
  plan_description: string;
  service_types: string[];
  service_type_codes: string[];
  benefit_percent: number;
  in_plan_network: string;
  coverage_level: string;
}
interface MessagesItem {
  message: string;
}
interface ContactsItem {
  contact_type: string;
  name: string;
  address: Address;
}
interface Address {
  city: string;
  zipcode: string;
  state: string;
  address_lines: string[];
}
interface CopayItem {
  messages: MessagesItem[];
  plan_description: string;
  service_types: string[];
  service_type_codes: string[];
  in_plan_network: string;
  coverage_level: string;
  copayment: Copayment;
}
interface Copayment {
  amount: string;
  currency: string;
}
interface DeductiblesItem {
  messages: MessagesItem[];
  eligibility_date?: string;
  plan_description: string;
  in_plan_network: string;
  coverage_level: string;
  time_period: string;
  benefit_amount: BenefitAmount;
  service_types: string[];
  service_type_codes: string[];
}
interface BenefitAmount {
  amount: string;
  currency: string;
}
interface OutOfPocketItem {
  plan_description: string;
  in_plan_network: string;
  coverage_level: string;
  benefit_amount: BenefitAmount;
  service_types: string[];
  service_type_codes: string[];
  time_period?: string;
}
interface LimitationsItem {
  messages: MessagesItem[];
  time_period_qualifier?: string;
  plan_description: string;
  coverage_level: string;
  service_types: string[];
  service_type_codes: string[];
  in_plan_network?: string;
}
interface PlansItem {
  plan_description: string;
  plan_number: string;
  group_description: string;
  group_number: string;
  insurance_type: string;
}
interface Payer {
  id: string;
  name: string;
}
interface Provider {
  first_name: string;
  last_name: string;
  npi: string;
}
interface Subscriber {
  address: Address;
  birth_date: string;
  gender: string;
  first_name: string;
  last_name: string;
  id: string;
  group_number: string;
}
interface Summary {
  deductible: Deductible;
  out_of_pocket: OutOfPocket;
}
interface Deductible {
  family: Family;
  individual: Individual;
}
interface Family {
  in_network: InNetwork;
  out_of_network: OutOfNetwork;
}
interface InNetwork {
  limit: Limit;
  applied: Applied;
  remaining: Remaining;
}
interface Limit {
  amount: string;
  currency: string;
}
interface Applied {
  amount: string;
  currency: string;
}
interface Remaining {
  amount: string;
  currency: string;
}
interface OutOfNetwork {
  limit: Limit;
  applied: Applied;
  remaining: Remaining;
}
interface Individual {
  in_network: InNetwork;
  out_of_network: OutOfNetwork;
}
interface OutOfPocket {
  family: Family;
  individual: Individual;
}
interface Pharmacy {
  is_eligible: boolean;
}
interface BenefitRelatedEntitiesItem {
  entity_identifier_code: string;
  entity_type: string;
  organization_name: string;
  address?: Address;
  eligibility_or_benefit_information: string;
  benefit_amount: BenefitAmount;
  service_types?: string[];
  service_type_codes?: string[];
}
