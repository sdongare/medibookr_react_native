import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";

export type RequestAppointmentResponse = RootObjectItem[];
interface RootObjectItem {
  id: number;
  provider: ProviderResultsItem;
  providerId: string;
  patientId: number;
  serviceId: number;
  dependentId: string;
  datetime: string;
  option1datetime: string;
  option1price: string;
  option2datetime: string;
  option2price: string;
  option3datetime: string;
  option3price: string;
  option4datetime: string;
  option4price: string;
  option5datetime: string;
  option5price: string;
  status: number;
  appointmentType: number;
  additionalNotes: string;
  comment: string;
  PatientId: number;
}
