export default interface SearchByNameResponse {
  count: number;
  limit: number;
  results: ProviderResultsItem[];
}

export interface ProviderResultsItem {
  serviceId: string;
  mapTileUrl: string;
  providerId: string;
  providerName: string;
  copay: string;
  credentials: string;
  practice: null;
  inNetwork: number;
  providerEmail: string;
  providerPhone: string;
  providerAddressLine1: string;
  providerAddressLine2: string;
  providerCity: string;
  providerState: string;
  providerZipcode: string;
  age: string;
  gender: string;
  latitude: string;
  longitude: string;
  npi: null;
  facilityFlag: boolean;
  inPracticeSince: string;
  specialty: string;
  preferredFlag: number;
  logo: null;
  inTheMedibookrFlag: null;
  status: string;
  distance: number;
  rating: string;
  ratingCount: number;
  oopPrice: string;
  insFlag: number;
  insDollarSigns: number;
  cashPrice: string;
  cashFlag: number;
  cashDollarSigns: number;
}
