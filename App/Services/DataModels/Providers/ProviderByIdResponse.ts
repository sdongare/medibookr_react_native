import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";

export interface ProviderByIdResponse {
  providerId: string;
  providerName: string;
  practice: null;
  providerEmail: string;
  providerAddress: string;
  providerCity: string;
  providerState: string;
  providerZipcode: string;
  providerPhone: string;
  description: string;
  latitude: string;
  longitude: string;
  npi: null;
  preferredFlag: boolean;
  logo: null;
  inTheMedibookrFlag: null;
  gender: string;
  age: string;
  inPracticeSince: string;
  specialty: string;
  credentials: string;
  facilityFlag: boolean;
  distance: number;
  caregivers: ProviderResultsItem[];
  affiliations: ProviderResultsItem[];
  certifications: CertificationsItem[];
  educations: EducationsItem[];
  services: ServicesItem[];
  reviews: ReviewsItem[];
  rating: string;
  countRating: number;
}

interface EducationsItem {
  medschoolName: string;
  year: string;
}

interface CertificationsItem {
  certificateId: string;
  name: string;
  boardName: string;
}

interface ServicesItem {
  cashPrice: number;
  serviceId: string;
  cptCode: string;
  serviceName: string;
}

export interface ReviewsItem {
  id: number;
  patientId: number;
  providerId: string;
  rating: number;
  reviewDate: string;
  reviewText: string;
}
