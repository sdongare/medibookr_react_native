import {Api} from "@root/App/Services/Api";
import {from} from "rxjs";

export default (): Api => {

  return {
    // Functions return fixtures
    common: {
      login: (username, password) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/login.json"),
      })),
      forgotPassword: (email) => from(Promise.resolve({
        ok: true,
        data: {success: true},
      })),
      register: (name, dob, registrationId, email, password) => from(Promise.resolve({
        ok: true,
        data: {},
      })),
      getHelp: () => from(Promise.resolve({ok: true})),
      pharmacyCompare: (data) => from(Promise.resolve({ok: true})),
    },
    auth: (token) => ({
      logout: () => from(Promise.resolve({
        ok: true,
        data: {},
      })),
      specialitySearch: (keyword, lat, long) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/specialities"),
      })),
      searchByName: (keyword, lat, long) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/byname.json"),
      })),
      serviceSearch: (keyword, lat, long) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/services.json"),
      })),
      providerByService: (id, lat, long, payer) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/providerByService.json"),
      })),
      providerBySpeciality: (id, lat, long, payer) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/providerBySpeciality.json"),
      })),
      patientProfile: (email) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/patientsinfo.json"),
      })),
      providerById: (id, lat, long) => from(Promise.resolve({
        ok: true,
        data: id === "IP00010356" ? require("../Fixtures/providerbyid.json") : require("../Fixtures/providerbyidf.json"),
      })),
      patientBenefits: (email) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/benefits.json"),
      })),
      getAppointments: (email) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/appointments.json"),
      })),
    }),
    google: {
      geocoding: (request) => from(Promise.resolve({
        ok: true,
        data: require("../Fixtures/geocoding.json"),
      })),
    },
  };
};

/*Promise<ApiResponse<LoginResponse>> => {
  return Promise.resolve({
    ok: true,
    data: require("../Fixtures/root.json"),
  } as ApiResponse<any>);
},
};*/
