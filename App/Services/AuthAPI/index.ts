// a library to wrap and simplify api calls
import {create as apicreate} from "apisauce";
import AppConfig from "@root/App/Config/AppConfig";
import {from} from "rxjs";
import SearchByNameResponse, {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {ProviderByIdResponse} from "@root/App/Services/DataModels/Providers/ProviderByIdResponse";
import {RequestAppointmentResponse} from "@root/App/Services/DataModels/Patient/RequestAppointmentResponse";

export interface AuthAPI {
  logout;
  searchByName;
  serviceSearch;
  specialitySearch;
  specialitySearchByKeyword;
  providerByService;
  providerBySpeciality;
  providerById;
  pcp;
  pharmacyCompare;
  resetPassword;
  infoMessage;
  patientProfile;
  getHelp;
  updatePatient;
  updatePatientPCP;
  patientBenefits;
  getAppointments;
  requestAppointment;
  getDrugName;
  sendFeedback;
  costEstimate;
  getInsurance;
  getResources;
  resetBaseURL;
}

// our "constructor"
const createAuthAPI = (authorization: string): AuthAPI => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apicreate({
    // base URL is read from the "constructor"
    baseURL: `${AppConfig.url()}api/`,
    // here are some default headers
    headers: {
      "accept-version": "v1",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${authorization}`,
    },
    // 30 seconds timeout...
    timeout: 30000,
  });


  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //

  const auth: AuthAPI = {
    getInsurance: (email) => from(api.get(`patients/insurance`, {email})),
    resetBaseURL: () => api.setBaseURL(`${AppConfig.url()}api/`),
    providerByService: (serviceId: string, lat: string, long: string, email) =>
      from(
        api.get<ProviderResultsItem[]>(
          "providers/by_service",
          {serviceId, lat, long, email},
        ),
      ),
    sendFeedback: (params) => from(api.post("contact_us/userFeedback", params)),
    costEstimate: (data, {email, lat, long}) => from(api.post("providers/costEstimateRequest?email="
      + email + "&lat=" + lat + "&long=" + long, data)),
    logout: (email) => from(api.get("patients/logout", {email})),
    getDrugName: (text) => from(api.get(`drugs/search?name=${text}&limit=10`)),
    providerBySpeciality: (specialtyId: string, lat: string, long: string, email) =>
      from(
        api.get<ProviderResultsItem[]>("providers/by_specialty", {
          specialtyId,
          lat,
          long,
          email,
        }),
      ),
    resetPassword: (data) => from(api.put("patients/password", data)),
    infoMessage: (data) => from(api.get("services/info-message?key="+data)),
    searchByName: (keyword, lat, long, email) => from(api.get<SearchByNameResponse>("providers/searchByName", {
      keyword,
      lat,
      long,
      email,
    })),
    serviceSearch: (keyword, lat, long, email) => from(api.get<ServicesSearchResponse>("services/search", {
      keyword,
      lat,
      long,
      email,
    })),
    specialitySearch: (keyword, lat, long, email) => from(api.get<SpecialtySearchResponse>("specialties/search", {
        keyword, lat, long, email,
      }),
    ),
    specialitySearchByKeyword: (keyword, lat, long, email) => from(api.get<SpecialtySearchResponse>("specialties/searchSpecialtyByKeyword/", {
        keyword, lat, long, email,
      }),
    ),
    patientProfile: (email) => from(api.get<PatientsInfo>("patients/", {email})),
    updatePatient: (data, email) => from(api.put("patients/?email=" + email, data)),
    updatePatientPCP: (pcp, email) => from(api.put(`patients/provider/${pcp}/?email=${email}`)),
    providerById: (id: string, lat: string, long: string, email: string) =>
      from(
        api.get<ProviderByIdResponse>(`providers/${id}`, {
          lat, long, email,
        }),
      ),
    // getHelp: (name, email, message, contactMethod) => from(api.post("contact_us/userFeedback?", {
    //   name, email, message, contactMethod,
    // })),
    getHelp: (question, contactMethod) =>
      from(
        api.post("contact_us/medicalBillHelp", {
          question,
          contactMethod,
        }),
      ),
    getResources: (patientId) => from(api.get(`clients/healthpartners/${patientId}`)),
    pcp: (email) => from(api.get("patients/provider", {email})),
    patientBenefits: (email) => from(api.get<BenefitsResponse>("patients/benefits", {email})),
    getAppointments: (email) =>
      from(api.get<RequestAppointmentResponse>("patients/appointments", {email})),
    requestAppointment: (data, email) =>
      from(api.post(`providers/appointments?email=${email}`, data)),
    pharmacyCompare: (params) =>
      from(
        api.post(
          `pharmacies/estimateReq/?drugName=${params.drugName}${
            params.dosage ? "&dosage=" + params.dosage : ""
            }${
            params.quantity ? "&quantity=" + params.quantity : ""
            }&contactMethod=${params.contactMethod}${
            params.comments ? "&comment=" + params.comments : ""
            }`,
        ),
      ),
  };

  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return auth;
};

// let's return back our create method as the default.
export default {
  createAPI: createAuthAPI,
};
