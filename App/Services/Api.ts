import authAPICreator, {AuthAPI} from "@root/App/Services/AuthAPI";
import apiCreator, {CommonAPI} from "@root/App/Services/UnsecureAPI";
import googleAPICreatpr, {GoogleAPI} from "@root/App/Services/GoogleAPI";
import RefillAPICreator, {RefillWiseAPI} from "@root/App/Services/RefillwiseAPI";

export interface Api {
  auth: (token: any) => AuthAPI;
  common: CommonAPI;
  refillWise: RefillWiseAPI;
  google: GoogleAPI;
  changeBaseUrl: () => void;
}

const common = apiCreator.createAPI();
const google = googleAPICreatpr.createGoogleAPI();
let auth: AuthAPI;

const API: Api = {
  auth: (token: string) => {
    auth = authAPICreator.createAPI(token);
    return auth;
  },
  refillWise: RefillAPICreator.createApi(),
  common,
  google,
  changeBaseUrl: () => {
    if (auth) {
      auth.resetBaseURL();
    }
    common.resetBaseURL();
  },
};

export default API;
