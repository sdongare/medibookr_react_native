import {Epic, ofType} from "redux-observable";
import {IDependencies} from "@root/App/Epics";
import {map, mergeMap} from "rxjs/operators";
import {RootState} from "@root/App/Reducers";
import {ResourceReducersActions} from "@root/App/Reducers/ResourceReducers";
import {getType} from "typesafe-actions";
import {of} from "rxjs";
import * as R from "ramda";

export const resourceEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(ResourceReducersActions.request)),
  mergeMap((action) => {
    const token =  R.path(["login", "data", "authToken"], state$.value);
    const patientID = (state$.value as RootState).patient.info.clientId;
    const resources = (state$.value as RootState).resources.data;

    if (resources) {
      return of(ResourceReducersActions.success(resources));
    } else {
      return api.auth(token).getResources(patientID).pipe(
        map((response: any) => {
          if (response.ok) {
            return ResourceReducersActions.success(response.data);
          } else {
            return ResourceReducersActions.failure({action, response});
          }
        }),
      );
    }
  }),
);
