import {Epic, ofType} from "redux-observable";
import {DashboardActions} from "@root/App/Reducers/DashboardReducers";
import {IDependencies} from "@root/App/Epics";
import {from, of, zip} from "rxjs";
import {combineAll, map, mergeMap, takeUntil} from "rxjs/operators";
import {ApiResponse} from "apisauce";
import {getLocation, notEmptyAndNull} from "@root/App/Lib/AppHelper";
import {LatLng} from "react-native-maps";
import {AuthAPI} from "@root/App/Services/AuthAPI";
import {getType} from "typesafe-actions";
import {Alert, AsyncStorage} from "react-native";
import {Actions} from "react-native-router-flux";
import * as R from "ramda";
import {getPatientEmailBase64} from "@root/App/Reducers/PatientsReducers";
import {SearchType} from "@root/App/Containers/SearchScreen/SearchScreen";
import {AppActions} from "@root/App/Reducers/AppReducers";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import moment from "moment";

const KEY_SEARCH_COUNT = "keySearchCount";
const KEY_SEARCH_50_TIME = "keySearchTime";

const providerSearch = (api: AuthAPI, keyword: string, location: LatLng, email: string) =>
  api.searchByName(keyword, location && location.latitude.toString(), location && location.longitude.toString(), email);

const serviceSearch = (api: AuthAPI, keyword: string, location: LatLng, email: string) =>
  api.serviceSearch(keyword, location.latitude.toString(), location.longitude.toString(), email);

const specialitySearch = (api: AuthAPI, keyword: string, location: LatLng, email: string) =>
  api.specialitySearch(keyword, location.latitude.toString(), location.longitude.toString(), email);

const specialitySearchByKeyword = (api: AuthAPI, keyword: string, location: LatLng, email: string) =>
  api.specialitySearchByKeyword(keyword, location.latitude.toString(), location.longitude.toString(), email);

export const epicSearchByName: Epic = (action$, state$, {api}) => action$.pipe(
  ofType(getType(DashboardActions.requestByName)),
  mergeMap((action) => {
    const location = getLocation(state$.value.location);
    const authToken = R.path(["login", "data", "authToken"], state$.value);
    const email = getPatientEmailBase64(state$.value);

    return providerSearch(api.auth(authToken), action.payload, location, email).pipe(
      takeUntil(action$.pipe(ofType(getType(DashboardActions.clearByName)))),
      map((response: any) => {
        if (response.ok && R.path(["data", "results", "status"], response) !== 400) {
          return DashboardActions.byNameSuccess(response.data);
        } else {
          return DashboardActions.failure({action, response});
        }
      }),
    );
  }),
);

export const epicSearchSpecialtyKeyword: Epic = ((action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(DashboardActions.requestSpecialtyByKeyword)),
  mergeMap((action): any => {
    const root = state$.value;
    const location = getLocation(root.location);
    const auth = R.path(["login", "data", "authToken"], root);

    if (!location) {
      return of(DashboardActions.failure({
        action,
        response: {data: {error: "Location not found"}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
      }));
    }
    const keyword = action.payload;

    /*Answers.logSearch(keyword, {
      searchType: "Search Specialty",
      searchAction: "Keyword",
    });*/

    const email = getPatientEmailBase64(state$.value);

    return api.auth(auth).specialitySearchByKeyword(action.payload, location.latitude, location.longitude, email)
      .pipe(
        mergeMap((response: any) => {
            if (response.ok && !R.isEmpty(response.data.results)) {
              const dataResult = response.data.results[action.payload];
              const results = R.compose(
                R.flatten,
                R.map(specialty => R.compose(R.map(R.assoc("specialty", specialty.name)), R.prop("providers"))(specialty)),
                R.values,
                R.prop("specialties"),
              )(dataResult);

              Actions.popTo("landing");
              Actions.push("search", {results, searchType: SearchType.Specialty, item: action.payload});

              return of(DashboardActions.specialitiesSuccess());
            } else {
              Actions.push("search", {results: [], searchType: SearchType.Specialty, item: action.payload});
              return of(DashboardActions.searchFailure());
            }
          },
        ));
  }),
));


export const epicSearchKeyword: Epic = ((action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(DashboardActions.request)),
  mergeMap(async (action) => {

    const searchCount = await AsyncStorage.getItem(KEY_SEARCH_COUNT);
    const actions = [];

    if (searchCount === "3" || searchCount === "50") {
      actions.push(LocationActions.checkLocationSetThroughGPS());

      if (searchCount === "50") {
        await AsyncStorage.setItem(KEY_SEARCH_50_TIME, moment().format())
      }
    } else if (parseFloat(searchCount) > 50) {
      const time = await AsyncStorage.getItem(KEY_SEARCH_50_TIME);

      if (time) {
        const monthAfter = moment(time).add(1, "month");
        if (moment().isAfter(monthAfter, "days")) {
          actions.push(LocationActions.checkLocationSetThroughGPS());
          await AsyncStorage.removeItem(KEY_SEARCH_50_TIME);
        }
      }
    }

    await AsyncStorage.setItem(KEY_SEARCH_COUNT, (parseFloat(searchCount ? searchCount : "0") + 1) + "");


    const root = state$.value;
    const location = getLocation(root.location);
    const auth = R.path(["login", "data", "authToken"], root);

    if (!location) {
      actions.push(DashboardActions.failure({
        action,
        response: {data: {error: "Location not found"}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
      }))
      return from(actions);
    }
    const keyword = action.payload;

    /*Answers.logSearch(keyword, {
      searchType: "Search All",
      searchAction: "Keyword",
    });*/

    const email = getPatientEmailBase64(state$.value);

    return zip(providerSearch(api.auth(auth), keyword, location, email), serviceSearch(api.auth(auth), keyword, location, email),
    specialitySearchByKeyword(api.auth(auth), keyword, location, email))
      .pipe(
        takeUntil(action$.pipe(ofType(getType(DashboardActions.cancel)))),
        mergeMap((arr: Array<ApiResponse<any>>) => {
            let dataSpecialty = arr[2].data;
            if (arr[2].ok) {
              const count = R.compose(R.length, R.keys, R.prop("results"))(dataSpecialty);
              dataSpecialty = R.assoc("count", count, dataSpecialty);
            }

            if (arr[0].status === 401 || arr[1].status === 401 || arr[2].status === 401) {
              actions.push(AppActions.actionReset());
            } else {
              actions.push(DashboardActions.success(
                {
                  term: action.payload,
                  provider: arr[0].ok ? arr[0].data : null,
                  service: arr[1].ok ? arr[1].data : null,
                  speciality: arr[2].ok ? dataSpecialty : null,
                }))
            }

            return from(actions);
          },
        ));
  }),
  mergeMap((obs) => from(obs)),
));

export const epicSearchSpecialtyName: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(DashboardActions.requestSpecialtyByName)),
  mergeMap((action) => {
    const root = state$.value;
    const location = getLocation(root.location);
    const authToken = R.path(["login", "data", "authToken"], root);

    if (!location) {
      return of(DashboardActions.failure({
        action,
        response: {data: {error: "Location not found"}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
      }));
    }

    const email = getPatientEmailBase64(state$.value);
    return specialitySearch(api.auth(authToken), action.payload, location, email).pipe(
      mergeMap((response: any) => {
        if (response.ok && R.path(["data", "results", "status"], response) !== 400) {

          if (!R.isEmpty(response.data.results)) {
            Actions.popTo("landing");
            Actions.push("search", {
              results: R.compose(
                R.flatten,
                R.map((data) => R.compose(R.map(R.assoc("specialty", data.name)), R.prop("providers"))(data)),
                R.values,
                R.prop("specialties"),
                R.head,
                R.values,
              )(response.data.results),
              searchType: SearchType.Specialty,
              item: action.payload,
            });
          } else {
            Alert.alert("Error", "No results found for your search, Please try again later", [
              {
                text: "OK",
              },
            ])
          }

          return of(DashboardActions.specialitiesSuccess());
        } else {
          return of(DashboardActions.failure({action, response}));
        }
      }),
    );
  }),
);

export const epicSearchAllSpecialities: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(DashboardActions.requestSpecialties)),
  mergeMap((action) => {
    const root = state$.value;
    const location = getLocation(root.location);
    const authToken = R.path(["login", "data", "authToken"], root);

    if (!location) {
      return of(DashboardActions.failure({
        action,
        response: {data: {error: "Location not found"}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
      }));
    }
    const auth = api.auth(authToken);
    const email = getPatientEmailBase64(state$.value);

    return from(action.payload).pipe(
      map((id) => auth.providerBySpeciality(id, location.latitude, location.longitude, email)
        .pipe(
          mergeMap((data: any) => of(data)),
        )),
      combineAll(),
      mergeMap((combined: any[]): any => {

        const results = [];
        combined.forEach((value) => {
          if (value.ok && value.data.status !== 400) {
            const arr = [];
            value.data.forEach((value1) => {
              arr.push(R.assoc("specialty", value1.searchedSpecialty ? value1.searchedSpecialty : value1.specialty, value1));
            });
            results.push(arr);
          }
        });
        if (!notEmptyAndNull(results)) {
          Alert.alert("No results", "No results found for your search.");
          return of(DashboardActions.failure({
            action,
            response: {data: {error: "No result found."}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
          }));
        }

        Actions.push("search", {
          results: R.uniqBy(R.prop("providerId"), R.flatten(results)),
          searchType: SearchType.Specialty,
          item: "",
        });
        return of(DashboardActions.specialitiesSuccess());
      }),
    );
  }),
);
