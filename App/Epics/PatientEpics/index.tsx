import {Epic, ofType} from "redux-observable";
import {IDependencies} from "@root/App/Epics";
import {from, of, zip} from "rxjs";
import {LoginActions} from "@root/App/Reducers/LoginReducers";
import {combineAll, delay, map, mergeMap} from "rxjs/operators";
import {getPatientEmailBase64, PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import {Alert} from "react-native";
import {RootState} from "@root/App/Reducers";
import {getType} from "typesafe-actions";
import {getLocation, LOCATION_NOT_FOUND_MESSAGE} from "@root/App/Lib/AppHelper";
import {Actions} from "react-native-router-flux";
import * as R from "ramda";
import moment from "moment";
import {RegistrationProcessActions} from "@root/App/Reducers/RegistrationProcessReducers";
import {Answers} from "react-native-fabric";
import checkBiometricAuth, {BIOMETRIC_ENABLE_STATUS} from "@root/App/Lib/AppHelper/BiometricVerification";
import * as Keychain from "react-native-keychain";
import AppConfig from "@root/App/Config/AppConfig";
import AsyncStorage from "@react-native-community/async-storage";
import {BiometricActions} from "@root/App/Reducers/BiometricReducers";
import {AppActions} from "@root/App/Reducers/AppReducers";
import Toast from 'react-native-toast-message'

export const epicLogin: Epic = (action$, _, {api}: IDependencies) => action$.pipe(
  ofType(getType(LoginActions.request)),
  mergeMap((action) => api.common.login(action.payload.email, action.payload.password).pipe(
    mergeMap(async (response: any) => {
      // Answers.logLogin("Direct", response.ok, {email: action.payload.email});
      if (response.ok) {
        const actions = [
          PatientAPIActions.setPatientEmail(action.payload.email),
          LoginActions.success(response.data),
          PatientAPIActions.requestInfo(action.meta),
          BiometricActions.setUserNamePassword({userName: action.payload.email, password: action.payload.password}),
        ]


        /* AsyncStorage.setItem("biometricEmail", action.payload.email);
                AsyncStorage.setItem("biometricPassword", action.payload.password); */
                
                Keychain.setGenericPassword(action.payload.email, action.payload.password);
        if (action.payload.remember) {
          AsyncStorage.setItem("email", action.payload.email);
        } else {
          AsyncStorage.removeItem("email");
        }
        return from(actions);
      } else {   
        var errorMessage = "The MediBookr platform is currently unavailable. Please try again later or contact customer support.";
        var responseError = R.path(["data", "errors", "0", "password"], response) || R.path(["data", "error"], response) || R.path(["data", "message"], response);
        if(responseError)
        {
          errorMessage = responseError;
        }      
        Toast.show({
          type: 'custom_error',                    
          text1: errorMessage,
          autoHide: false,
          props: {onPress: () => { Toast.hide() }}
        }) 
        return of(LoginActions.loginFailure());
      }
    }),
    mergeMap((obs$) => from(obs$)),
    ),
  ),
);

export const epicLogout: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(LoginActions.logout)),
  mergeMap((action) => {
    const auth = R.path(["login", "data", "authToken"], state$.value);

    if (!auth || state$.value.registration.isProcessing) {
      return of(LoginActions.failure({action, response: undefined}))
    }

    const email = getPatientEmailBase64(state$.value);
    return api.auth(auth).logout(email).pipe(
      map((response: any) => {
        if (response.ok) {
          return LoginActions.resetLogin();
        } else {
          return AppActions.actionReset()
        }
      }));
  }),
);

export const epicInsurance: Epic = (action$, state$, {api}) => action$.pipe(
  ofType(getType(PatientAPIActions.requestPatientInsurance)),
  mergeMap((action) => {
    const email = getPatientEmailBase64(state$.value);
    return api.auth(R.path(["login", "data", "authToken"], state$.value)).getInsurance(email).pipe(
      map((response: any) => {
        if (response.ok) {
          return PatientAPIActions.patientInsuranceSuccess(response.data);
        } else {
          return PatientAPIActions.infoError({action, response});
        }
      }),
    );
  }),
);

export const epicRegister: Epic = (action$, state$, {api}) => action$.pipe(
  ofType(getType(LoginActions.register)),
  mergeMap((action) => api.common.register(action.payload)
    .pipe(
      mergeMap((response: any): any => {

        if (response.ok) {
          return api.common.login(action.payload.email, action.payload.password).pipe(
            delay(500),
            mergeMap((responseLogin: any): any => {
              if (responseLogin.ok) {    
                
                /* AsyncStorage.setItem("biometricEmail", action.payload.email);
                AsyncStorage.setItem("biometricPassword", action.payload.password); */
                
                Keychain.setGenericPassword(action.payload.email, action.payload.password);          
                return of(
                  // LoginActions.request({email: action.payload.email, password: action.payload.password, remember: true}, true),
                  RegistrationProcessActions.toggleRegistration(),
                  LoginActions.registerSuccess(),
                  PatientAPIActions.patientInfoSuccess(response.data.patient, false),
                  PatientAPIActions.setPatientEmail(response.data.patient.email),
                  LoginActions.success(responseLogin.data),
                );
              } else {
                Toast.show({
                  type: 'custom_error',                    
                  text1: R.path(["data", "errors", "0", "password"], response) || R.path(["data", "error"], response) || R.path(["data", "message"], response),
                  autoHide: false,
                  props: {onPress: () => { Toast.hide() }}
                })
                return of(LoginActions.registerFailure());
              }
            }),
          )
        } else {
          Toast.show({
            type: 'custom_error',                    
            text1: R.path(["data", "errors", "0", "password"], response) || R.path(["data", "error"], response) || R.path(["data", "message"], response),
            autoHide: false,
            props: {onPress: () => { Toast.hide() }}
          })
          return of(LoginActions.registerFailure());
        }
      }),
    )),
);

export const epicForgotPassword: Epic = (action$, state$, {api}) => action$.pipe(
  ofType(getType(LoginActions.forgotPassword)),
  mergeMap((action) => api.common.forgotPassword(action.payload)
    .pipe(
      map((response: any) => {
        if (response.ok) {
          Alert.alert("Successful", "An email is sent to reset your password. Please check your mail for further instructions.", [
            {text: "OK",onPress: () => Actions.pop()},
          ]);
          return LoginActions.forgotPasswordSuccess();
        } else {
          return LoginActions.failure({action, response});
        }
      }),
    ),
  ),
);

export const epicPatientUpdate: Epic = (action$, state$, {api}: IDependencies) => {
  return action$.pipe(
    ofType(getType(PatientAPIActions.patientUpdateRequest)),
    mergeMap((action) => {
        const email = getPatientEmailBase64(state$.value);
        return api.auth(R.path(["login", "data", "authToken"], state$.value))
          .updatePatient(action.payload, email).pipe(
            delay(5000),
            map((response: any) => {
              if (response.ok) {
                Actions.pop();
                return PatientAPIActions.requestInfo(true);
              } else {
                return PatientAPIActions.infoError({action, response});
              }
            }))
      },
    ),
  );
};

export const epicPatientInfo: Epic = (action$, state$, {api}: IDependencies) => {
  return action$.pipe(
    ofType(getType(PatientAPIActions.requestInfo)),
    mergeMap((action) => {

      const email = getPatientEmailBase64(state$.value);
      return api.auth(R.path(["login", "data", "authToken"], state$.value))
        .patientProfile(email)
        .pipe(map((response: any) => {
          if (response.ok) {
            return PatientAPIActions.patientInfoSuccess(response.data, action.payload);
          } else {
            return PatientAPIActions.infoError({action, response});
          }
        }));
    }),
  );
};

export const epicResetPassword: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(PatientAPIActions.resetPasswordRequest)),
  mergeMap((action: any) => {
    const token = R.path(["login", "data", "authToken"], state$.value);
    return api.auth(token).resetPassword(action.payload).pipe(
      map((response: any) => {
        if (response.ok) {
          if (action.payload.onSuccess) {
            action.payload.onSuccess();
          }
          return PatientAPIActions.resetPasswordSuccess();
        } else {
          return PatientAPIActions.infoError({action, response});
        }
      }),
    );
  }),
);

export const epicDashboard: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(PatientAPIActions.requestDashboard)),
  mergeMap((action): any => {
    const token = R.path(["login", "data", "authToken"], state$.value);
    const location = getLocation(state$.value.location) || {latitude: 0, longitude: 0};

    const email = getPatientEmailBase64(state$.value);
    const auth = api.auth(token);

    return zip(auth.patientBenefits(email), auth.getAppointments(email)).pipe(
      mergeMap((zipped) => {
        if (R.path(["1", "data", "length"], zipped)) {
          zipped = R.assocPath(["1", "data"],
            R.filter(
              (value) => {
                return R.compose(R.gt(2), R.prop("status"))(value) &&
                  moment(R.prop("datetime", value)).isAfter(new Date());
              },
            )(zipped[1].data),
            zipped,
          );
        }
        const email = getPatientEmailBase64(state$.value);
        if (zipped[1].ok && R.path(["data", "length"], zipped[1])) {
          return from(R.map(R.prop("providerId"), zipped[1].data)).pipe(
            map((id) => auth.providerById(id, location.latitude, location.longitude, email)
              .pipe(
                mergeMap((data: any) => of(data)),
              )),
            combineAll(),
            mergeMap((combined: any[]): any => {
              combined.forEach((value, index) => {
                if (value.ok) {
                  zipped[1].data[index].provider = value.data;
                }
              });
              return of(zipped);
            }),
          );
        } else {
          return of(zipped);
        }
      }),
      mergeMap((response): any => {
        return of(
          response[0].ok ?
            PatientAPIActions.benefitsSuccess(response[0].data) : PatientAPIActions.infoError({
              action,
              response: response[0],
            }),
          response[1].ok && R.path(["data", "length"]) ?
            PatientAPIActions.appointmentsSuccess(response[1].data.filter((value) => !!value.provider))
            : PatientAPIActions.infoError({action, response: response[1]}),
          PatientAPIActions.pcpRequest(),
        );
      }),
    );
  }),
);

export const requestAppointmentEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(PatientAPIActions.appointmentRequest)),
  mergeMap((action) => {
    const rootState = state$.value as RootState;
    const data = {
      providerId: action.payload.provider.providerId,
      patientId: rootState.patient.info.id,
      serviceId: action.payload.provider.serviceId || 5074,
      appointmentType: action.payload.appointmentType,
      reasonForVisit: action.payload.reasonForVisit,
      status: 1,
    };

    const email = getPatientEmailBase64(state$.value);

    return api.auth(R.path(["login", "data", "authToken"], rootState)).requestAppointment(data, email).pipe(
      map((response: any) => {
        if (response.ok) {
          if (action.payload.onSuccess) {
            action.payload.onSuccess();
          }
          return PatientAPIActions.appointmentRequestSuccess();
        } else {
          return PatientAPIActions.infoError({action, response});
        }
      }),
    );
  }),
);

export const costEstimateEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(PatientAPIActions.costEstimateRequest)),
  mergeMap((action) => {
    const rootState = state$.value as RootState;

    const location = getLocation(rootState.location);

    if (!location) {
      return of(PatientAPIActions.infoError({
        action,
        response: {data: {error: LOCATION_NOT_FOUND_MESSAGE}, ok: false, problem: "UNKNOWN_ERROR", status: 0},
      }));
    }

    const data = {
      providerId: action.payload.provider.providerId,
      patientId: rootState.patient.info.id,
      serviceId: action.payload.provider.serviceId || action.payload.serviceId,
      notes: action.payload.note,
      contactMethod: action.payload.method,
    };

    const email = getPatientEmailBase64(state$.value);

    return api.auth(R.path(["login", "data", "authToken"], rootState)).costEstimate(data,
      {email, lat: location.latitude, long: location.longitude}).pipe(
      map((response: any) => {
        if (response.ok) {
          if (action.payload.onSuccess) {
            action.payload.onSuccess();
          }
          return PatientAPIActions.costEstimateSuccess();
        } else {
          return PatientAPIActions.infoError({action, response});
        }
      }),
    );
  }),
);
