import {Epic, ofType} from "redux-observable";
import {GetHelpActions, GetHelpRequestParams} from "@root/App/Reducers/GetHelpReducers";
import {getType} from "typesafe-actions";
import {map, mergeMap} from "rxjs/operators";
import {IDependencies} from "@root/App/Epics";
import {AnyAction} from "redux";
import * as R from "ramda";
import {AppActions} from "@root/App/Reducers/AppReducers";
import {Actions} from "react-native-router-flux";
import {renameKeys} from "@root/App/Transforms/ObjectTransform";

export const getHelpEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(GetHelpActions.request)),
  mergeMap((action: AnyAction) => {

    const authToken = R.path(["login", "data", "authToken"], state$.value);
    const payload = action.payload as GetHelpRequestParams;

    return api.auth(authToken).getHelp(payload.question, payload.contactMethod).pipe(
      map((response: any) => {
        if (response.ok) {
          if (action.payload.onSuccess) {
            action.payload.onSuccess();
          }
          return GetHelpActions.success();
        } else {
          return GetHelpActions.failure({action, response});
        }
      }),
    );
  }),
);

export const drugNameEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(GetHelpActions.requestDrugName)),
  mergeMap((action) => {
    return api.refillWise.nameLookup(action.payload).pipe(
      map((response: any) => {
        if (response.ok) {
          return GetHelpActions.drugNameSuccess(response.data);
        } else {
          return GetHelpActions.failure({action, response});
        }
      }),
    );
  }),
);

export const drugStructureEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(GetHelpActions.requestDrugStructure)),
  mergeMap((action) => {
    return api.refillWise.structure(action.payload).pipe(
      map((response: any) => {
        if (response.ok) {
          const success = R.compose(
            R.map(R.map(R.sort(R.descend(R.prop("Popularity"))))),
            R.head,
            R.values,
          )(response.data);
          return GetHelpActions.drugStructureSuccess(success);
        } else {
          return GetHelpActions.failure({action, response});
        }
      }),
    );
  }),
);


export const drugPriceEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(GetHelpActions.requestDrugPrice)),
  mergeMap((action) => {
    const {ZipCode, NDC, Quantity} = action.payload;
    return api.refillWise.pricingRequest(NDC, ZipCode, Quantity).pipe(
      map((response: any) => {
        if (response.ok) {
          R.compose(
            R.sort(R.ascend(R.path(["Price", "0", "Price"]))),
            R.uniqBy(R.prop("Name")),
            R.sort(R.ascend(R.prop("Distance"))),
          )(response.data);
          return GetHelpActions.drugPriceSuccess(response.data);
        } else {
          return GetHelpActions.failure({action, response});
        }
      }),
    );
  }),
);

export const pharmacyInfo: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(GetHelpActions.requestDrugInfo)),
  mergeMap((action) => {
    let couponInfo;

    return api.refillWise.couponRequest().pipe(
      mergeMap(response => {
        if (response.ok) {
          couponInfo = response.data
        }

        return api.refillWise.infoRequest(action.payload.NDCItem.NDC);
      }),
    ).pipe(
      map((response: any) => {
        if (response.ok) {
          const convertArrayToString = R.join("\n");

          const info = R.compose(
            R.fromPairs,
            R.map(R.over(R.lensIndex(1), convertArrayToString)),
            R.toPairs,
            renameKeys({
              Indications: "Uses",
              Contraindications: "Contraindications",
              HowToUses: "Directions",
              WarningCautions: "Warnings",
              SeriousSideEffects: "Side Effects",
              StorageDisposals: "Avoid",
            }),
            R.pick([
              "Indications",
              "Contraindications",
              "HowToUses",
              "WarningCautions",
              "SeriousSideEffects",
              "StorageDisposals",
            ]),
            R.evolve({
              SeriousSideEffects: R.concat(R.prop("LessSeriousSideEffects", response.data)),
              StorageDisposals: R.concat(R.prop("DrugFoodAvoidings", response.data)),
            }),
          )(response.data);

          Actions.push("stage3", {
            info,
            requestInfo: action.payload,
            couponInfo,
          });
          return GetHelpActions.drugInfoSuccess();
        } else {
          return GetHelpActions.failure({action, response});
        }
      }),
    );
  }),
);

export const pharmacyCompareEpic: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(GetHelpActions.requestDrugCompare)),
  mergeMap((action) => {
    const token = R.path(["login", "data", "authToken"], state$.value);
    return api.auth(token).pharmacyCompare(action.payload).pipe(
      map((response: any) => {
        if (response.ok) {
          if (action.payload.onSuccess) {
            action.payload.onSuccess();
          }
          return GetHelpActions.success();
        } else {
          return GetHelpActions.failure({action, response});
        }
      }),
    );
  }),
);

export const epicInfoMessage: Epic = (action$, state$, {api}: IDependencies) => {
  return action$.pipe(
    ofType(getType(GetHelpActions.requestInfoMessage)),
    mergeMap((action) => {
      return api.auth(R.path(["login", "data", "authToken"], state$.value))
        .infoMessage(action.payload)
        .pipe(map((response: any) => {
          if (response.ok) {
            return GetHelpActions.infoMessageSuccess(response.data);
          } else {
            return GetHelpActions.failure({action, response});
          }
        }));
    }),
  );
};
