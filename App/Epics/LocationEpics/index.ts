import {Epic, ofType} from "redux-observable";
import {catchError, map, mergeMap, switchMap} from "rxjs/operators";
import {IDependencies} from "@root/App/Epics";
import {from, of} from "rxjs";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import {getType} from "typesafe-actions";
import * as R from "ramda";
import {getLocation} from "@root/App/Lib/AppHelper";
import {RootState} from "@root/App/Reducers";
import {LatLng} from "react-native-maps";
import {check, PERMISSIONS, RESULTS} from "react-native-permissions";
import {Actions} from "react-native-router-flux";
import {AppActions} from "@root/App/Reducers/AppReducers";
import {Platform} from "react-native";
import Geolocation from '@react-native-community/geolocation';


export const epicLocation: Epic = (action$, _, {api}: IDependencies) => {
  let keyWord;
  let action;
  return action$.pipe(
    ofType(getType(LocationActions.request)),
    mergeMap((act) => {
      action = act;
      keyWord = action.payload;
      return api.google.geocoding(keyWord);
    }),
    switchMap((response: any): any => {
      if (response.ok && response.data.status === "OK") {
        if (action.meta) {
          return of(LocationActions.selected({
            keyword: keyWord,
            location: R.path(["data", "results", "0", "geometry", "location"], response),
          }));
        } else {
          return of(LocationActions.success({keyword: keyWord, data: response.data}));
        }
      } else {
        return of(LocationActions.failure({action, response}));
      }
    }),
  );
};

export const epicGpsToZipcode: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(LocationActions.requestZipCode)),
  mergeMap((action): any => {
    const root = state$.value as RootState;
    const location = getLocation(root.location);
    const zipUser = root.patient.info.zipcode;

    if (!location) {
      return of(LocationActions.successZipCode(zipUser));
    } else {
      return api.google.gpsGeocode(location.latitude, location.longitude)
        .pipe(
          map((response: any) => {
            if (response.ok && response.data.status === "OK") {
              const zip = R.compose(
                R.ifElse(R.isNil, R.always(undefined), R.prop("long_name")),
                R.find(R.pathEq(["types", "0"], "postal_code")),
                R.prop("address_components"),
                R.head,
              )(response.data.results);

              if (zip) {
                return LocationActions.successZipCode(zip)
              }

            }

            return LocationActions.successZipCode(zipUser)
          }),
        )
    }
  }),
);

export const epicLocationConsent: Epic = (action$, state$, {}: IDependencies) => action$.pipe(
  ofType(getType(LocationActions.checkLocationSetThroughGPS)),
  mergeMap((act) => {
    return from(check(Platform.select({
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    })));
  }),
  mergeMap((result) => {
    switch (result) {
      case RESULTS.UNAVAILABLE:
        // Actions.push("location-consent", {result});
        // return of(AppActions.actionVoid());
        return of(LocationActions.locationModal(result));
      case RESULTS.DENIED:
        // Actions.push("location-consent", {result});
        // return of(AppActions.actionVoid());
        return of(LocationActions.locationModal(result));
      case RESULTS.GRANTED:
        return from(new Promise((resolve, reject) => {

          Geolocation.getCurrentPosition((position) => {
            resolve(position.coords)
          }, (error) => {
            reject(error);
          });
        })).pipe(
          mergeMap((position: LatLng) => {
            return of(LocationActions.gpsLocation(position));
          }),
          catchError((err) => {
            // Actions.push("location-consent", {result});
            // return of(AppActions.actionVoid());
            return of(LocationActions.locationModal(result));
          }),
        );
      case RESULTS.BLOCKED:
        // Actions.push("location-consent", {result});
        // return of(AppActions.actionVoid());
        return of(LocationActions.locationModal(result));
      default:
        return of(AppActions.actionVoid());
    }
  }),
);
