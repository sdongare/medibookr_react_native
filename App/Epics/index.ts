import {combineEpics} from "redux-observable";
import {Api} from "@root/App/Services/Api";
import {
  epicSearchAllSpecialities,
  epicSearchByName,
  epicSearchKeyword,
  epicSearchSpecialtyKeyword,
  epicSearchSpecialtyName
} from "@root/App/Epics/SearchEpics";
import {epicRequestPCP, epicSearchProvider, epicSearchProviderById, epicUpdatePCP} from "@root/App/Epics/ProviderEpics";
import {epicGpsToZipcode, epicLocation, epicLocationConsent} from "@root/App/Epics/LocationEpics";
import {
  costEstimateEpic,
  epicDashboard,
  epicForgotPassword,
  epicInsurance,
  epicLogin,
  epicLogout,
  epicPatientInfo,
  epicPatientUpdate,
  epicRegister,
  epicResetPassword,
  requestAppointmentEpic,
} from "@root/App/Epics/PatientEpics";
import {epicNetworkError, sendFeedbackEpic} from "@root/App/Epics/AppEpics";
import {
  drugNameEpic,
  drugPriceEpic,
  drugStructureEpic,
  getHelpEpic,
  pharmacyCompareEpic, pharmacyInfo,
  epicInfoMessage
} from "@root/App/Epics/HelpEpics";
import {resourceEpic} from "@root/App/Epics/ResourcesEpic";
import {epicCheckBiometric, epicToggleBiometricLogin} from "@root/App/Epics/BiometricEpics";

export interface IDependencies {
  api: Api;
}

export default combineEpics(
  epicLogin,
  epicInsurance,
  epicSearchAllSpecialities,
  resourceEpic,
  sendFeedbackEpic,
  costEstimateEpic,
  epicForgotPassword,
  epicRegister,
  epicLogout,
  requestAppointmentEpic,
  epicSearchKeyword,
  epicSearchSpecialtyKeyword,
  epicSearchByName,
  epicUpdatePCP,
  epicRequestPCP,
  getHelpEpic,
  drugNameEpic,
  epicResetPassword,
  epicPatientUpdate,
  pharmacyCompareEpic,
  epicSearchProvider,
  epicSearchProviderById,
  epicNetworkError,
  epicLocation,
  epicPatientInfo,
  epicDashboard,
  epicInfoMessage,
  epicSearchSpecialtyName,
  drugPriceEpic,
  drugStructureEpic,
  epicGpsToZipcode,
  pharmacyInfo,
  epicLocationConsent,
  epicCheckBiometric,
  epicToggleBiometricLogin,
);
