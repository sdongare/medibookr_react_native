import {Epic, ofType} from "redux-observable";
import {ProviderActions} from "@root/App/Reducers/ProviderReducers";
import {delay, map, mergeMap, switchMap} from "rxjs/operators";
import {IDependencies} from "@root/App/Epics";
import {Actions} from "react-native-router-flux";
import {isService} from "@root/App/Lib/TypeHelper";
import {RootState} from "@root/App/Reducers";
import {of} from "rxjs";
import {getLocation, LOCATION_NOT_FOUND_MESSAGE, notEmptyAndNull} from "@root/App/Lib/AppHelper";
import {getType} from "typesafe-actions";
import {RegistrationProcessActions} from "@root/App/Reducers/RegistrationProcessReducers";
import {getPatientEmailBase64, PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import * as R from "ramda";
import {SearchType} from "@root/App/Containers/SearchScreen/SearchScreen";
import {convertToBreadCrumb} from "@root/App/Transforms/StringsTranform";

export const epicSearchProviderById: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(ProviderActions.requestById)),
  mergeMap((action) => {
    const root = state$.value as RootState;
    const authToken = R.path(["login", "data", "authToken"], root);
    const location = getLocation(root.location);

    if (!location) {
      return of(ProviderActions.failure({
        action,
        response: {data: {error: LOCATION_NOT_FOUND_MESSAGE}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
      }));
    }

    const email = getPatientEmailBase64(state$.value);
    return api.auth(authToken).providerById(action.payload, location.latitude, location.longitude, email).pipe(
      map((response: any) => {
        if (response.ok) {
          Actions.push("detail", {payload: response.data});
          return ProviderActions.successById();
        } else {
          return ProviderActions.failure({action, response});
        }
      }),
    );
  }),
);

export const epicSearchProvider: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(ProviderActions.request)),
  switchMap((action) => {
    const root = state$.value;

    const location = getLocation(root.location);
    const authToken = R.path(["login", "data", "authToken"], root);
    const email = getPatientEmailBase64(state$.value);

    if (!location) {
      return of(ProviderActions.failure({
        action,
        response: {data: {error: LOCATION_NOT_FOUND_MESSAGE}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
      }));
    }

    return (isService(action.payload.item) ?
      api.auth(authToken).providerByService((action.payload.item as Service).serviceId, location.latitude, location.longitude, email) :
      api.auth(authToken).providerBySpeciality((action.payload.item as Specialty).specialtyId, location.latitude, location.longitude, email))
      .pipe(
        map((response: any) => {
          if (response.ok) {
            const results = response.data;

            if (!notEmptyAndNull(results) || results.status === 400) {
              return ProviderActions.failure({
                action,
                response: {data: {error: "No result found."}, ok: false, problem: "UNKNOWN_ERROR", status: 0}
              });
            }

            Actions.push("search", {
              results,
              searchType: isService(action.payload.item) ? SearchType.Procedure : SearchType.Specialty,
              item: convertToBreadCrumb(action.payload.item),
            });
            return ProviderActions.success();
          } else {
            return ProviderActions.failure({action, response});
          }
        }),
      );

  }),
);

export const epicRequestPCP: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(PatientAPIActions.pcpRequest)),
  mergeMap((_) => {
    const authToken = R.path(["login", "data", "authToken"], state$.value);
    const email = getPatientEmailBase64(state$.value);

    return api.auth(authToken).pcp(email).pipe(
      map((response: any) => {
        if (response.ok) {
          return PatientAPIActions.pcpSuccess(response.data);
        } else {
          return PatientAPIActions.pcpFailure();
        }
      }),
    );
  }),
);

export const epicUpdatePCP: Epic = (action$, state$, {api}: IDependencies) => action$.pipe(
  ofType(getType(RegistrationProcessActions.updatePCP)),
  mergeMap((action) => {
    const authToken = R.path(["login", "data", "authToken"], state$.value);
    const email = getPatientEmailBase64(state$.value);

    return api.auth(authToken).updatePatientPCP(action.payload, email).pipe(
      mergeMap((response: any): any => {
        if (response.ok) {
          if (action.meta) {
            action.meta.onSuccess();
          }
          return of(PatientAPIActions.pcpSuccess(response.data), RegistrationProcessActions.updatePCPSuccess());
        } else {
          return of(RegistrationProcessActions.failure({action, response}));
        }
      }),
    );
  }),
);
