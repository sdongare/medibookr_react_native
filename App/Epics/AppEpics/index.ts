import {Epic, ofType} from "redux-observable";
import {map, mergeMap} from "rxjs/operators";
import {Alert} from "react-native";
import {AppActions} from "@root/App/Reducers/AppReducers";
import {ApiErrorResponse, NETWORK_ERROR} from "apisauce";
import {createStandardAction, getType} from "typesafe-actions";
import {ErrorResponse} from "@root/App/Services/DataModels/ErrorResponse";
import {ObservableInput, of} from "rxjs";
import {AnyAction} from "redux";
import * as R from "ramda";
import {IDependencies} from "@root/App/Epics";

export const networkFailure = createStandardAction(NETWORK_ERROR)<{
  action: AnyAction;
  response: ApiErrorResponse<ErrorResponse>;
}>();

export const epicNetworkError: Epic = (action$) =>
  action$.pipe(
    ofType(NETWORK_ERROR),
    mergeMap(
      (action): ObservableInput<{ type: string }> => {
        if (!action.payload.response) {
          return of(AppActions.actionVoid());
        }

        if (
          R.any(R.equals(action.payload.response.problem))([
            "NETWORK_ERROR",
            "TIMEOUT_ERROR",
          ])
        ) {
          return of(AppActions.actionRetry(action.payload.action));
        }

        Alert.alert(
          "Error",
          R.path(["payload", "response", "data", "errors", "0", "password"], action) ||
          R.path(["payload", "response", "data", "error"], action) ||
          R.path(["payload", "response", "data", "message"], action) ||
          "We are unable to get your request, Please try again later.",
          [{text: "OK"}],
        );
        if (action.payload.response.status === 401) {
          return of(AppActions.actionReset());
        } else {
          return of(AppActions.actionVoid());
        }
      },
    ),
  );

export const sendFeedbackEpic: Epic = (
  action$,
  state$,
  {api}: IDependencies,
) =>
  action$.pipe(
    ofType(getType(AppActions.actionSendFeedback)),
    mergeMap((action) => {
      const authToken =  R.path(["login", "data", "authToken"], state$.value);
      return api
        .auth(authToken)
        .sendFeedback({...action.payload})
        .pipe(
          map((response: any) => {
            if (response.ok) {
              if (action.payload.onSuccess) {
                action.payload.onSuccess();
                return AppActions.actionSendFeedbackSuccess();
              }
            } else {
              return AppActions.actionError({response, action});
            }
          }),
        );
    }),
  );
