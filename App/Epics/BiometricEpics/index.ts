import {Epic, ofType} from "redux-observable";
import {getType} from "typesafe-actions";
import {mergeMap} from "rxjs/operators";
import {from, of} from "rxjs";
import * as Keychain from "react-native-keychain";
import TouchID from "react-native-touch-id";
import {BiometricActions} from "@root/App/Reducers/BiometricReducers";
import AsyncStorage from "@react-native-community/async-storage";
import {BIOMETRIC_ENABLE_STATUS} from "@root/App/Lib/AppHelper/BiometricVerification";

export const epicToggleBiometricLogin: Epic = (action$, state$, dependencies) => action$.pipe(
  ofType(getType(BiometricActions.toggleBiometric)),
  mergeMap(async (action) => {
    const currentBiometricStatus = state$.value.biometric.isBiometricEnable;

    if (currentBiometricStatus) {
      await Keychain.resetGenericPassword();
      await AsyncStorage.removeItem(BIOMETRIC_ENABLE_STATUS);
    } else {
      await Keychain.setGenericPassword(state$.value.biometric.userName, state$.value.biometric.password);
      await AsyncStorage.setItem(BIOMETRIC_ENABLE_STATUS, "true");
    }
    return of(BiometricActions.setBiometricEnableStatus(!currentBiometricStatus));
  }),
  mergeMap((obs$) => from(obs$)),
);

export const epicCheckBiometric: Epic = (action$, state$, dependencies) => action$.pipe(
  ofType(getType(BiometricActions.checkBiometric)),
  mergeMap(async (action) => {
    let supportType;
    const biometricEnablePersiststatus = await AsyncStorage.getItem(BIOMETRIC_ENABLE_STATUS);
    supportType = await TouchID.isSupported()
      .then((res) => supportType = res)
      .catch((err) => console.log(err));

    if (action.payload && supportType && biometricEnablePersiststatus === "true") {
      action.payload();
    }

    return of(BiometricActions.setBiometricSupport(supportType),
      BiometricActions.setBiometricEnableStatus(biometricEnablePersiststatus === "true"));
  }),
  mergeMap((obs$) => from(obs$)),
);
