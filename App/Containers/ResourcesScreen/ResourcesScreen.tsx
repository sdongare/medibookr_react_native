import React from "react";
import {ActivityIndicator, Image, Linking, Text, View} from "react-native";
import Header from "@root/App/Components/Header/Header";
import styles from "./ResourcesScreenStyle";
import {Metrics} from "@root/App/Themes";
import {Container, Content} from "@codler/native-base";
import Divider from "@root/App/Components/Divider/Divider";
import {ResourceReducersActions} from "@root/App/Reducers/ResourceReducers";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  getResources: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  payload: any;
}

/**
 * The local state
 */
export interface State {
  showLoader: boolean[];
}

type Props = StateProps & DispatchProps & OwnProps;

class ResourcesScreen extends React.Component<Props, State> {

  state = {
    showLoader: [],
  };

  public componentDidMount(): void {
    this.props.getResources();
  }

  public render() {
    return (
      <Container>
        <Header/>
        <Content style={styles.content}>
          <Text style={styles.header}>
            Benefit Resources
          </Text>
          <Divider style={styles.divider}/>
          <Text style={styles.description}>
            Click a link below to access the website of your available MediBookr benefit resources.
          </Text>
          {
            this.props.payload &&
            this.props.payload.map((value, index) => (
                <View style={{flexDirection: "row", alignItems: "center"}} key={index}>

                  <Image
                    resizeMode={"contain"}
                    source={{uri: value.logo}}
                    onLoadStart={() => {
                      this.state.showLoader[index] = true;
                      this.setState(this.state);
                    }}
                    onLoadEnd={() => {
                      this.state.showLoader[index] = false;
                      this.setState(this.state);
                    }}
                    style={{width: Metrics.screenWidth * 0.25, height: 130, marginLeft: 20, marginRight: 18}}
                  />
                  <Text
                    style={{color: "blue", marginHorizontal: 20, flexWrap: "wrap"}}
                    onPress={() => Linking.openURL(value.link)}
                  >{value.link}
                  </Text>
                  {
                    !!this.state.showLoader[index] &&
                    <ActivityIndicator size={"large"} style={{position: "absolute", left: Metrics.screenWidth * 0.12}}/>
                  }

                </View>
              )
            )
          }

        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => ({
  payload: state.resources.data,
});

const mapDispatchToProps = (dispatch): DispatchProps => ({
  getResources: () => dispatch(ResourceReducersActions.request()),
});

export default connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps)(ResourcesScreen);
