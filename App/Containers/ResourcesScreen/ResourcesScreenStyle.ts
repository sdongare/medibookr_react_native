import {
  StyleSheet,
} from "react-native";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import {Colors, Fonts} from "../../Themes";

export default StyleSheet.create({
  content: {
    padding: 20,
  },
  divider: {
    marginVertical: 0,
  },
  header: {
    fontFamily: Fonts.type.latoBold,
    fontSize: 20,
    color: Colors.textDark,
  },
  description: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 16,
    color: Colors.textDark,
    marginTop: 20,
  },
  label: {
    marginTop: 15,
    fontFamily: Fonts.type.latoBold,
    fontSize: 16,
    color: Colors.textDark,
  },
  input: {
    ...ApplicationStyles.shadow,
    marginTop: 10,
    marginBottom: 20,
    backgroundColor: Colors.searchBg,
  },
  pickerStyle: {
    marginTop: 4,
    borderRadius: 3,
    width: "100%",
    borderColor: Colors.border,
    borderWidth: 1,
  },
});
