import * as React from "react";
import {ActivityIndicator, View} from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./LoaderStyle";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  forceLoading?: boolean;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  loading;
}

/**
 * The local state
 */
export interface State {
}

type Props = StateProps & DispatchProps & OwnProps;

class Loader extends React.Component<Props, State> {

  public render() {
    if (this.props.loading || this.props.forceLoading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            size={"large"}
            color={Colors.primary}
          />
        </View>
      );
    } else {
      return null;
    }

  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    loading: state.provider.fetching || state.login.fetching || state.patient.fetching || state.help.fetching
      || state.app.fetching || state.location.fetchingCluster || state.resources.fetching || state.registration.fetching ||
      state.dashboard.fetchingSpecialty || state.location.fetchingZipCode,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Loader) as React.ComponentClass<OwnProps>;
