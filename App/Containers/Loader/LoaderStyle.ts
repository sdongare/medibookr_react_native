import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";

export default StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    zIndex: 1000,
    position: "absolute",
    alignItems: "center",
    opacity: 0.7,
    backgroundColor: Colors.black,
  },
});
