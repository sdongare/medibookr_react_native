import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "@root/App/Themes";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    fontFamily: Fonts.type.latoBold,
    fontSize: Fonts.size.h4,
    color: Colors.textDark,
  },
  divider: {
    marginVertical: 0,
  },
  text: {
    flexWrap: "wrap",
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.regular,
    color: Colors.textGrey,
  },
  textPhone: {
    textDecorationLine: "underline",
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.regular,
    color: Colors.textBlue,
  },

  input: {
    ...ApplicationStyles.inputText,
    backgroundColor: Colors.searchBg,
    marginTop: 4,
  },
  textItemHeader: {
    fontFamily: Fonts.type.latoBold,
    fontSize: Fonts.size.h6,
    color: Colors.textBlue,
  },
  textDependent: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h5 + 2,
    color: Colors.textBlue,
  },
  textEdit: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h6,
    color: Colors.textBlue,
  },
  buttonText: {
    fontFamily: Fonts.type.latoBold,
    fontSize: Fonts.size.h6,
    color: Colors.white,
  },
  extraMargin: {
    marginTop: Metrics.doubleBaseMargin,
  },
  rowContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
