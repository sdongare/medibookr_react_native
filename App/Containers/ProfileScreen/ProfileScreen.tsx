import * as React from "react";
import {Alert, FlatList, Linking, Platform, Text, TextInput, View} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./ProfileScreenStyle";
import {Button, Container, Content} from "@codler/native-base";
import Header from "@root/App/Components/Header/Header";
import Divider from "@root/App/Components/Divider/Divider";
import {PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import * as R from "ramda";
import Colors from "@root/App/Themes/Colors";
import moment from "moment";
import {Actions} from "react-native-router-flux";
import * as Keychain from "react-native-keychain";
import {formatNumber} from "@root/App/Transforms/StringsTranform";
import AppConfig from "@root/App/Config/AppConfig";
import {DISABLE_DIALOG} from "@root/App/Lib/AppHelper/BiometricVerification";
import AsyncStorage from "@react-native-community/async-storage";
import Check from "@root/App/Components/Check";
import {BiometricActions} from "@root/App/Reducers/BiometricReducers";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  resetPassword: (data) => void;
  getInsurer: () => void;
  toggleBiometric: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  insurance: Insurance;
  info: PatientsInfo;
  isBiometricEnabled: boolean;
  biometricSupport: string | undefined;
}

/**
 * The local state
 */
export interface State {
  current: string;
  newPass: string;
  confirmPass: string;
}

type Props = StateProps & DispatchProps & OwnProps;

class ProfileScreen extends React.Component<Props, State> {
  public state = {
    current: "",
    newPass: "",
    confirmPass: "",
  };

  public componentDidMount(): void {
    this.props.getInsurer();
  }

  public render() {
    const enableText = this.props.isBiometricEnabled ? "Disable" : "Enable";
    const biometricMessage = this.props.biometricSupport === "FaceID" ? enableText + " face id login" :
      this.props.biometricSupport === "TouchID" ? Platform.select({
        android: enableText + " biometric login",
        ios: enableText + " touch id login",
      }) : enableText + " biometric login";
    return (
      <Container>
        <Header/>
        <Content padder={true} style={styles.container}>
          {
            !!R.path(["insurance", "insurerPlan"], this.props) &&
            <React.Fragment>
              <Text style={styles.header}>My Plan</Text>
              <Divider style={styles.divider}/>
              <Text style={[styles.textItemHeader, styles.extraMargin]}>{this.props.insurance.insurerPlan.name}</Text>
              <Text style={styles.text}>{"Group Number: " + this.props.insurance.groupNumber + "\n" +
              "Member Id: " + this.props.insurance.policyNumber}</Text>
            </React.Fragment>
          }

          <View style={[styles.rowContainer, styles.extraMargin]}>
            <Text style={styles.header}>My Profile</Text>
            {/*<Text style={styles.textDependent}>add dependent</Text>*/}
          </View>
          <Divider style={styles.divider}/>
          <FlatList
            data={[this.props.info]}
            renderItem={this.renderRowPlan}
            keyExtractor={this.keyExtractor}
          />

          <Text style={[styles.extraMargin, styles.header]}>Change PCP Provider</Text>
          <Divider style={styles.divider}/>
          <Button onPress={this.changePCP} full={true} info={true} style={styles.extraMargin}>
            <Text style={styles.buttonText}>Change PCP</Text>
          </Button>

          <Text style={[styles.extraMargin, styles.header]}>Change Map Application</Text>
          <Divider style={styles.divider}/>
          <Button onPress={this.clearDefaults} full={true} info={true} style={styles.extraMargin}>
            <Text style={styles.buttonText}>Clear Default</Text>
          </Button>


          {
            !!this.props.biometricSupport &&
            <>
              <Text style={[styles.extraMargin, styles.header]}>Biometric Settings</Text>
              <Divider style={styles.divider}/>
              <Check
                style={{marginVertical: 10}}
                color={Colors.primary}
                text={biometricMessage}
                isCheckProp={this.props.isBiometricEnabled}
                onClick={this.props.toggleBiometric}
              />
            </>
          }

          <Text style={[styles.extraMargin, styles.header]}>Change Password</Text>
          <Divider style={styles.divider}/>

          <TextInput
            secureTextEntry={true}
            placeholder={"Current Password"}
            value={this.state.current}
            style={[styles.input, styles.extraMargin]}
            placeholderTextColor={Colors.textSuggestion}
            onChangeText={this.onTextCurrent}
          />

          <TextInput
            secureTextEntry={true}
            placeholder={"New Password"}
            value={this.state.newPass}
            style={styles.input}
            placeholderTextColor={Colors.textSuggestion}
            onChangeText={this.onTextNewPass}
          />

          <TextInput
            secureTextEntry={true}
            placeholder={"Confirm New Password"}
            value={this.state.confirmPass}
            style={styles.input}
            placeholderTextColor={Colors.textSuggestion}
            onChangeText={this.onTextConfirmPass}
          />

          <Button onPress={this.onChangePasswordPress} full={true} info={true} style={styles.extraMargin}>
            <Text style={styles.buttonText}>Change Password</Text>
          </Button>

        </Content>
      </Container>
    );
  }

  private clearDefaults = async () => {
    await AsyncStorage.removeItem("DEFAULT_NAVIGATOR");
    Alert.alert("Success", "Default selection are removed.");
  };

  private clearBiometric = async () => {
    Keychain.resetInternetCredentials(AppConfig.environment.toString()).catch((e) => {
    });
    AsyncStorage.removeItem(DISABLE_DIALOG).catch((e) => {
    });

    Alert.alert("Success", "Biometric data has been cleared.");
  };

  private changePCP = () => {
    Actions.push("pcp-selection");
  }

  private onChangePasswordPress = () => {
    if (this.state.newPass !== this.state.confirmPass) {
      Alert.alert("Information", "New password and confirm password are not same.");
    } else if (R.isEmpty(this.state.current)) {
      Alert.alert("Information", "Please enter your current password.");
    } else if (R.or(R.isEmpty(this.state.newPass), R.isEmpty(this.state.confirmPass))) {
      Alert.alert("Information", "Please enter password.");
    } else {
      this.props.resetPassword({
        currentPassword: this.state.current, newPassword: this.state.newPass, onSuccess: () => {
          Alert.alert("Password Changed.", "Your password was changed successfully.");
        },
      });
    }
  };

  private onTextCurrent = (text) => this.setState({current: text});
  private onTextNewPass = (text) => this.setState({newPass: text});
  private onTextConfirmPass = (text) => this.setState({confirmPass: text});

  private keyExtractor = (item, index) => index.toString();

  private renderRowPlan = ({item, index}) => {
    const info = item as PatientsInfo;
    return (
      <View style={[styles.extraMargin]}>
        <View style={styles.rowContainer}>
          <Text style={styles.textItemHeader}>Member</Text>
          <Button
            transparent={true}
            onPress={() => {
              Actions.replace("update", {info: item});
            }}
          >
            <Text style={styles.textEdit}>edit</Text>
          </Button>

        </View>
        <Text style={styles.text}>{info.firstName + " " + info.lastName + "\n" + info.address + "\n" +
        moment(info.dob).format("L") + "\n" + info.email + "\n"}
          <Text
            style={styles.textPhone}
            onPress={() => Linking.openURL(`tel:${info.phone}`)}
          >{formatNumber(info.phone)}
          </Text>
        </Text>
      </View>
    );
  };
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  resetPassword: (data) => dispatch(PatientAPIActions.resetPasswordRequest(data)),
  getInsurer: () => dispatch(PatientAPIActions.requestPatientInsurance()),
  toggleBiometric: () => dispatch(BiometricActions.toggleBiometric()),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    insurance: state.patient.insurance,
    info: state.patient.info,
    isBiometricEnabled: state.biometric.isBiometricEnable,
    biometricSupport: state.biometric.biometricSupport,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen) as React.ComponentClass<OwnProps>;
