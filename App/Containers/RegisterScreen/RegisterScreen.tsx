import * as React from "react";
import {
  Alert,
  DatePickerAndroid,
  DatePickerIOS,
  ImageBackground,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  PickerItem
} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import styles from "./RegisterScreenStyle";
import {RootState} from "@root/App/Reducers";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {Body, Button, Container, Header, Left, Right, Text, View, Picker} from "@codler/native-base";
import {Colors, Fonts, Images, Metrics} from "@root/App/Themes";
import {Actions} from "react-native-router-flux";
import moment from "moment";
import {RegexEmail} from "@root/App/Containers/LoginScreen/LoginScreen";
import {LoginActions} from "@root/App/Reducers/LoginReducers";
import Check from "@root/App/Components/Check";
import FooterLogin from "@root/App/Components/FooterLogin";
import AppConfig, {ENVIRONMENT} from "@root/App/Config/AppConfig";
import {Subject} from "rxjs";
import {buffer, debounceTime, filter} from "rxjs/operators";
import AsyncStorage from "@react-native-community/async-storage";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  callRegister: (groupNumber, memberId, email, password) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {
  groupNumber: string;
  dob: Date;
  memberId: string;
  email: string;
  password: string;
  confirmPassword: string;
  showPicker: boolean;
  termsCondition: boolean;
  groupNumberError: string;
  memberIdError: string;
  emailError: string;
  passwordError: string;
  confirmPasswordError: string;
  termsConditionError: string;
  passwordShow: boolean;
  confirmPasswordShow: boolean;
  passwordConditionsShow: boolean;
  showDevOption: boolean;
  selectedEnvironment: string;
}

type Props = StateProps & DispatchProps & OwnProps;

class RegisterScreen extends React.Component<Props, State> {
  public state = {
    // name: __DEV__ ? "Matia Messemer" : "",
    groupNumber: "",
    dob: __DEV__ ? new Date(1981, 5, 12) : new Date(),
    // id: __DEV__ ? "3333697" : "",
    memberId: "",
    // email: __DEV__ ? "demo+mob2@medibookr.com" : "",
    email: "",
    // password: __DEV__ ? "demo123!" : "",
    // confirmPassword: __DEV__ ? "demo123!" : "",
    password: "",
    confirmPassword: "",
    showPicker: false,
    termsCondition : false,
    groupNumberError: "",
    memberIdError: "",
    emailError: "",
    passwordError: "",
    confirmPasswordError: "",
    termsConditionError: "",
    passwordShow: false,
    confirmPasswordShow: false,
    passwordConditionsShow: false,
    showDevOption: false,
    selectedEnvironment: Object.keys(ENVIRONMENT).find((e) => ENVIRONMENT[e] === AppConfig.environment),
  };

  private devClickSubject$ = new Subject<any>();
  private content;
  private inputRef: any;
  private inputRefPassword: any;

  constructor(props) {
    super(props);
    this.devClickSubject$.pipe(
      buffer(this.devClickSubject$.pipe(debounceTime(350))),
      filter((clickArray) => clickArray.length > 5),
    ).subscribe((next) => {
      this.setState({
        showDevOption: true,
      });
    });
  }

  public async componentDidMount() {
    const env = await AsyncStorage.getItem("env");
    if (env) {
      this.setState({selectedEnvironment: env});
      // @ts-ignore
      AppConfig.changeEnvironment(ENVIRONMENT[env]);
    }
    this.inputRef.setNativeProps({
        style: {
            fontFamily: Fonts.type.opensans,
        },
    });
    this.inputRefPassword.setNativeProps({
      style: {
          fontFamily: Fonts.type.opensans,
      },
    });
  }

  public checkState() {
    let validationOccured = true
    this.setState({
      groupNumberError: "",
      memberIdError: "",
      emailError: "",
      passwordError: "",
      confirmPasswordError: "",
      termsConditionError: "",
    })
    if(!this.state.groupNumber)
    {
      this.setState({ groupNumberError: 'This field is required.' })
      validationOccured = false
    }
    if(!this.state.memberId)
    {
      this.setState({ memberIdError: 'This field is required.' })
      validationOccured = false
    }    
    if(!this.state.password || !this.validateAndCheckPassword())
    {
      this.setState({ passwordError: 'Password must follow the below requirements.' })
      validationOccured = false
    }
    if(!this.state.confirmPassword || this.state.password != this.state.confirmPassword )
    {
      this.setState({ confirmPasswordError: 'Passwords do not match.' })
      validationOccured = false
    }
    if(!this.state.termsCondition)
    {
      this.setState({ termsConditionError: 'Please complete selection.' })
      validationOccured = false
    }
    if(!this.state.email)
    {
      this.setState({ emailError: 'Please provide valid email address.' })
      validationOccured = false
    } else if(!RegexEmail.test(this.state.email.toLowerCase()))
    {
      this.setState({ emailError: 'Please provide valid email address.' })
      validationOccured = false
    }
    return validationOccured
  }

  public validateAndCheckPassword = () => {
    const { password } = this.state
    const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*~|])[A-Za-z0-9!@#$%^&*~|]{8,25}$/
    let validatePassword = true    
    if (!pattern.test(password)) {      
      validatePassword = false
    }
    return validatePassword;
  }

  public setErrorMessage = (key, value ) => {
    this.setState({ [key] : value })
  }

  public registerClick = async () => {
    const message = this.checkState();
    console.warn(message)
    if (message) {
      this.props.callRegister(
        this.state.groupNumber,
        this.state.memberId,
        this.state.email,
        this.state.password);
    } 
    // else {
    //   Alert.alert("Info", message, [
    //     {text: "OK"},
    //   ]);
    // }
  };

  public back = () => {
    Actions.push("login");
  };

  public onFocusAll = (event) => {
    if (Platform.OS === "ios") {
      this.setState({
        showPicker: false,
      });
    }
  };

  public onFocusCreatePassword = (event) => {
    if (Platform.OS === "ios") {
      this.setState({
        showPicker: false,
      });
    }
    this.setState({ 
      passwordConditionsShow: true
    })
  };

  public onEndCreatePassword = (event) => {    
    this.setState({ 
      passwordConditionsShow: false
    })
  }; 

  public async dobClick() {
    Keyboard.dismiss();
    if (Platform.OS === "android") {
      try {
        // @ts-ignore
        const {action, year, month, day} = await DatePickerAndroid.open({
          date: this.state.dob,
        });
        if (action === DatePickerAndroid.dateSetAction) {
          // Selected year, month (0-11), day
          this.setState({
            dob: new Date(year, month, day),
          });
        }
      } catch ({code, message}) {
        console.warn("Cannot open date picker", message);
      }
    } else {
      this.setState({
        showPicker: !this.state.showPicker,
      });
    }
  }

  public devClick = () => {
    this.devClickSubject$.next("click");
  };

  public onDevSelected = (env) => {
    this.setState({selectedEnvironment: env, showDevOption: false});
    // @ts-ignore
    AppConfig.changeEnvironment(ENVIRONMENT[env]);
  };

  private onTermsAndCondition = () => this.setState({ termsCondition : !this.state.termsCondition });

  public render() {
    return (
      <Container>
        <ScrollView style={{ flex: 1 }}>
          <KeyboardAvoidingView
            style={{alignSelf: "stretch"}}
            contentContainerStyle={styles.container}
            ref={(ref) => (this.content = ref)}>                                      
            <ImageBackground source={Images.RegistrationBg} resizeMode={"stretch"} style={styles.backgroundBg}>
              <View style={styles.logoContainer}>   
                <TouchableWithoutFeedback onPress={() => this.devClick()}>                         
                  <Image source={Images.Logo} style={styles.logo} />
                </TouchableWithoutFeedback>
              </View>
              {
                this.state.showDevOption &&
                  <View style={{height: 40, width: (Metrics.screenWidth - 2*Metrics.section), alignSelf: 'center'}}>
                    <Picker style={styles.pickerStyle} mode={"dropdown"} selectedValue={this.state.selectedEnvironment}
                            onValueChange={this.onDevSelected}>
                      {Object.keys(ENVIRONMENT).map((value, index) => (
                        <PickerItem label={value} key={index} value={value}/>))}
                    </Picker>
                  </View>
              }
              <View style={styles.bodyContainer}>
                <Text style={styles.textHeader}>Register</Text>
                <View style={[styles.inputContainer, { paddingTop: 20 }]}>
                  <TextInput
                    style={[styles.input, this.state.groupNumberError ? { height: 65, paddingBottom: 20 } : { }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.groupNumber}
                    onChangeText={(text) => this.setState({groupNumber: text})}
                    onFocus={this.onFocusAll}
                    placeholder="Group Number"
                  /> 
                  <Text style={styles.errorFont}>{this.state.groupNumberError}</Text>                 
                </View>                
                <View style={styles.inputContainer}>
                  <TextInput
                    style={[styles.input, this.state.memberIdError ? { height: 65, paddingBottom: 20 } : { }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.memberId}
                    onChangeText={(text) => this.setState({memberId: text})}
                    onFocus={this.onFocusAll}
                    placeholder="Member ID"
                  />
                  <Text style={styles.errorFont}>{this.state.memberIdError}</Text>                 
                </View>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={[styles.input, this.state.emailError ? { height: 65, paddingBottom: 20 } : { }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({email: text})}
                    textContentType="emailAddress"
                    onFocus={this.onFocusAll}
                    placeholder="Preferred Email Address"
                  />
                  <Text style={styles.errorFont}>{this.state.emailError}</Text>
                </View>
                <View style={styles.inputContainer}>
                  <TextInput
                    ref={(ref) => (this.inputRefPassword = ref)}
                    style={[styles.input, this.state.passwordError ? { height: 65, paddingBottom: 20, paddingRight: 40 } : { paddingRight: 40 }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.password}
                    onChangeText={(text) => this.setState({password: text})}
                    secureTextEntry={!this.state.passwordShow}
                    onFocus={this.onFocusCreatePassword}
                    onEndEditing={this.onEndCreatePassword}
                    placeholder="Create Password"
                  />
                  {
                    <TouchableOpacity style={styles.passwordContainer} onPress={()=> this.setState({ passwordShow: !this.state.passwordShow })}>
                      <Icon
                        type="Feather"
                        name={this.state.passwordShow ? "eye-outline" : "eye-off-outline" }
                        style={styles.passwordIcon}
                      />   
                    </TouchableOpacity>
                  }                  
                  <Text style={styles.errorFont}>{this.state.passwordError}</Text>
                </View>
                {
                  this.state.passwordConditionsShow &&
                  <View style={{ flex: 1, paddingTop: 5 }}>
                    <View style={[styles.inputContainer, { paddingRight: 0 }]}>
                      <Text style={[styles.errorFont, { position: "relative", color: Colors.white }]}>Your password mus have:</Text>                  
                    </View>
                    <View style={[styles.inputContainer, { paddingTop: 5 }]}>
                      <Text style={[styles.errorFont, { position: "relative", color: Colors.white }]}>{"    "}8 or more characters</Text>                  
                    </View>
                    <View style={[styles.inputContainer, { paddingTop: 5 }]}>
                      <Text style={[styles.errorFont, { position: "relative", color: Colors.white }]}>{"    "}1 uppercase and lowercase letters (A-Z), (a-z)</Text>                  
                    </View>
                    <View style={[styles.inputContainer, { paddingTop: 5 }]}>
                      <Text style={[styles.errorFont, { position: "relative", color: Colors.white }]}>{"    "}1 numbers (0-9)</Text>                  
                    </View>
                    <View style={[styles.inputContainer, { paddingTop: 5 }]}>
                      <Text style={[styles.errorFont, { position: "relative", color: Colors.white }]}>{"    "}1 special characters (!-*).</Text>                  
                    </View>
                  </View>                  
                }                
                <View style={styles.inputContainer}>
                  <TextInput
                    ref={(ref) => (this.inputRef = ref)}
                    style={[styles.input, this.state.confirmPasswordError ? { height: 65, paddingBottom: 20, paddingRight: 40 } : { paddingRight: 40 }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.confirmPassword}
                    onChangeText={(text) => this.setState({confirmPassword: text})}
                    onFocus={this.onFocusAll}
                    secureTextEntry={!this.state.confirmPasswordShow}
                    placeholder="Confirm Password"
                  />
                  {
                    <TouchableOpacity style={styles.passwordContainer} onPress={()=> this.setState({ confirmPasswordShow: !this.state.confirmPasswordShow })}>
                      <Icon
                        type="Feather"
                        name={this.state.confirmPasswordShow ? "eye-outline" : "eye-off-outline" }
                        style={styles.passwordIcon}
                      />   
                    </TouchableOpacity>
                  }
                  <Text style={styles.errorFont}>{this.state.confirmPasswordError}</Text>
                </View>
                <View style={styles.checkBoxontainer}>
                  <Check 
                    isCheckProp={this.state.termsCondition}
                    text={"Accept the terms and conditions"}
                    onClick={this.onTermsAndCondition}
                    textStyle={{ textDecorationLine: 'underline' }}                  
                  />
                </View>
                <View style={[styles.checkBoxontainer, { paddingTop: 0 }]}>
                  <Text style={[styles.errorFont, { position: "relative" }]}>{this.state.termsConditionError}</Text>
                </View>
                <View style={styles.alreadyRegisterContainer}>
                  <TouchableOpacity onPress={this.back}>
                    <Text style={styles.alreadyRegisterText}>Already registered?</Text>
                  </TouchableOpacity>                  
                </View> 
                <View>

                </View> 
                <Button onPress={this.registerClick} full={true} info={true} style={styles.baseButton}>
                  <Text style={styles.buttonText}>Confirm</Text>
                </Button>
                <FooterLogin />                    
              </View>
            </ImageBackground>
          </KeyboardAvoidingView>
        </ScrollView>
      </Container>
    );
  }

}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  callRegister: (groupNumber, memberId, email, password) => dispatch(LoginActions.register({
    groupNumber, memberId, email, password,
  })),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen) as React.ComponentClass<OwnProps>;
