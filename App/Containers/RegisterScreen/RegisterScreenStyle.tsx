import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";
import Fonts from "../../Themes/Fonts";
import ApplicationStyles from "../../Themes/ApplicationStyles";
import Color from "color";

const height = 56;

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: Metrics.baseMargin,
  },
  pickerStyle: {
    marginTop: 18,
    width: "100%",
    borderRadius: 3,
    backgroundColor: Colors.searchBg,
    borderColor: Colors.border,
    borderWidth: 1,
  },
  backgroundBg: {
    width: "100%",
    height: "100%",
  },
  logoContainer: {
    paddingTop: 70,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: { 
    width: 218.4,
    height: 56,
    resizeMode: "stretch"
  },
  bodyContainer: {
    padding: Metrics.section,
    flex: 1
  },
  textHeader: {
    fontFamily: Fonts.type.opensans,
    color: Colors.white,
    fontSize: 24,
    paddingLeft: 4
  },
  alreadyRegisterContainer: {
    flexDirection: "row",
    paddingTop: 20,
    paddingBottom: 50,
    justifyContent: "center",
    alignSelf: "center"
  },
  alreadyRegisterText: {
    fontFamily: Fonts.type.opensans,
    color: Colors.white,
    fontSize: 20,
  },
  inputContainer: {
    flexDirection: "row",
    paddingTop: 15
  },
  passwordContainer: {
    position: "absolute",
    right: "5%",
    top: "58%",
    height: height
  },
  passwordIcon : {
    fontSize: 20,
    color: "#595959"
  },
  input: {
    ...ApplicationStyles.inputText,
    alignSelf: "stretch",
    height: height,
    borderRadius: 10,
    width: "100%",
    fontFamily: Fonts.type.opensans,
    paddingHorizontal: 15,
    color: Colors.black
  },
  checkBoxontainer: {
    flexDirection: "row",
    paddingTop: 10,
    justifyContent: "center",
    alignSelf: "center"
  },
  baseButton: {
    height: height,
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: Colors.white
  },
  buttonText: {
    textAlign: "center",
    color: Colors.blue,
    fontSize: 24,
    fontWeight: "600",
    fontFamily: Fonts.type.opensans
  },
  containerHeader: {
    backgroundColor: Colors.primary,
    height: 64,
  },
  text: {
    color: Colors.white,
  },
  icon: {
    color: Colors.white,
  },  
  errorFont: {
    position: "absolute",
    bottom: 8,
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.small,
    color: Colors.errorColor,
    left: 15
  }
});
