import React from "react";
import Header from "@root/App/Components/Header/Header";
import {Col, Container, Content, ListItem, Row, Text} from "@codler/native-base";
import {RootState} from "@root/App/Reducers";
import {connect} from "react-redux";
import {Icon} from "@codler/native-base";
import {Colors, Fonts} from "@root/App/Themes";
import styles from "./PharmacyStage2ScreenStyle";
import {
  ActivityIndicator,
  FlatList, Image,
  LayoutChangeEvent,
  LayoutRectangle,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import Divider from "@root/App/Components/Divider";
import * as R from "ramda";
import {DrugInfoRequest, GetHelpActions} from "@root/App/Reducers/GetHelpReducers";
import PickerComponent from "@root/App/Components/PickerComponent";
import {onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, tap} from "rxjs/operators";
import {getPharmacyStoreImage} from "@root/App/Lib/PharmacyHelper";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";
import PharmacyMapView from "@root/App/Components/PharmacyMapView/PharmacyMapView";
/**
 * The properties passed to the component
 */
export interface OwnProps {
  item: DrugResponse;
  zipCode: string;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestDrugPrice: (NDC: string, ZipCode: string, Quantity: string) => void;
  requestDrugStructure: (seo: string) => void;
  requestDrugName: (text: string) => void;
  clearDrugRequest: () => void;
  requestDrugInfo: (param: DrugInfoRequest) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  drugName?: DrugResponse[] | undefined;
  prices: PriceResponse[] | undefined;
  zipCode: string;
  structureResponse: any;
  fetchingDrug: boolean;
}

/**
 * The local state
 */
export interface State {
  locationText: string;
  layout?: LayoutRectangle;
  drugItem: DrugResponse;
  dosage: number;
  drugType: number;
  quantity: number;
  drugSearchText: string;
  expanding: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class PharmacyScreenStage2 extends React.Component<Props, State> {

  private keyword$ = new Subject<string>();

  constructor(props) {
    super(props);
    this.state = {
      drugItem: props.item,
      drugSearchText: props.item.DisplayName,
      locationText: props.zipCode,
      dosage: 0,
      drugType: 0,
      quantity: 0,
      expanding: false
    }

    onErrorResumeNext(
      this.keyword$.pipe(
        tap((text) => {
          this.setState({drugSearchText: text});
          this.props.clearDrugRequest();
        }),
        debounceTime(300),
        filter((value) => value.length > 2),
        distinctUntilChanged(),
      ),
    ).subscribe((value) => this.props.requestDrugName(value));
  }

  public _renderDrugItem = (value) => (
    <ListItem
      style={{flex: 1}}
      noIndent={true}
      onPress={() => {
        this.setState({drugSearchText: value.DisplayName, drugItem: value, quantity: 0, drugType: 0, dosage: 0});
        this.props.clearDrugRequest();
      }}
    >
      <Text>{value.DisplayName}</Text>
    </ListItem>
  )

  public callStructureApi() {
    this.props.requestDrugStructure(this.state.drugItem.SEOName)
  }

  public callPriceLookUpApi() {
    const ndcItem = this.getNDCItem();
    this.props.requestDrugPrice(ndcItem.NDC, this.state.locationText, ndcItem.Quantity);
  }

  public getNDCItem() {
    const drugTypeKey = R.compose(R.view(R.lensIndex(this.state.drugType)), R.keys)(this.props.structureResponse);
    const dosageKey = R.compose(R.view(R.lensIndex(this.state.dosage)), R.keys)(this.props.structureResponse[drugTypeKey]);
    return this.props.structureResponse &&
      this.props.structureResponse[drugTypeKey][dosageKey][this.state.quantity];
  }


  public componentDidMount(): void {
    // this.props.requestDrugPrice(this.props.item.NDC, this.props.zipCode);
    this.callStructureApi();
  }

  public componentDidUpdate(prevProps: Readonly<StateProps & DispatchProps & OwnProps>, prevState: Readonly<State>, snapshot?: any): void {

    if (prevState.quantity !== this.state.quantity || prevState.dosage !== this.state.dosage || prevState.drugType !== this.state.drugType
      || this.props.structureResponse !== prevProps.structureResponse
      || (this.state.locationText.length === 5 && this.state.locationText !== prevState.locationText)
    ) {
      this.callPriceLookUpApi()
    }

    if (this.state.drugItem.SEOName !== prevState.drugItem.SEOName) {
      this.callStructureApi()
    }
  }

  public render() {
    return (
      <Container>
        <Header hideMenu={true}/>
        <Content style={{padding: 20}}>
          <Text style={[styles.description, {color: Colors.danger, marginBottom: 10}]}>
            These prices are direct/self pay rates and are not applicable if using your insurance. Please compare with
            your insurance rate if applicable before choosing to pay with options below.
          </Text>
          <View style={styles.drugNameTextContainer} onLayout={this.onSearchTextLayout}>
            <TextInput
              value={this.state.drugSearchText}
              onChangeText={this.onDrugSearchTextChange}
              placeholderTextColor={Colors.textSuggestion}
              style={[styles.input, {flex: 1}]}
              placeholder="Drug Name"
            />
            {this.props.fetchingDrug && <ActivityIndicator style={styles.indicator} size={"small"}/>}
          </View>
          {
            this.state.layout && !!this.props.drugName && this.props.drugName.length > 0 &&
            <ScrollView style={[styles.drugResultContainer, {top: this.state.layout.y + this.state.layout.height + 5}]}>
              {R.map(this._renderDrugItem)(this.props.drugName)}
            </ScrollView>
          }
          <TextInput
            value={this.state.locationText}
            keyboardType={"numeric"}
            maxLength={5}
            onChangeText={this.onLocationChange}
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, styles.rowContainer, {flex: 1}]}
            placeholder="Zip code"
          />
          {
            !!this.props.structureResponse ?
              <>
                <PickerComponent
                  style={styles.rowContainer}
                  onItemSelected={(index) => {
                    this.setState({
                      drugType: index,
                    })
                  }}
                  items={R.keys(this.props.structureResponse)}
                />
                <Row style={styles.rowContainer}>
                  <Col>
                    <PickerComponent
                      onItemSelected={(index) => {
                        this.setState({
                          dosage: index,
                        })
                      }}
                      items={R.compose(
                        R.keys,
                        R.view(R.lensIndex(this.state.drugType)),
                        R.values,
                      )(this.props.structureResponse)}
                    />
                  </Col>
                  <Col>
                    <PickerComponent
                      style={styles.inputRight}
                      onItemSelected={(index) => {
                        this.setState({
                          quantity: index,
                        })
                      }}
                      items={R.map(R.prop("Quantity"), R.compose(
                        R.view(R.lensIndex(this.state.dosage)),
                        R.values,
                        R.view(R.lensIndex(this.state.drugType)),
                        R.values,
                      )(this.props.structureResponse))}
                    />
                  </Col>
                </Row>
              </> :
              <Text style={styles.header}>
                Oh no there is an error
              </Text>
          }
          <View style={{ flex: 1, paddingTop: 15 }}>
            <View style={{backgroundColor: Colors.magento, flexDirection: "row" }} >
              <View style={{ flex: 5, alignSelf: "center", justifyContent: "center", paddingLeft: 10 }}>
                <Text style={{ color: Colors.white, fontSize: 16, fontFamily: Fonts.type.opensans }}>
                  Map View
                </Text>
              </View>
              <View style={{ flex: 5, justifyContent: "flex-end", alignItems: "flex-end", paddingRight: 10 }}>
                <TouchableOpacity onPress={()=> this.setState({ expanding: !this.state.expanding })}>
                  <Icon style={{color: Colors.white}} name={this.state.expanding ? "angle-double-up" : "angle-double-down"} type={"FontAwesome"}/>
                </TouchableOpacity>
              </View>              
            </View>            
            {
              this.state.expanding && this.props.prices && this.props.prices.length > 0 && 
                <PharmacyMapView data={this.props.prices}/>
            }
          </View>
          <Text style={styles.header}>
            Compare Pharmacies
          </Text>
          <Divider style={styles.divider}/>

          {this.props.prices && this.props.prices.length ?
            <FlatList
              data={this.props.prices}
              renderItem={this.renderItem}
            /> :
            <Text style={styles.header}>
              Oh no there is an error
            </Text>
          }


        </Content>
      </Container>
    );
  }

  private onDrugSearchTextChange = (text) => {
    this.keyword$.next(text);
  };

  private onSearchTextLayout = ({nativeEvent: {layout}}: LayoutChangeEvent) => this.setState({layout});

  private onLocationChange = (text) => {
    this.setState({locationText: text})
  };

  private renderItem = ({item}) => {

    const pickAddress = R.compose(
      R.join(", "),
      R.reject(R.isEmpty),
      R.values,
      R.pick(["Address1", "Address2", "City", "PostalCode", "State"]),
    );

    const logo = getPharmacyStoreImage(item.Name);

    return (
      <TouchableOpacity
        style={styles.itemBorder}
        onPress={() => {
          const drugTypeKey = R.compose(R.view(R.lensIndex(this.state.drugType)), R.keys)(this.props.structureResponse) as string;
          const dosageKey = R.compose(R.view(R.lensIndex(this.state.dosage)), R.keys)(this.props.structureResponse[drugTypeKey]) as string;
          this.props.requestDrugInfo({
            NDCItem: this.getNDCItem(),
            PriceResponse: item,
            ConsumptionType: drugTypeKey,
            Dosage: dosageKey,
            DrugItem: this.state.drugItem,
          });
        }}
      >
        <Row>
          <View style={styles.fullContainer}>
            <Row style={styles.imageContainer}>
              {
                !!logo ?
                  <Image style={{flex: 1, height: 40}} source={logo} resizeMode={"contain"}/>
                  :
                  <Text style={{flex: 1}}>{item.Name}</Text>
              }

              <Text style={styles.textDistance}>{item.Distance.toFixed(2)}
                <Text style={styles.textMiles}>miles</Text></Text>
            </Row>

            <Text style={styles.textAddress}>{pickAddress(item.Address)}</Text>
          </View>
          <View style={styles.priceContainer}>
            <Text style={styles.priceText}>{parseAmount(item.Price[0].Price.toFixed(2))}</Text>
          </View>
        </Row>
      </TouchableOpacity>
    );
  };
}

const mapDispatchToProps = (dispatch: Function): DispatchProps => ({
  requestDrugStructure: (seo) => dispatch(GetHelpActions.requestDrugStructure(seo)),
  requestDrugPrice: (NDC, ZipCode, Quantity) => dispatch(GetHelpActions.requestDrugPrice({NDC, ZipCode, Quantity})),
  requestDrugName: (text) => dispatch(GetHelpActions.requestDrugName(text)),
  clearDrugRequest: () => dispatch(GetHelpActions.clearDrugName()),
  requestDrugInfo: (param) => dispatch(GetHelpActions.requestDrugInfo(param)),
});


const mapStateToProps = (state: RootState): StateProps => {
  return ({
    drugName: state.help.drugNames,
    prices: state.help.drugPrices,
    zipCode: state.location.zipCodeForDrugs,
    structureResponse: state.help.drugStructureResponse,
    fetchingDrug: state.help.fetchingDrugName,
  })
};

export default connect(mapStateToProps, mapDispatchToProps)(PharmacyScreenStage2);
