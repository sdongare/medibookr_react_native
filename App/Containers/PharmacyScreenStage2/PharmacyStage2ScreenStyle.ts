import {
  StyleSheet,
} from "react-native";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import {Colors, Metrics} from "../../Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  content: {
    flex: 1,
    padding: 20,
  },
  inputRight: {
    marginLeft: 10,
  },
  rowContainer: {
    marginTop: 10,
  },
  drugResultContainer: {
    backgroundColor: Colors.searchBg,
    position: "absolute",
    alignSelf: "stretch",
    maxHeight: 200,
    ...ApplicationStyles.shadow,
    zIndex: 100,
  },
  drugNameTextContainer: {
    flexDirection: "row",
  },
  indicator: {
    position: "absolute",
    top: 43,
    right: Metrics.smallMargin,
    alignSelf: "center",
  },
  textButton: {
    color: Colors.white,
  },
  header: {
    fontFamily: Fonts.type.latoBold,
    fontSize: 20,
    marginTop: 20,
    color: Colors.textDark,
  },
  fullContainer: {
    flex: 1,
  },
  imageContainer: {margin: 10, alignItems: "flex-end", alignContent: "flex-end"},
  textDistance: {
    textAlign: "right", minWidth: 100, fontFamily: Fonts.type.opensansBold, color: Colors.textGrey, fontSize: 18,
  },
  textMiles: {
    fontFamily: Fonts.type.opensans, color: Colors.textGrey, fontSize: 18,
  },
  textAddress: {
    fontFamily: Fonts.type.opensans, color: Colors.textGrey, fontSize: 12, marginHorizontal: 10, marginBottom: 10,
  },
  priceContainer: {
    padding: 10, minWidth: 80, justifyContent: "center", alignItems: "center", backgroundColor: Colors.primaryLight,
  },
  priceText: {
    color: Colors.white, fontSize: 24, fontFamily: Fonts.type.latoBold,
  },
  itemBorder: {
    borderWidth: 3,
    marginVertical: 10,
    borderColor: Colors.primaryLight,
  },
  description: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 16,
    color: Colors.textDark,
    marginTop: 10,
  },
  divider: {
    marginVertical: 0,
  },
  button: {
    ...ApplicationStyles.shadow,
    marginLeft: 10,
  },
  label: {
    marginTop: 12,
    fontFamily: Fonts.type.latoBold,
    fontSize: 16,
    color: Colors.textDark,
  },
  input: {
    ...ApplicationStyles.inputText,
    backgroundColor: Colors.searchBg,
  },
  pickerText: {
    width: "100%",
  },
  pickerStyle: {
    marginTop: 4,
    flex: 1,
    borderRadius: 3,
    borderColor: Colors.border,
    borderWidth: 1,
  },
});
