import * as React from "react";
import {connect} from "react-redux";
// Styles
import ReduxNavigation from "@root/App/Navigation/ReduxNavigation";
import {Root, StyleProvider} from "@codler/native-base";
import getTheme from "../../../native-base-theme/components";
import Loader from "@root/App/Containers/Loader/Loader";
import {RootState} from "@root/App/Reducers";
import {Alert, PermissionsAndroid, Platform} from "react-native";
import {LatLng} from "react-native-maps";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import InteractionProvider from "react-native-interaction-provider";
import {LoginActions} from "@root/App/Reducers/LoginReducers";
import Geolocation from "@react-native-community/geolocation";


interface IProps {
}

interface State {
}

interface IDispatchProps {
  setGpsLocation: (location: LatLng) => void;
  logout: () => void;
}

interface IStateProps {
}

type Props = IProps & IDispatchProps & IStateProps;

export class RootContainer extends React.Component<Props, State> {

   public render() {
    return (
      <StyleProvider style={getTheme()}>
        <InteractionProvider
          timeout={15 * 60 * 1000} // idle after 15m
          onInactive={this.props.logout}

        >
          <Root>
            <ReduxNavigation/>
            <Loader/>
          </Root>
        </InteractionProvider>
      </StyleProvider>
    );
  }
}

const mapStateToProps = (state: RootState): IStateProps => ({});

const mapDispatchToProps = (dispatch: Function): IDispatchProps => {
  return {
    setGpsLocation: (location) => dispatch(LocationActions.gpsLocation(location)),
    logout: () => dispatch(LoginActions.logout()),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
