import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import Fonts from "@root/App/Themes/Fonts";
import { F } from "ramda";

const height = 56;

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    marginHorizontal: Metrics.baseMargin,
  },
  bodyContainer: {
    height: 320, 
    marginHorizontal:6,
    alignSelf: 'center',
    backgroundColor: Colors.transparent ,
  },
  logoImage:{
    position:'absolute',
    height:'39.5%',
    width:'100%',
  },
  scrollView:{
   flexGrow: 1,
   justifyContent: 'center',
   backgroundColor:'rgba(52, 52, 52, 0.0)',
  },
  baseButton: {
    height: height,
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "#2e6da1"
  },
  logoButton: {
    height: 40,
    width:150,
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor:Colors.transparent,
    position:'absolute',
    zIndex:10,
    left:'6.67%',
  },
  buttonText: {
    textAlign: "center",
    color: Colors.white,
    fontSize: 24,
    fontWeight: "600",
    fontFamily: Fonts.type.sourceSansProSemibold
  },
 
  textHeader: {
    fontFamily: Fonts.type.sfprodisplaySemiBold,
    color: Colors.black,
    fontSize: 30,
    textAlign: "center",
  },
  inputContainer: {
    flexDirection: "row",
    paddingTop: 20,
    marginHorizontal:16,
  },
  textInfo: {
    fontFamily: Fonts.type.sourceSansProRegular,
    color: "#262626",
    fontSize: 16,
    marginTop:20,
    marginHorizontal: 30,
    textAlign: "left",
  },
  input: {
    ...ApplicationStyles.inputText,
    alignSelf: "stretch",
    height: height,
    borderRadius: 10,
    width: "100%",
    fontFamily: Fonts.type.opensans,
    paddingHorizontal: 30,
    fontSize: 16,
    color: Colors.black
  },
  errorFont: {
    position: "absolute",
    bottom: 8,
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.small,
    color: Colors.errorColor,
    left: 15
  },
  forgotButton:{
    flex: 5,
    alignItems: "flex-end",
    paddingTop: 20,
    marginHorizontal:16,
    }
});
