import * as React from "react";
import {
  Alert,
  BackHandler,
  ImageBackground,
  Keyboard,
  Dimensions,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { connect } from "react-redux";
import * as Redux from "redux";
// Styles
import styles from "./ForgotPasswordScreenStyle";
import { RootState } from "@root/App/Reducers";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Button, Container, Header, Picker, Text } from "@codler/native-base";
import { LoginActions, LoginRequestParams } from "@root/App/Reducers/LoginReducers";
import { Fonts, Images, Metrics } from "@root/App/Themes";
import { Actions } from "react-native-router-flux";
import Colors from "@root/App/Themes/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import { LocationActions } from "@root/App/Reducers/LocationReducers";
import { BiometricActions } from "@root/App/Reducers/BiometricReducers";
import colors from "@root/App/Themes/Colors";
import DeviceInfo from "react-native-device-info";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  forgotPassword: (email) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  
}

/**
 * The local state
 */
export interface State {
  email: string;
  emailError: string;
}

type Props = StateProps & DispatchProps & OwnProps;

export const RegexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class ForgotPasswordScreen extends React.Component<Props, State> {
  public state = {
    email: "",
    emailError: "",
  };

  private backHandler: any;
  private inputRef: any;

  constructor(props) {
    super(props);
    
  }


  public handleBackPress = () => {
    return false;
  }

  public componentWillUnmount(): void {
    this.backHandler?.remove()
  }

  public async componentDidMount() {
    this.inputRef.setNativeProps({
      style: {
        fontFamily: Fonts.type.opensans,
      },
    });
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);

    let email;
    email = await AsyncStorage.getItem("email").catch((e) => {
    });
    this.setState({ email });
  }

  public onEmailText = (text) => {
    this.setState({ email: text });
  };


  public validateEmail = () => {
    this.setState({ emailError: "" })
    var validationOccured = true;
    if (!this.state.email) {
      this.setState({ emailError: 'Please provide valid email address.' })
      validationOccured = false
    }
    else if (!RegexEmail.test(this.state.email.toLowerCase())) {
      this.setState({ emailError: 'Please provide valid email address.' })
      validationOccured = false
    }
    return validationOccured;
  }
  public logoClick = () => {
       Actions.pop()
       
  }
  public forgotPasswordClick = () => {
    const message = this.validateEmail();
    if (message) {
      this.props.forgotPassword(this.state.email);
    } else {
      Alert.alert("Invalid email", "Please enter a valid email id", [
        { text: "OK" },
      ]);
    }
  };
//
  public render() {

    const top = DeviceInfo.hasNotch() ? '6.0%' : '4.0%';
    return (
      <Container>
       <Button onPress={this.logoClick} full={true} info={true} style={{...styles.logoButton,top:top}}>
       <Image source={Images.applogo} style={{flex:1}} resizeMode='stretch'/>
      </Button>
      <Image source={Images.ResetPasswordBg} style={styles.logoImage} resizeMode='stretch'>
      </Image>
      <ScrollView
          contentContainerStyle={styles.scrollView} >
          <View style={styles.container}>
            <View style={styles.bodyContainer}>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 8 }}>
                  <Text style={styles.textHeader}>Password Reset Request</Text>
                  <Text style={styles.textInfo}>Enter the email address of the account that
                    you wish to change the password for,if it is a
                    valid account you will receive an email with a
                    link to change the password.</Text>
                </View>
              </View>
              <View style={styles.inputContainer}>
                <TextInput
                  style={[styles.input, this.state.emailError ? { height: 65, paddingBottom: 20 } : {}]}
                  placeholderTextColor={Colors.placeHolderSuggestion}
                  value={this.state.email}
                  onChangeText={(text) => this.setState({ email: text })}
                  textContentType="emailAddress"
                  placeholder="Preferred Email Address"
                />
                <Text style={styles.errorFont}>{this.state.emailError}</Text>
              </View>
              <View style={styles.forgotButton}>
                <Button onPress={this.forgotPasswordClick} full={true} info={true} style={styles.baseButton}>
                  <Text style={styles.buttonText}>Submit</Text>
                </Button>
              </View>
            </View>
          </View>
        </ScrollView>

      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  forgotPassword: (email) => dispatch(LoginActions.forgotPassword(email)),
 
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen);
