import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
    padding: 20
  },
  text: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h6,
    color: Colors.textDark,
    marginTop: Metrics.smallMargin,
  },
  textIcons: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h5,
    color: Colors.textDark,
    marginTop: Metrics.smallMargin,
  },
  header: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 38,
    marginTop: Metrics.baseMargin,
    color: Colors.textDark,
  },
  mail: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h4,
    marginTop: Metrics.baseMargin,
    color: Colors.textBlue,
    // textAlign: "center"
  },
  phone: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h3,
    color: Colors.textBlue,
    marginTop: Metrics.doubleBaseMargin,
  },
  socialContainer: {
    flexDirection: "row",
    marginVertical: Metrics.doubleBaseMargin,
    justifyContent: "space-around",
  },
  fbIcon: {
    backgroundColor: "#2a65b1",
  },
  twitterIcon: {
    backgroundColor: "#48d0d4",
  },
  instaIcon: {
    backgroundColor: "#d5593f",
  },
  liIcon: {
    backgroundColor: "#3d99ca",
  },
});
