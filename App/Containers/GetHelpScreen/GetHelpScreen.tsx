import * as React from "react";
import {Linking, Text, View, ScrollView, Platform} from "react-native";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./GetHelpScreenStyle";
import {Button, Container, Content, Icon, ListItem} from "native-base";
import Header from "@root/App/Components/Header/Header";
import {formatNumber} from "@root/App/Transforms/StringsTranform";
import {PhoneNumber} from "@root/App/Lib/AppHelper";
import {Actions} from "react-native-router-flux";
import {Colors} from "@root/App/Themes";
import AppConfig from "@root/App/Config/AppConfig";
import TooltipComponenet from "@root/App/Components/TooltipComponenet";
import AsyncStorage from "@react-native-community/async-storage";
import {GetHelpActions} from "@root/App/Reducers/GetHelpReducers";
import Ionicons from 'react-native-vector-icons/Ionicons';

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestInfoMessage: (key: string) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  infoMessage: InfoMessage | undefined;
}

/**
 * The local state
 */
export interface State {
  toolTipVisible: boolean;
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>;

class GetHelpScreen extends React.Component<Props, State> {

  public state = {
    toolTipVisible: false
  };

  public openMail = () => {
    const mailLink = "mailto:support@medibookr.com";

    Linking.openURL(mailLink);
  };

  public openFb = () => {
    const mailLink = "https://web.facebook.com/MediBookrcom-1679558365659141/?_rdc=1&_rdrx";
    Linking.openURL(mailLink);
  };

  public openTwitter = () => {
    const mailLink = "https://twitter.com/MediBookr/";
    Linking.openURL(mailLink);
  };

  public openInsta = () => {
    const mailLink = "https://www.instagram.com/medibookr/";
    Linking.openURL(mailLink);
  };

  public openLinkedIn = () => {
    const mailLink = "https://www.linkedin.com/company/medibookr/";
    Linking.openURL(mailLink);
  };

  public openPhone = () => {
    const numberLink = `tel:${formatNumber(PhoneNumber)}`;
    Linking.openURL(numberLink);
  };

  public componentDidMount = async () => {
    var getHelpTooltipView = await AsyncStorage.getItem('getHelpTooltipView');
    if(!getHelpTooltipView)
    {
      this.props.requestInfoMessage("concierge-services");
      setTimeout(()=> this.setState({ toolTipVisible: true }), 500) 
      AsyncStorage.setItem('getHelpTooltipView', 'view');     
    }    
  }

  public render() {
    var tooltipContent = [];
    if(this.state.toolTipVisible && this.props.infoMessage && Object.keys(this.props.infoMessage).length > 0 && this.props.infoMessage.key == "concierge-services")
    {
      tooltipContent.push(
        <TooltipComponenet 
          position={"top"}
          heading={this.props.infoMessage.title}
          description={this.props.infoMessage.message}
          style={{ top: Platform.OS == "ios" ? "1%" : "2%" }}
        /> 
      ) 
    }    
    return (
      <Container>
        <Header/>
        <ScrollView>
          <View style={styles.container}>
          <Text style={styles.header}>Contact MediBookr</Text>
          <Text style={[styles.text, {paddingTop: 3}]}>Talk to a knowledgeable patient advocate, send us a note or live chat.</Text>
          <Text style={[styles.text, {paddingTop: 10}]}>Office Hours: </Text>
          <Text style={[styles.text, {marginTop: 2}]}>Mon - Fri (8:00 am - 5:00 pm) CST</Text>
          <Text style={styles.phone} onPress={this.openPhone}>{formatNumber(PhoneNumber)}</Text>
          <Text style={[styles.text, {paddingTop: 5}]}>Reach a patient advocate now for help finding care, comparing options and reviewing bills.</Text>
          {tooltipContent}            
          <Text style={styles.mail} onPress={this.openMail}>support@medibookr.com</Text>
          <View style={styles.socialContainer}>
            <Button style={styles.fbIcon} onPress={this.openFb}>
              <Icon
                name={"facebook"}
                type={"FontAwesome"}
              />
            </Button>

            <Button style={styles.twitterIcon} onPress={this.openTwitter}>
              <Icon
                name={"twitter"}
                type={"FontAwesome"}
              />
            </Button>

            <Button style={styles.instaIcon} onPress={this.openInsta}>
              <Icon
                name={"instagram"}
                type={"FontAwesome"}
              />
            </Button>

            <Button style={styles.liIcon} onPress={this.openLinkedIn}>
              <Icon
                name={"linkedin"}
                type={"FontAwesome"}
              />
            </Button>
          </View>

          <ListItem
            noIndent={true}
            noBorder={true}
            onPress={() => {
              Actions.push("faqs");
            }}
          >
            <Icon style={{color: Colors.primary, width: 40, marginRight: 20}} name={"question-circle"} type={"FontAwesome"}/>
            <Text style={styles.textIcons}>Frequently Asked Questions</Text>
          </ListItem>

          <ListItem
            noIndent={true}
            noBorder={true}
            onPress={() => {
              Actions.push("feedback");
            }}
          >
            <Ionicons style={{color: Colors.primary, width: 40, marginRight: 20, fontSize: 30}} name={"ios-bulb"}/>
            <Text style={styles.textIcons}>Send us feedback</Text>
          </ListItem>

          <ListItem
            noIndent={true}
            noBorder={true}
            onPress={() => {
              Actions.push("link", {uri: AppConfig.url() + "static/contact.html"});
            }}
          >
            <Ionicons style={{color: Colors.primary, width: 40, marginRight: 20, fontSize: 28 }} name={"ios-chatbubbles"} />
            <Text style={styles.textIcons}>Chat With Us</Text>
          </ListItem>
        </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  requestInfoMessage: (key) => dispatch(GetHelpActions.requestInfoMessage(key)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    infoMessage: state.help && state.help.infoMessage,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GetHelpScreen) as React.ComponentClass<OwnProps>;
