import * as React from "react";
import {ActivityIndicator, Alert, FlatList, View} from "react-native";
import {Body, Button, Card, CardItem, Form, Grid, Input, Item, Row, Text, Thumbnail} from "@codler/native-base";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./RegistrationProcess3ScreenStyle";
import SearchByNameResponse, {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {DashboardActions} from "@root/App/Reducers/DashboardReducers";
import {getGenderImage, getLocation} from "@root/App/Lib/AppHelper";
import {RegistrationProcessActions} from "@root/App/Reducers/RegistrationProcessReducers";
import {onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, tap} from "rxjs/operators";
import {LatLng} from "react-native-maps";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import * as R from "ramda";
import {formatNumber, getZipCode} from "@root/App/Transforms/StringsTranform";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  handlePage: (num: number) => void
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestNames: (name: string) => void;
  changePCP: (pcp: string) => void;
  requestFixLocation: (location: string) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  loading: boolean;
  results: SearchByNameResponse;
  pcp: PCPResponse;
  location: LatLng | undefined;
  city: string | undefined;
}

/**
 * The local state
 */
export interface State {
  name: string;
  selected: PCPResponse;
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>

class RegistrationProcess3Screen extends React.Component<Props, State> {

  private keyword$ = new Subject<string>();

  constructor(props: Props) {
    super(props);
    this.state = {
      name: "",
      selected: props.pcp,
    };

    onErrorResumeNext(
      this.keyword$.pipe(
        tap((x) => {
          if (!this.props.location) {
            Alert.alert("No Location found", "Please enable permissions to locate you or update your address in previous screen, Thank you.");
          }
        }),
        debounceTime(300),
        filter((value) => value.length > 2),
        filter(((value) => !!this.props.location)),
        distinctUntilChanged(),
      ),
    ).subscribe((value) => this.props.requestNames(value));
  }

  public componentWillMount(): void {
    if (!this.props.location && this.props.city) {
      this.props.requestFixLocation(this.props.city);
    }
  }


  public renderRow = ({item}) => {
    const data = item as ProviderResultsItem;
    return (
      <Card>
        <CardItem
          button={true}
          onPress={() => {
            // @ts-ignore
            this.setState({selected: data});
          }}
        >
          <Body style={styles.cardContainer}>
            <View style={styles.cardHeader}>
              <Thumbnail
                square={true}
                source={getGenderImage(data)}
              />
            </View>
            <View style={styles.cardBody}>
              <Text
                style={styles.cardBodyTextHeading}>{data.providerName}{data.facilityFlag ? "" : ", " + data.credentials}</Text>
              <Text style={styles.cardBodyText}>{data.providerAddressLine1}</Text>
              {!!data.providerAddressLine2 &&
              <Text style={styles.cardBodyText}>{data.providerAddressLine2}</Text>
              }
              <Text
                style={styles.cardBodyText}>{`${data.providerCity}, ${data.providerState} ${getZipCode(data.providerZipcode)}`}</Text>
            </View>
            <View style={styles.cardFooter}>
              <Text style={styles.cardBodyTextHeading}>{data.providerCity}, {data.providerState}</Text>
              <Text style={styles.cardBodyText}>Distance: {data.distance.toFixed(2)} Miles</Text>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  };

  public render() {
    // @ts-ignore
    return (
      <View style={styles.container}>
        <Text style={styles.header}>
          Search For Your Primary Care Physician
        </Text>
        <View style={styles.searchBox}>
          <Form>
            <Item regular={true}>
              <Input
                style={{backgroundColor: "#F5F5F5"}}
                placeholder="Search by name..."
                onChangeText={(text) => this.keyword$.next(text)}
              />
              {
                this.props.loading &&
                <ActivityIndicator size={"small"} style={{position: "absolute", right: 10, alignSelf: "center"}}/>}
            </Item>
          </Form>
          {
            this.props.results &&
            <FlatList
              style={styles.searchList}
              data={this.props.results.results}
              renderItem={this.renderRow}
            />
          }
        </View>
        {
          this.state.selected && !this.state.selected.message &&
          <React.Fragment>
            <Grid>
              <Row style={styles.headingAlt}>
                <Text style={styles.header}>
                  Your Selected Primary Care Physician
                </Text>
              </Row>
            </Grid>
            <Card>
              <CardItem>
                <Body style={styles.cardContainer}>
                  <View style={styles.cardHeader}>
                    <Thumbnail
                      square={true}
                      source={getGenderImage(this.state.selected)}
                    />
                  </View>
                  <View style={styles.cardBody}>
                    <Text
                      style={[
                        styles.cardBodyTextHeading,
                        styles.cardBodyTextHeadingAlt,
                      ]}
                    >
                      {`${this.state.selected.name || this.state.selected.providerName}${this.state.selected.facilityFlag ? "" : ", " + this.state.selected.credentials}`}
                    </Text>
                    {
                      !!(this.state.selected.phone || this.state.selected.providerPhone) &&
                      <Text
                        style={styles.cardBodyText}>Phone: {formatNumber(this.state.selected.phone || this.state.selected.providerPhone)}</Text>
                    }
                    <Text style={styles.cardBodyText}>
                      {this.state.selected.addressLine1 || this.state.selected.providerAddressLine1}
                    </Text>
                    {
                      !!(this.state.selected.addressLine2 || this.state.selected.providerAddressLine2) &&
                      <Text style={styles.cardBodyText}>
                        {this.state.selected.addressLine2 || this.state.selected.providerAddressLine2}
                      </Text>
                    }

                    <Text style={styles.cardBodyText}>
                      {`${this.state.selected.city || this.state.selected.providerCity}, ${this.state.selected.state || this.state.selected.providerState} ${getZipCode(this.state.selected.zipcode || this.state.selected.providerZipcode)}`}
                    </Text>

                  </View>
                  <View style={styles.cardFooter}>
                    {!this.state.selected.facilityFlag &&
                    <React.Fragment>
                      <Text style={styles.cardBodyTextHeading}>Age: {this.state.selected.age}</Text>
                      <Text style={styles.cardBodyText}>Gender: {this.state.selected.gender}</Text>
                    </React.Fragment>
                    }
                    {/*<Text style={styles.cardBodyText}>Distance: {this.props.pcp.distance} Miles</Text>*/}
                  </View>
                </Body>
              </CardItem>
            </Card>
          </React.Fragment>
        }

        <View style={styles.navBtns}>
          <Button
            onPress={() => this.props.handlePage(1)}
            style={styles.button}
            rounded={false}
            primary={true}
            small={true}
          >
            <Text>Back</Text>
          </Button>
          <Button
            onPress={() => {
              this.props.handlePage(3);
              if (this.state.selected) {
                this.props.changePCP(this.state.selected.providerId);
              }
            }}
            style={styles.button}
            rounded={false}
            primary={true}
            small={true}
          >
            <Text>Next</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  requestNames: (name) => dispatch(DashboardActions.requestByName(name)),
  changePCP: (pcp) => dispatch(RegistrationProcessActions.updatePCP(pcp, undefined)),
  requestFixLocation: (location = "Dallas") => dispatch(LocationActions.request(location, true)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    loading: state.dashboard.nameFetching,
    results: state.dashboard.nameResult,
    pcp: state.patient.pcp,
    city: state.patient && R.path(["info", "city"], state.patient),
    location: state.location && getLocation(state.location),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationProcess3Screen) as React.ComponentClass<OwnProps>;
