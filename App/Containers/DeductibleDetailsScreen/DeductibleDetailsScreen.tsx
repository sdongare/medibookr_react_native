import * as React from "react";
import {Alert, Image, Text, TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import {NavigationAction, NavigationDrawerScreenOptions, NavigationScreenProps, NavigationState} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
import {Colors, Images} from "../../Themes";
import Metrics from "../../Themes/Metrics";

// Styles
import styles from "./DeductibleDetailsScreenStyle";
import {Container, Content} from "@codler/native-base";
import Header from "@root/App/Components/Header/Header";
import Divider from "@root/App/Components/Divider/Divider";
import DeductibleStatus from "@root/App/Components/DeductibleStatus/DeductibleStatus";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  summary: Summary;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>;

class DeductibleDetailsScreen extends React.Component<Props, State> {

  public state = {};

  public hasFamilyDeductible() {
    return true;
  }

  public hasIndividualDeductible() {
    return true;
  }

  public render() {
    const family = this.props.summary.deductible.family.in_network;
    const individual = this.props.summary.deductible.individual.in_network;

    return (
      <Container>
        <Header/>
        <Content padder={true}>
          <Text style={styles.title}>Deductible Status</Text>
          <Divider style={styles.divider}/>
          {
            this.hasFamilyDeductible() &&
            <React.Fragment>
              <Text style={styles.subTitle}>Family Deductible</Text>
              <DeductibleStatus
                title={""}
                total={parseInt(family.limit.amount, 10)}
                deducted={parseInt(family.applied.amount, 10)}
                color={Colors.greenDark}
              />
            </React.Fragment>
          }

          <Divider/>

          {
            this.hasIndividualDeductible() &&
            <React.Fragment>
              <Text style={styles.subTitle}>Individual Deductibles</Text>
              <DeductibleStatus
                title={""}
                total={parseInt(individual.limit.amount, 10)}
                deducted={parseInt(individual.applied.amount, 10)}
                color={Colors.orange}
              />
            </React.Fragment>
          }
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(DeductibleDetailsScreen) as React.ComponentClass<OwnProps>;
