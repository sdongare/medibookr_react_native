import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  title: {
    fontFamily: Fonts.type.opensansBold,
    fontSize: Fonts.size.regular,
    color: Colors.textDark,
  },
  divider: {
    marginVertical: 0,
    marginBottom: Metrics.baseMargin,
  },
  subTitle: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.regular,
    color: Colors.textDark,
  },
});
