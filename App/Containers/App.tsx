import "@root/App/Config";
import DebugConfig from "@root/App/Config/DebugConfig";
import createStore from "@root/App/Reducers";
import React from "react";
import {Provider} from "react-redux";
import Reactotron from "reactotron-react-native";
import RootContainer from "./RootContainer";
import Toast from 'react-native-toast-message';
import {toastConfig} from "@root/App/Components/Toaster/Toaster";

// create our store
const {store, api} = createStore();

export const ResetAPI = () => api.changeBaseUrl();

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends React.Component {
  public render() {

    return (
      <Provider store={store}>
        <RootContainer/>      
        <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
      </Provider>
    );
  }
}

// allow reactotron overlay for fast design in dev mode
// @ts-ignore
export default DebugConfig.useReactotron ? Reactotron.overlay(App) : App;
