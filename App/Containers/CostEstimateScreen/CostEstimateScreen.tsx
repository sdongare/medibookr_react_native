import React from "react";
import {Alert, Text, TextInput} from "react-native";
import styles from "./CostEstimateScreenStyle";
import {Colors} from "@root/App/Themes";
import {Body, Button, Container, Content, Header, Icon, Left, Picker, Right, Row,} from "@codler/native-base";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";
import {Actions} from "react-native-router-flux";
import {CostEstimateRequestParam, PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";

/**
 * The properties passed to the component
 */
interface OwnProps {
  provider: ProviderResultsItem;
  serviceId: string;
}

/**
 * The properties mapped from Redux dispatch
 */
interface DispatchProps {
  costEstimate?: (params: CostEstimateRequestParam) => void;
}

/**
 * The properties mapped from the global state
 */
interface StateProps {
  options?: Array<{ [key: string]: string }>;
}

/**
 * The local state
 */
interface State {
  selectedOption: string;
  query: string;
}

type Props = StateProps & DispatchProps & OwnProps;

const INITIAL_STATE = {
  selectedOption: "email",
  query: "",
};


class CostEstimateScreen extends React.Component<Props, State> {
  public state = INITIAL_STATE;
  private content: any;

  public render() {
    return (
      <Container>
        <Header>
          <Left/>
          <Body>
            <Text style={{textAlign: "center"}}>Get Cost Estimate</Text>
          </Body>
          <Right>
            <Button onPress={Actions.pop.bind(this)} transparent={true}>
              <Icon name={"close"}/>
            </Button>
          </Right>
        </Header>
        <Content style={styles.content} ref={(ref) => this.content = ref}>
          <Text style={[styles.description, {marginTop: 20, marginBottom: 15}]}>
            Our team will research your estimated costs and payment options for this provider and service. After
            contacting the provider, we will contact you using the information in your profile.
          </Text>

          <Text style={styles.label}>
            Add a note
          </Text>
          <TextInput
            value={this.state.query}
            multiline={true}
            numberOfLines={6}
            onFocus={(event) => setTimeout(() => this.content._root.scrollToEnd(), 500)}
            onChangeText={this.onQuestionInput}
            placeholderTextColor={Colors.textSuggestion}
            style={styles.input}
          />

          <Text style={[styles.label, {marginBottom: 10}]}>
            Preferred Contact Method
          </Text>
          <Row>
            <Picker
              placeholder="Select Option"
              placeholderStyle={{color: Colors.textSuggestion}}
              textStyle={styles.pickerText}
              selectedValue={this.state.selectedOption}
              style={styles.pickerStyle}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({
                  selectedOption: itemValue,
                });
              }}
              mode="dropdown"
            >
              {
                this.props.options &&
                this.props.options.map((value) => {
                  const key = Object.keys(value)[0];
                  return (
                    <Picker.Item
                      key={key}
                      label={value[key]}
                      value={key}
                    />);
                })
              }
            </Picker>
            <Icon
              name={"caretdown"} type={"AntDesign"}
              style={{color: Colors.black, position: "absolute", fontSize: 14, right: 10, alignSelf: "center"}}
            />
          </Row>


          <Button full={true} onPress={this.onPress} info={true} style={styles.button}>
            <Text>Send Request</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  private onQuestionInput = (text) => {
    this.setState({
      query: text,
    });
  };

  private onPress = () => {

    this.props.costEstimate({
      note: this.state.query,
      method: this.state.selectedOption,
      provider: this.props.provider,
      serviceId: this.props.serviceId,
      onSuccess: () => {
        this.setState(INITIAL_STATE);
        Alert.alert("Thank You !!", "Your request has been sent successfully");
      },
    });
  };
}

const mapStateToProps = (state: RootState): StateProps => ({
  options: state.help.contactMethods,
});

const mapDispatchToProps = (dispatch: Function): DispatchProps => ({
  costEstimate: (params: CostEstimateRequestParam) => dispatch(PatientAPIActions.costEstimateRequest(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CostEstimateScreen);
