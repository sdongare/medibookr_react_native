import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  button: {
    marginTop: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    alignSelf: "center",
  },
  buttonText: {
    color: Colors.white,
  },
  text: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.medium,
    color: Colors.textDark,
  },
  textItalic: {
    marginTop: Metrics.doubleBaseMargin,
    fontFamily: Fonts.type.emphasis,
    fontSize: Fonts.size.medium,
    fontWeight: "bold",
    color: Colors.textDark,
  },

});
