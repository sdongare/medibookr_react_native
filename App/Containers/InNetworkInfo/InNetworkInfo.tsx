import * as React from "react";
import {Text} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./InNetworkInfoStyle";
import {Body, Button, Container, Content, Header} from "@codler/native-base";
import {Actions} from "react-native-router-flux";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {

}

type Props = StateProps & DispatchProps & OwnProps;

class InNetworkInfo extends React.Component<Props, State> {
  public state = {};

  public render() {
    return (
      <Container>
        <Header>
          <Body>
          <Text>In-Network vs. Out-of-Network</Text>
          </Body>
        </Header>
        <Content padder={true}>
          <Text style={styles.text}>
            {"Some physicians, hospitals, and other providers are not a part of your insurance plan network. Those providers are deemed \"Out-of-Network\" " +
            "and treatment by them may result in higher costs for your employer and yourself. \"In-Network\" providers are typically a lower cost option " +
            "if paying through insurance.\n" +
            "\n\n" +
            "MediBookr utilizes extensive resource to help you choose the right provider, however \"In-network\" and \"Out-of-Network\" status can " +
            "change suddenly. It is your responsibility to verify how a provider processes and charges for its services. Please, always double check " +
            "with a provider to verify their network status prior to receiving care.\n" +
            "\n"}
          </Text>
          <Text style={styles.textItalic}>{
            "MediBookr shall not be liable for any losses, costs, damages, expenses, fees or uncovered charges as a result of using this application or " +
            "receiving or not receiving care from a listed provider.\n"}</Text>

          <Button info={true} onPress={Actions.pop.bind(this)} style={styles.button}>
            <Text style={styles.buttonText}>Got It!</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(InNetworkInfo) as React.ComponentClass<OwnProps>;
