import * as React from "react";
import {Text, View} from "react-native";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./RegistrationProcess2ScreenStyle";
import {Button, Form, Input, Item, Label, Left, Right} from "@codler/native-base";
import {Colors} from "@root/App/Themes";
import * as R from "ramda";
import {PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import {formatNumber} from "@root/App/Transforms/StringsTranform";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  handlePage: (page: number) => void
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  updateAddress: (params: ProfileAddressParams) => void
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  profile: PatientsInfo;
}

/**
 * The local state
 */
export interface State {
}

export interface ProfileAddressParams {
  phone?: string
  address?: string
  state?: string
  zipcode?: string
  city?: string
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>

class RegistrationProcess2Screen extends React.Component<Props, ProfileAddressParams> {
  public state = {
    zipcode: "",
    city: "",
    phone: "",
    address: "",
    state: "",
  };

  public pickFromProfile = (props) => {
    return R.pickAll(["zipcode", "address", "state", "city", "phone"], props.profile || {
      zipcode: "",
      address: "",
      state: "",
      city: "",
      phone: "",
    });
  };

  public componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
    // this.setState(this.pickFromProfile(nextProps));
  }

  public componentDidMount(): void {
    // this.setState(this.pickFromProfile(this.props));
  }

  public onPhone = (text) => {
    this.setState({phone: text});
  };

  public onAddress = (text) => {
    this.setState({address: text});
  };

  public onCity = (text) => {
    this.setState({city: text});
  };

  public onState = (text) => {
    this.setState({state: text});
  };

  public onZip = (text) => {
    this.setState({zipcode: text});
  };

  public render() {
    const nextDisable = R.any(R.isEmpty, R.values(this.state)) ||
      (this.state.phone && R.replace(/[^0-9]/g, "", this.state.phone).length !== 10);
    return (
      <View style={styles.container}>
        <Text style={styles.header}>
          Please provide your Address and Contact information.
        </Text>
        <Text style={styles.text}>
          We need your address details so that we can suggest providers based on
          your location.
        </Text>

        <Form>
          <Item stackedLabel={true}>
            <Label>Phone Number</Label>
            <Input maxLength={14} keyboardType={"phone-pad"} onChangeText={this.onPhone} value={formatNumber(this.state.phone)}/>
          </Item>
          <Item stackedLabel={true}>
            <Label>Address</Label>
            <Input onChangeText={this.onAddress} value={this.state.address}/>
          </Item>
          <Item stackedLabel={true}>
            <Label>City</Label>
            <Input onChangeText={this.onCity} value={this.state.city}/>
          </Item>
          <Item stackedLabel={true}>
            <Label>State</Label>
            <Input
              onChangeText={this.onState}
              autoCapitalize={"characters"}
              maxLength={2}
              value={this.state.state}
            />
          </Item>
          <Item stackedLabel={true}>
            <Label>Zip Code</Label>
            <Input
              onChangeText={this.onZip}
              keyboardType={"numeric"}
              maxLength={9}
              value={this.state.zipcode}
            />
          </Item>
          <Item bordered={false} style={{borderBottomWidth: 0, marginTop: 20}}>
            <Left>
              <Button
                onPress={() => this.props.handlePage(0)}
                rounded={false}
                style={{padding: 5}}
                info={true}
                small={true}
              >
                <Text style={{color: Colors.white}}>Back</Text>
              </Button>
            </Left>
            <Right>
              <Button
                disabled={nextDisable}
                onPress={() => {
                  if (R.not(R.equals(this.pickFromProfile(this.props), this.state))) {
                    this.props.updateAddress({...this.state, phone: R.replace(/[^\d]/, "", this.state.phone)});
                  }

                  this.props.handlePage(2);
                }}
                rounded={false}
                style={[{padding: 5}, nextDisable ? {backgroundColor: Colors.grey} : null]}
                info={true}
                small={true}
              >
                <Text style={{color: Colors.white}}>Next</Text>
              </Button>
            </Right>
          </Item>
        </Form>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  updateAddress: (params) => dispatch(PatientAPIActions.patientUpdateRequest(params)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    profile: state.patient && state.patient.info,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegistrationProcess2Screen) as React.ComponentClass<OwnProps>;
