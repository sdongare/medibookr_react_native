import { StyleSheet } from "react-native"
import { Colors, Fonts, Metrics } from "../../Themes/"

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginBottom: Metrics.doubleBaseMargin
  },
  header: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.opensansBold,
    color: Colors.textDark,
    textAlign: "center",
    marginVertical: Metrics.baseMargin
  },
  text: {
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.medium,
    textAlign: "center",
    color: Colors.textDark,
    marginVertical: Metrics.baseMargin
  },
  button: {
    alignSelf: "center",
    marginTop: Metrics.baseMargin
  },
  icon: {
    fontSize: Fonts.size.medium,
    marginRight: Metrics.baseMargin,
    color: Colors.primary
  },
  navBtns: {
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    flexDirection: "row"
  }
})
