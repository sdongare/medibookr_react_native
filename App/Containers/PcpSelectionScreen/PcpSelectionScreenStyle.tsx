import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginBottom: Metrics.doubleBaseMargin,
  },
  header: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.opensansBold,
    color: Colors.textDark,
    textAlign: "center",
    marginVertical: Metrics.baseMargin,
    justifyContent: "center",
  },
  headingAlt: {
    marginBottom: 10,
    justifyContent: "center",
  },
  searchBox: {
    flex: 1,
  },
  searchList: {
    flex: 1,
    borderWidth: 1,
    borderColor: "lightgray",
    marginBottom: Metrics.doubleBaseMargin,
    padding: Metrics.baseMargin,
  },
  searchListHeading: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.openSansSemiBold,
    color: Colors.textGrey,
    textAlign: "left",
  },
  cardContainer: {
    flexDirection: "row",
  },
  cardHeader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  cardBody: {
    flex: 2,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  cardFooter: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  cardBodyTextHeading: {
    flex: 1,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.openSansSemiBold,
    flexWrap: "wrap",
  },
  cardBodyTextHeadingAlt: {
    flex: 2,
  },
  cardBodyText: {
    flex: 1,
    fontSize: Fonts.size.tiny,
    fontFamily: Fonts.type.opensans,
    flexWrap: "wrap",
  },
  navBtns: {
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  button: {
    alignSelf: "center",
    backgroundColor: Colors.iconColor,
    marginTop: Metrics.doubleSection,
  },
});
