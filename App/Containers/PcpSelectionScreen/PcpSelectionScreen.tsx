import * as React from "react";
import {ActivityIndicator, Alert, FlatList, Text, View} from "react-native";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./PcpSelectionScreenStyle";
import {Body, Card, CardItem, Container, Form, Input, Item, Thumbnail} from "@codler/native-base";
import SearchByNameResponse, {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {getGenderImage} from "@root/App/Lib/AppHelper";
import {getZipCode} from "@root/App/Transforms/StringsTranform";
import {onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, tap} from "rxjs/operators";
import {DashboardActions} from "@root/App/Reducers/DashboardReducers";
import {RegistrationProcessActions} from "@root/App/Reducers/RegistrationProcessReducers";
import Header from "@root/App/Components/Header";
import * as R from "ramda";
import {Metrics} from "@root/App/Themes";
import {Actions} from "react-native-router-flux";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestNames: (name: string) => void;
  changePCP: (pcp: string, onSuccess: () => void) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  loading: boolean;
  results: ProviderResultsItem[];
}

/**
 * The local state
 */
export interface State {
  searchText: string;
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>;

class PcpSelectionScreen extends React.Component<Props, State> {

  public state = {
    searchText: "",
  };

  private keyword$ = new Subject<string>();

  constructor(props: Props) {
    super(props);

    onErrorResumeNext(
      this.keyword$.pipe(
        tap((value) => this.setState({searchText: value})),
        debounceTime(300),
        filter((value) => value.length > 2),
        distinctUntilChanged(),
      ),
    ).subscribe((value) => this.props.requestNames(value));
  }

  public renderRow = ({item}) => {
    const data = item as ProviderResultsItem;
    return (
      <Card>
        <CardItem
          button={true}
          onPress={() => {
            this.props.changePCP(data.providerId, () => {
              Alert.alert("Success", "Your PCP has been updated successfully", [
                {
                  text: "OK",
                  onPress: () => Actions.pop(),
                },
              ], {cancelable: false});
            });
          }}
        >
          <Body style={styles.cardContainer}>
            <View style={styles.cardHeader}>
              <Thumbnail
                square={true}
                source={getGenderImage(data)}
              />
            </View>
            <View style={styles.cardBody}>
              <Text
                style={styles.cardBodyTextHeading}>{data.providerName}{data.facilityFlag ? "" : ", " + data.credentials}</Text>
              <Text style={styles.cardBodyText}>{data.providerAddressLine1}</Text>
              {!!data.providerAddressLine2 &&
              <Text style={styles.cardBodyText}>{data.providerAddressLine2}</Text>
              }
              <Text
                style={styles.cardBodyText}>{`${data.providerCity}, ${data.providerState} ${getZipCode(data.providerZipcode)}`}</Text>
            </View>
            <View style={styles.cardFooter}>
              <Text style={styles.cardBodyTextHeading}>{data.providerCity}, {data.providerState}</Text>
              <Text style={styles.cardBodyText}>Distance: {data.distance.toFixed(2)} Miles</Text>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  };

  public render() {
    return (
      <Container style={styles.container}>
        <Header hideMenu={true}/>
        <Text style={styles.header}>
          Search For Your Primary Care Physician
        </Text>
        <Form style={{marginBottom: 10, padding: Metrics.baseMargin}}>
          <Item regular={true}>
            <Input
              style={{backgroundColor: "#F5F5F5"}}
              placeholder="Search by name..."
              value={this.state.searchText}
              onChangeText={(text) => this.keyword$.next(text)}
            />
            {this.props.loading &&
            <ActivityIndicator size={"small"} style={{position: "absolute", right: 10, alignSelf: "center"}}/>}
          </Item>
        </Form>
        <FlatList
          style={styles.searchList}
          data={this.props.results}
          renderItem={this.renderRow}
        />
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  requestNames: (name) => dispatch(DashboardActions.requestByName(name)),
  changePCP: (pcp, onSuccess) => dispatch(RegistrationProcessActions.updatePCP(pcp, {
    onSuccess,
  })),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    loading: state.dashboard.nameFetching,
    results: R.pathOr([], ["dashboard", "nameResult", "results"], state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PcpSelectionScreen) as React.ComponentClass<OwnProps>;
