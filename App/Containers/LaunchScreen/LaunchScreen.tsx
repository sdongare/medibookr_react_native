import React, {Component} from "react";
import {Text, View} from "react-native";
import styles from "./LaunchScreenStyles";

export default class LaunchScreen extends Component {
  public render() {
    return (
      <View style={styles.centerContainer}>
        <Text>Launch Screen</Text>
      </View>
    );
  }
}
