import React from "react";
import {Button, Container, Text} from "@codler/native-base";
import {RootState} from "@root/App/Reducers";
import {connect} from "react-redux";
import {FlatList, Image, View} from "react-native";
import styles from "./PharmacyStage3ScreenStyle";
import Divider from "@root/App/Components/Divider";
import Header from "@root/App/Components/Header";
import {NavigationScreenProps} from "react-navigation";
import * as R from "ramda";
import {DrugInfoRequest} from "@root/App/Reducers/GetHelpReducers";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";
import CouponHelper, {CouponIdParameter, NUMBER} from "@root/App/Containers/PharmacyScreenStage3/CouponHelper";
import {couponPriceAddress} from "@root/App/Transforms/StringsTranform";
import RNPrint from "react-native-print";
import {Colors} from "@root/App/Themes";
import {getPharmacyStoreImage} from "@root/App/Lib/PharmacyHelper";


/**
 * The properties passed to the component
 */
export interface OwnProps {
  info: Map<string, string>;
  requestInfo: DrugInfoRequest;
  couponInfo: CouponIdParameter;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
}

/**
 * The local state
 */
export interface State {
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<OwnProps>;

class PharmacyScreenStage3 extends React.Component<Props, State> {

  public renderItem = ({item}) => {
    return (
      <View>
        <Text style={styles.subHeader}>{item}</Text>
        <Text style={styles.description}>{this.props.info[item]}</Text>
      </View>
    )
  };

  public print = () => {
    RNPrint.print({
      html: CouponHelper({
        CouponId: this.props.couponInfo,
        number: NUMBER,
        request: this.props.requestInfo,
      }),
    }).then(response => {
    }).catch(err => console.log(err));
  };


  public render() {
    const logo = getPharmacyStoreImage(this.props.requestInfo.PriceResponse.Name);
    return (
      <Container>
        <Header/>
        <FlatList
          contentContainerStyle={{padding: 20}}
          ListHeaderComponent={<View>
            <Text style={styles.header}>
              Compare Pharmacies
            </Text>
            <Divider style={styles.divider}/>

            <Text style={[styles.description, {color: Colors.danger, marginBottom: 10}]}>
              These prices are direct/self pay rates and are not applicable if using your insurance. Please compare with
              your insurance rate if applicable before choosing to pay with options below.
            </Text>

            <Text style={styles.description}>
              Print the coupon below and take it to your local pharmacy to get the lowest prices in your area.
            </Text>

            <View style={styles.cContainer}>
              <Text style={styles.cTextHeader}>{parseAmount(this.props.requestInfo.PriceResponse.Price[0].Price.toFixed(2))}
              </Text>
              <View style={styles.padder}>
                <View style={{justifyContent: "space-between", flexDirection: "row"}}>
                  <View>
                    {
                      !!logo ?
                        <Image style={{height: 40, width: 150}} source={logo} resizeMode={"contain"}/>
                        :
                        <Text>{this.props.requestInfo.PriceResponse.Name}</Text>
                    }

                    <Text style={styles.cDistance}>Nearest: {this.props.requestInfo.PriceResponse.Distance.toFixed(1)}
                      <Text style={styles.cMiles}> miles</Text></Text>
                    <Text
                      style={styles.cAddress}
                    >{couponPriceAddress(this.props.requestInfo.PriceResponse.Address)}
                    </Text>
                  </View>
                  <Text
                    style={styles.cBinNumber}
                  >ID {this.props.couponInfo.cardId + "\n"}GRP {this.props.couponInfo.groupId + "\n"}BIN {this.props.couponInfo.binNumber + "\n"}
                    PCN {this.props.couponInfo.pcn + "\n"}CVC {this.props.couponInfo.cvc}
                  </Text>
                </View>

                <Text
                  style={styles.cInfo}
                >{this.props.requestInfo.DrugItem.DisplayName} ({this.props.requestInfo.ConsumptionType}, {this.props.requestInfo.Dosage},
                  {this.props.requestInfo.NDCItem.Quantity})
                </Text>
                <Text style={styles.cInquiry}>This is not insurance.{"\n"}Questions? Call us: {NUMBER}</Text>
              </View>
            </View>

            <Button onPress={this.print} info={true} full={true} style={styles.button}>
              <Text style={styles.textButton}>Print Coupon</Text>
            </Button>

            <Text style={styles.title}>Information about {this.props.requestInfo.DrugItem.DisplayName}</Text>
          </View>
          }
          data={R.keys(this.props.info)}
          renderItem={this.renderItem}
        />
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Function): DispatchProps => ({});

const mapStateToProps = (state: RootState): StateProps => {
  return ({})
};

export default connect(mapStateToProps, mapDispatchToProps)(PharmacyScreenStage3);
