import {DrugInfoRequest} from "@root/App/Reducers/GetHelpReducers";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";
import {couponPriceAddress} from "@root/App/Transforms/StringsTranform";

export const NUMBER = "1 (855) 279-9027";

export interface CouponHtmlParameter {
  CouponId: CouponIdParameter;
  request: DrugInfoRequest;
  number: string;
}

export interface CouponIdParameter {
  cardId: string;
  groupId: string;
  binNumber: string;
  pcn: string;
  cvc: string;
}

export default (params: CouponHtmlParameter) => {
  return `<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <title></title>
  <style>
    .pharmacy-stage-three {
      display: flex;
      flex-direction: column;
      margin-top: 30px;
    }

    .pharmacy-stage-three h3 {
      margin: auto;
    }

    .pharmacy-stage-three .price {
      flex: 0.5;
      align-items: center;
      justify-content: center;
    }

    .pharmacy-stage-three .price h2 {
      font-family: Lato;
      font-size: 28px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: -0.19px;
      text-align: right;
      color: #000;
    }

    .pharmacy-stage-three .stage-three-card {
      display: flex;
      flex-direction: column;
      border: 3px solid #019fc6;
      border-top: 30px solid #019fc6;
    }

    .pharmacy-stage-three .stage-three-card .pharmacy-card {
      width: 100%;
      height: 90px;
      display: flex;
    }

    .pharmacy-stage-three .stage-three-card .pharmacy-card .section {
      flex: 1;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
    }

    .pharmacy-stage-three .stage-three-card .pharmacy-card .section img {
      height: 40px;
    }

    .pharmacy-stage-three .stage-three-card .pharmacy-card .section p, .pharmacy-stage-three .stage-three-card .pharmacy-card .section strong {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      font-size: 24px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #4a4a4a;
      margin: 0;
    }

    .pharmacy-stage-three .stage-three-card .pharmacy-card .section span {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      font-size: 12px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: 0.21px;
      color: #4a4a4a;
    }

    .pharmacy-stage-three .stage-three-card .pharmacy-card .section:nth-child(2) {
      flex: 2;
      align-items: flex-start;
      justify-content: center;
    }

    .pharmacy-stage-three .stage-three-card-details {
      display: flex;
      height: auto;
      width: 100%;
      justify-content: space-between;
      align-items: center;
    }

    .pharmacy-stage-three .stage-three-card-details .stage-three-title {
      flex: 2;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .pharmacy-stage-three .stage-three-card-details .stage-three-card-info {
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 0;
      margin-top: 5px;
    }

    .pharmacy-stage-three .stage-three-card-footer {
      display: flex;
      justify-content: space-around;
      align-items: center;
      height: 40px;
      width: 100%;
    }

    .pharmacy-stage-three .stage-three-card-footer p {
      font-weight: bold;
      text-align: center;
    }

    .pharmacy-stage-three .stage-three-card-footer p:first-child {
      flex: 3;
    }

    .pharmacy-stage-three .stage-three-card-footer p:last-child {
      flex: 0.5;
    }

    .pharmacy-stage-three button {
      width: 169px;
      height: 38px;
      margin-top: 50px;
      border-radius: 2px;
      box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
      background: #019fc6;
      font-family: Lato;
      font-size: 22px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
      color: #fff;
      border: none;
      margin: 30px auto;
    }

    .pharmacy-stage-three .stage-three-info {
      display: flex;
      flex-direction: column;
    }

    .pharmacy-stage-three .stage-three-info h2 {
      margin: 0;
      margin-top: 20px;
    }

    .pharmacy-stage-three .stage-three-info p {
      margin: 0;
      margin-top: 10px;
      margin-bottom: 10px;
    }

    footer {
      display: none;
    }
  </style>
</head>

<body style="padding: 20px;">

<div class="pharmacy-stage-three">
  <div class="stage-three-card">
    <div class="pharmacy-card">
      <div class="section" style="padding: 20px;">
        <h3>${params.request.PriceResponse.Name}</h3>
      </div>
      <div class="section">
        <p><strong>Nearest: ${params.request.PriceResponse.Distance.toFixed(1)}</strong> miles</p>
        ${couponPriceAddress(params.request.PriceResponse.Address)}
      </div>
      <div class="section price">
        <h2>
          ${parseAmount(params.request.PriceResponse.Price[0].Price.toFixed(2))}
        </h2>
      </div>
    </div>
    <div class="stage-three-card-details">
      <div class="stage-three-title">
        <h3 style="padding: 20px;">${params.request.DrugItem.DisplayName} (${params.request.ConsumptionType}, ${params.request.Dosage},
          ${params.request.NDCItem.Quantity})</h3>
      </div>
      <div class="stage-three-card-info">
        <p>ID ${params.CouponId.cardId}</p>
        <p>GRP ${params.CouponId.groupId}</p>
        <p>BIN ${params.CouponId.binNumber}</p>
        <p>PCN ${params.CouponId.pcn}</p>
        <p>CVC ${params.CouponId.cvc}</p>
      </div>
    </div>
    <div class="stage-three-card-footer">
      <p>Questions? Call us: ${params.number + "\n"}This is not insurance.</p>
    </div>
  </div>
</div>
</body>

</html>`;
}
