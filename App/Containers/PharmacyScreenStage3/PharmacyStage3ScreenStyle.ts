import {StyleSheet,} from "react-native";
import Fonts from "@root/App/Themes/Fonts";
import {ApplicationStyles, Colors} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  header: {
    fontFamily: Fonts.type.latoBold,
    fontSize: 20,
    color: Colors.textDark,
  },
  title: {
    fontFamily: Fonts.type.latoBold,
    fontWeight: "bold",
    fontSize: 20,
    marginTop: 20,
    color: Colors.textDark,
  },
  subHeader: {
    fontFamily: Fonts.type.latoBold,
    fontSize: 18,
    marginVertical: 15,
    color: Colors.textDark,
  },
  description: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 16,
    color: Colors.textDark,
  },
  padder: {
    padding: 10,
  },
  textButton: {color: Colors.white, fontSize: 22, fontFamily: Fonts.type.opensansBold},
  button: {...ApplicationStyles.shadow, marginHorizontal: 20},
  divider: {
    marginTop: 0,
  },
  cTextHeader: {
    alignSelf: "stretch",
    textAlign: "right",
    backgroundColor: Colors.primaryLight,
    padding: 5,
    color: Colors.white,
    fontSize: 24,
    fontFamily: Fonts.type.latoBold,
  },
  cContainer: {
    borderWidth: 2,
    borderColor: Colors.primaryLight,
    marginVertical: 10,
  },

  cDistance: {
    color: Colors.textGrey,
    fontSize: 16,
    fontFamily: Fonts.type.opensansBold,
  },
  cMiles: {
    color: Colors.textGrey,
    fontSize: 15,
    fontFamily: Fonts.type.opensans,
  },
  cAddress: {
    color: Colors.textGrey,
    fontSize: 14,
    fontFamily: Fonts.type.opensans,
  },
  cInfo: {
    color: Colors.textGrey,
    fontSize: 22,
    fontFamily: Fonts.type.opensansBold,
    marginVertical: 20,
    textAlign: "center",
  },
  cBinNumber: {
    color: Colors.textGrey,
    fontSize: 14,
    fontFamily: Fonts.type.opensans,
  },
  cInquiry: {
    color: Colors.textGrey,
    fontSize: 12,
    fontFamily: Fonts.type.opensansBold,
  },
});
