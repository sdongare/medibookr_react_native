import * as React from "react";
import {Alert, Image, ImageBackground, PermissionsAndroid, Platform, SafeAreaView, Text, View, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
import {Images} from "../../Themes";
// Styles
import styles from "./LocationConsentScreenStyle";
import {Button} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import {LoginActions} from "@root/App/Reducers/LoginReducers";
import {LatLng} from "react-native-maps";
import RNPermissions, {PERMISSIONS, RESULTS} from "react-native-permissions";
import Geolocation from "@react-native-community/geolocation";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  result: any;
  // modalVisible: () => void;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  setGpsLocation: (location: LatLng) => void;
  locationModalClose: () => void;
  oneTimeOpenLocationModal: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  locationResult: string;
  locationModalWindow: boolean;
}

/**
 * The local state
 */
export interface State {

}

type Props = StateProps & DispatchProps & OwnProps;

class LocationConsentScreen extends React.Component<Props, State> {
  public state = {}

  public back = () => {    
    this.props.locationModalClose();
    this.props.oneTimeOpenLocationModal();
  }

  public grantAccess = async () => {
    // alert(this.props.result)
    switch (this.props.locationResult) {
      case RESULTS.UNAVAILABLE:
        const resultUnAvailable = await RNPermissions.request(
          Platform.select({
            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
          }));
        // if (resultUnAvailable === RESULTS.GRANTED) {
          Geolocation.getCurrentPosition((position) => {
            this.props.setGpsLocation(position.coords);
            this.props.locationModalClose();
            this.props.oneTimeOpenLocationModal();
          });
        // }
        break;
      case RESULTS.DENIED:
        this.props.locationModalClose();
        const result = await RNPermissions.request(
          Platform.select({
            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
          }));
        // if (result === RESULTS.GRANTED) {
          Geolocation.getCurrentPosition((position) => {
            this.props.setGpsLocation(position.coords);
            this.props.locationModalClose();
            this.props.oneTimeOpenLocationModal();
          });
        // }
        break;
      case RESULTS.GRANTED:
        Geolocation.getCurrentPosition((position) => {
          this.props.setGpsLocation(position.coords);
          this.props.locationModalClose();
          this.props.oneTimeOpenLocationModal();
        });
        break;
      case RESULTS.BLOCKED:
        this.props.locationModalClose();
        RNPermissions.openSettings();
        break;
    }
    // Actions.pop();
    // this.props.modalVisible();
  }


  public render() {
    return (
      // <SafeAreaView style={styles.container}>
      //   <ImageBackground
      //     source={Images.Splash}
      //     resizeMode={"stretch"}
      //     style={styles.container}
      //   >
          <View style={styles.consentContainer}>
            <Text style={styles.textHeader}>Let us find the best providers near you</Text>
            <Image
              source={Images.LocationMarkerWhite}
              resizeMode={"contain"}
              style={styles.mapIcon}
            />
            <Text style={styles.text}>
              Granting permission to use your location allow us  to automaticallly search for providers near you.
            </Text>

            <View style={styles.buttonContainer}>
              <Button transparent={true} onPress={this.grantAccess} style={styles.textButtonContainer}>
                <Text style={styles.locationAccessButtonText}>Continue</Text>
              </Button>
            </View>  

             {/* <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={this.back} style={styles.laterTextButtonContainer}>
                <Text style={styles.locationDenyButton}>Don't Allow</Text>
              </TouchableOpacity>
            </View>                       */}
          </View>
      //   </ImageBackground>
      // </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  setGpsLocation: (location) => dispatch(LocationActions.gpsLocation(location)),
  locationModalClose: () => dispatch(LocationActions.locationModalClose()),
  oneTimeOpenLocationModal: () => dispatch(LoginActions.oneTimeOpenLocationModal()),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    locationResult: state.location && state.location.locationResult,
    locationModalWindow: state.location && state.location.locationModalWindow,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationConsentScreen) as React.ComponentClass<OwnProps>;
