import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: "flex-end",
  },
  icon: {
    color: Colors.textDark,
    marginRight: 0,
  },
  text: {
    color: Colors.white,
    fontFamily: Fonts.type.opensans,
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    paddingHorizontal: 15
  },
  logo: {
    height: 100,
    marginVertical: 20,
  },
  consentContainer: {
    backgroundColor: Colors.primary,
    alignItems: "center",
    // margin: 30,
    // marginBottom: 70,
    padding: 20,
    borderRadius: 15,
  },
  textHeader: {
    fontFamily: Fonts.type.opensansBold,
    fontSize: 16,
    textAlign: "center",
    color: Colors.white,
    lineHeight: 28,
    paddingHorizontal: 15
  },
  mapIcon: {
    marginVertical: 20,
    width: 96,
    height: 96,
    resizeMode: "stretch"
  },
  textButtonContainer: {
    paddingHorizontal: 50,
    alignSelf: "center",
    // marginVertical: 5,
    justifyContent: "center",
    backgroundColor: Colors.white,
    height: 26,
    alignItems: "center",
    borderRadius: 5
  },
  laterTextButtonContainer: {
    paddingHorizontal: 50,
    alignSelf: "center",
    // marginVertical: 5,
    justifyContent: "center",
    backgroundColor: Colors.primary,
    height: 26,
    alignItems: "center",
  },
  locationAccessButtonText: {
    fontFamily: Fonts.type.opensans,
    fontSize: 12,
    textAlign: "center",
    color: Colors.primary,
    fontWeight: "600"
  },
  locationDenyButton: {
    fontFamily: Fonts.type.opensans,
    fontSize: 14,
    color: Colors.white,
    textDecorationLine: 'underline',
  },
  buttonContainer:{
    flexDirection: "row",
    paddingTop: 20
  }
});
