import { StyleSheet } from "react-native";
import { Colors, Metrics } from "../../Themes/";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  totalContainer: {
    backgroundColor: Colors.primary,
    justifyContent: "center",
    alignItems: "center",
    margin: 20,
    padding: 20,
  },
  scrollContentContainer: {
    paddingBottom: 20,
  },
  contentContainer: {
    backgroundColor: Colors.searchBg,
    alignItems: "center",
    margin: 20,
    padding: 10,
  },
  textTotal: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.regular,
    color: Colors.white,
  },
  text: {
    color: Colors.textSuggestion,
    fontFamily: Fonts.type.openSansSemiBold,
    fontSize: Fonts.size.medium,
  },
  textWhite: {
    color: Colors.white,
    fontFamily: Fonts.type.openSansSemiBold,
    fontSize: Fonts.size.medium,
  },
});
