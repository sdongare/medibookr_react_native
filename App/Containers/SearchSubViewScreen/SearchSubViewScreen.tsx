import * as React from "react";
import {ActivityIndicator, ImageBackground, ScrollView, Text, View} from "react-native";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
import {Colors, Fonts, Images} from "../../Themes";
// Styles
import styles from "./SearchSubViewScreenStyle";
import {Button, Container, ListItem} from "@codler/native-base";
import {DashboardActions, DashboardSuccessParams} from "@root/App/Reducers/DashboardReducers";
import * as R from "ramda";
import Header from "@root/App/Components/Header/Header";
import SearchBar from "@root/App/Components/SearchBar";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import KeywordAutoSuggestBox from "@root/App/Components/KeywordAutoSuggestBox/KeywordAutoSuggestBox";
import Metrics from "@root/App/Themes/Metrics";
import {Actions} from "react-native-router-flux";
import {SearchType} from "@root/App/Containers/SearchScreen/SearchScreen";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  data: any;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  searchForSpeciality: (name: string) => void;
  keywordsCall: (request: string) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  keyword: string;
  searchedData: any;
  keywordFetch: boolean;
}

/**
 * The local state
 */
export interface State {

}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>;

class SearchSubViewScreen extends React.Component<Props, State> {

  public state = {
    searchedData: [],
  };

  public renderEmpty = () => {
    return (<Text style={[styles.text, {margin: 10}]}>No results found for your search please try with different
      keyword</Text>);
  };

  public getData = () => {
    if (this.isString()) {
      return this.state.searchedData;
    } else {
      return this.props.data;
    }
  }

  componentWillReceiveProps(nextProps: any): void {
    if (this.isString() && nextProps.searchedData && this.state.searchedData.length === 0) {
      this.setState({searchedData: nextProps.searchedData})
    }
  }

  public replaceX = (text, keyword) => {
    if (R.contains(keyword, text)) {
      const splitted = R.split(keyword, text)
      return splitted.map(((value, index) => {
        return [
          index !== 0 ?
            <Text
              key={index}
              style={[styles.text, {
                textDecorationLine: "underline",
                fontFamily: Fonts.type.opensansBold,
              }]}
            >{keyword}
            </Text> : null,
          value
        ];
      }))
    } else {
      return text;
    }
  }

  public renderSpecialty = (item) => {
    const dataResult = this.getData().speciality.results[item];
    const count = R.compose(
      R.reduce(R.add, 0),
      R.values,
      R.map(R.prop("providerCount")),
      R.values,
      R.prop("specialties"),
    )(dataResult);
    return (
      <ListItem
        noIndent={true}
        button={true}
        onPress={() => {
          this.props.searchForSpeciality(item);
        }}
      >
        <Text style={styles.text}>{this.replaceX(item, this.props.keyword)}</Text>
      </ListItem>
    )
  };

  public renderData = (data) => {
    const keyword = this.props.keyword;
    const provider = R.pathOr(0, ["provider", "count"], data);
    const speciality = R.pathOr(0, ["speciality", "count"], data);
    const services = R.pathOr(0, ["service", "count"], data);

    const total = (provider + services);

    return (
      <>
        <View style={[styles.contentContainer]}>
          <Text style={[styles.text, {marginVertical: 10}]}>Please select from the options below:</Text>
          <ScrollView contentContainerStyle={styles.scrollContentContainer}>
            <View>
              <ListItem
                button={true}
                noIndent={true}
                onPress={() => {
                  const results = R.path(["provider", "results"], data);
                  if (!results) {
                    return;
                  }
                  Actions.popTo("landing");
                  Actions.push("search", {
                    results: data.provider.results,
                    searchType: SearchType.Provider,
                    item: keyword,
                  });
                }}
              >
                <Text
                  style={styles.text}>{this.replaceX(`Providers with ${this.props.keyword} in their names`, this.props.keyword)}</Text>
              </ListItem>

              {
                !!services &&
                <ListItem
                  button={true}
                  noIndent={true}
                  onPress={() => {
                    Actions.popTo("landing");
                    Actions.push("picker", {list: data.service.results, listType: "service", keyword});
                  }}
                >
                  <Text
                    style={styles.text}>{this.replaceX(`Find procedures for ${this.props.keyword}`, this.props.keyword)}</Text>
                </ListItem>
              }

              {
                !!speciality &&
                <>
                  <Text style={[styles.text, {margin: 10}]}>Specialties found</Text>
                  {
                    R.compose(R.map(this.renderSpecialty), R.keys, R.path(["speciality", "results"]))(data)
                  }
                </>
              }
            </View>
          </ScrollView>
        </View>
      </>
    );
  };

  public isString = () => {
    return R.type(this.props.data) === "String"
  }

  componentDidMount(): void {
    if (this.isString()) {
      this.setState({
        term: this.props.keyword,
      })
      setTimeout(() => {
        this.searchbar.setSearchState(this.props.keyword);
        this.props.keywordsCall(this.props.keyword);
      }, 500)
    }
  }

  public render() {


    return (
      <Container>
        <Header/>
        <SearchBar ref={r => this.searchbar = r} style={ApplicationStyles.shadow}/>
        {
          !this.isString() &&
          <KeywordAutoSuggestBox/>
        }
        <ImageBackground style={styles.container} source={Images.Splash} resizeMode={"contain"}>
          {!!this.getData() ? this.renderData(this.getData()) : this.renderEmpty()}
        </ImageBackground>
        {
          this.props.keywordFetch &&
          <View style={{
            flex: 1,
            width: "100%",
            height: "100%",
            justifyContent: "center",
            zIndex: 1000,
            position: "absolute",
            alignItems: "center",
            opacity: 0.7,
            backgroundColor: Colors.black,
          }}
          >
            <ActivityIndicator
              size={"large"}
            />
          </View>
        }

      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  searchForSpeciality: (name) => dispatch(DashboardActions.requestSpecialtyByKeyword(name)),
  keywordsCall: (request) => dispatch(DashboardActions.request(request)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    searchedData: state.dashboard.data,
    keyword: state.dashboard.term,
    keywordFetch: state.dashboard.fetching,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchSubViewScreen) as React.ComponentClass<OwnProps>;
