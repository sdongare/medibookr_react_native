import {
  StyleSheet,
} from "react-native";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import {Colors, Metrics} from "../../Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  content: {
    flex: 1,
    padding: 20,
  },
  drugResultContainer: {
    backgroundColor: Colors.searchBg,
    position: "absolute",
    alignSelf: "stretch",
    maxHeight: 200,
    ...ApplicationStyles.shadow,
    zIndex: 100,
  },
  drugNameTextContainer: {
    flexDirection: "row", alignSelf: "stretch", flex: 1,
  },
  indicator: {
    position: "absolute",
    top: 43,
    right: Metrics.smallMargin,
    alignSelf: "center",
  },
  textButton: {
    color: Colors.white,
  },
  header: {
    fontFamily: Fonts.type.latoBold,
    fontSize: 20,
    color: Colors.textDark,
  },
  description: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 16,
    color: Colors.textDark,
    marginTop: 10,
  },
  divider: {
    marginVertical: 0,
  },
  button: {
    ...ApplicationStyles.shadow,
  },
  label: {
    marginTop: 12,
    fontFamily: Fonts.type.latoBold,
    fontSize: 16,
    color: Colors.textDark,
  },
  input: {
    ...ApplicationStyles.inputText,
    backgroundColor: Colors.searchBg,
    marginTop: 30,
    flex: 1,
  },
  pickerText: {
    width: "100%",
  },
  pickerStyle: {
    marginTop: 4,
    flex: 1,
    borderRadius: 3,
    borderColor: Colors.border,
    borderWidth: 1,
  },
});
