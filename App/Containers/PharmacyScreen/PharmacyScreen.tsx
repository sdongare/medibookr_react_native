import React from "react";
import {ActivityIndicator, Alert, LayoutChangeEvent, LayoutRectangle, ScrollView, TextInput, View, Platform} from "react-native";
import Header from "@root/App/Components/Header/Header";
import styles from "./PharmacyScreenStyle";
import {Button, Container, Content, ListItem, Text} from "@codler/native-base";
import {Colors} from "@root/App/Themes";
import Divider from "@root/App/Components/Divider/Divider";
import {RootState} from "@root/App/Reducers";
import {connect} from "react-redux";
import {GetHelpActions} from "@root/App/Reducers/GetHelpReducers";
import {onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, distinctUntilChanged, filter} from "rxjs/operators";
import {Actions} from "react-native-router-flux";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import * as R from "ramda";
import TooltipComponenet from "@root/App/Components/TooltipComponenet";
import AsyncStorage from "@react-native-community/async-storage";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestDrugName: (text: string) => void;
  requestZipCode: () => void;
  clearDrugRequest: () => void;
  requestInfoMessage: (key: string) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  drugName?: DrugResponse[] | undefined;
  zipCode: string;
  fetchingDrug: boolean;
  infoMessage: InfoMessage | undefined;
}

/**
 * The local state
 */
export interface State {
  drugSearchText: string;
  drugItem: DrugResponse;
  locationText: string;
  layout?: LayoutRectangle;
  toolTipVisible: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

const INITIAL_STATE = {
  drugSearchText: "",
  locationText: "",
  layout: null,
  drugItem: null,
  toolTipVisible: false
};

class PharmacyScreen extends React.Component<Props, State> {

  private keyword$ = new Subject<string>();
  private scroll: any;

  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;

    onErrorResumeNext(
      this.keyword$.pipe(
        debounceTime(300),
        filter((value) => value.length > 2),
        distinctUntilChanged(),
      ),
    ).subscribe((value) => this.props.requestDrugName(value));

  }

  public componentDidMount = async () => {
    this.props.requestZipCode();
    var pharmacyTooltipView = await AsyncStorage.getItem('pharmacyTooltipView');
    if(!pharmacyTooltipView)
    {
      this.props.requestInfoMessage("compare-pharmacies");
      setTimeout(()=> this.setState({ toolTipVisible: true }), 1000) 
      AsyncStorage.setItem('pharmacyTooltipView', 'view');     
    }   
  }

  public componentWillReceiveProps(nextProps: Readonly<StateProps & DispatchProps & OwnProps>, nextContext: any): void {
    if (this.props.zipCode !== nextProps.zipCode) {
      this.setState({locationText: nextProps.zipCode})
    }
  }

  public _renderDrugItem = (value) => (
    <ListItem
      noIndent={true}
      onPress={() => {
        this.setState({drugSearchText: value.DisplayName, drugItem: value});
        this.props.clearDrugRequest();
      }}
    >
      <Text>{value.DisplayName}</Text>
    </ListItem>
  )

  public render() {
    var tooltipContent = [];
    if(this.state.toolTipVisible && this.props.infoMessage && Object.keys(this.props.infoMessage).length > 0 && this.props.infoMessage.key == "compare-pharmacies")
    {
      tooltipContent.push(
        <TooltipComponenet 
          position={"top"}
          heading={this.props.infoMessage.title}
          description={this.props.infoMessage.message}
          style={{ top: Platform.OS == "ios" ? "3.5%" : "4%" }}
        /> 
      )
    }
    return (
      <Container>
        <Header/>
        <Content style={styles.content} ref={(ref) => this.scroll = ref}>
          <Text style={styles.header}>Compare Pharmacies</Text>
          <Divider style={styles.divider}/>
          <Text style={[styles.description, {color: Colors.danger, marginBottom: 10}]}>
            These prices are direct/self pay rates and are not applicable if using your insurance. Please compare with
            your insurance rate if applicable before choosing to pay with options below.
          </Text>
          <Text style={styles.description}>
            Fill your prescription at the lowest cost and most convenient location.
            {"\n\n"}
            MediBookr’s discount prescription service helps you find the best prices near you for your prescription.
          </Text>
          {tooltipContent}
          <View style={styles.drugNameTextContainer} onLayout={this.onSearchTextLayout}>            
            <TextInput
              value={this.state.drugSearchText}
              onChangeText={this.onDrugSearchTextChange}
              placeholderTextColor={Colors.textSuggestion}
              style={[styles.input, {flex: 1}]}
              placeholder="Drug Name"
            />
            {this.props.fetchingDrug && <ActivityIndicator style={styles.indicator} size={"small"}/>}
          </View>

          {
            this.state.layout && !!this.props.drugName && this.props.drugName.length > 0 &&
            <ScrollView style={[styles.drugResultContainer, {top: this.state.layout.y + this.state.layout.height + 5}]}>
              {R.map(this._renderDrugItem)(this.props.drugName)}
            </ScrollView>
          }   
          <TextInput
            value={this.state.locationText.length > 0 ? "Zip code: " + this.state.locationText : ""}
            keyboardType={"numeric"}
            maxLength={15}
            onChangeText={this.onLocationChange}
            placeholderTextColor={Colors.textSuggestion}
            style={styles.input}
            placeholder="Enter Zip code"
          />          
          <View
            style={{alignContent: "center", marginHorizontal: 50, marginTop: 30, paddingBottom: 500}}
          >
            <Button onPress={this.onPress} info={true} full={true} style={styles.button}>
              <Text style={styles.textButton}>Search</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }

  private onDrugSearchTextChange = (text) => {
    this.setState({drugSearchText: text, drugItem: null});
    this.keyword$.next(text);
  };

  private onLocationChange = (text) => {
    this.setState({locationText: text.replace("Zip code: ", "")})
  };

  private onSearchTextLayout = ({nativeEvent: {layout}}: LayoutChangeEvent) => this.setState({layout});

  private onPress = () => {
    if (!this.state.drugItem) {
      Alert.alert("Drug not selected", "Please select drug from the search results.");
    } else {
      Actions.push("stage2", {item: this.state.drugItem, zipCode: this.state.locationText})
    }
  };
}

const mapDispatchToProps = (dispatch: Function): DispatchProps => ({
  requestDrugName: (text) => dispatch(GetHelpActions.requestDrugName(text)),
  clearDrugRequest: () => dispatch(GetHelpActions.clearDrugName()),
  requestZipCode: () => dispatch(LocationActions.requestZipCode()),
  requestInfoMessage: (key) => dispatch(GetHelpActions.requestInfoMessage(key)),
});

const mapStateToProps = (state: RootState): StateProps => ({
  drugName: state.help.drugNames,
  fetchingDrug: state.help.fetchingDrugName,
  zipCode: state.location.zipCodeForDrugs,
  infoMessage: state.help && state.help.infoMessage,
});

export default connect(mapStateToProps, mapDispatchToProps)(PharmacyScreen);
