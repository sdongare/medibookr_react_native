import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.searchBg,
  },
  toastContainer: {
    padding: Metrics.baseMargin,
    backgroundColor: Colors.black,
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
  },
  toastText: {
    flex: 1,
    color: Colors.white,
    fontFamily: Fonts.type.opensans,
    fontSize: 12
  },
  toastDissmissText: {
    color: Colors.white,
    fontFamily: Fonts.type.opensans,
    fontSize: 12,
    textDecorationLine: "underline"
  },
  toastIcon: {
    width: 24,
    height: 24,
    resizeMode: "cover"
  },
  breadcrumbContainer: {
    flexDirection: "row", padding: 10, alignItems: "center",
  },
  searchBar: {
    flex: 1,
  },
  map: {
    ...ApplicationStyles.shadow,
    flex: 1,
    height: 200,
  },
  filterButton: {
    padding: Metrics.marginVertical,
    alignSelf: "center",
  },
  list: {
    paddingHorizontal: Metrics.baseMargin - 2,
  },
  noResult: {
    flex: 1,
    flexDirection: "row",
    padding: Metrics.section,
    justifyContent: "center",
    alignItems: "center",
  },
  headerText: {
    flex: 1,
    fontSize: Fonts.size.medium - 1,
    color: Colors.textHeader,
  },
  iconBack: {
    padding: 5,
    color: Colors.textHeader,
  },
  filterIcon: {
    marginLeft: 0,
    marginRight: 0,
    color: Colors.textDark,
    fontSize: 38,
  },
  searchContainer: {
    ...ApplicationStyles.shadow,
    flexDirection: "row",
    backgroundColor: Colors.searchBg,
    alignContent: "center",
  },
  loadMoreFooter: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderTopWidth: 1.5,
    borderTopColor: 'black'
  },
  loadMoreButton: {
    marginTop:30,
    padding: 10,
    backgroundColor: Colors.primary,
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  loadMoreText: {
    color: Colors.white,
    fontSize: 15,
    textAlign: 'center',
    fontFamily: Fonts.type.opensans
  },
  modalContainer: { 
    backgroundColor: Colors.white,
    position: "absolute",
    borderRadius: 15,
    top: "15%",
    width: "100%",
    padding: 15,
    paddingVertical: 30
  },
  modalView: {
    flexDirection: "row",
    justifyContent: "center",
  },
  modalMessage: { 
    textAlign: "center",
    fontSize: 22,
    fontFamily: Fonts.type.opensans,
    lineHeight: 32,
    color: "#000000"
  },
});
