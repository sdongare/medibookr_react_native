import * as React from "react";
import {Animated, Easing, EmitterSubscription, FlatList, Keyboard, Text, TouchableOpacity, View, Image} from "react-native";
// Styles
import styles from "./SearchScreenStyle";
import Header from "@root/App/Components/Header/Header";
import {Button, Container, Icon} from "@codler/native-base";
import SearchBar from "@root/App/Components/SearchBar/SearchBar";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import SearchItem from "@root/App/Components/SearchItem/SearchItem";
import Filter, {IFilterState, INITIAL_STATE} from "@root/App/Components/Filter/Filter";
import * as R from "ramda";
import KeywordAutoSuggestBox from "@root/App/Components/KeywordAutoSuggestBox/KeywordAutoSuggestBox";
import ClusterMapView from "@root/App/Components/ClusterMapView/ClusterMapView";
import {notEmptyAndNull} from "@root/App/Lib/AppHelper";
import {Actions} from "react-native-router-flux";
import {ApplicationStyles, Metrics, Images} from "@root/App/Themes";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";
import DraggableHandle from "@root/App/Components/DraggableHandle";
import * as Redux from "redux";
import {ProviderActions} from "@root/App/Reducers/ProviderReducers";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";
import NoSearchResultModal from "@root/App/Components/NoSearchResultModal";
import Modal from 'react-native-modal';
import FastImage from "react-native-fast-image";

/**
 * The properties passed to the component
 */

export enum SearchType {
  Provider,
  Specialty,
  Procedure,
}

export interface OwnProps {
  results: ProviderResultsItem[];
  item: string;
  searchType: SearchType;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  term: string;
}

interface IDispatch {
  callProviderById?: (id: string) => void;
}

/**
 * The local state
 */
export interface State {
  filtered: ProviderResultsItem[];
  filteredMap: ProviderResultsItem[];
  filterOptions: IFilterState;
  mapLoading: boolean;
  headerHeight: number;
  searchHeight: number;
  showMessage: boolean;
  heightMap: number;
  heightStaticImage: number;
  posY: number;
  filterX: Animated.Value;
  filterVisibility: boolean;
  keyboardHeight: number;
  loadMore: number;
  modalVisible: boolean;
}

type Props = StateProps & DispatchProps & OwnProps & IDispatch;

function _InitialState() {
  return INITIAL_STATE;
}

class SearchScreen extends React.Component<Props, State> {
  private flatList;
  private readonly specialties: string[];
  private readonly insuranceRange: number[];
  private readonly cashRange: number[];
  private map: any;
  private keyboardDidShowListener: EmitterSubscription;
  private keyboardDidHideListener: EmitterSubscription;

  constructor(props: Props) {
    super(props);

    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this.keyboardDidHide);

    this.state = {
      filtered: null,
      filteredMap: null,
      filterOptions: null,
      mapLoading: false,
      headerHeight: 0,
      searchHeight: 0,
      showMessage: true,
      posY: 0,
      filterVisibility: false,
      heightMap: 0,
      filterX: new Animated.Value(Metrics.screenWidth),
      heightStaticImage: 0,
      keyboardHeight: 0,
      loadMore: 0,
      modalVisible: true
    };

    const oooPrices =
      R.compose(
        R.filter((value) => value != null && value != 0),
        R.map((R.prop("oopPrice"))),
      )(props.results);

    if (oooPrices.length >= 2) {
      const max = R.reduce(R.max, -Infinity, oooPrices);
      const min = R.reduce(R.min, Infinity, oooPrices);

      const diff = max - min;
      const rangeStep = (start, step, stop) => R.map(
        (n) => start + step * n,
        R.range(0, (1 + (stop - start) / step) >>> 0),
      );
      const bucket = diff / 10;

      this.insuranceRange = R.map(Math.round, rangeStep(min, bucket, max));
    } else {
      this.insuranceRange = [0]
    }

    const cashPrices =
      R.compose(
        R.filter((value) => value != null && value != 0),
        R.map((R.prop("cashPrice"))),
      )(props.results);

    if (cashPrices.length >= 2) {
      const max = R.reduce(R.max, -Infinity, cashPrices);
      const min = R.reduce(R.min, Infinity, cashPrices);

      const diff = max - min;
      const rangeStep = (start, step, stop) => R.map(
        (n) => start + step * n,
        R.range(0, (1 + (stop - start) / step) >>> 0),
      );
      const bucket = diff / 10;

      this.cashRange = R.map(Math.round, rangeStep(min, bucket, max));
    }

    this.specialties =
      R.compose(
        R.prepend("All Specialties"),
        R.uniq,
        R.filter(R.compose(R.not, R.isEmpty, R.trim)),
        R.reject(R.isNil),
        R.map(R.prop("specialty")),
      )(props.results);
  }


  public animateFilter = (slideIn: boolean) => {
    this.setState({filterVisibility: slideIn}, () => {
      Animated.timing(this.state.filterX, {
        toValue: slideIn ? 0 : Metrics.screenWidth,
        duration: 500,
        easing: Easing.ease,
      }).start(() => {
      });
    })
  };

  public getFiltered = (options: IFilterState) => {
    const filtered = R.map(
      (value) => {
        if (this.insuranceRange && parseFloat(value.oopPrice) >= 0) {
          let oopPrice;
          let dollarSigns;

          const first = R.findLast(R.lte(R.__, parseFloat(value.oopPrice)))(this.insuranceRange);
          const second = R.find(R.gte(R.__, parseFloat(value.oopPrice)))(this.insuranceRange);

          if (value.oopPrice == 0) {
            dollarSigns = 0;
            oopPrice = "$0";
          } else if (first === second) {
            if (R.indexOf(first, this.insuranceRange) === 0) {
              dollarSigns = 1;
              oopPrice = parseAmount(first) + " - " + parseAmount(this.insuranceRange[1]);
            } else {
              dollarSigns = 5;
              oopPrice = parseAmount(this.insuranceRange[this.insuranceRange.length - 2])
                + " - " + parseAmount(second);
            }
          } else {
            const i = R.indexOf(second, this.insuranceRange);
            dollarSigns = i / 2;
            oopPrice = parseAmount(first) + " - " + parseAmount(second);
          }

          dollarSigns = !dollarSigns ? 1 : dollarSigns;
          return R.merge(value, {insDollarSigns: dollarSigns, oopPriceText: oopPrice});
        } else if (this.cashRange && value.cashPrice) {
          let dollarSigns;

          const first = R.findLast(R.lte(R.__, parseFloat(value.cashPrice)))(this.cashRange);
          const second = R.find(R.gte(R.__, parseFloat(value.cashPrice)))(this.cashRange);

          if (first === second) {
            dollarSigns = R.indexOf(first, this.cashRange) === 0 ? 1 : 5;
          } else {
            const i = R.indexOf(second, this.cashRange);
            dollarSigns = i / 2;
          }

          dollarSigns = !dollarSigns ? 1 : dollarSigns;
          return R.merge(value, {cashDollarSigns: dollarSigns});
        } else {
          return value;
        }
      },
    )(this.props.results)
      .filter((value) => options.bestValue ? value.preferredFlag : true)
      .filter((value) => options.unrated ? true : parseInt(value.rating) > 0)
      .filter((value) => options.rating > 0 ? parseInt(value.rating) >= options.rating : true)
      .filter((value) => options.cashPrice > 0 ? value.cashDollarSigns <= options.cashPrice : true)
      .filter((value) => options.insurancePrice > 0 ? value.insDollarSigns <= options.insurancePrice : true)
      .filter((value) => {
        if (options.male && options.female) {
          return value.gender === "M" || value.gender === "F";
        } else if (options.male) {
          return value.gender === "M";
        } else if (options.female) {
          return value.gender === "F";
        } else {
          return true;
        }
      }) // Check Gender
      .filter((value) => {
        // Age Filter
        const age = parseInt(value.age, 10);
        if (age) {
          return age >= options.age.selectedMin && age <= options.age.selectedMax;
        } else {
          return true;
        }
      })
      .filter(((value) => options.specialty > 0 ? R.equals(this.specialties[options.specialty], value.specialty) : true))
      .filter((value) => value.distance <= options.distance.selected) // Distance Filter
      .filter((value) => options.facilityName ? R.contains(R.toLower(options.facilityName))(R.toLower(value.providerName)) : true) // Facility Filter
      .filter((value) => options.outOfNetwork ? true : !!value.inNetwork);

    const oopPriceSort = (provider) => {
      if (parseFloat(provider.oopPrice) > 0) {
        return provider.oopPrice;
      } else {
        return Infinity;
      }
    }    
    return R.pipe(
      R.cond(
        [
          [
            R.always(options.sort === 0 && this.props.searchType === SearchType.Provider),
            R.sortWith([
              R.ascend(R.prop("scoreValue")),
              R.ascend(oopPriceSort),
              R.ascend(R.prop("distance")),
              R.ascend(R.prop("providerName")),
            ]),
          ],
          [
            R.always(options.sort === 0 && this.props.searchType !== SearchType.Provider),
            R.sortWith([
              R.ascend(oopPriceSort),
              R.ascend(R.prop("distance")),
              R.ascend(R.prop("providerName")),
            ]),
          ],                   
          [
            R.always(this.props.searchType !== SearchType.Provider),
            R.sortWith([
              R.ascend(R.prop("preferredFlag")),
              R.ascend(oopPriceSort),
              R.ascend(R.prop("distance")),
              R.ascend(R.prop("providerName")),
            ]),
          ],
          [R.T, R.identity],
        ],
      ),     
      R.cond(
        [
          [
            R.always(options.sort !== 4),
            R.sortBy((a: ProviderResultsItem) => {
              const rating = a.rating.length ? a.rating : "0";
              switch (options.sort) {
                case 0:
                  return a.copay;
                case 1:
                  return !a.preferredFlag;
                case 2:
                  return a.distance;
                case 3:
                  return a.providerName;                
                case 5:
                  return parseFloat(rating);
                case 6:
                  return 5 - parseFloat(rating);   
              }
            }),        
          ],
          [
            R.always(options.sort === 4),
            R.sortWith([
              R.descend(R.prop("providerName")),
            ]),
          ],
        ]
      ),      
    )(filtered);
  }

  public componentWillUnmount(): void {
    this.keyboardDidHideListener.remove();
    this.keyboardDidShowListener.remove();
  }

  public componentDidMount() {
    const filterOptions = _InitialState();
    /*Answers.logSearch(this.props.term, {
      searchType: this.props.searchType === SearchType.Provider ? "Name" : this.props.searchType === SearchType.Specialty ? "Speciality" : "Services",
      searchAction: "Completed",
    });*/

    const filtered = this.getFiltered(filterOptions);
    // @ts-ignore
    this.setState({filtered, filteredMap: filtered, filterOptions, mapLoading: false, loadMore: 1, modalVisible: false });
  }

  public filterButtonClick = () => {
    this.animateFilter(!this.state.filterVisibility);
    Keyboard.dismiss();
  }

  public reset = () => this.applyFilter(_InitialState());

  public applyFilter = (options: IFilterState) => {
    const filtered = this.getFiltered(options);
    // @ts-ignore
    this.setState({filtered, filteredMap: filtered, filterOptions: options});
    this.animateFilter(false);
    if (this.map) {
      this.map.setThisState(filtered);
    }
  }

  public setLoader = () => {
    this.setState({
      mapLoading: true,
    });
  };

  public onNewData = (data) => {
    this.setState({
      filteredMap: data,
      mapLoading: false,
    });
  };

  public getBreadCrumbHeaderText() {
    // @ts-ignore
    // const lastLevel = convertToBreadCrumb(this.props.item);
    // const lastLevelText = lastLevel ? ` for \"${lastLevel}\"` : "";

    /*const breadCrumbs = `${propType === "Provider" ? "" : capitalizeOrNull(propType)}${propType === "Provider"
      ? `"${this.props.item}"` : " >> " + convertToBreadCrumb(this.props.item)}`;*/
    let text;
    const isOutOfNetwork = this.state.filterOptions.outOfNetwork;
    const textNetwork = `In-Network${isOutOfNetwork ? " and Out-of-Network" : ""}`;

    switch (this.props.searchType) {
      case SearchType.Provider:
      case SearchType.Specialty:
        text = textNetwork + " results for \"" + this.props.item + "\"";
        break;
      default:
        text = textNetwork + " results for \"" + this.props.term + "\"\n" + this.props.item;
        break;
    }

    return text;
  }


  public render() {  
    let loadMoreView = []
    if (this.state.filteredMap) {
      if(this.state.filteredMap.length > 0)
      {
        if(this.state.filteredMap.length >= 3 && this.state.loadMore == 1)
        {
          loadMoreView.push(...R.uniqBy(R.prop("providerId"))(this.state.filteredMap).splice(0, 3))
        }      
        else {
          loadMoreView.push(...R.uniqBy(R.prop("providerId"))(this.state.filteredMap))
        } 
      }      
    }
    return (
      <Container style={styles.container}>
        <View onLayout={(event) => this.setState({headerHeight: event.nativeEvent.layout.height})}>
          <Header/>
        </View>

        <View
          style={[styles.searchContainer]}
          onLayout={(event) => this.setState({searchHeight: event.nativeEvent.layout.height})}
        >
          <SearchBar style={styles.searchBar} keyboardOpen={true}/>
          <Button style={styles.filterButton} transparent={true} icon={true} onPress={this.filterButtonClick}>
            <Icon style={styles.filterIcon} name="tune" type="MaterialIcons"/>
          </Button>
        </View>
        <KeywordAutoSuggestBox/>
        {
          notEmptyAndNull(this.state.filtered) && !this.state.modalVisible &&
          <React.Fragment>
            {
              this.props.term !== "telehealth" &&
              <View
                onLayout={(event) => this.setState({posY: event.nativeEvent.layout.y})}>
                <DraggableHandle
                  text={"Map View"}
                  isAccess={true}
                  newDesign={true}
                  posY={this.state.posY}
                  onHeightChanged={(height) => {
                    if (height > 0) {
                      this.setState({heightMap: height});
                    } else {
                      this.setState({heightMap: undefined});
                    }
                  }}
                  view={
                    <ClusterMapView
                      ref={(r) => this.map = r}
                      style={[styles.map, {height: this.state.heightMap}]}
                      onMarkerPress={(item) => {
                        this.setState({filteredMap: R.sortBy((value) => item.providerId !== value.providerId)(this.state.filteredMap)});
                        this.flatList.scrollToOffset({offset: 0, animated: false});
                      }}
                      data={R.filter(R.compose(notEmptyAndNull, R.prop("latitude")))(this.state.filtered)}
                      setLoader={this.setLoader}
                      suggestedHeight={Metrics.screenHeight - this.state.posY - 300}
                      onGetNewData={this.onNewData}
                    />}
                  maxHeight={Metrics.screenHeight - this.state.posY - 300}
                />
              </View>
            }


            <FlatList
              ref={(ref) => this.flatList = ref}
              contentContainerStyle={styles.list}
              ListHeaderComponent={(props, context) => (<View style={styles.breadcrumbContainer}>
                <Text style={styles.headerText}>{this.getBreadCrumbHeaderText()}</Text>
              </View>)}
              ListFooterComponent={(props, context) => {
                if(this.state.filteredMap.length >= 3 && this.state.loadMore == 1)
                {
                  return (
                    <View style={styles.loadMoreFooter}>
                      <TouchableOpacity activeOpacity={0.9} style ={styles.loadMoreButton}
                      onPress={()=> this.setState({ loadMore: 2 })} >
                          <Text style={styles.loadMoreText}>
                            Load More
                          </Text>                      
                      </TouchableOpacity>                    
                  </View>
                  )
                }
                else {
                  return (<View></View>)
                }
              }}
              maxToRenderPerBatch={5}
              initialNumToRender={5}
              updateCellsBatchingPeriod={100}
              windowSize={10}
              keyExtractor={this.keyExtractor}
              data={loadMoreView}
              renderItem={this.renderItem}
            />
          </React.Fragment>
        }

        {
          <Animated.View
            style={{
              transform: [{translateX: this.state.filterX}],
              width: "100%", height: "100%", position: "absolute",
              zIndex: 5, elevation: 5,
            }}
          >
            <TouchableOpacity
              style={{width: "100%", height: "100%", position: "absolute"}}
              onPress={this.filterButtonClick}
            >
              <View style={{width: "100%", height: "100%"}}/>
            </TouchableOpacity>
            <Filter
              style={{
                top: this.state.headerHeight+80, ...ApplicationStyles.shadow,
                // maxHeight: Metrics.screenHeight - this.state.headerHeight - this.state.keyboardHeight - 10,
              }}
              state={this.state.filterOptions}
              onDone={this.applyFilter}
              specialties={this.specialties}
              enableDollar={!!this.insuranceRange || !!this.cashRange}
              onReset={this.reset}
              onClose={this.filterButtonClick}
            />
          </Animated.View>
        }

        {!notEmptyAndNull(this.state.filtered) && !this.state.modalVisible &&
        <Text style={[styles.noResult]}>Please expand filter to get more results,
          No results found.</Text>
        }

        {!notEmptyAndNull(this.state.filtered) && !this.state.modalVisible &&
          <NoSearchResultModal />
        }

        {
          this.state.showMessage &&
          <View style={styles.toastContainer}>
            <Button onPress={this.info} transparent={true} style={{ paddingHorizontal: 8 }}>
              <Image source={Images.InformationCircle} style={styles.toastIcon} />
            </Button>
            <Text style={styles.toastText}>
              While this information is updated regularly, check with your health plan or provider to confirm network status and coverage.
            </Text>            
            <Button onPress={this.closeToast} transparent={true} style={{ paddingHorizontal: 8, justifyContent: "flex-end", alignItems: "flex-end" }}>
              <Text style={styles.toastDissmissText}>Dismiss</Text>
            </Button>            
          </View>
        }
        
        <Modal customBackdrop={<View style={{flex: 1, backgroundColor: "rgba(0,0,0,0.4)"}} />} isVisible={this.state.modalVisible}>          
          <View style={styles.modalContainer}>
            <View style={styles.modalView}>
              <FastImage
                source={Images.SpinnerBar}
                resizeMode={"cover"}
                style={{width: 192, height: 64}}
              />          
            </View>                    
            <View style={styles.modalView}>
              <Text style={styles.modalMessage}>
                Please wait…
              </Text>             
            </View>
            <View style={styles.modalView}>             
              <Text style={styles.modalMessage}>
                We are finding the best options for in-network care near your.
              </Text>
            </View>
          </View>
        </Modal>

      </Container>
    );
  }

  private info = () => {
    Actions.push("network-info");
  };
  private closeToast = () => {
    this.setState({showMessage: false});
  };

  private keyExtractor = (item, index) => index;

  private renderItem = ({item, index}) => {
    return (
      <SearchItem
        searchType={this.props.searchType}
        item={item}
        index={index.toString()}
      />);
  };

  private keyboardDidHide = () => {
    this.setState({keyboardHeight: 0})
  }

  private keyboardDidShow = (e) => {
    this.setState({keyboardHeight: e.endCoordinates.height})
  }
}

const mapStateToProps = (state: RootState): StateProps => ({
  term: state.dashboard.term,
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): IDispatch => ({
  callProviderById: (id) => dispatch(ProviderActions.requestById(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
