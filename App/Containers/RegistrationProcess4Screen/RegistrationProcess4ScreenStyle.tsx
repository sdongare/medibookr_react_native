import { StyleSheet } from "react-native"
import { Colors, Fonts, Metrics } from "../../Themes/"

export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: Metrics.doubleBaseMargin
  },
  button: {
    alignSelf: "center",
    backgroundColor: Colors.iconColor,
    marginTop: Metrics.doubleSection
  },
  header: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.opensansBold,
    color: Colors.textDark,
    textAlign: "center",
    marginVertical: Metrics.baseMargin
  },
  subHeader: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.opensansBold,
    color: Colors.textDark,
    textAlign: "center",
    marginVertical: Metrics.baseMargin
  },
  col: {
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0.8,
    borderColor: "lightgray"
  }
})
