import * as React from "react";
import {Text, View} from "react-native";
import {Col, Grid} from "@codler/native-base";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import styles from "./RegistrationProcess4ScreenStyle";
import * as R from "ramda";
import {PatientAPIActions} from "@root/App/Reducers/PatientsReducers";

/**
 * The properties passed to the component
 */
export interface OwnProps {
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  loadInsurance: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  insurance: Insurance;
}

/**
 * The local state
 */
export interface State {
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>

class RegistrationProcess4Screen extends React.Component<Props, State> {

  public componentDidMount(): void {
    this.props.loadInsurance();
  }

  public render() {
    if (R.path(["insurance", "insurerPlan"], this.props)) {
      return (
        <View style={styles.container}>
          <Text style={styles.header}>
            Registration Process Completed Successfully!
          </Text>
          <Text style={styles.subHeader}>Your Insurance Details</Text>
          <View style={styles.container}>
            <Grid>
              <Col style={styles.col}>
                <Text>Insurance Name:</Text>
              </Col>
              <Col style={styles.col}>
                <Text>{this.props.insurance.insurerPlan.name}</Text>
              </Col>
            </Grid>
            <Grid>
              <Col style={styles.col}>
                <Text>Group #:</Text>
              </Col>
              <Col style={styles.col}>
                <Text>{this.props.insurance.groupNumber}</Text>
              </Col>
            </Grid>
            <Grid>
              <Col style={styles.col}>
                <Text>Member ID:</Text>
              </Col>
              <Col style={styles.col}>
                <Text>{this.props.insurance.policyNumber}</Text>
              </Col>
            </Grid>
          </View>
        </View>
      );
    } else {
      return (<Text style={styles.header}>You do not have any insurance registered.</Text>);
    }

  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  loadInsurance: () => dispatch(PatientAPIActions.requestPatientInsurance()),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    insurance: state.patient.insurance,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationProcess4Screen) as React.ComponentClass<OwnProps>;
