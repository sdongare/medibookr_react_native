import * as React from "react";
import {Alert, Image, PermissionsAndroid, Platform, Text, View} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import styles from "./LandingScreenStyle";
import {RootState} from "@root/App/Reducers";
import SearchBar from "@root/App/Components/SearchBar/SearchBar";
import {Images} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";
import {Container, Content} from "@codler/native-base";
import Header from "@root/App/Components/Header/Header";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import KeywordAutoSuggestBox from "@root/App/Components/KeywordAutoSuggestBox/KeywordAutoSuggestBox";
import {LatLng} from "react-native-maps";
import Geolocation from '@react-native-community/geolocation';


/**
 * The properties passed to the component
 */
export interface OwnProps {
  style?: any;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  setGpsLocation: (location: LatLng) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  name?: string;
}

/**
 * The local state
 */
export interface State {
}

type Props = StateProps & DispatchProps & OwnProps;

class LandingScreen extends React.Component<Props, State> {

  public componentDidMount() {
    if (Platform.OS === "ios") {
      Geolocation.requestAuthorization();
      Geolocation.getCurrentPosition((position) => {
        this.props.setGpsLocation(position.coords);
      });
    } else if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location Permission",
          message: "App needs to access your location",
        }).then(
        (response) => {
          if (response === PermissionsAndroid.RESULTS.GRANTED) {
            Geolocation.getCurrentPosition((position) => {
              this.props.setGpsLocation(position.coords);
            }, ((error) => {

            }), {
              enableHighAccuracy: false,
              timeout: 15000,
              maximumAge: 10000,
            });
          } else {
            Alert.alert(null, "Unable to get permission, Location permission is required to run this application");
          }
        }, (reason) => {
          Alert.alert(null, "Problem getting your location. Possible reason could be low gps signals. Auto location may not work");
        });

    }
  }

  public render() {
    return (
      <Container>
        <Header hideBack={true}/>
        <SearchBar style={ApplicationStyles.shadow}/>
        <KeywordAutoSuggestBox/>
        <Content style={{flex: 1}}>

          <View style={styles.billboardContainer}>
            <Image style={styles.imageBillboard} source={Images.Billboard} resizeMode="stretch"/>
            <Text style={[Fonts.style.h1, styles.welcomeText]}>Welcome Back, {this.props.name}</Text>
          </View>
          <View style={styles.container}/>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  setGpsLocation: (location) => dispatch(LocationActions.gpsLocation(location)),
});

const mapStateToProps = (state: RootState): StateProps => {
  return {
    name: state.patient.info && state.patient.info.fullName,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LandingScreen) as React.ComponentClass<OwnProps>;
