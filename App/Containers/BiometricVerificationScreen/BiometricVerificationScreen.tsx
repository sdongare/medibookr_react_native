import * as React from "react";
import {Image, TouchableOpacity, Platform, SafeAreaView, Text, View} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
import {Images} from "../../Themes";
// Styles
import styles from "./BiometricVerificationScreenStyle";
import {Button} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import AsyncStorage from "@react-native-community/async-storage";
import {DISABLE_DIALOG} from "@root/App/Lib/AppHelper/BiometricVerification";
import {LoginActions} from "@root/App/Reducers/LoginReducers";
import checkBiometricAuth, {BIOMETRIC_ENABLE_STATUS} from "@root/App/Lib/AppHelper/BiometricVerification";
import Modal from 'react-native-modal';
import * as Keychain from "react-native-keychain";
import {BiometricActions} from "@root/App/Reducers/BiometricReducers";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  // result: any;
  // reject: Function;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  toggleLoader: (value: boolean) => void;
  setBiometricEnableStatus: (value: boolean) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  biometricSupport: string;
}

/**
 * The local state
 */
export interface State {
  modalVisible: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class BiometricVerificationScreen extends React.Component<Props, State> {
  public state = {
    modalVisible: false
  }

  public componentDidMount = async () => {
    const hasSetup = await checkBiometricAuth()
    console.warn(hasSetup)
    if(hasSetup)
    {
      this.setState({ modalVisible: true })
    }
  }

  public render() {
    const biometricMessage = this.props.biometricSupport === "FaceID" ? "Face ID" :
      this.props.biometricSupport === "TouchID" ? Platform.select({
        android: "Biometric",
        ios: "Touch ID",
      }) : "Biometric";    
    return (
      <Modal isVisible={this.state.modalVisible} style={styles.modalContainer} animationIn={"zoomInDown"} animationOut="zoomOutUp">
        <View style={styles.modalPadding}>
          <View style={styles.consentContainer}>
            <Text style={styles.textHeader}>Login quickly and securely with Quick Login</Text>
            <Image
              source={Images.FingerPrint}
              resizeMode={"contain"}
              style={styles.mapIcon}
            />
            <Text style={styles.text}>
              Activating Quick Login allows our application to access the biometric scanner(s) in your device automating the logging in process.
            </Text>

            <View style={styles.buttonContainer}>
              <Button transparent={true} onPress={this.grantAccess} style={styles.textButtonContainer}>
                <Text style={styles.accessButtonText}>Allow and Enable Quick Login</Text>
              </Button>
            </View>  

            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={this.back} style={styles.laterTextButtonContainer}>
                <Text style={styles.denyButton}>Not Right Now</Text>
              </TouchableOpacity>
            </View>                      
          </View>
        </View>
      </Modal>
    )
  }

  private grantAccess = async () => {
    // this.isAllowed = true;
    // Actions.pop();    
    /*let biometricEmail = await AsyncStorage.getItem("biometricEmail");
    let biometricPassword = await AsyncStorage.getItem("biometricPassword");
    biometricEmail = biometricEmail ? biometricEmail : "";
    biometricPassword = biometricPassword ? biometricPassword : "";
    Keychain.setGenericPassword(biometricEmail, biometricPassword);*/
    
    AsyncStorage.setItem(BIOMETRIC_ENABLE_STATUS, "true")
    this.props.setBiometricEnableStatus(true)
    this.props.toggleLoader(true)
    this.setState({ modalVisible: false })
    AsyncStorage.setItem(DISABLE_DIALOG, "true").catch((error) => console.log(error));
  };

  private back = () => {
    // Actions.pop();
    AsyncStorage.setItem(DISABLE_DIALOG, "true").catch((error) => console.log(error));
    this.setState({ modalVisible: false })
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  toggleLoader: (value) => dispatch(LoginActions.toggleLoader(value)),
  setBiometricEnableStatus: (value) => dispatch(BiometricActions.setBiometricEnableStatus(true))
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    biometricSupport: state.biometric.biometricSupport,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BiometricVerificationScreen) as React.ComponentClass<OwnProps>;