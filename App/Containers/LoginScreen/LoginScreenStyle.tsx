import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import Fonts from "@root/App/Themes/Fonts";
import { F } from "ramda";

const height = 56;

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    resizeMode: "center",
    backgroundColor: Colors.white,
    padding: Metrics.baseMargin,
  },
  pickerStyle: {
    marginTop: 18,
    width: "100%",
    borderRadius: 3,
    backgroundColor: Colors.searchBg,
    borderColor: Colors.border,
    borderWidth: 1,
  },
  bodyContainer: {
    padding: Metrics.section,
    paddingTop: 60,
    flex: 1
  },
  backgroundBg: {
    width: "100%",
    height: "100%",
  },
  logo: { 
    width: 218.4,
    height: 56,
    resizeMode: "stretch"
  },
  logoContainer: {
    paddingTop: 150,
    alignItems: "center",
    justifyContent: "center",
  },
  pickerCarrot: {
    position: "absolute",
    right: 8,
    top: 24,
    fontSize: 14,
    color: Colors.textDark,
  },
  baseButton: {
    height: height,
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: Colors.white
  },
  buttonText: {
    textAlign: "center",
    color: Colors.blue,
    fontSize: 24,
    fontWeight: "600",
    fontFamily: Fonts.type.opensans
  },
  signInText: {
    textAlign: "center",
  },
  inputContainer: {
    flexDirection: "row",
    paddingTop: 15
  },
  // passwordContainer: {
  //   marginTop: 20,
  //   flexDirection: "row",
  // },
  passwordContainer: {
    position: "absolute",
    right: "5%",
    top: "65%",
    // height: height
  },
  passwordIcon : {
    fontSize: 20,
    color: "#595959"
  },
  textHeader: {
    fontFamily: Fonts.type.opensans,
    color: Colors.white,
    fontSize: 24,
    paddingLeft: 4
  },
  input: {
    ...ApplicationStyles.inputText,
    alignSelf: "stretch",
    height: height,
    borderRadius: 10,
    width: "100%",
    fontFamily: Fonts.type.opensans,
    paddingHorizontal: 15,
    fontSize: 16,
    color: Colors.black
  },
  errorFont: {
    position: "absolute",
    bottom: 8,
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.small,
    color: Colors.errorColor,
    left: 15
  },
  textRegister: {
    fontFamily: Fonts.type.latoBold,
    color: Colors.white,
    textAlign: "center",
    fontSize: Fonts.size.regular,
  },
  registerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 8,
    alignItems: "baseline",
  },
  registerButtonText: {
    fontFamily: Fonts.type.latoBold,
    color: Colors.white,
    fontSize: Fonts.size.regular,
    textDecorationLine: "underline",
  },
  containerHeader: {
    backgroundColor: Colors.primary,
    justifyContent: "center",
    alignItems: "center",
    height: 64,
  },
  text: {
    color: Colors.white,
  },
  alreadyRegisterContainer: {
    flexDirection: "row",
    paddingVertical: 20,
    justifyContent: "center",
    alignSelf: "center"
  },
  alreadyRegisterText: {
    fontFamily: Fonts.type.opensans,
    color: Colors.white,
    fontSize: 20,
  },
  fotgotText: {
    fontFamily: Fonts.type.opensans,
    color: Colors.white,
    fontSize: 14,
  },
  fingerPrintImage: {
    width: 32,
    height: 31
  }
});
