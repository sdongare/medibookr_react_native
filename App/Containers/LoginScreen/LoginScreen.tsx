import * as React from "react";
import {
  Alert,
  BackHandler,
  ImageBackground,
  Keyboard,
  PickerItem,
  Dimensions,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import styles from "./LoginScreenStyle";
import {RootState} from "@root/App/Reducers";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {Button, Container, Header, Picker, Text} from "@codler/native-base";
import {LoginActions, LoginRequestParams} from "@root/App/Reducers/LoginReducers";
import {Fonts, Images, Metrics} from "@root/App/Themes";
import {Actions} from "react-native-router-flux";
import Colors from "@root/App/Themes/Colors";
import Check from "@root/App/Components/Check/Check";
import AppConfig, {ENVIRONMENT} from "@root/App/Config/AppConfig";
import {Subject} from "rxjs";
import {buffer, debounceTime, filter} from "rxjs/operators";
import TouchID from "react-native-touch-id";
import * as Keychain from "react-native-keychain";
import AsyncStorage from "@react-native-community/async-storage";
import RNPermissions, {check, PERMISSIONS, RESULTS} from "react-native-permissions";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import {BiometricActions} from "@root/App/Reducers/BiometricReducers";
import FooterLogin from "@root/App/Components/FooterLogin";
import {BIOMETRIC_ENABLE_STATUS} from "@root/App/Lib/AppHelper/BiometricVerification";
import Toast from 'react-native-toast-message'

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  callLogin: (param: LoginRequestParams) => void;
  forgotPassword: (email) => void;
  checkLocation: () => void;
  checkBiometricSupport: (callback: Function) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  hasBiometric: boolean;
  biometricType?: string;
}

/**
 * The local state
 */
export interface State {
  showPassword: boolean;
  textViewPassword: string;
  password: string;
  email: string;
  isBioMetric: boolean;
  remember: boolean;
  showDevOption: boolean;
  selectedEnvironment: string;
  modalVisible: boolean;
  emailError: string;
  passwordError: string;
}

type Props = StateProps & DispatchProps & OwnProps;

export const RegexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class LoginScreen extends React.Component<Props, State> {
  public state = {
    textViewPassword: "View",
    showPassword: false,
    password: "",
    email: "",
    remember: true,
    showDevOption: false,
    isBioMetric: undefined,
    selectedEnvironment: Object.keys(ENVIRONMENT).find((e) => ENVIRONMENT[e] === AppConfig.environment),
    modalVisible: false,
    emailError: "",
    passwordError: ""
  };

  private devClickSubject$ = new Subject<any>();
  private backHandler: any;
  private inputRef: any;

  constructor(props) {
    super(props);
    this.devClickSubject$.pipe(
      buffer(this.devClickSubject$.pipe(debounceTime(350))),
      filter((clickArray) => clickArray.length > 5),
    ).subscribe((next) => {
      this.setState({
        showDevOption: true,
      });
    });
  }

  public modalVisible = () => {
    this.setState({ modalVisible: !this.state.modalVisible })
  }

  public handleBackPress = () => {
    if (Actions.currentScene === "login") {

      BackHandler.exitApp();
      return true;
    }
    return false;
  }

  public componentWillUnmount(): void {
    this.backHandler.remove()
  }

  public async componentDidMount() {
    const env = await AsyncStorage.getItem("env");
    if (env) {
      this.setState({selectedEnvironment: env});
      // @ts-ignore
      AppConfig.changeEnvironment(ENVIRONMENT[env]);
    }
    this.inputRef.setNativeProps({
        style: {
            fontFamily: Fonts.type.opensans,
        },
    });
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);


    /*if (__DEV__) {
      this.props.callLogin({
        email: "syed@medibookr.com",
        password: "demo123!",
        remember: false,
      });
    }*/

    const result = await RNPermissions.check(Platform.select({
      ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    }));

    if (result !== RESULTS.BLOCKED) {
      this.props.checkLocation();
    }

    let email;
    email = await AsyncStorage.getItem("email").catch((e) => {
    });

    this.setState({email});
    this.props.checkBiometricSupport(() => {
      this.fingerScan();
    });    
  }

  public onViewPasswordClick = () => {
    this.setState({
      textViewPassword: this.state.showPassword ? "View" : "Hide",
      showPassword: !this.state.showPassword,
    });
  };

  public onPasswordText = (text) => {
    this.setState({password: text});
  };

  public onEmailText = (text) => {
    this.setState({email: text});
  };


  public forgotPassword = () => {
    Actions.push("forgot-password");
/*
    if (this.state.email && RegexEmail.test(this.state.email.toLowerCase())) {
      this.props.forgotPassword(this.state.email);
    } else {
      Alert.alert("Invalid email address", "Please enter a valid email address.", [
        {text: "OK"},
      ]);
    }*/
  };

  public registerClick = () => {
    Actions.push("register");
  };

  public validateSignIn = () => {
    this.setState({ emailError: "", passwordError: "" })
    var validationOccured = true;
    if(!this.state.email)
    {
      this.setState({ emailError: 'Please provide valid email address.' })
      validationOccured = false
    }
    else if(!RegexEmail.test(this.state.email.toLowerCase()))
    {
      this.setState({ emailError: 'Please provide valid email address.' })
      validationOccured = false
    }
    if(!this.state.password)
    {
      this.setState({ passwordError: 'This field is required.' })
      validationOccured = false
    }
    return validationOccured;
  }

  public signInClick = () => {
    Toast.hide();
    const message = this.validateSignIn();
    if (message) {
      if (RegexEmail.test(this.state.email.toLowerCase()) && this.state.password.length > 4) {
        this.props.callLogin({email: this.state.email, password: this.state.password, remember: this.state.remember});
      } else {
        Alert.alert("Invalid email or password", "Please enter a valid email id and password.", [
          {text: "OK"},
        ]);
      }
    }    
  };


  public startScan = async () => {
    return TouchID.authenticate("Authorize to Login", {
      passcodeFallback: true,
    });
  };

  public fingerScan = async () => {

   // let hasCredentials;
 //   hasCredentials = await Keychain.getGenericPassword().catch((e) => console.log(e));
    const biometricEnablePersiststatus = await AsyncStorage.getItem(BIOMETRIC_ENABLE_STATUS);

    if (biometricEnablePersiststatus) {
      const success = await this.startScan().catch((e) => {
        Alert.alert("Error in Authentication", e.message || "Unable to authenticate this user!");
      });

      if (!success) {
        return;
      }

      const credentials = await Keychain.getGenericPassword().catch(e => console.log(e));
      if (credentials) {
        this.props.callLogin({
          email: credentials.username,
          password: credentials.password,
          remember: this.state.remember,
        });
      }

    }
  };

  public rememberMeClick = () => {
    this.setState({remember: !this.state.remember});
  };

  public devClick = () => {
    this.devClickSubject$.next("click");
  };

  public onDevSelected = (env) => {
    this.setState({selectedEnvironment: env, showDevOption: false});
    // @ts-ignore
    AppConfig.changeEnvironment(ENVIRONMENT[env]);
    AsyncStorage.setItem("env", env);
  };

  public onLayout = (event) => {
    const {x, y, height, width} = event.nativeEvent.layout;
    const newLayout = {
        height: height ,
        width: width,
        left: x,
        top: y,
      };
  }
  
  public render() {
    const biometricMessage = this.props.biometricType === "FaceID" ? "Face ID login" :
      this.props.biometricType === "TouchID" ? Platform.select({
        android: "Biometric login",
        ios: "Touch ID login",
      }) : "Biometric login";
    return (
      <Container>
         <ScrollView style={{ flex: 1 }} >          
            <ImageBackground source={Images.RegistrationBg} resizeMode={"stretch"} style={styles.backgroundBg}>
              <View style={styles.logoContainer}>
                <TouchableWithoutFeedback onPress={() => this.devClick()}>              
                  <Image source={Images.Logo} style={styles.logo} />
                </TouchableWithoutFeedback>
              </View>
              {
                  this.state.showDevOption &&
                    <View style={{height: 40, width: (Metrics.screenWidth - 2*Metrics.section), alignSelf: 'center'}}>
                      <Picker style={styles.pickerStyle} mode={"dropdown"} selectedValue={this.state.selectedEnvironment}
                              onValueChange={this.onDevSelected}>
                        {Object.keys(ENVIRONMENT).map((value, index) => (
                          <PickerItem label={value} key={index} value={value}/>))}
                      </Picker>
                    </View>
              }
              <View style={styles.bodyContainer}>
                <View style={{ flexDirection: "row" }}>
                  <View style={{ flex: 8 }}>
                    <Text style={styles.textHeader}>Login</Text>
                  </View>
                  <View style={{ flex: 2, alignItems: "flex-end" }}>
                    {
                    !!this.props.hasBiometric &&
                      <TouchableOpacity onPress={this.fingerScan}>
                        <Image source={Images.FingerPrint} style={styles.fingerPrintImage} />
                      </TouchableOpacity>
                    }
                  </View>
                </View>                
                <View style={[styles.inputContainer, { paddingTop: 20 }]}>
                  <TextInput
                    style={[styles.input, this.state.emailError ? { height: 65, paddingBottom: 20 } : { }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({email: text})}
                    textContentType="emailAddress"
                    placeholder="Preferred Email Address"
                  />
                  <Text style={styles.errorFont}>{this.state.emailError}</Text>
                </View>
                <View style={[styles.inputContainer, { paddingTop: 20 }]}>
                  <TextInput
                    ref={(ref) => (this.inputRef = ref)}
                    style={[styles.input, this.state.passwordError ? { height: 65, paddingBottom: 20 } : { }]}
                    placeholderTextColor={Colors.placeHolderSuggestion}
                    value={this.state.password}
                    onChangeText={(text) => this.setState({password: text})}
                    secureTextEntry={!this.state.showPassword}
                    placeholder="Enter your password"
                  />
                  {
                    <TouchableOpacity style={styles.passwordContainer} onPress={()=> this.setState({ showPassword: !this.state.showPassword })}>
                      <Icon
                        name={this.state.showPassword ? "eye-outline" : "eye-off-outline" }
                        style={styles.passwordIcon}
                      />   
                    </TouchableOpacity>
                  }
                  <Text style={styles.errorFont}>{this.state.passwordError}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <View style={{ flex: 5 }}> 
                    <Check text={"Remember me"} color={Colors.black} isCheckProp={this.state.remember}
                      onClick={this.rememberMeClick}
                      textStyle={styles.fotgotText}  
                    />
                  </View>
                  <View style={{ flex: 5, alignItems: "flex-end", paddingTop: 15 }}> 
                    <TouchableOpacity onPress={this.forgotPassword}>
                      <Text style={styles.fotgotText}>Forgot password?</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.alreadyRegisterContainer}>
                  <TouchableOpacity onPress={this.registerClick}>
                    <Text style={styles.alreadyRegisterText}>Need to register?</Text>
                  </TouchableOpacity>                  
                </View> 
                <View style={{ flex: 5, alignItems: "flex-end", paddingTop: 60 }}> 
                  <Button onPress={this.signInClick} full={true} info={true} style={styles.baseButton}>
                    <Text style={styles.buttonText}>Confirm</Text>
                  </Button>
                </View>
                <FooterLogin />
              </View>                 
            </ImageBackground>
        </ScrollView>

        {/* <SafeAreaView style={{backgroundColor: Colors.primary}}>
          <Header style={[styles.containerHeader]}>
            <Text style={styles.text}>Sign In To MediBookr</Text>
          </Header>
        </SafeAreaView>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <ImageBackground source={Images.LoginBg} resizeMode={"stretch"} style={styles.container}>

            <TextInput
              value={this.state.email}
              onChangeText={this.onEmailText}
              placeholderTextColor={Colors.textSuggestion}
              style={[styles.input, {marginTop: 80}]}
              placeholder="Email"
            />

            <View style={[styles.passwordContainer]}>
              <TextInput
                onChangeText={this.onPasswordText}
                value={this.state.password}
                placeholderTextColor={Colors.textSuggestion}
                secureTextEntry={!this.state.showPassword}
                style={[styles.input, {
                  marginRight: -10,
                  flex: 1,
                  borderBottomRightRadius: 0,
                  borderTopRightRadius: 0,
                  borderRightColor: "transparent",
                }]}
                placeholder="Password"
              />
              <Button success={true} style={[styles.baseButton, {width: 80}]} onPress={this.onViewPasswordClick}>
                <Text>{this.state.textViewPassword}</Text>
              </Button>
            </View>

            <View style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignSelf: "stretch",
              backgroundColor: "#dfdfdf",
              opacity: 0.5,
              marginTop: 10,
            }}>
              {!!this.props.hasBiometric &&
              <Button transparent={true} onPress={this.fingerScan}>
                <Icon name={"finger-print"}/>
                <Text>{biometricMessage}</Text>
              </Button>
              }

              <Check text={"Save Email"} color={Colors.primary} isCheckProp={this.state.remember}
                     onClick={this.rememberMeClick}/>
            </View>


            <Button success={true} style={[styles.baseButton, {alignSelf: "stretch", marginTop: 20}]}
                    onPress={this.signInClick}>
              <Text style={styles.signInText}>Sign In</Text>
            </Button>


            <TouchableOpacity onPress={this.forgotPassword}>
              <Text style={[styles.textRegister, {marginTop: 10, textDecorationLine: "underline"}]}>Forgot My
                Password</Text>
            </TouchableOpacity>

            <View style={styles.registerContainer}>
              <Text style={styles.textRegister}>Not a member? </Text>
              <TouchableOpacity onPress={this.registerClick}>
                <Text style={styles.registerButtonText}>Register</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.registerContainer}>
              <TouchableOpacity onPress={this.modalVisible}>
                <Text style={styles.registerButtonText}>Location Modal</Text>
              </TouchableOpacity>
            </View>
            {
              this.state.showDevOption &&
              <View style={{alignSelf: "stretch"}}>
                <Picker style={styles.pickerStyle} mode={"dropdown"} selectedValue={this.state.selectedEnvironment}
                        onValueChange={this.onDevSelected}>
                  {Object.keys(ENVIRONMENT).map((value, index) => (
                    <PickerItem label={value} key={index} value={value}/>))}
                </Picker>
                <Icon name={"caretdown"} type={"AntDesign"} style={styles.pickerCarrot}/>
              </View>
            }
            {
              !this.state.showDevOption &&
              <Text style={{marginTop: 10, flex: 1, alignSelf: "stretch"}} onPress={this.devClick}/>
            }
          </ImageBackground>
        </TouchableWithoutFeedback>
        
        <Modal isVisible={this.state.modalVisible} style={styles.modalContainer} animationIn={"zoomInDown"} animationOut="zoomOutUp">
          <View style={styles.modalPadding}>
            <LocationConsentScreen modalVisible={this.modalVisible}/>
          </View>
        </Modal> */}
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  callLogin: (param) => dispatch(LoginActions.request(param, false)),
  forgotPassword: (email) => dispatch(LoginActions.forgotPassword(email)),
  checkLocation: () => dispatch(LocationActions.checkLocationSetThroughGPS()),
  checkBiometricSupport: (callback) => dispatch(BiometricActions.checkBiometric(callback)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    hasBiometric: state.biometric.isBiometricEnable && state.biometric.biometricSupport,
    biometricType: state.biometric.biometricSupport,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
