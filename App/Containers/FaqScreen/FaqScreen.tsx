import * as React from "react";
import {Text} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
import {Colors, Fonts} from "../../Themes";
// Styles
import styles from "./FaqScreenStyle";
import {Body, Button, Container, Content, Icon, Left, Right, Header} from "@codler/native-base";
import {Actions} from "react-native-router-flux";

const faqs = {
  "General Question": [
    {
      title: "what-is-medibookr",
      question: "1. What is MediBookr?",
      answer: "MediBookr is a healthcare shopping platform with both web and mobile App that are free for employees to easily “Find, Compare, and Schedule” with the right healthcare providers for your care needs. We partner with your employer to provide these services at no cost to you, and we do not share any sensitive or personal data with your employer.",
    },
    {
      title: "how-can-medibookr-benefit-you",
      question: "2. How can MediBookr benefit you?",
      answer: "MediBookr is a cost-savings service that you can use to compare providers’ price and quality reviews in order to help you choose the best-value provider. Once a provider is chosen, the employee can then book an appointment directly through the app and save time. MediBookr also helps keep track of your benefits based on their real-time insurance data, so that deductibles, copays, and in-network/out-of-network status are readily available.",
    },
    {
      title: "how-do-I-reset-my-password",
      question: "3. How do I reset my password?",
      answer: "To reset your password: From the website, click “Sign In” at the top right corner of the screen and then click the blue “Forgot Password” button. From the phone app, open the app and click the blue “Forgot Password” button on the welcome screen.Enter the same work email address you used when signing up (if you do not remember, please contact support@medibookr.com ) and click “Submit”. Please check your email and follow the instructions. If you are still having difficulty, please contact us through the chat feature on the app or website, or email support@medibookr.com ",
    },
    {
      title: "is-my-login-ID-and-password-the-same-for-both-your-web-and-mobile-app",
      question: "4. Is my login ID and password the same for both your web and mobile app?",
      answer: "Yes, the same login ID and password access both MediBookr’s web and mobile app.",
    },
    {
      title: "how-accessible-is-the-app",
      question: "5. How accessible is the app?",
      answer: "Users have 24/7/365 mobile app and web access to all of the benefits MediBookr provides. If you need assistance, please use the chat feature on the app or website to contact us directly with any issues or concerns.Concierge Services are offered Monday-Friday 8am-5pm CST. Feel free to text us through our “live chat”, email, or call us directly and we will respond within 24 hours. For Member Support:  Reach us at support@medibookr.com.",
    },
    {
      title: "is-medibookr-available-on-all-mobile-devices",
      question: "6. Is MediBookr available on all mobile devices?",
      answer: "Yes, MediBookr is available to download for both iOS and Android.",
    },
    {
      title: "can-my-spouse-also-download-the-mediBookr-app-to-access-medibookr-services",
      question: "7. Can my spouse also download the MediBookr App to access MediBookr’s services?",
      answer: "Yes, please email support@medibookr.com with your name and your spouse’s contact information including their full name, email, and date of birth. They will then receive an email with instructions on how to activate their account.",
    },
    {
      title: "can-you-help-me-review-my-bill",
      question: "8. Can you help me review my bill?",
      answer: "Though this service will be offered in the near future, MediBookr does not offer this at this time. We will update you as soon as this service becomes available. Please contact your insurance company directly for any questions regarding your Explanation of Benefits (EOB).",
    },
    {
      title: "will-I-know-if-a-doctor-or-provider-accepts-my-insurance",
      question: "9. Will I know if a doctor or provider accepts my insurance?",
      answer: "Providers move in- and out-of-network for various reasons, and the only way to verify if a provider accepts your insurance plan’s network is to double check with the provider directly. You can do this yourself or by contacting MediBookr’s concierge services to contact the provider on your behalf.",
    },
    {
      title: "why-am-I-not-able-to-find-a-lot-of-providers-for-the-service-I-was-searching-for",
      question: "10. Why am I not able to find a lot of providers for the service I was searching for?",
      answer: "It takes time for MediBookr’s data scientists to analyze providers’ information and then add them to MediBookr’s database.  MediBookr adds new providers and updates our database each week, so please check back soon. If you would like us to add a specific provider, please send us a message through our chat service on the app or email us at support@medibookr.com with the provider’s name and contact information.",
    },
    {
      title: "are-there-any-fees-associated-with-medibookr-services",
      question: "11. Are there any fees associated with MediBookr services?",
      answer: "No, there are no fees to the employee/user for any of MediBookr’s services. Your employer has partnered with us to help save you time and money as a benefit to you.",
    },
    {
      title: "where-does-my-insurance-benefit-status-come-from",
      question: "12. Where does my insurance benefit status come from?",
      answer: "We receive your benefit status from your insurance provider and it is updated frequently. If you have questions or concerns about the status shown please let us know and we can contact your benefits administrator to cross reference with your insurance provider. Please keep in mind that if there are services performed which haven’t not yet reached your status from the insurance provider, the status shown may not be updated to include those services.",
    },
    {
      title: "can-I-add-my-children-as-dependents",
      question: "13. Can I add my children as dependents?",
      answer: "Currently, you can add dependents by choosing your Profile – Add Dependent and filling in the fields.",
    },
    {
      title: "why-dont-I-see-my-physician-in-my-results-list",
      question: "14. Why don’t I see my physician in my results list?",
      answer: "Though we have tried to establish a very comprehensive list of physicians, it is possible that a few doctors’ names may have been missed. Please contact our Concierge services through the Chat feature on the app/website or email us at support@medibookr.com with the physician’s name and contact information so we can add it to our database. We appreciate your help!",
    },
    {
      title: "can-I-contact-the-concierge-if-I-have-questions-regarding-medibookr",
      question: "15. Can I contact the concierge if I have questions regarding MediBookr?",
      answer: "Please contact our Concierge Services for any assistance with your healthcare needs through the Chat feature on the app/website, or email us at support@medibookr.com. Concierge services are available Monday-Friday 8-5pm CST and we will respond to you within 24 hours. If contacting us outside of our normal business hours, we will get back to you as soon as possible. You can read our FAQ section to try and answer your questions in the meantime. Thank you!",
    },
    {
      title: "any-other-questions-not-covered-here",
      question: "16. Any other questions not covered here?",
      answer: "If you have any further questions, please feel free to contact us through our Chat feature on our app and website, by email at support@medibookr.com,  or at (800) 822-8936. Please allow 24 hours for us to get back to you.",
    },
  ],
  "Ratings": [
    {
      title: "where-do-the-star-ratings-come-from-that-are-shown",
      question: "1. Where do the star ratings come from that are shown?",
      answer: "MediBookr is focused on providing our users with as much information available regarding the patient experience. We do this by aggregating rating data from multiple sources such as Google, Yelp and Facebook.",
    },
    {
      title: "can-providers-pay-to-get-good-ratings-or-to-get-the-status-of-best-value",
      question: "2.  Can providers pay to get good ratings or to get the status of “Best Value”?",
      answer: "No, MediBookr is a neutral platform that objectively evaluates provider’s value based on their true price and users’ ratings, and does not receive payments from providers.",
    },

  ],
  "Cost Estimates": [
    {
      title: "can-medibookr-guarantee-prices",
      question: "1. Can MediBookr guarantee prices?",
      answer: "Though the estimates provided are as accurate as possible, MediBookr cannot guarantee any prices as pricing can change for various reasons on the provider or insurance levels.  Examples of possible price discrepancies include denials by insurance companies and additional charges billed for at the provider level.",
    },
    {
      title: "what-is-a-cash-estimate-and-where-do-the-prices-listed-come-from",
      question: "2.  What is a “Cash Estimate” and where do the prices listed come from?",
      answer: "Patients who pay up front (using cash, debit card or HSA) often get better deals than their insurance plans have negotiated for them. MediBookr lists “cash prices” offered by certain providers who have agreed to have us post their price for specific services. You may see some of these providers listed under the “MediBookr Cash Network” name, however their address and phone number are available to view and to schedule an appointment. If the cash price is not listed and you would like to know the estimate, please contact our concierge services at support@medibookr.com, and we will help you obtain the estimated cash fees.",
    },
    {
      title: "where-do-the-dollar-sign-estimates-come-from",
      question: "3. Where do the dollar sign estimates come from?",
      answer: "MediBookr creates cost estimates for providers by analyzing data from health insurance claims and data warehouses. We have performed extensive market research to determine the comparison between providers as an aggregate representation and for specific services.",
    },
    {
      title: "why-are-dollar-signs-provided-instead-of-an-actual-price",
      question: "4. Why are dollar signs provided instead of an actual price?",
      answer: "We may provide an Insurance or cash price reference in the form of dollar signs to show the market difference in price comparison between providers. We do this for a few reasons. We may not have enough data to calculate an actual price estimate for a provider, and/or we may not be able to infer what service/s the provider may perform based on your search, i.e. when searching by a symptom. However we have performed market research to determine the comparison between providers as an aggregate representation. If you would like our assistance in contacting the specific provider for pricing inquiries, please contact our concierge services at support@medibookr.com so that we can contact the provider on your behalf and get you the correct estimated pricing.",
    },
    {
      title: "how-can-I-give-feedback-about-medibookrs-cost-estimates",
      question: "5. How can I give feedback about MediBookr’s cost estimates?",
      answer: "If you find any inaccuracies in our estimates, please contact us so we can continuously improve the quality of our data. Please email us at support@medibookr.com with any relevant pricing information we should consider.",
    },
    {
      title: "how-did-you-determine-best-value-providers",
      question: "6. How did you determine “Best Value” providers?",
      answer: "We use an algorithm based on all providers’ prices and ratings and only grant “Best Value” to low cost providers who meet the objective standards set by MediBookr’s data scientists. MediBookr is a neutral platform that objectively evaluates provider’s value; we do not accept payment from providers for any services.",
    },
  ],
  "Privacy and Ethics": [
    {
      title: "is-my-privacy-protected-on-the-medibookr-app-and-website",
      question: "1. Is my privacy protected on the MediBookr app and website?",
      answer: "Your privacy on both the MediBookr app and website are completely secure and HIPAA protected. We will not share with your employer or sell any of your personal health data for any purpose. For more information about our privacy practices, see our Privacy Policy.",
    },
    {
      title: "can-medibookrs-patient-advocacy-and-concierge-team-provide-me-with-medical-advice-if-I-give-them-permission",
      question: "2.  Can MediBookr’s Patient Advocacy and Concierge team provide me with medical advice if I give them permission?",
      answer: "No, MediBookr does not provide any medical advice as we are not medical professionals. If you are in need of medical advice, our services can help guide you to a provider that may be able to assist you. If you are experiencing a medical emergency, please call 911.",
    },
  ],
  "Appointment Scheduling": [
    {
      title: "can-you-help-me-schedule-with-a-provider",
      question: "1. Can you help me schedule with a provider?",
      answer: "Yes, absolutely! Please contact our Concierge services through the Chat feature on the app/website or at support@medibookr.com  with any requests for help in scheduling a provider. Please make sure you provide us with your availability (e.g. mornings/afternoons, Mon/Wed only, etc.) and the best contact number to reach you at.",
    },
    {
      title: "can-medibookr-guarantee-being-able-to-schedule-an-appointment-with-a-particular-doctor-or-provider",
      question: "2. Can MediBookr guarantee being able to schedule an appointment with a particular doctor or provider?",
      answer: "Unfortunately, there are some factors that prevent MediBookr from being able to guarantee an appointment with a physician. These factors include a physician that is not taking new patients, an inability to find an appointment time that fits both of your schedules, and if a referral is required from another physician to see a specialist. If you contact our Patient Advocacy and Concierge services on the chat feature on the app or website, we can help you find an appointment with a similar provider that can help treat your specific needs.",
    },
    {
      title: "what-if-I-have-questions-about-the-specific-provider-I-am-interested-in-scheduling",
      question: "3. What if I have questions about the specific provider I am interested in scheduling?",
      answer: "If you have any questions about a specific provider, you may either contact the provider directly from the profile information provided on the app or contact our Concierge services to have us contact the provider for you. You can contact us through the app or website by utilizing our Chat feature, or you can email us at support@medibookr.com.",
    },
    {
      title: "how-do-I-schedule-an-appointment-in-the-medibookr-app",
      question: "4. How do I schedule an appointment in the MediBookr App?",
      answer: "Click on the “Request an Appointment” button once you have chosen a provider and fill out your desired appointment time requests. You will then receive a message through the app with multiple appointment choices to choose from and can respond directly in the app confirming your appointment time. If you need any assistance, have any questions, or would like our Concierge Services to schedule an appointment for you, please contact us through the Chat feature on the app or at support@medibookr.com. Please make sure you provide us with your availability (e.g. mornings/afternoons, Mon/Wed only, etc.) and the best contact number to reach you at.",
    },
    {
      title: "am-I-charged-by-medibookr-for-scheduling-an-appointment",
      question: "5. Am I charged by MediBookr for scheduling an appointment?",
      answer: "No, there is no charge for booking an appointment through MediBookr.",
    },
    {
      title: "can-I-add-my-children-as-dependents-and-book-appointments-for-them",
      question: "6. Can I add my children as dependents and book appointments for them?",
      answer: "Currently, you can add dependents by choosing your Profile – Add Dependent and filling in the fields. However, the functionality to create appointments for them using the platform is not available but is something we are working on to provide in the near future.",
    },
    {
      title: "what-if-I-need-to-cancel-my-appointment-will-I-be-charged",
      question: "7. What if I need to cancel my appointment? Will I be charged?",
      answer: "If you need to cancel an appointment, please contact the provider as soon as possible. Though MediBookr does not charge you for a cancellation, each provider will have their own cancellation policy and you may be charged by that provider if the cancellation did not occur within a certain period of time. Please double check when scheduling your appointment if there is a cancellation policy. If you need help with cancellation, please contact our Concierge services through the Chat feature on the app or at support@medibookr.com ",
    },
    {
      title: "what-information-can-I-review-about-the-provider-before-I-book-an-appointment",
      question: "8. What information can I review about the provider before I book an appointment?",
      answer: "Before scheduling an appointment, please review the provider profile which will have their contact information, cost estimate in the form of dollar signs, potential rating information, map of provider location, and a description of what type of medicine the provider practices.",
    },
  ],
};


/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {

}

type Props = StateProps & DispatchProps & OwnProps;

class FaqScreen extends React.Component<Props, State> {
  public state = {};

  public render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left/>
          <Body>
          <Text style={{textAlign: "center"}}>Frequently Asked Questions</Text>
          </Body>
          <Right>
            <Button onPress={Actions.pop.bind(this)} transparent={true}>
              <Icon name={"close"}/>
            </Button>
          </Right>
        </Header>
        <Content style={{paddingHorizontal: 20, flex: 1}} padder={true}>
          {Object.keys(faqs).map((value, index) => {
            return (
              <React.Fragment key={index}>
                <Text key={index} style={{color: Colors.textBlue, marginTop: 20, fontFamily: Fonts.type.latoBold}}>{value}</Text>
                {
                  faqs[value].map((valueFaqs, indx) => {
                    return (
                      <React.Fragment key={indx}>
                        <Text key={indx} style={{fontFamily: Fonts.type.latoBold, marginTop: 20}}>{valueFaqs.question}</Text>
                        <Text key={indx + "2"} style={{fontFamily: Fonts.type.latoRegular}}>{valueFaqs.answer}</Text>
                      </React.Fragment>

                    );
                  })
                }
              </React.Fragment>

            );
          })}
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(FaqScreen) as React.ComponentClass<OwnProps>;
