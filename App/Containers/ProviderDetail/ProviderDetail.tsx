import React, {Component, Fragment} from "react";
import {Button, Card, CardItem, Col, Container, Content, Grid, Icon, Row} from "@codler/native-base";
import {connect} from "react-redux";
import Images from "@root/App/Themes/Images";
import styles from "./ProviderDetailStyle";
import Header from "@root/App/Components/Header";
import {Alert, Image, Linking, Text, View} from "react-native";
import MapView, {LatLng, Marker} from "react-native-maps";
import Divider from "@root/App/Components/Divider/Divider";
import {formatNumber, getCityState, joinString} from "@root/App/Transforms/StringsTranform";
import {separateReviews} from "@root/App/Lib/DetailsScreenHelper";
import RatingStarBar from "@root/App/Components/RatingStarBar/RatingStarBar";
import * as R from "ramda";
import {ProviderByIdResponse} from "@root/App/Services/DataModels/Providers/ProviderByIdResponse";
import {Colors, Metrics} from "@root/App/Themes";
import {getGenderImage, notEmptyAndNull} from "@root/App/Lib/AppHelper";
import RequestAppointmentButton from "@root/App/Components/RequestAppointmentButton/RequestAppointmentButton";
import {Actions} from "react-native-router-flux";
import {RootState} from "@root/App/Reducers";
import LaunchNavigator from "react-native-launch-navigator";
import AsyncStorage from "@react-native-community/async-storage";

interface OwnProps {
  payload: ProviderByIdResponse;
}

interface StateProps {
  gps: LatLng;
}

interface State {

}

type Props = OwnProps & StateProps;

class ProviderDetail extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  public renderProvider() {
    const data = this.props.payload;

    return (
      <Fragment>
        {
          // In practice since
          notEmptyAndNull(data.inPracticeSince) &&
          <Fragment>
            <Text style={styles.subHeader}>In practice since:</Text>
            <Text style={styles.indentText}>{(new Date()).getFullYear() - parseInt(data.inPracticeSince, 10)}</Text>
          </Fragment>
        }
        {
          !R.isEmpty(data.certifications) &&
          <Fragment>
            <View style={{flexDirection: "row", justifyContent: "space-between"}}>
              <Text style={styles.subHeader}>Board Certifications:</Text>
              <Text
                style={styles.whyDoesMatter}
                onPress={() => {
                  Alert.alert("Board Certifications", "A board certification is an official recognition for doctors who have met " +
                    "specific requirements set by national medical specialty boards in the United States.\n\n" +
                    "This indicates that a doctor is highly qualified in the medical field in which he or she practices. A board-certified " +
                    "doctor is more likely than a non-board-certified doctor to have the most current skills and knowledge about how to treat your medical condition.\n" +
                    "\nThis information is proprietary data maintained in a copyrighted database compilation owned by Elsevier, Inc. " +
                    "in cooperation with the American Board of Medical Specialties. Copyright © 2018 Elsevier, Inc. and American Board of Medical Specialties. All rights reserved.");
                }}
              >Certification Significance
              </Text>
            </View>

            {data.certifications.map((value, index) => (
              <Text
                key={index}
                style={styles.indentText}
              >{
                R.compose(
                  R.head,
                  R.filter(notEmptyAndNull),
                  R.values,
                  R.pick(["boardName", "name", "certificateName"]),
                )(value)}
              </Text>
            ))}
          </Fragment>
        }

        {
          !R.isEmpty(data.educations) &&
          <Fragment>
            <Text style={styles.subHeader}>Education:</Text>
            {
              data.educations.map((value, index) => (
                <View style={[styles.educationContainer, index !== 0 ? {marginTop: Metrics.doubleBaseMargin} : null]}
                      key={index}>
                  <Text style={[styles.textNormal, {flex: 1}]}>{value.medschoolName}</Text>
                  <Text style={styles.indentText}>{value.year}</Text>
                </View>

              ))
            }
          </Fragment>
        }

        {
          !R.isEmpty(data.affiliations) &&
          <Fragment>
            <Text style={styles.subHeader}>Affiliations:</Text>
            {
              data.affiliations.map((value) => (
                <Fragment key={value.providerId}>
                  <Text style={styles.indentTextBold}>{value.providerName}</Text>
                  <Text
                    style={styles.indentText}>{joinString(" ", [value.providerAddressLine1, value.providerAddressLine2]) + "\n" +
                  getCityState(value)}</Text>
                </Fragment>
              ))
            }
          </Fragment>
        }

      </Fragment>
    );
  }

  public _renderFacility() {
    const data = this.props.payload;
    if (R.isEmpty(data.caregivers)) {
      return null;
    }
    return (
      <React.Fragment>
        {/* Care Givers */}
        <Text style={styles.subHeader}>Caregivers at this location:</Text>
        {
          data.caregivers.map((value) => (
            <Text key={value.providerId} style={styles.indentTextBold}>{value.providerName}</Text>
          ))
        }
      </React.Fragment>
    );
  }

  public _renderReviews() {
    const data = this.props.payload;

    if (!data.reviews.length) {
      return null;
    }
    const grouped = separateReviews(data.reviews);

    return (
      <Fragment>
        <Divider/>
        <Text style={styles.textBoldHeader}>Review Summary</Text>
        {
          grouped.map((value, index) => {
            const totalReviews = data.reviews.length;
            const percent = value.length / totalReviews;
            if (index !== 5) {
              return (
                <Row key={index} style={styles.reviewFirstRow}>
                  <Text style={styles.textBoldHeader}>{5 - index}</Text>
                  <Icon style={styles.star} name="star" type={"FontAwesome"}/>
                  <View style={[styles.bar, {width: 200 * percent}]}/>
                </Row>
              );
            }
          })
        }
        {
          data.reviews.map((value) => (
            <View key={value.providerId}>
              <Row style={styles.reviewFirstRow}>
                <RatingStarBar rating={value.rating}/>
                <Text>{value.reviewDate}</Text>
              </Row>
              <Text style={styles.reviewFirstRow}>{value.reviewText}</Text>
            </View>


          ))
        }
      </Fragment>
    );
  }

  public render() {
    const data = this.props.payload;

    if (!data) {
      return null;
    }

    const region = {
      latitude: parseFloat(data.latitude),
      longitude: parseFloat(data.longitude),
      latitudeDelta: 0.005, longitudeDelta: 0.005,
    };

    return (
      <Container>
        <Header style={styles.header}/>
        <Content style={styles.content}>
          <Card style={styles.card}>
            <CardItem cardBody={true}>
              <Grid>
                <Row style={styles.row}>
                  <Image source={getGenderImage(data)} style={styles.icon}/>
                  <Col style={styles.titleContainer}>
                    <Text numberOfLines={3}
                          style={styles.title}>{data.providerName}{data.credentials ? ", " + data.credentials : ""}</Text>
                    <Text style={styles.subTitle}>{data.specialty}</Text>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col>
                    <Text style={styles.state}>{getCityState(data)}</Text>
                    {
                      data.distance !== null &&
                      <Row style={{alignItems: "baseline"}}>
                        <Text style={styles.distance}>{data.distance.toFixed(0)}</Text>
                        <Text style={styles.miles}> miles</Text>
                      </Row>
                    }
                  </Col>
                </Row>

                <RequestAppointmentButton style={{marginTop: 0, marginBottom: 10}}
                                          onPress={() => Actions.push("request-appointment", {provider: data})}/>

                <MapView
                  initialRegion={region}
                  style={styles.map}
                >
                  <Marker coordinate={region} image={Images.Marker}/>
                </MapView>

                <Button
                  style={{
                    alignSelf: "flex-end",
                    paddingHorizontal: Metrics.baseMargin,
                    marginTop: Metrics.baseMargin,
                    marginRight: Metrics.baseMargin,
                  }}
                  info={true}
                  onPress={async () => {
                    if (this.props.gps) {
                      const defaultNav = await AsyncStorage.getItem("DEFAULT_NAVIGATOR");
                      if (defaultNav) {
                        const options = {
                          start: `${this.props.gps.latitude}, ${this.props.gps.longitude}`,
                        };
                        // @ts-ignore
                        options.app = defaultNav;
                        // @ts-ignore
                        LaunchNavigator.navigate(this.props.payload.providerAddress, options);
                      } else {
                        Actions.push("gps-navigation", {
                          userGps: this.props.gps,
                          providerAddress: this.props.payload.providerAddress,
                        });
                      }
                    } else {
                      Alert.alert("No Location Found", "Your current location not found, Please try again later.");
                    }
                  }}
                >
                  <Text style={{color: Colors.white}}>Get Directions</Text>
                </Button>

                <View style={styles.viewContent}>
                  <Divider/>

                  {/* Address and Phone */}

                  <Text style={styles.textBold}>Practice address:</Text>
                  <Text style={styles.textNormal}>{data.providerAddress + "\n" + getCityState(data)}</Text>
                  <Text style={styles.textBold}>Phone:</Text>
                  <Text
                    style={[styles.textNormal, {color: Colors.textBlue, textDecorationLine: "underline"}]}
                    onPress={() => Linking.openURL("tel:" + data.providerPhone)}
                  >{formatNumber(data.providerPhone)}
                  </Text>

                  {/* Description */
                    this.renderDescription(data.description)
                  }

                  {data.facilityFlag ? this._renderFacility() : this.renderProvider()}

                </View>

              </Grid>
            </CardItem>
          </Card>
        </Content>

      </Container>
    );
  }

  private renderDescription = (description) => {
    if (description) {
      if (R.contains("http", description)) {
        const urlExpression = /https?:\/\/\S+/g;
        const urls = R.match(urlExpression, description);
        const data = R.split(urlExpression, description);

        return (
          <Fragment>
            <Divider/>
            <Text style={styles.textNormal}>
              {
                // If this is a single url split won't return any array so treat the whole text as url else
                // map with the logic of converting text to url
                data.length > 0 ?
                  data.map(((value, index) => {
                    return [
                      value,
                      index !== 0 ?
                        <Text key={index} style={styles.textNormalLink}
                              onPress={() => Linking.openURL(urls[index - 1])}>{urls[index - 1]}</Text> : null,
                    ];
                  })) :
                  <Text style={styles.textNormalLink} onPress={() => Linking.openURL(description)}>{description}</Text>
              }
            </Text>
          </Fragment>
        );
      } else {
        return (
          <Fragment>
            <Divider/>
            <Text style={styles.textNormal}>{description}</Text>
          </Fragment>
        );
      }
    } else {
      return null;
    }
  };
}

const mapStateToProps = (state: RootState): StateProps => ({
  gps: state.location.gps,
});

export default connect(mapStateToProps)(ProviderDetail);
