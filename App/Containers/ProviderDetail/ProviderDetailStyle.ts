import {StyleSheet} from "react-native";
import {Metrics, Colors} from "@root/App/Themes";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import Fonts from "@root/App/Themes/Fonts";

const text = {
  normal: {
    marginTop: Metrics.textSpacing,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.opensans,
  },
  bold: {
    marginTop: Metrics.textSpacing,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.latoBold,
  },
};

export default StyleSheet.create({
  header: {
    ...ApplicationStyles.shadow,
  },
  viewContent: {
    padding: Metrics.headerMargin,
    zIndex: -1,
  },
  content: {
    backgroundColor: Colors.searchBg,
    padding: Metrics.smallMargin,
    paddingVertical: Metrics.smallMargin,
    zIndex: -1,
  },
  titleContainer: {
    paddingHorizontal: Metrics.headerMargin,
    justifyContent: "space-between",
  },
  card: {
    ...ApplicationStyles.shadow,
    marginBottom: 15,
  },
  icon: {
    alignSelf: "center",
    margin: Metrics.smallMargin,
  },
  row: {
    margin: Metrics.headerMargin,
  },
  reviewFirstRow: {
    alignItems: "baseline",
    marginTop: 10,
  },
  bar: {
    borderRadius: 2,
    height: 20,
    backgroundColor: Colors.primary,
  },
  star: {
    color: Colors.greyStar,
    fontSize: 18,
    marginLeft: 15,
  },
  textBoldHeader: {
    ...text.bold,
    fontSize: Fonts.size.regular,
  },
  textBold: {
    ...text.bold,
  },
  textNormal: {
    ...text.normal,
  },
  textNormalLink: {
    ...text.normal,
    color: Colors.textBlue,
    textDecorationLine: "underline",
  },
  whyDoesMatter: {
    ...text.normal,
    color: Colors.textBlue,
    marginTop: Metrics.doubleBaseMargin,
  },
  subHeader: {
    ...text.bold,
    marginTop: Metrics.doubleBaseMargin,
  },
  indentText: {
    ...text.normal,
    marginLeft: Metrics.doubleBaseMargin,
  },
  indentTextBold: {
    ...text.bold,
    marginLeft: Metrics.doubleBaseMargin,
  },
  title: {
    fontSize: Fonts.size.h4 - 2,
    color: Colors.textHeader,
    fontFamily: Fonts.type.latoBold,
  },
  state: {
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.regular,
    color: Colors.text,
  },
  distance: {
    fontFamily: Fonts.type.opensansBold,
    fontSize: Fonts.size.h5,
    color: Colors.text,
  },
  miles: {
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.input,
    color: Colors.text,
  },
  appointment: {
    alignSelf: "center",
    fontFamily: Fonts.type.opensansBold,
    color: Colors.cyan,
    fontSize: Fonts.size.medium,
  },
  subTitle: {
    color: Colors.text,
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.regular,
  },
  educationContainer: {
    marginLeft: Metrics.doubleBaseMargin,
    flexDirection: "row",
    alignItems: "flex-end",
  },
  map: {
    flex: 1,
    height: 150,
  },
});
