import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "../../Themes/";

export default StyleSheet.create({
  stepIndicator: {
    marginVertical: 50,
  },
  page: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  step: {
    marginTop: 20,
    marginBottom: 20,
  },
  text: {
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.medium,
    textAlign: "left",
    color: Colors.textDark,
    marginVertical: Metrics.baseMargin,
    paddingLeft: 20,
    alignItems: "center",
  },
  textSpace: {
    marginBottom: 40,
  },
  button: {
    alignSelf: "center",
    backgroundColor: Colors.iconColor,
    marginTop: Metrics.doubleSection,
  },
  icon: {
    fontSize: Fonts.size.h4,
    color: Colors.iconColor,
  },
  header: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.opensansBold,
    color: Colors.textDark,
    textAlign: "center",
    marginVertical: Metrics.baseMargin,
  },
  textBox: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
    alignItems: "flex-start",
    paddingLeft: Metrics.doubleBaseMargin,
  },
  centerAlign: {
    alignSelf: "center",
  },
  viewContent: {
    flex: 1,
    margin: 20,
  },
  navBtns: {
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  modalContainer: {
    position: "absolute",
    top: 50,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center"
  },
  modalPadding: { 
    flex: 1,
    padding: 20
  },
});
