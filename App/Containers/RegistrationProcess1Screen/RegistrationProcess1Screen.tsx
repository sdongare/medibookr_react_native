import * as React from "react";
import {View} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
// import styles from "./RegistrationProcess1ScreenStyle"
import {Button, Container, Content, ListItem, Text} from "@codler/native-base";
import StepIndicator from "react-native-step-indicator";
import Header from "@root/App/Components/Header/Header";
import styles from "./RegistrationProcess1ScreenStyle";
// Load Registartion Process Components
import RegistrationProcess2Screen from "@root/App/Containers/RegistrationProcess2Screen";
import RegistrationProcess3Screen from "@root/App/Containers/RegistrationProcess3Screen";
import RegistrationProcess4Screen from "@root/App/Containers/RegistrationProcess4Screen";
import {RegistrationProcessActions} from "@root/App/Reducers/RegistrationProcessReducers";
import LocationConsentScreen from "@root/App/Containers/LocationConsentScreen/LocationConsentScreen";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import Modal from 'react-native-modal';

const labels = [
  "Complete Profile",
  "Personal Info",
  "Primary Care Physician",
  "Insurance",
];
const customStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 40,
  separatorStrokeWidth: 3,
  currentStepStrokeWidth: 5,
  stepStrokeCurrentColor: "#009dd8",
  stepStrokeFinishedColor: "#009dd8",
  stepStrokeUnFinishedColor: "#009dd8",
  separatorFinishedColor: "#009dd8",
  separatorUnFinishedColor: "#009dd8",
  stepIndicatorFinishedColor: "#009dd8",
  stepIndicatorUnFinishedColor: "lightgray",
  stepIndicatorCurrentColor: "#009dd8",
  stepIndicatorLabelFontSize: 15,
  currentStepIndicatorLabelFontSize: 15,
  stepIndicatorLabelCurrentColor: "#fff",
  stepIndicatorLabelFinishedColor: "#ffffff",
  stepIndicatorLabelUnFinishedColor: "#009dd8",
  labelColor: "#000",
  labelSize: 14,
  currentStepLabelColor: "#009dd8",
  borderWidth: 0,
};

/**
 * The properties passed to the component
 */
export interface OwnProps {
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  toggleRegistration: () => void;
  requestLocationCheck: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  locationModalWindow: boolean;
}

/**
 * The local state
 */
export interface State {
  currentPage: number
}

type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>

class RegistrationProcess1Screen extends React.Component<Props, State> {
  public state = {
    currentPage: 0,
  };

  public componentDidMount = async () => {
    this.props.requestLocationCheck();
  }

  public renderSteps = (currentPage) => {
    if (currentPage === 0) {
      return (
        <React.Fragment>
          <Text style={styles.header}>
            Thank you for registering your MediBookr account !
          </Text>

          <Text style={[styles.text, styles.textSpace]}>
            To get the best of what MediBookr has to offer, please take a minute
            to fill out your profile.
          </Text>

          <ListItem iconLeft={true} noBorder={true}>
            <Icon
              type="FontAwesome"
              name="calendar-check-o"
              style={styles.icon}
            />
            <Text style={styles.text}>
              Personal Information is used to schedule the right appointment.
            </Text>
          </ListItem>

          <ListItem iconLeft={true} noBorder={true}>
            <Icon type="FontAwesome" name="vcard" style={styles.icon}/>
            <Text style={styles.text}>
              Primary Care Physician information helps to document your profile information.
            </Text>
          </ListItem>

          <Button
            onPress={() => this.handlePage(1)}
            style={styles.button}
            rounded={false}
            info={true}
            small={true}
          >
            <Text>Complete My Profile</Text>
          </Button>
        </React.Fragment>
      );
    }

    if (currentPage === 1) {
      return (
        <RegistrationProcess2Screen handlePage={this.handlePage}/>
      );
    }

    if (currentPage === 2) {
      return (
        <RegistrationProcess3Screen handlePage={this.handlePage}/>
      );
    }

    if (currentPage === 3) {
      return (
        <View>
          <RegistrationProcess4Screen/>
          <View style={styles.navBtns}>
            <Button
              onPress={() => this.handlePage(2)}
              style={styles.button}
              rounded={false}
              primary={true}
              small={true}
            >
              <Text>Back</Text>
            </Button>
            <Button style={styles.button} rounded={false} primary={true} small={true} onPress={this.props.toggleRegistration}>
              <Text>Go to Dashboard</Text>
            </Button>
          </View>
        </View>
      );
    }
  };

  public render() {
    const {currentPage} = this.state;
    return (
      <Container>
        <Header/>
        <Content padder={true}>
          <View style={styles.step}>
            <StepIndicator
              stepCount={4}
              customStyles={customStyles}
              currentPosition={this.state.currentPage}
              labels={labels}
            />
          </View>
          <View style={styles.viewContent}>
            {this.renderSteps(currentPage)}
          </View>
        </Content>
        <Modal isVisible={this.props.locationModalWindow} style={styles.modalContainer} animationIn={"zoomInDown"} animationOut="zoomOutUp">
          <View style={styles.modalPadding}>
            <LocationConsentScreen />
          </View>
        </Modal>
      </Container>
    );
  }

  private handlePage = (page: number) => {
    this.setState({
      currentPage: page,
    });
  };
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  toggleRegistration: () => dispatch(RegistrationProcessActions.toggleRegistration()),
  requestLocationCheck: () => dispatch(LocationActions.checkLocationSetThroughGPS()),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    locationModalWindow: state.location && state.location.locationModalWindow,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationProcess1Screen) as React.ComponentClass<OwnProps>;
