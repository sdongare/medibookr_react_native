import * as React from "react";
import {Alert, DatePickerAndroid, DatePickerIOS, Platform, TextInput, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import styles from "./UpdateDependentScreenStyle";
import {RootState} from "@root/App/Reducers";
import {Body, Button, Container, Content, Header, Icon, Left, Right, Text, View} from "@codler/native-base";
import {Colors} from "@root/App/Themes";
import {Actions} from "react-native-router-flux";
import moment from "moment";
import {PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import {RegexEmail} from "@root/App/Containers/LoginScreen/LoginScreen";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  callUpdate: (data) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
}

/**
 * The local state
 */
export interface State {
  dob: Date;
  address: string;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  state: string;
  city: string;
  zipcode: string;
  showPicker: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class UpdateDependentScreen extends React.Component<Props, State> {

  constructor(props) {
    super(props);
    const info = props.info as PatientsInfo;

    this.state = {
      dob: moment(info.dob).toDate(),
      address: info.address,
      email: info.email,
      firstName: info.firstName,
      lastName: info.lastName,
      phone: info.phone,
      state: info.state,
      zipcode: info.zipcode,
      city: info.city,
      showPicker: false,
    };
  }

  public checkState() {
    if (this.state.firstName.length === 0) {
      return "Please enter first name";
    } else if (this.state.lastName.length === 0) {
      return "Please enter last name";
    } else if (this.state.address.length === 0) {
      return "Please enter address";
    } else if (!RegexEmail.test(this.state.email.toLowerCase())) {
      return "Please enter valid email address";
    } else if (this.state.phone.length === 0) {
      return "Please enter phone";
    } else if (this.state.state.length === 0) {
      return "Please enter state";
    } else if (this.state.city.length === 0) {
      return "Please enter city";
    } else if (this.state.zipcode.length !== 5) {
      return "Please enter valid zip";
    } else {
      return "";
    }
  }

  public registerClick = () => {
    const message = this.checkState();
    if (message.length === 0) {
      this.props.callUpdate({
        ...this.state,
        dob: moment(this.state.dob).format("YYYY-MM-DD"),
      });
    } else {
      Alert.alert("Info", message, [
        {text: "OK"},
      ]);
    }
  };

  public back = () => {
    Actions.pop();
  };

  public async dobClick() {
    if (Platform.OS === "android") {
      try {
        const {action, year, month, day} = await DatePickerAndroid.open({
          date: this.state.dob,
        });
        if (action === DatePickerAndroid.dateSetAction) {
          // Selected year, month (0-11), day
          this.setState({
            dob: new Date(year, month, day),
          });
        }
      } catch ({code, message}) {
        console.warn("Cannot open date picker", message);
      }
    } else {
      this.setState({
        showPicker: !this.state.showPicker,
      });
    }
  }

  public render() {
    return (
      <Container>
        <Header style={styles.containerHeader}>
          <Left>
            <Button transparent={true} onPress={this.back}>
              <Icon style={styles.icon} name="arrow-back"/>
              <Text style={styles.text}>Back</Text>
            </Button>
          </Left>
          <Body>
          <Text style={styles.text}>Update Info</Text>
          </Body>
          <Right/>
        </Header>

        <Content padder={true} style={styles.container}>

          <TextInput
            style={[styles.input]}
            placeholderTextColor={Colors.textSuggestion}
            value={this.state.firstName}
            onChangeText={(text) => this.setState({firstName: text})}
            textContentType="name"
            placeholder="First Name"
          />

          <TextInput
            style={[styles.input, {marginTop: 10}]}
            placeholderTextColor={Colors.textSuggestion}
            value={this.state.lastName}
            onChangeText={(text) => this.setState({lastName: text})}
            textContentType="name"
            placeholder="Last Name"
          />

          <TouchableOpacity style={[styles.input, {marginTop: 10, justifyContent: "center"}]} onPress={this.dobClick.bind(this)}>
            <Text style={[{color: Colors.textSuggestion, fontSize: 14}]}>{`Date of Birth : ${moment(this.state.dob).format("YYYY-MM-DD")}`}</Text>
          </TouchableOpacity>

          <TextInput
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {marginTop: 10}]}
            value={this.state.address}
            onChangeText={(text) => this.setState({address: text})}
            placeholder="Address"
          />
          <TextInput
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {marginTop: 10}]}
            value={this.state.email}
            onChangeText={(text) => this.setState({email: text})}
            textContentType="emailAddress"
            placeholder="Email"
            keyboardType="email-address"
          />
          <TextInput
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {marginTop: 10}]}
            value={this.state.phone}
            textContentType="telephoneNumber"
            onChangeText={(text) => this.setState({phone: text})}
            placeholder="Phone Number"
          />
          <TextInput
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {marginTop: 10}]}
            value={this.state.state}
            onChangeText={(text) => this.setState({state: text})}
            placeholder="State"
          />

          <TextInput
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {marginTop: 10}]}
            value={this.state.city}
            onChangeText={(text) => this.setState({city: text})}
            placeholder="City"
          />

          <TextInput
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {marginTop: 10}]}
            value={this.state.zipcode}
            onChangeText={(text) => this.setState({zipcode: text})}
            placeholder="Zip Code"
          />

          <Button success={true} style={[styles.baseButton, {alignSelf: "stretch", marginTop: 10}]} onPress={this.registerClick}>
            <Text style={styles.buttonText}>Update Patient Info</Text>
          </Button>
        </Content>
        {
          this.state.showPicker &&
          <View style={{backgroundColor: "#fff"}}>
            <Button
              dark={true}
              transparent={true}
              style={{alignSelf: "flex-end", margin: 5}}
              onPress={() => {
                this.setState({
                  showPicker: false,
                });
              }}
            >
              <Text>Done</Text>
            </Button>
            <DatePickerIOS
              mode="date"
              onDateChange={(newDate) => {
                this.setState({
                  dob: newDate,
                });
              }}
              date={this.state.dob}
            />
          </View>
        }
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  callUpdate: (data) => dispatch(PatientAPIActions.patientUpdateRequest(data)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateDependentScreen) as React.ComponentClass<OwnProps>;
