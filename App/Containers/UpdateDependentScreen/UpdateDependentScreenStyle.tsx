import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";
import Fonts from "../../Themes/Fonts";
import ApplicationStyles from "../../Themes/ApplicationStyles";

const height = 45;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  textHeader: {
    fontFamily: Fonts.type.latoBold,
    color: Colors.textHeader,
    fontSize: Fonts.size.h5,
  },
  input: {
    ...ApplicationStyles.inputText,
    alignSelf: "stretch",
    height: height,
  },
  baseButton: {
    height: height,
    justifyContent: "center",
  },
  buttonText: {
    textAlign: "center",
  },
  containerHeader: {
    backgroundColor: Colors.primary,
  },
  text: {
    color: Colors.white,
  },
  icon: {
    color: Colors.white,
  },
});
