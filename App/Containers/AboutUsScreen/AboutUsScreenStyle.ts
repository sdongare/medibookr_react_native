import {StyleSheet} from "react-native";
import {Colors, Fonts, Metrics} from "../../Themes/";

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Metrics.navBarHeight,
    backgroundColor: Colors.background,
  },
  text: {
    marginTop: Metrics.baseMargin,
    fontFamily: Fonts.type.opensans,
    fontSize: Fonts.size.medium,
    color: Colors.textDark,
  },
  textBlue: {
    color: Colors.textBlue,
    textDecorationLine: "underline",
  },
});
