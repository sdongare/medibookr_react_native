import * as React from "react";
import {Linking, Text, View} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
import {Images} from "../../Themes";
import Metrics from "../../Themes/Metrics";

// Styles
import styles from "./AboutUsScreenStyle";
import DeviceInfo from "react-native-device-info";
import {Container, Content} from "@codler/native-base";
import Header from "@root/App/Components/Header/Header";
import AppConfig from "@root/App/Config/AppConfig";
import {Actions} from "react-native-router-flux";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {

}

type Props = StateProps & DispatchProps & OwnProps;

class AboutUsScreen extends React.Component<Props, State> {
  public state = {};

  public termsUrl = () => AppConfig.url() + "static/terms.html";
  public privacyUrl = () => AppConfig.url() + "static/privacy.html";

  public termClick = () => {
    Actions.push("link", {uri: this.termsUrl()});
  };

  public privacyClick = () => {
    Actions.push("link", {uri: this.privacyUrl()});
  };

  public render() {
    return (
      <Container>
        <Header/>
        <Content padder={true}>
          <View>
            <Text style={styles.text}>Version : {DeviceInfo.getReadableVersion()}</Text>
            <Text style={[styles.text, styles.textBlue]} onPress={this.termClick}>Terms and Conditions</Text>
            <Text style={[styles.text, styles.textBlue]} onPress={this.privacyClick}>Privacy Policy</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutUsScreen) as React.ComponentClass<OwnProps>;
