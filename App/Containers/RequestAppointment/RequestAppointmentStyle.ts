import {StyleSheet} from "react-native";
import {Colors, Metrics, Fonts} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Metrics.navBarHeight,
    backgroundColor: Colors.background,
  },
  grid: {
    padding: Metrics.baseMargin,
  },
  row: {
    flexDirection: "row",
  },
  title: {
    fontSize: Fonts.size.h5,
    fontFamily: Fonts.type.latoBold,
    color: Colors.textDark,
  },
  pickerCarrot: {
    position: "absolute",
    right: 8,
    top: 14,
    fontSize: 14,
    color: Colors.textDark,
  },
  pickerStyle: {
    marginTop: 8,
    width: "100%",
    borderRadius: 3,
    backgroundColor: Colors.searchBg,
    borderColor: Colors.border,
    borderWidth: 1,
  },
  state: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.latoRegular,
    color: Colors.textDark,
  },
  label: {
    marginTop: Metrics.doubleBaseMargin,
    fontFamily: Fonts.type.latoBold,
    fontSize: 16,
    color: Colors.textDark,
  },
  input: {
    marginTop: 10,
    padding: Metrics.baseMargin,
    marginBottom: 20,
    height: 145,
    backgroundColor: Colors.searchBg,
    borderRadius: 3,
    borderColor: Colors.border,
    borderWidth: 1,
  },
});
