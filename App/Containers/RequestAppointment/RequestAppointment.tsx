import * as React from "react";
import {Alert, Image, PickerItem, Text, TextInput, View} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import styles from "./RequestAppointmentStyle";
import {Body, Button, Container, Content, Header, Icon, Left, Picker, Right} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {RootState} from "@root/App/Reducers";
import {getGenderImage} from "@root/App/Lib/AppHelper";
import RatingStarBar from "@root/App/Components/RatingStarBar/RatingStarBar";
import {PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import RequestAppointmentButton from "@root/App/Components/RequestAppointmentButton/RequestAppointmentButton";
import {Colors} from "@root/App/Themes";
import * as R from "ramda";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  provider: ProviderResultsItem;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestAppointment: (data) => void
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {
  selectedTime: number;
  reason: string;
}

type Props = StateProps & DispatchProps & OwnProps;

const APPOINTMENT_TIMES = ["Next Available", "Next Available: Early Morning", "Next Available: Evening", "As Soon As Possible", "Weekend Mornings", "Weekend Afternoons"];

class RequestAppointment extends React.Component<Props, State> {

  public state = {
    selectedTime: 0,
    reason: "",
  };

  private content: any;

  public render() {
    return (
      <Container>
        <Header>
          <Left/>
          <Body>
            <Text style={{textAlign: "center"}}>
              Request An Appointment
            </Text>
          </Body>
          <Right>
            <Button transparent={true} onPress={this.close}>
              <Icon name={"close"}/>
            </Button>
          </Right>
        </Header>

        <Content style={styles.grid} contentContainerStyle={{paddingBottom: 30}} ref={(ref) => this.content = ref}>
          <View style={styles.row}>
            <Image source={getGenderImage(this.props.provider)}/>
            <View style={{marginLeft: 15}}>
              <Text style={styles.title}>{this.props.provider.providerName}</Text>
              <Text style={styles.state}>{this.props.provider.providerState}</Text>
              <RatingStarBar style={{marginTop: 10}} rating={this.props.provider.rating}/>
            </View>
          </View>
          <Text style={styles.label}>What is your preferred time</Text>
          <View>
            <Picker
              style={styles.pickerStyle}
              mode={"dropdown"}
              selectedValue={this.state.selectedTime}
              onValueChange={this.onTimeSelected}
            >
              {APPOINTMENT_TIMES.map((value, index) => (<PickerItem label={value} key={index} value={index}/>))}
            </Picker>
            <Icon name={"caretdown"} type={"AntDesign"} style={styles.pickerCarrot}/>
          </View>

          <Text style={styles.label}>
            Reason for visit
          </Text>
          <TextInput
            value={this.state.reason}
            multiline={true}
            numberOfLines={6}
            onFocus={(event) => setTimeout(() => this.content._root.scrollToEnd(), 500)}
            onChangeText={this.onReasonText}
            placeholderTextColor={Colors.textSuggestion}
            style={styles.input}
          />
          <RequestAppointmentButton onPress={this.costEstimate}/>
        </Content>
      </Container>
    );
  }

  private onReasonText = (text) => {
    this.setState({
      reason: text,
    });
  };

  private costEstimate = () => {
    this.props.requestAppointment({
      appointmentType: this.state.selectedTime + 1,
      reasonForVisit: R.isEmpty(this.state.reason) ? "No visit reason provided" : this.state.reason,
      onSuccess: () => {
        Alert.alert("Thank You !!", "Your appointment request has been sent to the provider. You will hear from the provider or our concierge service soon.", [
            {onPress: Actions.pop.bind(this), text: "OK"},
          ],
          {cancelable: false});
      },
      provider: this.props.provider,
    });
  }

  private close = () => Actions.pop();

  private onTimeSelected = (value) => this.setState({selectedTime: value});
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  requestAppointment: (data) => dispatch(PatientAPIActions.appointmentRequest(data)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestAppointment) as React.ComponentClass<OwnProps>;
