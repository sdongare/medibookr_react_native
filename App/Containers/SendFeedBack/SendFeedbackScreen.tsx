import React from "react";
import {Alert, TextInput} from "react-native";
import styles from "./SendFeedbackScreenStyle";
import {Colors} from "@root/App/Themes";
import {Body, Button, Container, Content, Header, Icon, Left, Right, Text} from "@codler/native-base";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";
import {Actions} from "react-native-router-flux";
import {AppActions, SendFeedbackParams} from "@root/App/Reducers/AppReducers";

/**
 * The properties passed to the component
 */
interface OwnProps {
}

/**
 * The properties mapped from Redux dispatch
 */
interface DispatchProps {
  sendFeedback?: (params: SendFeedbackParams) => void;
}

/**
 * The properties mapped from the global state
 */
interface StateProps {
}

/**
 * The local state
 */
interface State {
  query: string;
  name: string;
  email: string;
}

type Props = StateProps & DispatchProps & OwnProps;

const INITIAL_STATE = {
  query: "",
  name: "",
  email: "",
};


class SendFeedbackScreen extends React.Component<Props, State> {
  public state = INITIAL_STATE;

  public render() {
    return (
      <Container>
        <Header>
          <Left/>
          <Body>
          <Text style={{textAlign: "center"}}>Send Us Feedback</Text>
          </Body>
          <Right>
            <Button onPress={Actions.pop.bind(this)} transparent={true}>
              <Icon name={"close"}/>
            </Button>
          </Right>
        </Header>
        <Content style={styles.content}>

          <Text style={styles.label}>
            Name
          </Text>
          <TextInput
            value={this.state.name}
            multiline={true}
            numberOfLines={1}
            onChangeText={this.onNameInput}
            placeholderTextColor={Colors.textSuggestion}
            style={styles.input}
          />

          <Text style={styles.label}>
            Email
          </Text>
          <TextInput
            value={this.state.email}
            multiline={true}
            numberOfLines={1}
            onChangeText={this.onEmailInput}
            placeholderTextColor={Colors.textSuggestion}
            style={styles.input}
          />

          <Text style={styles.label}>
            Request / Suggestion
          </Text>
          <TextInput
            value={this.state.query}
            multiline={true}
            numberOfLines={6}
            onChangeText={this.onQuestionInput}
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, {height: 145}]}
          />

          <Button full={true} onPress={this.onPress} info={true} style={styles.button}>
            <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  private onQuestionInput = (text) => {
    this.setState({
      query: text,
    });
  };

  private onNameInput = (text) => {
    this.setState({
      name: text,
    });
  };
  private onEmailInput = (text) => {
    this.setState({
      email: text,
    });
  };

  private onPress = () => {

    if (this.state.query && this.state.name && this.state.email) {
      this.props.sendFeedback({
        message: this.state.query,
        email: this.state.email,
        name: this.state.name,
        onSuccess: () => {
          this.setState(INITIAL_STATE);
          Alert.alert("Send Feedback", "Thanks for contacting us, we will be in touch!");
        },
      });
    } else {
      Alert.alert("Information required", "Please provide all the information required.");
    }


  };
}

const mapStateToProps = (state: RootState): StateProps => ({});

const mapDispatchToProps = (dispatch: Function): DispatchProps => ({
  sendFeedback: (params: SendFeedbackParams) => dispatch(AppActions.actionSendFeedback(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SendFeedbackScreen);
