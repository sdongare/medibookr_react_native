import {
  StyleSheet,
} from "react-native";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import {Colors, Metrics} from "../../Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  content: {
    flex: 1,
    padding: Metrics.doubleBaseMargin,
  },
  divider: {
    marginVertical: 0,
  },
  button: {
    ...ApplicationStyles.shadow,
    marginTop: Metrics.doubleBaseMargin,
  },
  header: {
    fontFamily: Fonts.type.latoBold,
    fontSize: 20,
    color: Colors.textDark,
  },
  description: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 16,
    color: Colors.textDark,
  },
  phone: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: 16,
    textDecorationLine: "underline",
    color: Colors.textBlue,
  },
  label: {
    marginTop: 10,
    fontFamily: Fonts.type.latoBold,
    fontSize: 16,
    color: Colors.textDark,
  },
  input: {
    marginTop: 10,
    padding: Metrics.baseMargin,
    marginBottom: 20,
    backgroundColor: Colors.searchBg,
  },
  pickerStyle: {
    marginTop: 8,
    width: "100%",
    borderRadius: 3,
    backgroundColor: Colors.searchBg,
    borderColor: Colors.border,
    borderWidth: 1,
  },
});
