import {StyleSheet} from "react-native";
import {Colors, Metrics} from "../../Themes/";

export default StyleSheet.create({
  root: {
    backgroundColor: Colors.searchBg,
  },
  contentContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: Metrics.baseMargin,
    width: null,
    height: Metrics.screenHeight,
    alignSelf: "stretch",
  },
  iconBack: {
    color: Colors.white,
  },

  headerTextContainer: {
    backgroundColor: Colors.primary,
    flexDirection: "row",
    margin: Metrics.marginVertical,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  headerText: {
    flex: 1,
    flexWrap: "wrap",
    color: Colors.white,
    padding: Metrics.marginHorizontal,
  },
});
