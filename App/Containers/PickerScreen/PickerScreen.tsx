import * as React from "react";
import {NavigationScreenProps} from "react-navigation";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import {Container, Content} from "@codler/native-base";
import SearchAccordion from "@root/App/Components/SearchAccordion/SearchAccordion";
import Header from "@root/App/Components/Header/Header";
import SearchBar from "@root/App/Components/SearchBar/SearchBar";
import {ImageBackground, Text, View} from "react-native";
import styles from "./PickerScreenStyle";
import {Actions} from "react-native-router-flux";
import {capitalizeOrNull} from "@root/App/Transforms/StringsTranform";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import KeywordAutoSuggestBox from "@root/App/Components/KeywordAutoSuggestBox/KeywordAutoSuggestBox";
import {RootState} from "@root/App/Reducers";
import {Images} from "@root/App/Themes";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  list: ResultsItem[];
  listType: string;
  keyword: string;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
}


type Props = StateProps & DispatchProps & OwnProps & NavigationScreenProps<{}>;

class SearchCollapsibleScreen extends React.Component<Props, OwnProps> {

  constructor(props) {
    super(props);
    this.state = props;
  }

  public componentWillReceiveProps(props) {
    this.setState(props);
  }

  public render() {
    return (
      <Container style={styles.root}>
        <Header/>
        <SearchBar style={ApplicationStyles.shadow}/>
        <KeywordAutoSuggestBox/>
        <View style={styles.contentContainer}>

          <ImageBackground resizeMode={"contain"} style={styles.container} source={Images.PickerBg}>
            <View style={styles.headerTextContainer}>
              <Text style={styles.headerText}>{`Narrow your search showing matches for "${this.state.keyword}" in ${capitalizeOrNull(this.state.listType)}`}</Text>
            </View>
            <SearchAccordion dataArray={this.state.list} level={0} type={this.state.listType}/>
          </ImageBackground>

        </View>

      </Container>
    );
  }

  public back = () => {
    Actions.pop();
  };
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchCollapsibleScreen);
