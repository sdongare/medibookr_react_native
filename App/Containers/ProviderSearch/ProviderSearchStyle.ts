import {StyleSheet} from "react-native";
import {ApplicationStyles, Colors, Fonts, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  root: {
    // justifyContent: "center",
    // alignContent: "center",
  },
  divider: {
    marginVertical: 0,
    width: "100%",
  },
  dividerSmall: {
    height: 3,
    backgroundColor: Colors.textGrey,
    width: 60,
  },
  text: {
    marginTop: Metrics.smallMargin,
    fontSize: Fonts.size.regular,
    textAlign: "center",
    fontFamily: Fonts.type.opensans,
    color: Colors.textGrey,
  },
  boldText: {
    marginTop: Metrics.smallMargin,
    fontSize: Fonts.size.h3,
    fontFamily: Fonts.type.latoRegular,
    color: Colors.textGrey,
  },
  icon: {
    marginTop: 40,
    fontSize: Fonts.size.h1,
    color: Colors.textBlue,
  },
  title: {
    color: Colors.textGrey,
    fontFamily: Fonts.type.latoBold,
    textAlign: "left",
    fontSize: Fonts.size.h5,
  },
  header: {
    marginTop: 40,
    color: Colors.textBlue,
    textAlign: "center",
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.h3,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.searchBg,
  },
  contentContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
});
