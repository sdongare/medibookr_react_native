import * as React from "react";
import {connect} from "react-redux";
import { View, ScrollView, Platform } from "react-native";
import * as Redux from "redux";
import {RootState} from "../../Reducers";
// Styles
import Header from "@root/App/Components/Header/Header";
import {Container, Content, Icon, Text} from "@codler/native-base";
import SearchBar from "@root/App/Components/SearchBar/SearchBar";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import Divider from "@root/App/Components/Divider";
import TooltipComponenet from "@root/App/Components/TooltipComponenet";
import styles from "./ProviderSearchStyle";
import KeywordAutoSuggestBox from "@root/App/Components/KeywordAutoSuggestBox/KeywordAutoSuggestBox";
import AsyncStorage from "@react-native-community/async-storage";
import {GetHelpActions} from "@root/App/Reducers/GetHelpReducers";

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestInfoMessage: (key: string) => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  infoMessage: InfoMessage | undefined;
}

/**
 * The local state
 */
export interface State {
  toolTipVisible: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class ProviderSearch extends React.Component<Props, State> {
  public state = {
    toolTipVisible: false
  };

  public componentDidMount = async () => {
    var searchTooltipView = await AsyncStorage.getItem('searchTooltipView');
    if(!searchTooltipView)
    {
      this.props.requestInfoMessage("search-and-compare-providers");
      setTimeout(()=> this.setState({ toolTipVisible: true }), 1000) 
      AsyncStorage.setItem('searchTooltipView', 'view');     
    }    
  }

  public render() {
    var tooltipContent = [];
    if(this.state.toolTipVisible && this.props.infoMessage && Object.keys(this.props.infoMessage).length > 0 && this.props.infoMessage.key == "search-and-compare-providers")
    {
      tooltipContent.push(
        <TooltipComponenet 
          position={"bottom"}
          heading={this.props.infoMessage.title}
          description={this.props.infoMessage.message}
          style={{ top: Platform.OS === "ios" ? "-5%" : "-7%" }}
        /> 
      )
    }
    return (
      <Container style={styles.root}>
        <Header/>                
        <SearchBar style={ApplicationStyles.shadow}/>     
        {tooltipContent}   
        <KeywordAutoSuggestBox/>
        <ScrollView style={{ flex: 1 }}>
        <Content padder={true} style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text style={styles.title}>Search and Compare Providers</Text>
          <Divider style={styles.dividerSmall}/>

          <Icon style={styles.icon} name={"search"} type={"FontAwesome"}/>
          <Text style={styles.boldText}>Search</Text>
          <Text style={styles.text}>Search by name, specialty, symptom or procedure</Text>
          <Icon style={styles.icon} name={"check-square"} type={"FontAwesome"}/>
          <Text style={styles.boldText}>Compare</Text>
          <Text style={styles.text}>Find the right provider by comparing price estimates for your insurance plan, cash pay rates, user reviews, and updated provider information.</Text>
          <Icon style={styles.icon} name={"user-md"} type={"FontAwesome"}/>
          <Text style={styles.boldText}>Schedule</Text>
          <Text style={styles.text}>Send messages directly to the provider using the "Request an Appointment" feature. We monitor all requests to make sure the provider replies promptly.</Text>
        </Content>
        </ScrollView>
      </Container>

    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  requestInfoMessage: (key) => dispatch(GetHelpActions.requestInfoMessage(key)),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    infoMessage: state.help && state.help.infoMessage,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProviderSearch) as React.ComponentClass<OwnProps>;
