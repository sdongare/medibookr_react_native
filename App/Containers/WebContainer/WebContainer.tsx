import * as React from "react";
import {connect} from "react-redux";
import * as Redux from "redux";
import { WebView } from "react-native-webview";

// Styles
import styles from "./WebContainerStyle";
import {Container} from "@codler/native-base";
import Header from "@root/App/Components/Header";
import {RootState} from "@root/App/Reducers";
import Loader from "@root/App/Containers/Loader/Loader";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  uri: string;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {

}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {
  loading: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class WebContainer extends React.Component<Props, State> {
  public state = {
    loading: false,
  };

  public loadStart = () => this.setState({loading: true});

  public loadEnd = () => this.setState({loading: false});

  public render() {
    return (
      <Container>
        <Header hideMenu={true}/>
        <WebView
          onLoadStart={this.loadStart}
          onLoadEnd={this.loadEnd}
          style={styles.container}
          source={{uri: this.props.uri}}
        />
        <Loader forceLoading={this.state.loading}/>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(WebContainer) as React.ComponentClass<OwnProps>;
