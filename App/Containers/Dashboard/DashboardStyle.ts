import {StyleSheet} from "react-native";
import {Colors, Fonts} from "@root/App/Themes";

const txtSmall = {
  fontSize: 20,
  fontFamily: Fonts.type.latoRegular,
  color: Colors.textDark,
};

export default StyleSheet.create({
  contentContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  quickLinkText: {
    textAlign: "center",
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.latoRegular,
    color: Colors.textHeader,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.searchBg,
  },
  headerContainer: {
    justifyContent: "space-between",
  },
  appointmentRow: {
    marginVertical: 10,
  },
  appointmentButtonContainer: {
    justifyContent: "flex-end",
  },
  appointmentButton: {
    paddingHorizontal: 10,
    alignSelf: "flex-end",
  },
  seeAll: {
    color: Colors.textBlue,
    fontFamily: Fonts.type.latoRegular,
    fontSize: 20,
  },
  header: {
    fontSize: 20,
    fontFamily: Fonts.type.opensansBold,
    color: Colors.textDark,
  },
  textBig: {
    fontSize: 22,
    fontFamily: Fonts.type.latoRegular,
    color: Colors.textDark,
  },
  textSmall: {...txtSmall},
  priceOrange: {...txtSmall, color: Colors.orange},
  priceGreen: {...txtSmall, color: Colors.green},
  modalContainer: {
    position: "absolute",
    top: 50,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center"
  },
  modalPadding: { 
    flex: 1,
    padding: 20
  },
  noPieChartData: {
    fontSize: 20,
    fontWeight: '400',
    margin: 15,
    textAlign: "center",
  }
});
