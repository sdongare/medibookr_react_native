import * as React from "react";
import {connect} from "react-redux";
import * as Redux from "redux";
import * as R from "ramda";
// Styles
import {PatientAPIActions} from "@root/App/Reducers/PatientsReducers";
import {GetHelpActions} from "@root/App/Reducers/GetHelpReducers";
import {Col, Container, Row, Text} from "@codler/native-base";
import {RootState} from "@root/App/Reducers";
import Header from "@root/App/Components/Header/Header";
import styles from "./DashboardStyle";
import Divider from "@root/App/Components/Divider/Divider";
import {Colors, Images} from "@root/App/Themes";
import PieChart from "@root/App/Components/PieChart/PieChart";
import {RequestAppointmentResponse} from "@root/App/Services/DataModels/Patient/RequestAppointmentResponse";
import {
  BackHandler,
  FlatList,
  Image,
  Linking,
  NativeEventSubscription,
  ScrollView,
  TouchableOpacity,
  View,
} from "react-native";
import moment from "moment";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";
import KeywordAutoSuggestBox from "@root/App/Components/KeywordAutoSuggestBox/KeywordAutoSuggestBox";
import SearchBar from "@root/App/Components/SearchBar";
import {formatNumber} from "@root/App/Transforms/StringsTranform";
import {Actions} from "react-native-router-flux";
import {LatLng} from "react-native-maps";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import {getLocation} from "@root/App/Lib/AppHelper";
import DraggableHandle from "@root/App/Components/DraggableHandle";
import {DashboardActions} from "@root/App/Reducers/DashboardReducers";
import LocationConsentScreen from "@root/App/Containers/LocationConsentScreen/LocationConsentScreen";
import Modal from 'react-native-modal';
import AsyncStorage from "@react-native-community/async-storage";
import TooltipComponenet from "@root/App/Components/TooltipComponenet";
import BiometricVerificationScreen from "@root/App/Containers/BiometricVerificationScreen/BiometricVerificationScreen";
import {check, PERMISSIONS, RESULTS} from 'react-native-permissions';

/**
 * The properties passed to the component
 */
export interface OwnProps {

}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  requestDashboard: () => void;
  requestInfoMessage: (key: string) => void;
  requestFixLocation: (location: string) => void;
  accessTheRight: (term: string) => void;
  requestLocationCheck: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  summary: Summary;
  city: string;
  location: LatLng | undefined;
  isActive: boolean;
  benefits: {
    deductibleTotal: number;
    deductibleRemaining: number;
    deductibleApplied: number;
    oopTotal: number;
    oopRemaining: number;
    oopApplied: number;
  } | undefined;
  appointments: RequestAppointmentResponse | undefined;
  infoMessage: InfoMessage | undefined;
  pcp: PCPResponse;
  locationModalWindow: boolean;
  openLocationModal: boolean;
}

/**
 * The local state
 */
export interface State {
  seeAllAppointments: boolean;
  posY: number;
  panScroll: boolean;
  heightStaticImage: number;
  toolTipVisible: boolean;
  isPermissionGranted: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class Dashboard extends React.Component<Props, State> {
  public state = {
    seeAllAppointments: true,
    heightStaticImage: 0,
    posY: 0,
    panScroll: true,
    toolTipVisible: false,
    isPermissionGranted: true,
  };
  private backHandler: NativeEventSubscription;


  public handleBackPress = () => {
    if (Actions.currentScene === "dashboard") {

      BackHandler.exitApp();
      return true;
    }
    return false;
  }

  public componentDidMount = async () => {
    this.props.requestDashboard();
    this.props.requestLocationCheck();
    this.checkForLocationPermission()
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    var dashboardTooltipView = await AsyncStorage.getItem('dashboardTooltipView');
    if(!dashboardTooltipView)
    {
      this.props.requestInfoMessage("review-your-expenses");
      setTimeout(()=> this.setState({ toolTipVisible: true }), 1000) 
      AsyncStorage.setItem('dashboardTooltipView', 'view');     
    }
  }

  public componentWillUnmount(): void {
    this.backHandler.remove()
  }

  public renderAppointmentContent(item) {
    if (item.state === 5) {
      return (<Text style={styles.textSmall}>Appointment Canceled</Text>);
    } else if (!item.datetime) {
      return (
        <Text
          style={styles.textSmall}>{"Date: " + moment(item.datetime).format("L").replace("Invalid date", "") + "\n" +
        "Time: " + moment(item.datetime).format("LT").replace("Invalid date", "")}</Text>
      );
    } else if (!!item.datetime) {
      return (<Text style={styles.textSmall}>Appointment Not Offered Yet</Text>);
    } else {
      return null;
    }
  }

  public renderAppointmentRow = ({item, index}) => {
    return (
      <View>
        <Text style={styles.header}>{R.path(["provider", "providerName"], item)}</Text>
        <Row style={styles.appointmentRow}>
          <Col>
            {this.renderAppointmentContent(item)}
          </Col>
          <Col style={styles.appointmentButtonContainer}>
            {/*<Button warning={true} style={styles.appointmentButton}>
              <Text>Edit</Text>
            </Button>*/}
          </Col>
        </Row>
      </View>
    );
  };

  public checkForLocationPermission (): void {

    check(PERMISSIONS.IOS.LOCATION_ALWAYS)
      .then((result) => {
        this.setState({isPermissionGranted: RESULTS.GRANTED == result});
      })
      .catch((error) => {
        // …
      });
      }
    
  public saperatorAppointmentRow = () => (
    <Divider style={{marginHorizontal: 40}}/>
  );

  public appointmentSeeAllClick = () => {
    this.setState({seeAllAppointments: !this.state.seeAllAppointments});
  };

  public renderAppointment = () => {
    if (R.path(["appointments", "length"], this.props)) {
      return (
        <React.Fragment>
          <Row style={[styles.headerContainer, {marginTop: 10}]}>
            <Text style={styles.header}>Upcoming Appointments</Text>
            {/*
            <Text style={styles.seeAll} onPress={this.appointmentSeeAllClick}>{this.state.seeAllAppointments ? "Show Few" : "See All"}</Text>
*/}
          </Row>
          <Divider style={{marginVertical: 0, marginBottom: 10}}/>
          <FlatList
            data={this.props.appointments}
            renderItem={this.renderAppointmentRow}
            ItemSeparatorComponent={this.saperatorAppointmentRow}
          />
        </React.Fragment>
      );
    } else {
      return (
        <Row style={[styles.headerContainer, {marginTop: 10}]}>
          <Text style={styles.header}>No upcoming appointments</Text>
        </Row>
      );
    }
  };

  public renderPieChart = (benefits) => {
    if(this.props.isActive) {
      return (
        <View>
          <Text style={[styles.textBig, {marginTop: 16, textAlign: "center"}]}>Your Deductible</Text>
          <Text style={[styles.textSmall, {marginVertical: 7, textAlign: "center"}]}> You've paid
          {<Text style={styles.priceOrange}>{` ${parseAmount(benefits.deductibleApplied)}`}</Text>} of {parseAmount(benefits.deductibleTotal)} </Text>
                
            <PieChart
              size={300}
              color={Colors.orange}
              progress={(benefits.deductibleApplied / benefits.deductibleTotal) * 100}
              text={parseAmount(benefits.deductibleRemaining)}
            />
                
          <Text style={[styles.textBig, {marginTop: 26, textAlign: "center"}]}>Your Family Deductible</Text>
          <Text style={[styles.textSmall, {marginVertical: 7, textAlign: "center"}]}> You’ve paid
            {<Text
              style={styles.priceGreen}>{` ${parseAmount(benefits.oopApplied)}`}</Text>} of {parseAmount(benefits.oopTotal)}
          </Text>

            <PieChart
              size={300}
              color={Colors.green}
              progress={(benefits.oopApplied / benefits.oopTotal) * 100}
              text={parseAmount(benefits.oopRemaining)}
            />
        </View>
      )
    } else {
      return( 
        <View>
          <Text style={styles.noPieChartData}> Coming Soon </Text>
        </View> 
      )
    } 
  }

  public showAllDeductible = () => Actions.push("deductibles", {summary: this.props.summary});

  public searchFunction = (data) => {    
    this.props.accessTheRight(data)
  }

  public render() {

    const benefits = this.props.benefits;
    var tooltipContent = [];
    if(this.state.toolTipVisible && !this.props.locationModalWindow && this.props.infoMessage && Object.keys(this.props.infoMessage).length > 0 && this.props.infoMessage.key == "review-your-expenses")
    {
      tooltipContent.push(
        <TooltipComponenet 
          position={"top"}
          heading={this.props.infoMessage.title}
          description={this.props.infoMessage.message}
          style={{ top: "3%" }}
        /> 
      )
    }
    return (
      <Container style={styles.container}>
        <Header hideBack={true}/>
        <SearchBar style={ApplicationStyles.shadow}/>
        <KeywordAutoSuggestBox/>
        <ScrollView
          onLayout={(event) => this.setState({posY: event.nativeEvent.layout.y})}
          scrollEnabled={this.state.panScroll}
        >
          <DraggableHandle
            isAccess={true}
            isScrolling={(scroll) => this.setState({panScroll: !scroll})}
            text={"Quickly search for providers"}
            posY={this.state.posY}
            newDesign={false}
            style={{width: "100%"}}
            view={(
              <View style={{
                backgroundColor: Colors.white,
                padding: 20,
                width: "100%",
                paddingVertical: 10,
                flexDirection: "row",
                alignItems: "flex-start",
                justifyContent: "space-between",
              }}>
                <TouchableOpacity style={{justifyContent: "center", alignItems: "center"}}
                                  onPress={() => this.searchFunction("telehealth")}>
                  <Image source={Images.AccessRightTelemedicine} resizeMode={"contain"}
                         onLayout={(event) => this.setState({heightStaticImage: event.nativeEvent.layout.height})}/>
                  <Text style={styles.quickLinkText}>Telemedicine</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{justifyContent: "center", alignItems: "center"}}
                                  onPress={() => this.searchFunction("retail clinic")}>
                  <Image source={Images.AccessRightHospitalER} resizeMode={"contain"}/>
                  <Text style={styles.quickLinkText}>{"Retail\nClinic"}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{justifyContent: "center", alignItems: "center"}}
                                  onPress={() => this.searchFunction("pcp")}>
                  <Image source={Images.AccessRightDoctorsOffice} resizeMode={"contain"}/>
                  <Text style={styles.quickLinkText}>{"Primary\nCare Center"}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{justifyContent: "center", alignItems: "center"}}
                                  onPress={() => this.searchFunction("urgent care")}>
                  <Image source={Images.AccessRightUrgentCare} resizeMode={"contain"}/>
                  <Text style={styles.quickLinkText}>{"Urgent\nCare"}</Text>
                </TouchableOpacity>

              </View>
            )}
            maxHeight={this.state.heightStaticImage + 50}
          />          
          <View style={{padding: 10}}>
            {
              benefits &&
              <React.Fragment>
                <Row style={styles.headerContainer}>
                  <Text style={styles.header}>Deductible Status</Text>
                  {
                    !!this.props.summary &&
                    <Text style={styles.seeAll} onPress={this.showAllDeductible}>See All</Text>
                  }

                </Row>
                <Divider style={{marginVertical: 0}}/>
                {tooltipContent}
                {this.renderPieChart(benefits)}
              </React.Fragment>
            }
            {this.renderAppointment()}
            {
              this.props.pcp &&
              <React.Fragment>
                <Row style={[styles.headerContainer, {marginTop: 16}]}>
                  <Text style={styles.header}>Primary Care Physician</Text>
                </Row>
                <Divider style={{marginVertical: 0}}/>
                <Text style={[styles.textBig, {marginVertical: 7}]}>
                  {this.props.pcp.name}
                </Text>
                <Text style={[styles.textBig]}>
                  {this.props.pcp.addressLine1}, {this.props.pcp.state} {this.props.pcp.zipcode}
                </Text>
                <Text
                  style={[styles.textBig, {textDecorationLine: "underline", color: Colors.textBlue}]}
                  onPress={() => Linking.openURL("tel:" + this.props.pcp.phone)}
                >
                  {formatNumber(this.props.pcp.phone)}
                </Text>
              </React.Fragment>
            }
          </View>

          <Modal isVisible={this.props.locationModalWindow && this.props.openLocationModal && !this.state.isPermissionGranted } style={styles.modalContainer} animationIn={"zoomInDown"} animationOut="zoomOutUp">
            <View style={styles.modalPadding}>
              <LocationConsentScreen />
            </View>
          </Modal>
          <BiometricVerificationScreen />
        </ScrollView>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({
  requestDashboard: () => dispatch(PatientAPIActions.requestDashboard()),
  requestInfoMessage: (key) => dispatch(GetHelpActions.requestInfoMessage(key)),
  requestFixLocation: (location = "Dallas") => dispatch(LocationActions.request(location, true)),
  accessTheRight: (term: string) => dispatch(DashboardActions.requestSpecialtyByKeyword(term)),
  requestLocationCheck: () => dispatch(LocationActions.checkLocationSetThroughGPS()),
});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {
    pcp: state.patient && state.patient.pcp,
    location: state.location && getLocation(state.location),
    locationModalWindow: state.location && state.location.locationModalWindow,
    openLocationModal: state.login && state.login.openLocationModal,
    city: R.path(["patient", "info", "city"], state),
    summary: R.path(["patient", "benefits", "data", "summary"], state),
    isActive: R.path(["patient","benefits","data","active"], state),
    benefits: R.path(["patient", "benefits", "data", "summary"], state) ? {
      deductibleTotal: parseFloat(R.pathOr("0", ["patient", "benefits", "data", "summary", "deductible", "individual", "in_network", "limit", "amount"], state)),
      deductibleApplied: parseFloat(R.pathOr("0", ["patient", "benefits", "data", "summary", "deductible", "individual", "in_network", "applied", "amount"], state)),
      deductibleRemaining: parseFloat(R.pathOr("0", ["patient", "benefits", "data", "summary", "deductible", "individual", "in_network", "remaining", "amount"], state)),
      oopTotal: parseFloat(R.pathOr("0", ["patient", "benefits", "data", "summary", "deductible", "family", "in_network", "limit", "amount"], state)),
      oopApplied: parseFloat(R.pathOr("0", ["patient", "benefits", "data", "summary", "deductible", "family", "in_network", "applied", "amount"], state)),
      oopRemaining: parseFloat(R.pathOr("0", ["patient", "benefits", "data", "summary", "deductible", "family", "in_network", "remaining", "amount"], state)),
    } : undefined,
    appointments: state.patient && state.patient.appointments,
    infoMessage: state.help && state.help.infoMessage,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
