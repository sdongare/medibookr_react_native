import React from "react";
import {Alert, Linking, TextInput} from "react-native";
import Header from "@root/App/Components/Header/Header";
import styles from "./BillHelpScreenStyle";
import {Colors} from "@root/App/Themes";
import {Button, Container, Content, Icon, Picker, Row, Text,} from "@codler/native-base";
import Divider from "@root/App/Components/Divider/Divider";
import {connect} from "react-redux";
import {GetHelpActions, GetHelpRequestParams,} from "@root/App/Reducers/GetHelpReducers";
import {RootState} from "@root/App/Reducers";
import {formatNumber} from "@root/App/Transforms/StringsTranform";
import {PhoneNumber} from "@root/App/Lib/AppHelper";

/**
 * The properties passed to the component
 */
interface OwnProps {}

/**
 * The properties mapped from Redux dispatch
 */
interface DispatchProps {
  sendFeedback?: (params: GetHelpRequestParams) => void;
}

/**
 * The properties mapped from the global state
 */
interface StateProps {
  options?: Array<{ [key: string]: string }>;
}

/**
 * The local state
 */
interface State {
  selectedOption: string;
  query: string;
}

type Props = StateProps & DispatchProps & OwnProps;

class BillHelpScreen extends React.Component<Props, State> {
  public state = {
    selectedOption: "email",
    query: "",
  };
  private content: any;

  public render() {
    return (
      <Container>
        <Header />
        <Content style={styles.content} ref={(ref) => (this.content = ref)}>
          <Text style={styles.header}>
            Get Help Understanding Your Medical Bill
          </Text>
          <Divider style={styles.divider} />
          <Text
            style={[styles.description, { marginTop: 20, marginBottom: 15 }]}
          >
            We are here to help you with any questions or concerns about
            upcoming care, or bills you have received. We know that the medical
            billing process can be confusing and frustrating and our experts are
            standing by. Contact us directly at{" "}
            <Text onPress={this.call} style={styles.phone}>
              {formatNumber(PhoneNumber)}
            </Text>
            , or fill in the contact request below.
          </Text>
          <Text style={[styles.label, { marginBottom: 10 }]}>
            Preferred Contact Method
          </Text>
          <Row>
            <Picker
              placeholder="Select Option"
              placeholderStyle={{ color: Colors.textSuggestion }}
              selectedValue={this.state.selectedOption}
              style={styles.pickerStyle}
              textStyle={styles.pickerText}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({
                  selectedOption: itemValue,
                });
              }}
              mode="dropdown"
            >
              {this.props.options &&
                this.props.options.map((value) => {
                  const key = Object.keys(value)[0];
                  return (
                    <Picker.Item key={key} label={value[key]} value={key} />
                  );
                })}
            </Picker>
            <Icon
              name={"caretdown"} type={"AntDesign"}
              style={{
                color: Colors.black,
                position: "absolute",
                fontSize: 14,
                right: 10,
                alignSelf: "center",
              }}
            />
          </Row>

          <Text style={styles.label}>Questions/Request</Text>
          <Text style={styles.description}>
            Please be as specific as possible.
          </Text>
          <TextInput
            value={this.state.query}
            multiline={true}
            numberOfLines={6}
            onFocus={(event) =>
              setTimeout(() => this.content._root.scrollToEnd(), 500)
            }
            onChangeText={this.onQuestionInput}
            placeholderTextColor={Colors.textSuggestion}
            style={[styles.input, styles.pickerStyle]}
          />
          <Button onPress={this.onPress} info={true} style={styles.button}>
            <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  private call = () => {
    Linking.openURL(`tel:${formatNumber(PhoneNumber)}`);
  };

  private onQuestionInput = (text) => {
    this.setState({
      query: text,
    });
  };

  private onPress = () => {
    if (this.state.query.length === 0) {
      Alert.alert(
        "Provide Message",
        "Please, provide some details on what we can help with.",
      );
    } else {
      this.props.sendFeedback({
        question: this.state.query,
        contactMethod: this.state.selectedOption,
        onSuccess: () => {
          this.setState({
            query: "",
            selectedOption: "email",
          });
          Alert.alert(
            "Message Sent",
            "We have received your message, your request will be resolved as soon as possible.",
          );
        },
      });
    }
  };
}

const mapStateToProps = (state: RootState): StateProps => ({
  options: state.help.contactMethods,
});

const mapDispatchToProps = (dispatch: Function): DispatchProps => ({
  sendFeedback: (params) => dispatch(GetHelpActions.request(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BillHelpScreen);
