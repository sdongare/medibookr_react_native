import * as React from "react";
import { FlatList, Platform, Text, View} from "react-native";
import {connect} from "react-redux";
import * as Redux from "redux";
// Styles
import {Body, Button, Container, Content, Header, Icon, Left, Right} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import {RootState} from "@root/App/Reducers";
import LaunchNavigator from "react-native-launch-navigator";
import Check from "@root/App/Components/Check/Check";
import * as R from "ramda";
import {LatLng} from "react-native-maps";
import {Colors} from "@root/App/Themes";
import Divider from "@root/App/Components/Divider/Divider";
import AsyncStorage from "@react-native-community/async-storage";

/**
 * The properties passed to the component
 */
export interface OwnProps {
  userGps: LatLng;
  providerAddress: string;
}

/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {

}

/**
 * The local state
 */
export interface State {
  selected: number;
  allApp: string[];
  defaultSelected: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;


class GetDirections extends React.Component<Props, State> {
  public state = {
    selected: 0,
    allApp: [],
    defaultSelected: false,
  };

  public async componentDidMount() {
    const allApps = await LaunchNavigator.getAvailableApps();

    const picked = R.pickBy(R.equals(true), allApps);

    if (R.isEmpty(picked)) {
      Actions.pop();
      const options = {
        start: `${this.props.userGps.latitude}, ${this.props.userGps.longitude}`,
      };

      if (Platform.OS === "android") {
        // @ts-ignore
        options.app = LaunchNavigator.APP.GOOGLE_MAPS;
      } else {
        // @ts-ignore
        options.app = LaunchNavigator.APP.APPLE_MAPS;
        // @ts-ignore
        options.launchMode = LaunchNavigator.LAUNCH_MODE.MAPKIT;
      }

      // @ts-ignore
      LaunchNavigator.navigate(this.props.providerAddress, options);
    } else {
      this.setState({
        allApp: R.keys(picked),
      });
    }
  }

  public renderRow = ({item, index}) => {
    return (
      <Check
        color={Colors.textBlue}
        text={LaunchNavigator.getAppDisplayName(item)}
        isCheckProp={index === this.state.selected}
        onClick={() => {
          this.setState({
            selected: index,
          });
        }}
      />
    );
  };

  public getDirections = async () => {
    if (this.state.defaultSelected) {
      await AsyncStorage.setItem("DEFAULT_NAVIGATOR", this.state.allApp[this.state.selected]);
    }

    const options = {
      start: `${this.props.userGps.latitude}, ${this.props.userGps.longitude}`,
    };
    // @ts-ignore
    options.app = this.state.allApp[this.state.selected];
    // @ts-ignore
    LaunchNavigator.navigate(this.props.providerAddress, options);
  };

  public render() {
    return (
      <Container>
        <Header>
          <Left/>
          <Body>
          <Text style={{textAlign: "center"}}>
            Select Navigation Option
          </Text>
          </Body>
          <Right>
            <Button transparent={true} onPress={this.close}>
              <Icon name={"close"}/>
            </Button>
          </Right>
        </Header>

        <Content padder={true}>
          {
            !!this.state.allApp && this.state.allApp.length > 0 &&
            <React.Fragment>
              <FlatList
                data={this.state.allApp}
                extraData={this.state.selected}
                renderItem={this.renderRow}
              />

              <Divider/>
              <Check
                text={"Make this selection default"}
                color={Colors.textDark}
                isCheckProp={this.state.defaultSelected}
                onClick={() => this.setState({defaultSelected: !this.state.defaultSelected})}
              />

              <View style={{flexDirection: "row"}}>
                <Button style={{flex: 1, margin: 5, justifyContent: "center"}} onPress={this.getDirections}>
                  <Text style={{color: Colors.white}}>Get Directions</Text>
                </Button>
                <Button style={{flex: 1, margin: 5, justifyContent: "center"}} onPress={this.close}>
                  <Text style={{color: Colors.white}}>Go Back</Text>
                </Button>

              </View>
            </React.Fragment>

          }

        </Content>

      </Container>
    );
  }

  private close = () => Actions.pop();

}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): DispatchProps => ({});

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(GetDirections) as React.ComponentClass<OwnProps>;
