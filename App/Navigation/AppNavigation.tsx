import * as React from "react";
import {Drawer, Modal, Router, Scene, Stack} from "react-native-router-flux";
import PickerScreen from "@root/App/Containers/PickerScreen/PickerScreen";
import DrawerComponent from "@root/App/Components/DrawerComponent/DrawerComponent";
import SearchScreen from "@root/App/Containers/SearchScreen/SearchScreen";
import ProviderDetail from "@root/App/Containers/ProviderDetail/ProviderDetail";
import ResourcesScreen from "@root/App/Containers/ResourcesScreen/ResourcesScreen";
import BillHelpScreen from "@root/App/Containers/BillHelpScreen/BillHelpScreen";
import PharmacyScreen from "@root/App/Containers/PharmacyScreen/PharmacyScreen";
import PharmacyScreenStage2 from "@root/App/Containers/PharmacyScreenStage2/PharmacyScreenStage2";
import Dashboard from "@root/App/Containers/Dashboard/Dashboard";
import ProviderSearch from "@root/App/Containers/ProviderSearch/ProviderSearch";
import ProfileScreen from "@root/App/Containers/ProfileScreen/ProfileScreen";
import GetHelpScreen from "@root/App/Containers/GetHelpScreen/GetHelpScreen";
import UpdateDependentScreen from "@root/App/Containers/UpdateDependentScreen/UpdateDependentScreen";
import WebContainer from "@root/App/Containers/WebContainer/WebContainer";
import RequestAppointment from "@root/App/Containers/RequestAppointment/RequestAppointment";
import InNetworkInfo from "@root/App/Containers/InNetworkInfo/InNetworkInfo";
import CostEstimateScreen from "@root/App/Containers/CostEstimateScreen/CostEstimateScreen";
import SendFeedbackScreen from "@root/App/Containers/SendFeedBack/SendFeedbackScreen";
import FaqScreen from "@root/App/Containers/FaqScreen/FaqScreen";
import AboutUsScreen from "@root/App/Containers/AboutUsScreen/AboutUsScreen";
import DeductibleDetailsScreen from "@root/App/Containers/DeductibleDetailsScreen/DeductibleDetailsScreen";
import SearchSubViewScreen from "@root/App/Containers/SearchSubViewScreen/SearchSubViewScreen";
import GetDirections from "@root/App/Containers/GetDirections/GetDirections";
import PharmacyScreenStage3 from "@root/App/Containers/PharmacyScreenStage3/PharmacyScreenStage3";
import PcpSelectionScreen from "@root/App/Containers/PcpSelectionScreen/PcpSelectionScreen";
import LocationConsentScreen from "@root/App/Containers/LocationConsentScreen/LocationConsentScreen";
import BiometricVerificationScreen from "@root/App/Containers/BiometricVerificationScreen/BiometricVerificationScreen";

// Manifest of possible screens

const router = () => {
  return (
    <Router backAndroidHandler={() => false}>
      <Modal key="root-modal" hideNavBar={true}>
        <Stack key="root" hideNavBar={true}>
          <Drawer
            key="drawer"
            drawerPosition={"right"}
            initial={true}
            drawerWidth={300}
            contentComponent={DrawerComponent}
            hideNavBar={true}
          >
            <Stack key="main" hideNavBar={true}>
              <Scene
                key="resources"
                hideNavBar={true}
                component={ResourcesScreen}
              />
              <Scene
                key="provider"
                hideNavBar={true}
                component={ProviderSearch}
              />
              <Scene
                key="dashboard"
                hideNavBar={true}
                component={Dashboard}
                initial={true}
              />
              <Scene
                key="deductibles"
                hideNavBar={true}
                component={DeductibleDetailsScreen}
              />
              <Scene
                key="bill-help"
                hideNavBar={true}
                component={BillHelpScreen}
              />
              <Stack key={"pharmacy"} hideNavBar={true}>
                <Scene
                  key="main"
                  initial={true}
                  hideNavBar={true}
                  component={PharmacyScreen}
                />
                <Scene
                  key="stage2"
                  hideNavBar={true}
                  component={PharmacyScreenStage2}
                />
                <Scene
                  key="stage3"
                  hideNavBar={true}
                  component={PharmacyScreenStage3}
                />

              </Stack>

              <Scene
                type="reset"
                key="picker"
                hideNavBar={true}
                component={PickerScreen}
              />
              <Scene
                type="reset"
                key="search"
                hideNavBar={true}
                component={SearchScreen}
              />
              <Scene
                key={"detail"}
                hideNavBar={true}
                component={ProviderDetail}
              />
              <Scene
                key={"profile"}
                hideNavBar={true}
                component={ProfileScreen}
              />
              <Scene
                key={"pcp-selection"}
                hideNavBar={true}
                component={PcpSelectionScreen}
              />
              <Scene
                key={"about"}
                hideNavBar={true}
                component={AboutUsScreen}
              />
              <Scene key="subview" hideNavBar={true} component={SearchSubViewScreen}/>

              <Stack key={"help"} hideNavBar={true}>
                <Scene
                  key={"main"}
                  hideNavBar={true}
                  component={GetHelpScreen}
                  initial={true}
                />
                <Scene
                  key={"update"}
                  hideNavBar={true}
                  component={UpdateDependentScreen}
                />
              </Stack>

              {/* <Tabs
                key="tabbar"
                tabBarPosition={"bottom"}
                tabStyle={{marginHorizontal: 5}}
                labelStyle={{fontSize: 12, color: Colors.white}}
                backToInitial={true}
                swipeEnabled={true}
                showLabel={true}
                activeBackgroundColor={Colors.primary}
                inactiveBackgroundColor={Colors.grey}
              >
                <Scene key="tab_1" component={RegistrationProcess1Screen} initial={true} hideNavBar={true} tabBarLabel="1"/>
                <Scene key="tab_2" component={RegistrationProcess2Screen} hideNavBar={true} tabBarLabel="2"/>
                <Scene key="tab_3" component={RegistrationProcess3Screen} hideNavBar={true} tabBarLabel="3"/>
                <Scene key="tab_4" component={RegistrationProcess4Screen} hideNavBar={true} tabBarLabel="4"/>

              </Tabs> */}
              <Scene key={"link"} hideNavBar={true} component={WebContainer}/>
            </Stack>
          </Drawer>
        </Stack>
        <Scene key="location-consent" hideNavBar={true} component={LocationConsentScreen}/>
        <Scene
          key={"request-appointment"}
          hideNavBar={true}
          component={RequestAppointment}
        />
        <Scene
          key={"network-info"}
          hideNavBar={true}
          component={InNetworkInfo}
        />
        <Scene
          key={"cost-estimate"}
          hideNavBar={true}
          component={CostEstimateScreen}
        />
        <Scene
          key={"feedback"}
          hideNavBar={true}
          component={SendFeedbackScreen}
        />
        <Scene key={"gps-navigation"} hideNavBar={true} component={GetDirections}/>
        <Scene key={"faqs"} hideNavBar={true} component={FaqScreen}/>
        <Scene key="biometric-consent" hideNavBar={true} component={BiometricVerificationScreen}/>
      </Modal>
    </Router>
  );
};

export default router;
