import {RootState} from "@root/App/Reducers";
import * as React from "react";
import {connect} from "react-redux";
import AppNavigation from "./AppNavigation";
import LoginNavigation from "@root/App/Navigation/LoginNavigation";
import RegistrationNavigation from "@root/App/Navigation/RegistrationNavigation";
import {LoginActions} from "@root/App/Reducers/LoginReducers";

// here is our redux-aware smart component

interface IStateProps {
  isLoggedIn?: boolean;
  isRegistration: boolean;
}


interface IDispatchProps {
}

type Props = IStateProps & IDispatchProps

function ReduxNavigation(props: Props) {
  if (props.isRegistration) {
    return <RegistrationNavigation/>;
  } else if (props.isLoggedIn) {
    return (
        <AppNavigation/>
    );
  } else {
    return <LoginNavigation/>;
  }
}

const mapDispatchToProps = (dispatch: any): IDispatchProps => ({
});

const mapStateToProps = (state: RootState): IStateProps => ({
  isLoggedIn: state.login.data !== null,
  isRegistration: state.registration.isProcessing,
});
export default connect(mapStateToProps, mapDispatchToProps)(ReduxNavigation);
