import * as React from "react";
import {View} from "react-native";
import {Router, Scene, Stack} from "react-native-router-flux";
import AsyncStorage from "@react-native-community/async-storage";
import LoginScreen from "@root/App/Containers/LoginScreen/LoginScreen";
import RegisterScreen from "@root/App/Containers/RegisterScreen/RegisterScreen";
import ForgotPasswordScreen from "@root/App/Containers/ForgotPassword/ForgotPasswordScreen";

import BiometricVerificationScreen from "@root/App/Containers/BiometricVerificationScreen/BiometricVerificationScreen";

// Manifest of possible screens
class router extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      installationRouting: 0
    }
  }

  public componentWillMount = async () => {
    let installationView = await AsyncStorage.getItem("installationView");
    if(installationView)
    {
      this.setState({ installationRouting: 1 })
    }
    else {
      this.setState({ installationRouting: 2 })
      AsyncStorage.setItem("installationView", "viewed")
    }    
  }

  render() {
    if(this.state.installationRouting == 0)
    {
      return(<View></View>)
    }
    else if(this.state.installationRouting == 1){
      return (
        <Router backAndroidHandler={() => false}>
          <Stack key="root" hideNavBar={true}>
            <Scene type="reset" key="login" hideNavBar={true} component={LoginScreen}/>
            <Scene key="register" hideNavBar={true} component={RegisterScreen}/>
            <Scene key="biometric-consent" hideNavBar={true} component={BiometricVerificationScreen}/>
            <Scene key="forgot-password" hideNavBar={true} component={ForgotPasswordScreen}/>

          </Stack>
        </Router>
      );
    }
    else {
      return (
        <Router backAndroidHandler={() => false}>
          <Stack key="root" hideNavBar={true}>
            <Scene key="register" hideNavBar={true} component={RegisterScreen}/>
            <Scene key="login" hideNavBar={true} component={LoginScreen}/>
            <Scene key="biometric-consent" hideNavBar={true} component={BiometricVerificationScreen}/>
            <Scene key="forgot-password" hideNavBar={true} component={ForgotPasswordScreen}/>

          </Stack>
        </Router>
      );
    }    
  }  
};

export default router;
