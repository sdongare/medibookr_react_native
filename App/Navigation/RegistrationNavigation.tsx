import * as React from "react";
import {Router, Scene, Stack} from "react-native-router-flux";
import RegistrationProcess1Screen from "@root/App/Containers/RegistrationProcess1Screen/RegistrationProcess1Screen";

// Manifest of possible screens


const router = () => {
  return (
    <Router>
      <Stack key="root" hideNavBar={true}>
        <Scene type="reset" key="main" hideNavBar={true} component={RegistrationProcess1Screen} initial={true}/>
      </Stack>
    </Router>
  );
};

export default router;
