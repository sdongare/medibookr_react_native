import * as R from "ramda";
import StringMask from "string-mask";

const capitalize = R.compose(
  R.join(""),
  R.juxt([R.compose(R.toUpper, R.head), R.tail]),
);

export const joinString = R.curry((separator: string, array) =>
  R.compose(
    R.join(separator),
    R.filter(
      R.compose(
        R.not,
        R.allPass([R.isEmpty, R.isNil]),
      )),
  )(array));

export const capitalizeOrNull = R.ifElse(R.equals(null), R.identity, capitalize);

export const getCityState = (item: { providerCity: string, providerState: string }) => R.join(", ")([item.providerCity, item.providerState]);

export const convertToBreadCrumb = (service: any) => {

  const filterWithKeys = (pred) => R.pipe(
    R.toPairs,
    // @ts-ignore
    R.filter(R.apply(pred)),
    R.fromPairs,
  );

  return R.compose(
    R.join(" >> "),
    R.values,
    filterWithKeys((key, val) => key.startsWith("level") && val !== null && !R.isEmpty(val)),
  )(service);
};

export const formatNumber = (phone) => {
  if (phone) {
    const actual = R.replace(/[^0-9]+/g, "", phone);
    if (actual.length > 6) {
      return new StringMask("(000) 000-0000").apply(actual);
    } else if (actual.length > 3) {
      return new StringMask("(000) 000").apply(actual);
    } else {
      return actual;
    }
  } else {
    return phone;
  }

};

String.prototype.splice = function (idx, rem, str) {
  return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

export const getZipCode = (zipcode: String) => zipcode.length > 5 ? zipcode.splice(5, 0, "-") : zipcode;

export const couponPriceAddress =
  R.compose(
    R.join(", "),
    R.reject(R.isEmpty),
    R.values,
    R.pick(["Address1", "Address2", "City", "PostalCode", "State"]));
