import * as R from "ramda";
import {ReviewsItem} from "@root/App/Services/DataModels/Providers/ProviderByIdResponse";

/**
 *
 * @param reviews
 * return an array containing all ratings 5,4,3,2,1,0 as an array object
 * [[{5},{5}],[{4}],...]
 *
 * @return Array<Array<ReviewItem>>
 */
export const separateReviews = (reviews: ReviewsItem[]) => {
  let myObj = R.groupBy((m: ReviewsItem) => m.rating.toString())(reviews);
  return R.reverse(R.times(u => R.ifElse(R.isNil, R.always([]), R.identity)(myObj[u]), 6));
};

