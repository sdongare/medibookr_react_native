import {LatLng, Region} from "react-native-maps";
import * as R from "ramda";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import supercluster, {Cluster, Supercluster} from "supercluster";

export function getRegion(data: ProviderResultsItem[]): Region | null {

  const latlngData = extractLatLng(data);

  const latArray = R.map(R.prop("latitude"))(latlngData);
  const lngArray = R.map(R.prop("longitude"))(latlngData);

  const minLat = R.reduce(R.min, Infinity)(latArray) as number;
  const maxLat = R.reduce(R.max, -Infinity)(latArray) as number;
  const minLng = R.reduce(R.min, Infinity)(lngArray) as number;
  const maxLng = R.reduce(R.max, -Infinity)(lngArray) as number;

  return {
    latitude: (minLat + maxLat) / 2,
    longitude: (maxLng + minLng) / 2,
    latitudeDelta: maxLat - minLat + 0.2,
    longitudeDelta: maxLng - minLng + 0.2,
  } as Region;

}

export function getRegionPharmacy(data: PriceResponse[]): Region | null {

  const latlngData = extractLatLngPharmacy(data);

  const latArray = R.map(R.prop("latitude"))(latlngData);
  const lngArray = R.map(R.prop("longitude"))(latlngData);

  const minLat = R.reduce(R.min, Infinity)(latArray) as number;
  const maxLat = R.reduce(R.max, -Infinity)(latArray) as number;
  const minLng = R.reduce(R.min, Infinity)(lngArray) as number;
  const maxLng = R.reduce(R.max, -Infinity)(lngArray) as number;

  return {
    latitude: (minLat + maxLat) / 2,
    longitude: (maxLng + minLng) / 2,
    latitudeDelta: maxLat - minLat + 0.2,
    longitudeDelta: maxLng - minLng + 0.2,
  } as Region;

}

export async function getDataInRegion(data: ProviderResultsItem[], region: Region) {
  const bb = [
    (region.longitude - ((region.longitudeDelta) / 2) * 1.1),
    (region.latitude - ((region.latitudeDelta) / 2) * 1.1),
    (region.longitude + ((region.longitudeDelta) / 2) * 1.1),
    (region.latitude + ((region.latitudeDelta) / 2) * 1.1),
  ];

  return data.filter((value) => {
    const lat = parseFloat(value.latitude);
    const long = parseFloat(value.longitude);

    // 1 - 0 North West, 3 - 2  South East Corners
    // Check if lat long falls in Bounding Box
    return (bb[1] <= lat && lat <= bb[3] && bb[0] <= long && long <= bb[2]) || lat === 0;
  });
}

export async function getDataInRegionPharmacy(data: PriceResponse[], region: Region) {
  const bb = [
    (region.longitude - ((region.longitudeDelta) / 2) * 1.1),
    (region.latitude - ((region.latitudeDelta) / 2) * 1.1),
    (region.longitude + ((region.longitudeDelta) / 2) * 1.1),
    (region.latitude + ((region.latitudeDelta) / 2) * 1.1),
  ];

  return data.filter((value) => {
    const lat = parseFloat(value.Address.Latitude.toString());
    const long = parseFloat(value.Address.Longitude.toString());

    // 1 - 0 North West, 3 - 2  South East Corners
    // Check if lat long falls in Bounding Box
    return (bb[1] <= lat && lat <= bb[3] && bb[0] <= long && long <= bb[2]) || lat === 0;
  });
}

export function extractLatLng(data: ProviderResultsItem[]): LatLng[] | null {
  if (R.isNil(data) || R.isEmpty(data)) {
    return null;
  }
  return R.compose(
    R.map((v: ProviderResultsItem) => ({
      latitude: parseFloat(v.latitude),
      longitude: parseFloat(v.longitude),
    })),
  )(data);
}

export function extractLatLngPharmacy(data: PriceResponse[]): LatLng[] | null {
  if (R.isNil(data) || R.isEmpty(data)) {
    return null;
  }
  return R.compose(
    R.map((v: PriceResponse) => ({
      latitude: parseFloat(v.Address.Latitude.toString()),
      longitude: parseFloat(v.Address.Longitude.toString()),
    })),
  )(data);
}


export function getZoomLevel(region: Region) {
  if (!region) {
    return 0;
  }
  const angle = region.longitudeDelta;
  return Math.round(Math.log(360 / angle) / Math.LN2);
}

export async function createCluster(data: ProviderResultsItem[], zoom: number) {
  const cluster = supercluster({
    maxZoom: 11,
    radius: 50,
  });
  return cluster.load(convertToGeoJSON((data)).features);
}

export function filterFromCluster(cluster: Cluster[]): ProviderResultsItem[] {
  return cluster.map((v) => v.properties.item);
}

export function createRegions(cluster: Supercluster, region: Region, zoom: number) {
  return cluster.getClusters([
    region.longitude - (region.longitudeDelta),
    region.latitude - (region.latitudeDelta),
    region.longitude + (region.longitudeDelta),
    region.latitude + (region.latitudeDelta),
  ], zoom);
}

function convertToGeoJSON(data: ProviderResultsItem[]) {
  const features = [];
  data.map((value) => {
    const obj = ({
      type: "Map",
      geometry: {
        type: "Point",
        coordinates: [
          value.longitude,
          value.latitude,
        ],
      },
      properties: {point_count: 0, item: value},
    });
    features.push(obj);
  });

  return {
    type: "MapCollection",
    features,
  };
}
