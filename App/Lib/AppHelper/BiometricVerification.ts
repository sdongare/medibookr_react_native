import {Alert} from "react-native";
import TouchID from "react-native-touch-id";
import AsyncStorage from "@react-native-community/async-storage";
import {Actions} from "react-native-router-flux";

export const DISABLE_DIALOG = "disableDialog";
export const BIOMETRIC_ENABLE_STATUS = "enableStatusBiometric";


export async function hasBiometricOnDevice() {
  return TouchID.isSupported().catch((e) => {
    return false;
  });
}

export default async function checkBiometricAuth() {
  const isRemember = await AsyncStorage.getItem(DISABLE_DIALOG).catch((error) => console.log(error));
  const hasBiometrics = await hasBiometricOnDevice();
  if (!isRemember && hasBiometrics) {
    return true;
  } else {
    return false;
  }
}
