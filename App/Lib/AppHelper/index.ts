import * as R from "ramda";
import {LocationState} from "../../App/Reducers/LocationReducers";
import {Images} from "../../Themes";

export const LOCATION_NOT_FOUND_MESSAGE = "Location not found.\n" +
  "Please make sure location permission is granted from settings.";

export const notEmptyAndNull = (data) => !(data === 0 || R.isEmpty(data) || R.isNil(data));

export const getLocation = (locationState: LocationState) =>
  locationState ? (locationState.selected ? locationState.selected : locationState.gps) : undefined;

export const getGenderImage = (provider) => {
  if (provider.facilityFlag) {
    return Images.Facility;
  } else if (provider.gender === "M") {
    return Images.ProviderMale;
  } else if (provider.gender === "F") {
    return Images.ProviderFemale;
  } else {
    return Images.ProviderUnknown;
  }
};

export const PhoneNumber = "800 822-8936";

