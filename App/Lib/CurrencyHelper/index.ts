export function parseAmount(amount: number | string) {
  /*const formatter =  Intl.NumberFormat("en-US", {
    currency: "USD",
    style: "currency",
    maximumFractionDigits: 0,
    minimumFractionDigits: 0,
  });
*/
  if (amount) {
    // let amnt = amount.toString();

    // for (let i = amnt.length; i > 0; i -= 3) {
    //   if (i !== amnt.length) {
    //     amnt = amnt.splice(i, 0, ",");
    //   }
    // }
    // return "$" + amnt.replace(",.", ".");
    const amntInThousant = amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return `$${amntInThousant}`;
  } else {
    return "$0";
  }

}
