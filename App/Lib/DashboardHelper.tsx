import * as R from "ramda";
import {Colors} from "@root/App/Themes";
import Color from "color";

export const getLevelData = (level: number, items: ResultsItem[], type: string = "service"): { [index: string]: ResultsItem[] } => {
  return R.groupBy<ResultsItem>(R.path([type, `level_${level}`]))(items);
};

export const getKeyMap = (data: any) =>
  R.keysIn(data).map((value, index) => ({title: value, content: data[value], length: data[value].length}));

export function getBGColor(level: number) {
  const color = Color(Colors.primaryLight);

  switch (level) {
    case 0:
      return Colors.primary;
    default:
      return color.lighten(0.15 * level).hex();
  }
}

export const getLastLevel = (item: ResultsItem) => {
  let level = 0;
  while (level < 10) {
    if (!item[`level_${level + 1}`]) {
      return item[`level_${level}`];
    }
    level++;
  }
};

export const hasMoreLevel = (array: ResultsItem[], level: number, type: string = "service") => {

  return !R.all((a) => {
    let levelString = a[type] ? a[type][`level_${level + 2}`] : "";
    return R.isNil(levelString) || R.isEmpty(levelString);
  })(array);

  // return array.length > 0 && array[0][type][`level_${level + 2}`] && array[0][type][`level_${level + 2}`] != null;
};

