import {Images} from "@root/App/Themes";

export const getPharmacyStoreImage = (store: string) => {
  switch (store) {
    case "Walgreens":
      return Images.StoreWalgreens;
    case "Kroger Pharmacy":
      return Images.StoreKroger;
    case "Tom Thumb Pharmacy":
      return Images.StoreTomThumb;
    case "Savon Pharmacy":
      return Images.StoreSavOn;
    case "Walmart Neighborhood Market":
      return Images.StoreWalmartNeighborhood;
    case "CVS Pharmacy":
      return Images.StoreCVS;
    case "Walmart Pharmacy":
      return Images.StoreWalmartPharmacy;
    default:
      return null;
  }
};
