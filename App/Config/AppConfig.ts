// Simple React Native specific changes
import {ResetAPI} from "@root/App/Containers/App";

export enum ENVIRONMENT {
  QA = "qa",
  DEV = "dev",
  PROD = "api",
  DEMO = "demo",
  STAGE = "staging",
}

let CURRENT_ENVIRONMENT = __DEV__ ? ENVIRONMENT.DEV : ENVIRONMENT.PROD;

export function getApiUrl(environment: ENVIRONMENT) {
  switch (environment) {
    case ENVIRONMENT.QA:
    case ENVIRONMENT.DEV:
    case ENVIRONMENT.STAGE:
    case ENVIRONMENT.DEMO:
      return environment + "api";
    case ENVIRONMENT.PROD:
      return "api";
  }
}

(function f() {
  // @ts-ignore
  if (__DEV__ && CURRENT_ENVIRONMENT === ENVIRONMENT.PROD) {
    throw  new Error("Please switch back to QA or DEV");
  }
})();

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  url: () => `https://${getApiUrl(CURRENT_ENVIRONMENT)}.medibookr.com/`,
  googleKey: "AIzaSyBwGvbdE5D3b-MmsHIOtor_hHK5JOKtpKA",
  changeEnvironment: (env: ENVIRONMENT) => {
    CURRENT_ENVIRONMENT = env;
    ResetAPI();
  },
  environment: CURRENT_ENVIRONMENT,
};
