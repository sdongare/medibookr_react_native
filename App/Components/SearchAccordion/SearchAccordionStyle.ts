import {StyleSheet} from "react-native";
import {Metrics, Colors} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
  },
  leaf: {
    padding: Metrics.baseMargin,
    color: Colors.white
  }
});
