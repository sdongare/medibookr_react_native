import * as React from "react";
import {View} from "react-native";
import styles from "./SearchAccordionStyle";
import {getKeyMap, getLevelData, hasMoreLevel} from "../../Lib/DashboardHelper";
import AccordionHeader from "@root/App/Components/AccordionHeader/AccordionHeader";
import AccordionContent from "@root/App/Components/AccordionContent/AccordionContent";
import {Accordion} from "@codler/native-base";
import * as R from "ramda";
import {notEmptyAndNull} from "@root/App/Lib/AppHelper";

interface Props {
  level: number;
  dataArray: ResultsItem[];
  type: string;
}

const getKeyCount = R.compose(R.length, R.keys);

const SearchAccordion: React.SFC<Props> = ({level, type, dataArray}: Props) => {


  const levelData = getLevelData(level, dataArray, type);
  const levelDataNext = getLevelData(level + 1, dataArray, type);
  const keyMap = getKeyMap(levelData);
  const renderHeader = (item, expanded) => {

    const hasContent = notEmptyAndNull(R.path([0, type, `level_${level + 1}`], item.content));
    const hasMoreContent = notEmptyAndNull(R.path([0, type, `level_${level + 2}`], item.content));

    const topElement = level !== 0 && keyMap[0].title === item.title;
    if (hasContent) {
      return (
        <AccordionHeader
          title={`${item.title} (${hasMoreContent ? getKeyCount(levelDataNext) : item.length})`}
          level={level}
          isOpened={expanded}
          isTopElement={topElement}
        />);
    } else {
      return <AccordionContent level={level} item={item.content[0][type]} isTopElement={topElement}/>;
    }
  };

  return (
    <View style={styles.container}>
      <Accordion
        style={{backgroundColor: "transparent"}}
        dataArray={keyMap}
        renderHeader={renderHeader}
        renderContent={
          ((item) => {
            const hasLevel = hasMoreLevel(item.content, level, type);

            if (hasLevel) {
              return (<SearchAccordion dataArray={item.content} level={level + 1} type={type}/>);
            } else {
              return item.content.map((value, index) => {
                return (
                  <AccordionContent key={index} level={level + 1} isTopElement={index === 0} item={value[type]}/>);
              });
            }
          })}
      />
    </View>
  );
};

export default SearchAccordion;
