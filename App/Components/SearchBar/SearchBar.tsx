import * as React from "react";
import {ActivityIndicator, Keyboard, Text, TextInput, View} from "react-native";
import styles from "./SearchBarStyle";
import {Colors} from "@root/App/Themes";
// import {Icon} from "native-base";
import {connect} from "react-redux";
import Icon from "react-native-vector-icons/MaterialIcons";
import {onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, filter, map, mergeMap} from "rxjs/operators";
import {DashboardActions, DashboardSuccessParams} from "@root/App/Reducers/DashboardReducers";
import {RootState} from "@root/App/Reducers";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import {LatLng} from "react-native-maps";
import * as R from "ramda";
import {Actions} from "react-native-router-flux";
import {SearchType} from "@root/App/Containers/SearchScreen/SearchScreen";

interface IProps {
  onSearchInput?: (keyword: string) => void;
  onLocationInput?: (location: string) => void;
  style?: any;
  keyboardOpen?: boolean;
}

interface IStateProps {
  keywordFetch?: boolean;
  data?: DashboardSuccessParams;
  locationFetch?: boolean;
  currentLocation?: LatLng | null;
  locationName?: string | null;
  term?: string;
}

interface IDispatchProps {
  keywordsCall?: (request: string) => void;
  locationCall?: (request: string) => void;
  cancelRequest?: () => void;
  searchForSpeciality: (ids: string[]) => void;
  cancelLocationRequest?: () => void;
}

interface IState {
  term: string
}

type Props = IProps & IDispatchProps & IStateProps;

class SearchBar extends React.Component<Props, IState> {

  private keyword$ = new Subject<string>();
  private location$ = new Subject<string>();

  public setSearchState(text: string) {
    this.setState({term: text})
  }

  constructor(props) {
    super(props);
    this.state = {term: props.term};
    onErrorResumeNext(
      this.keyword$.pipe(
        debounceTime(300),
        map(R.trim),
        filter((value) => value.length >= 4 || R.contains(R.toLower(value),
          ["pcp", "std", "uti", "gi", "ct", "mri", "ent", "flu", "mra", "ob", "gyn", "uti", "ekg", "lab", "ear", "eye"])),
      ),
    ).subscribe((value) => this.props.keywordsCall(value));

    onErrorResumeNext(
      this.location$.pipe(
        debounceTime(1000),
        map(R.trim),
        filter((value) => value.length > 4 || value.length === 0),
      ),
    ).subscribe((value) => {
      this.props.cancelRequest();
      this.props.locationCall(value);
    });
  }


  public componentDidMount() {
    this.props.cancelRequest();
    this.props.cancelLocationRequest();
    setTimeout(() => {
      Keyboard.dismiss();
    }, 100);
  }

  public onSubmit = (e) => {
    const {data} = this.props;
    if (data) {
      const provider = R.pathOr(0, ["provider", "count"], data);
      const speciality = R.pathOr(0, ["speciality", "count"], data);
      const services = R.pathOr(0, ["service", "count"], data);

      console.log(R.compose(R.length, R.filter(R.complement(R.equals(0))))([provider, speciality, services]));

      if (R.not(R.compose(R.gt(1), R.length, R.filter(R.complement(R.equals(0))))([provider, speciality, services]))) {
        this.props.keywordsCall(this.state.term)
        Actions.popTo("landing");
        Actions.push("subview", {data: this.props.data});
      } else if (provider !== 0) {
        this.props.cancelRequest();
        Actions.popTo("landing");
        Actions.push("search", {results: data.provider.results, searchType: SearchType.Provider, item: data.term});
      } else if (speciality !== 0) {
        this.props.cancelRequest();
        Actions.popTo("landing");
        // @ts-ignore
        this.props.searchForSpeciality(R.map(R.prop("specialtyId"), data.speciality.results));
      } else if (services !== 0) {
        this.props.cancelRequest();
        Actions.popTo("landing");
        Actions.push("picker", {list: data.service.results, listType: "service", keyword: this.state.term});
      }
    } else {
      this.props.keywordsCall(this.state.term)
      this.props.cancelRequest();
      Actions.push("subview", {data: this.props.term})
    }
  };

  public clearText = () => {
    this.setState({term: ""});
    this.props.cancelRequest();
  };

  public render() {
    return (
      <View style={[styles.container, this.props.style]}>

        <View style={styles.textContainer}>
          <Text>Find :</Text>
          <TextInput
            style={styles.input}
            placeholder={"Doctor, Procedure, Specialty"}
            underlineColorAndroid={Colors.transparent}
            onChangeText={this.onSearchInput}
            value={this.state.term}
            autoCorrect={false}
            onSubmitEditing={this.onSubmit}
            autoCapitalize={"none"}
            autoFocus={this.props.keyboardOpen ? false : true}
            onBlur={this.blurKeywords}
            placeholderTextColor={Colors.textSuggestion}
          />
          {this.props.keywordFetch && <ActivityIndicator size="small" color={Colors.primary}/>}
          {!!this.state.term && this.state.term.length > 0 &&
          <Icon style={[styles.searchIcon, {paddingHorizontal: 10}]} name={"close"} onPress={this.clearText}/>}

        </View>
        <View style={[styles.textContainer, {marginTop: 1}]}>
          <Text>Location :</Text>
          <TextInput
            style={styles.input}
            autoCorrect={false}
            autoCapitalize={"none"}
            placeholder={this.props.currentLocation ? "Near You" : "Enter City Name"}
            defaultValue={this.props.locationName}
            underlineColorAndroid={Colors.transparent}
            placeholderTextColor={Colors.textDark}
            onBlur={this.blurLocation}
            onChangeText={this.onLocationInput}
          />
          {this.props.locationFetch && <ActivityIndicator size="small" color={Colors.primary}/>}
          {!this.props.locationFetch && <Icon style={styles.searchIcon} name="search"/>}
        </View>

      </View>
    );
  }

  public onSearchInput = (text: string) => {
    this.props.cancelRequest();
    this.setState({term: text});
    this.keyword$.next(text);     
  };

  public onLocationInput = (text: string) => {
    this.props.cancelLocationRequest();
    this.location$.next(text);
  };

  public blurKeywords = () => {
    this.props.cancelRequest();
  };

  public blurLocation = () => {
    this.props.cancelLocationRequest();
  };

}

const mapDispatchToProps = (dispatch: Function): IDispatchProps => ({
  keywordsCall: (request) => dispatch(DashboardActions.request(request)),
  locationCall: (request) => dispatch(LocationActions.request(request, false)),
  cancelRequest: () => dispatch(DashboardActions.cancel()),
  cancelLocationRequest: () => dispatch(LocationActions.cancel()),
  searchForSpeciality: (ids) => dispatch(DashboardActions.requestSpecialties(ids)),
});

const mapStateToProps = (state: RootState): IStateProps => {
  return {
    keywordFetch: state.dashboard.fetching,
    locationFetch: state.location.fetching,
    data: state.dashboard.data,
    currentLocation: state.location.gps,
    locationName: state.location.input,
    term: state.dashboard.term,
  };
};

export default connect<IStateProps, IDispatchProps, IProps>(mapStateToProps, mapDispatchToProps, null, {forwardRef: true})(SearchBar);
