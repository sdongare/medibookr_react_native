import * as React from "react";
import styles from "./PieChartStyle";
import {G, Path, Svg, Text} from "react-native-svg";
import {arc, pie} from "d3-shape";
import {Colors, Fonts} from "@root/App/Themes";

interface Props {
  text: string;
  color: string;
  size: number;
  style?: any;
  progress: number;
}


const PieChart: React.SFC<Props> = ({size, style, text, color, progress}: Props) => {

  const arcs = pie()
    .startAngle(Math.PI / 2)
    .endAngle(-Math.PI / 2)
    ([progress, 100 - progress]);

  const arcGen = arc()
    .outerRadius(size / 2)
    .innerRadius(size / 5);

  return (
    <Svg width={size} height={(size / 2) + 30} style={[styles.container, style]}>
      <G x={size / 2} y={size / 2}>
        {
          arcs.map((gen: any, index) => (
            <React.Fragment>
              <Path
                fill={index === 0 ? color : Colors.grey}
                d={arcGen(gen)}
              />
              <Path
                fill={"transparent"}
                stroke={Colors.grey}
                strokeWidth={1}
                d={arcGen(gen)}
              />
            </React.Fragment>
          ))
        }
      </G>
      <Text
        fill={color}
        fontSize="22"
        fontFamily={Fonts.type.latoBold}
        x={`${size / 2}`}
        y={`${size / 2}`}
        textAnchor="middle"
      >{text}
      </Text>
      <Text
        fill={Colors.textDark}
        fontFamily={Fonts.type.latoRegular}
        fontSize="18"
        x={`${size / 2}`}
        y={`${(size / 2) + 20}`}
        textAnchor="middle"
      >
        Remaining
      </Text>
    </Svg>
  );
};

export default PieChart;
