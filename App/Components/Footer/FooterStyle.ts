import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.primary,
    padding: Metrics.baseMargin
  },
  containerRow: {
    flexDirection: "row",
    margin: Metrics.smallMargin,
    justifyContent: "space-around",
  },
  text: {
    color: Colors.white,
    fontSize: 12,
    textAlign: "center",
    fontFamily: Fonts.type.opensans,
  },
  textBold: {
    fontSize: 12,
    color: Colors.white,
    fontFamily: Fonts.type.openSansSemiBold
  }

});
