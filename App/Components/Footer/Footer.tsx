import * as React from "react";
import {Alert, Linking, Text, View} from "react-native";
import styles from "./FooterStyle";
import AppConfig from "@root/App/Config/AppConfig";
import {Actions} from "react-native-router-flux";

interface Props {
}

const termsUrl = () => AppConfig.url() + "static/terms.html";
const privacyUrl = () => AppConfig.url() + "static/privacy.html";

const termClick = () => {
  Actions.push("link", {uri: termsUrl()});
};

const privacyClick = () => {
  Actions.push("link", {uri: privacyUrl()});
};

const Footer: React.SFC<Props> = ({}: Props) => (
  <View style={styles.container}>
    <View style={styles.containerRow}>
      <Text style={styles.textBold} onPress={termClick}>Terms and Conditions</Text>
      <Text style={styles.textBold} onPress={privacyClick}>Privacy Policy</Text>
    </View>

    <Text style={styles.text}>Copyright © 2017 MediBookr, Inc. All Rights Reserved.</Text>
  </View>
);

export default Footer;
