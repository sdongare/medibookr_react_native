import {StyleSheet} from "react-native";
import {Colors, Metrics, Fonts} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    marginTop: Metrics.baseMargin,
  },
  bar: {
    marginTop: Metrics.smallMargin,
    width: "100%",
    height: 20,
    borderColor: Colors.ratingBorder,
    backgroundColor: Colors.ratingBackground,
    borderWidth: 1,
  },
  barInside: {
    margin: 1,
    position: "absolute",
    height: 20,
  },
  textTitle: {
    fontSize: Fonts.size.h6,
    color: Colors.textDark,
    fontFamily: Fonts.type.latoRegular,
  },
  textRemaining: {
    fontSize: Fonts.size.medium,
    color: Colors.textDark,
    fontFamily: Fonts.type.latoRegular,
  },
  containerDescription: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  textDescriptionOrange: {
    color: Colors.orange,
  },
  textDescription: {
    fontSize: Fonts.size.regular,
    color: Colors.textDark,
    fontFamily: Fonts.type.latoRegular,
  },
  textAmount: {
    textAlign: "center",
    alignSelf: "flex-end",
    fontSize: Fonts.size.h4,
    fontFamily: Fonts.type.latoBold,
    color: Colors.orange,
  },

});
