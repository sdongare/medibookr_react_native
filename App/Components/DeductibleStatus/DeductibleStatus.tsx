import * as React from "react";
import {Text, View} from "react-native";
import styles from "./DeductibleStatusStyle";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";

interface Props {
  title: string;
  total: number;
  deducted: number;
  color: string;
}

const DeductibleStatus: React.SFC<Props> = ({title, color, total, deducted}: Props) => {
  const width = Math.trunc(deducted / total * 100);
  const widthPercent = isNaN(width) ? null : `${width}%`;
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>{title}</Text>
      <View style={styles.bar}>
        <View style={[styles.barInside, {backgroundColor: color, width: widthPercent}]}/>
      </View>
      <View style={styles.containerDescription}>
        <Text style={styles.textDescription}>Paid {<Text style={styles.textDescriptionOrange}>{parseAmount(deducted)}</Text>} of {parseAmount(total)}</Text>
        <Text style={styles.textAmount}>{parseAmount(total - deducted)}{<Text style={styles.textDescription}>{`\nRemaining`}</Text>}</Text>
      </View>
    </View>
  );
};

export default DeductibleStatus;
