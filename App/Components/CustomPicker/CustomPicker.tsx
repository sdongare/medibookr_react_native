import * as React from "react";
import {PickerProps, Text, TextStyle, View, Platform} from "react-native";
import styles from "./CustomPickerStyle";
import {Icon, Picker} from "@codler/native-base";

interface Props extends PickerProps {
  list: any[],
  textStyle?: TextStyle;
}

class CustomPicker extends React.PureComponent<Props> {
  constructor(props) {
    super(props)
    this.state = {
      height: 0,
    }
  }

  render() {
    const {selectedValue, textStyle, list, ...remaining} = this.props;
    return (
      <View style={[styles.container, {alignItems: "center", justifyContent: "center"}]}>

        <Picker
          mode="dropdown"
          style={{width: "100%", opacity: 1}}
          textStyle={styles.pickerText}
          selectedValue={selectedValue}
          {...remaining}
        >
          {list.map((value, index) => (<Picker.Item key={index} label={value} value={index}/>))}
        </Picker>
        {
          Platform.OS == "ios" &&
          <Icon
            name={"down"}
            type={"AntDesign"}
            style={[styles.icon]}
          />
        }        
      </View>
    )
  }
}

export default CustomPicker;
