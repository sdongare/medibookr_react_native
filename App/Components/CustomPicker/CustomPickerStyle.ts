import {StyleSheet} from "react-native";
import {Colors, Fonts} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    height: 26,
    borderWidth: 0.5,
    borderColor: "#989898",
    backgroundColor: Colors.white
  },
  pickerText: {
    width: "100%",
    fontSize: 14,
    fontFamily: Fonts.type.opensans,
    color: "#595959"
  },
  icon: {
    color: Colors.black, position: "absolute", fontSize: 14, right: 10, alignSelf: "center",
  },
  text: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    fontFamily: Fonts.type.opensans, fontSize: 16, color: Colors.textDark, position: "absolute"
  },
});
