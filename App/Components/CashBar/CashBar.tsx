import * as React from "react";
import styles from "./CashBarStyle";
import Colors from "@root/App/Themes/Colors";
import StarRating from "react-native-star-rating";

interface Props {
  rating: number;
  size?: number;
  style?: any;
  color?: string;
  colorDisabled?: string;
  onPress?: (rating) => void;
}

const CashBar: React.SFC<Props> = ({rating, size, onPress, color, colorDisabled, style}: Props) => (
  <StarRating
    disabled={!onPress}
    selectedStar={onPress}
    starSize={size ? size : 30}
    emptyStarColor={colorDisabled ? colorDisabled : Colors.greyStar}
    fullStar={"dollar"}
    emptyStar={"dollar"}
    fullStarColor={color ? color : Colors.primaryLight}
    containerStyle={[styles.container, style]}
    starStyle={styles.star}
    rating={Math.round(rating)}
  />
);

export default CashBar;
