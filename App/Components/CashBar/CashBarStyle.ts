import {StyleSheet} from "react-native";
import Colors from "@root/App/Themes/Colors";
import Metrics from "@root/App/Themes/Metrics";

export default StyleSheet.create({
  container: {
    justifyContent: "flex-start",
  },
  star: {
    marginRight: Metrics.smallMargin,
  },
});
