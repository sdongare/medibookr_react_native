import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  container: {
    position: "absolute",
  },
  headerText: { 
    fontSize: 14,
    fontFamily: Fonts.type.opensans,
    color: "#595959",
    fontWeight: "bold",
    lineHeight: 23
  },
  desriptionText: { 
    fontSize: 14, 
    fontFamily: Fonts.type.opensans,
    color: "#595959",
    lineHeight: 23
  },
  button: {
    backgroundColor: "#084c8d",
    paddingVertical: 3,
    paddingHorizontal: 10
  },
  buttonText: {
    color: Colors.white,
    fontFamily: Fonts.type.opensans,
    fontSize: 14,
  }
});
