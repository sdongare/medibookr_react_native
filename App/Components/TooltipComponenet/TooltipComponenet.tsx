import * as React from "react";
import {Text, View, TouchableOpacity} from "react-native";
import styles from "./TooltipComponenetStyle";
import Tooltip from 'react-native-walkthrough-tooltip';

export interface State {
  toolTipVisible: boolean;
}

export interface StateProps {
  position: string;
  heading: string;
  description: string;
  style: any;
}

type Props = StateProps;

class TooltipComponenet extends React.PureComponent<Props, State> {
  public state = {
    toolTipVisible: true
  }

  render() {
    return (
      <Tooltip
        backgroundStyle={[styles.container, this.props.style]}
        isVisible={this.state.toolTipVisible}
        content={<View style={{ flex: 1, padding: 10 }}>
          <View style={{ flexDirection: "row" }}>
              <Text style={styles.headerText}>
                {this.props.heading}
              </Text>
          </View>
          <View style={{ flexDirection: "row", paddingTop: 5 }}>
              <Text style={styles.desriptionText}>
                {this.props.description}
              </Text>
          </View>
          <View style={{ flexDirection: "row", paddingTop: 15, justifyContent: "flex-end" }}>
              <TouchableOpacity style={styles.button} onPress={() => this.setState({ toolTipVisible: false })}>
                <Text style={styles.buttonText}>
                  Got it
                </Text>
              </TouchableOpacity>
          </View>
        </View>}
        placement={this.props.position}
        backgroundColor={"rgba(0,0,0,0.1)"}
        contentStyle={{ width: 250 }}
        onClose={() => this.setState({ toolTipVisible: false })}
      >
          <View style={{ width: "100%" }}></View>          
      </Tooltip>
    )
  }
}

export default TooltipComponenet;
