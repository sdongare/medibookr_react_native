import * as React from "react";
import {
  Body,
  Container,
  Header,
  Left,
  List,
  ListItem,
  Text
} from "@codler/native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialIcon from "react-native-vector-icons/MaterialCommunityIcons";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import styles from "./DrawerComponentStyle";
import {LoginActions} from "@root/App/Reducers/LoginReducers";
import 'react-native-gesture-handler'

interface IProps {
}

interface IDispatchProps {
  logout?: () => void
}

type Props = IProps & IDispatchProps

enum Menus {
  activity = "My Activity",
  searchProvider = "Search and Compare Providers",
  searchPharmacies = "Compare Pharmacies",
  helpBill = "Get Help with a Medical Bill",
  benefit = "Go to My Benefits Resources",
  profile = "View or Edit My Profile",
  help = "Get Help",
  about = "About",
  logout = "Logout",
}

const DrawerComponent: React.SFC<Props> = ({logout}: Props) => (
  <Container style={styles.container}>
    <Header style={styles.header}/>
    <List
      dataArray={Object.keys(Menus)}
      renderRow={rowData => {
        let iconName = "";
        switch (Menus[rowData]) {
          case Menus.activity:
            iconName = "tachometer";
            break;
          case Menus.profile:
            iconName = "user";
            break;
          case Menus.benefit:
            iconName = "list";
            break;
          case Menus.searchPharmacies:
            iconName = "medkit";
            break;
          case Menus.searchProvider:
            iconName = "search";
            break;
          case Menus.helpBill:
            iconName = "clipboard";
            break;
          case Menus.help:
            iconName = "question-circle-o";
            break;
          case Menus.logout:
            iconName = "logout";
            break;
          case Menus.about:
            iconName = "information-outline";
            break;
          default:
            iconName = "close";
            break;
        }

        return (
          <ListItem
            style={styles.listContainer}
            iconLeft={true}
            button={true}
            noIndent={true}
            onPress={() => {
              switch (Menus[rowData]) {
                case Menus.activity:
                  Actions.reset("dashboard");
                  break;
                case Menus.profile:
                  Actions.push("profile");
                  break;
                case Menus.benefit:
                  Actions.push("resources");
                  break;
                case Menus.searchPharmacies:
                  Actions.push("pharmacy");
                  break;
                case Menus.searchProvider:
                  Actions.push("provider");
                  break;
                case Menus.helpBill:
                  Actions.push("bill-help");
                  break;
                case Menus.help:
                  Actions.push("help");
                  break;
                case Menus.about:
                  Actions.push("about");
                  break;
                case Menus.logout:
                  logout();
                  break;
                default:
                  break;
              }
            }}
          >
            <Left style={styles.iconContainer}>
              {
                Menus[rowData] == "About" || Menus[rowData] == "Logout" ?
                <MaterialIcon style={styles.icon} name={iconName}/>
                :
                <Icon style={styles.icon} name={iconName}/>
              }
            </Left>
            <Body style={styles.bodyContainer}>
            <Text style={styles.text}>{Menus[rowData]}</Text>
            </Body>
          </ListItem>
        );
      }}
    />
  </Container>
);

const mapDispatchToProps = (dispatch: Function): IDispatchProps => ({
  logout: () => dispatch(LoginActions.logout())
});

// @ts-ignore
export default connect(
  null,
  mapDispatchToProps
)(DrawerComponent);
