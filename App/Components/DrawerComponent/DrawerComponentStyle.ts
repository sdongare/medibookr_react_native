import {StyleSheet} from "react-native";
import {Colors} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: Colors.background,
  },
  listContainer: {
    backgroundColor: Colors.searchBg,
    marginTop: 1,
  },
  iconContainer: {
    flex: 0,
  },
  icon: {
    textAlign: "center",
    color: Colors.textHeader,
    fontSize: 15
  },
  bodyContainer: {
    flex: 2
  },
  text: {
    color: Colors.textHeader
  }
});
