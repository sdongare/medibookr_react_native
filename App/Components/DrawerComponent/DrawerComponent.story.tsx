import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import DrawerComponent from "./DrawerComponent";

storiesOf("DrawerComponent", module)
  .add("Default", () => (
    <DrawerComponent />
  ));