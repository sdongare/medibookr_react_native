import * as React from "react";
import {Text} from "react-native";
import styles from "./CustomCheckStyle";
import {Button, Icon} from "@codler/native-base";

interface Props {
  text: string;
  isCheckProp: boolean;
  onClick: () => void;
  color?: string;
  style?: any;
  textStyle?: any;
  checkBoxColor: string;
}

const Check: React.SFC<Props> = ({text, isCheckProp, onClick, style, color, textStyle, checkBoxColor}) =>
  (<Button transparent={true} style={[styles.checkContainer, style]} onPress={onClick}>
    <Icon name={isCheckProp ? "check-box" : "check-box-outline-blank"} type="MaterialIcons" style={[styles.check, {color:  checkBoxColor ? checkBoxColor : null}] }/>
    <Text style={[styles.checkLabel, color ? {color} : null, textStyle]}>{text}</Text>
  </Button>);

export default Check;
