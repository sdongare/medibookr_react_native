import {StyleSheet} from "react-native";
import Metrics from "@root/App/Themes/Metrics";
import Colors from "@root/App/Themes/Colors";

export default StyleSheet.create({
  newAppointmentButton: {
    marginTop: Metrics.doubleBaseMargin,
    width: Metrics.screenWidth * 0.6,
    alignSelf: "center",
    justifyContent: "center",
  },
  newAppointmentText: {
    color: Colors.white,
  },
});
