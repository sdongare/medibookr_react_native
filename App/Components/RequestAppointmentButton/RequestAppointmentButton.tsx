import * as React from "react";
import {Text} from "react-native";
import styles from "./RequestAppointmentButtonStyle";
import {Button} from "@codler/native-base";

interface Props {
  style?: any;
  onPress: () => void;
  text?: string;
}

const RequestAppointmentButton: React.SFC<Props> = ({style, onPress, text}: Props) => (
  <Button style={[styles.newAppointmentButton, style]} info={true} onPress={onPress}>
    <Text style={styles.newAppointmentText}>{text ? text : "Request Appointment"}</Text>
  </Button>
);

export default RequestAppointmentButton;
