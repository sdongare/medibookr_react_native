import * as React from "react";
import Colors from "@root/App/Themes/Colors";
import StarRating from "react-native-star-rating";
import styles from "./RatingStarBarStyle";

interface Props {
  rating: number;
  size?: number;
  style?: any;
  color?: string;
  colorDisabled?: string;
  onPress?: (rating) => void;
}

const RatingStarBar: React.SFC<Props> = ({rating, size, style, color, colorDisabled, onPress}: Props) => (
  <StarRating
    disabled={!onPress}
    emptyStar={"star"}
    selectedStar={onPress}
    starSize={size ? size : 30}
    emptyStarColor={colorDisabled ? colorDisabled : Colors.greyStar}
    fullStarColor={color ? color : Colors.primary}
    containerStyle={[styles.container, style]}
    starStyle={styles.star}
    rating={rating}
  />
);

export default RatingStarBar;
