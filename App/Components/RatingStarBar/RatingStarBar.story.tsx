import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import RatingStarBar from "./RatingStarBar";

storiesOf("RatingStarBar", module)
  .add("Default", () => (
    <RatingStarBar />
  ));