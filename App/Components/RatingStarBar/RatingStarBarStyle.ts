import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    justifyContent: "flex-start",
  },
  star: {
    marginRight: Metrics.smallMargin,
  },
});
