import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import NumericInputBox from "./NumericInputBox";

storiesOf("FilterDistanceSlider", module)
  .add("Default", () => (
    <NumericInputBox />
  ));