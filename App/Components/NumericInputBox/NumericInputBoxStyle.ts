import {StyleSheet} from "react-native";
import {Colors} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";


export default StyleSheet.create({
  inputStyle: {
    width: 50,
    backgroundColor: Colors.white,
    borderWidth: 0.3,
    fontSize: 14
  },
  iconStyle: { 
    fontSize: 13,
    borderWidth: 0.3,
    width: "100%",
    textAlign: "center",
  }
});
