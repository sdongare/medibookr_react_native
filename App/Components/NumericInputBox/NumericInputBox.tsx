import * as React from "react";
import styles from "./NumericInputBoxStyle";
import NumericInput from 'react-native-numeric-input'

interface Props {
  values: number[];
  min: number;
  max: number;
  onValueChange?: (values: number[], type: string) => void;
  type: string;
}

const NumericInputBox: React.SFC<Props> = ({values, onValueChange, min, max, type}) =>
  (<NumericInput           
    inputStyle={styles.inputStyle}        
    iconStyle={styles.iconStyle}
    borderColor={"#d9d9d9"}
    totalHeight={27}
    totalWidth={65}
    minValue={min}
    maxValue={max}
    value={values[0]}
    initValue={values[0]}
    onChange={(value) => onValueChange([value], type)} 
    type={"up-down"}
    editable={false}
  />);

export default NumericInputBox;