import * as React from "react";
import {View, Image} from "react-native";
import styles from "./NoSearchResultModalStyle";
import {Button, Text} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";
import Modal from 'react-native-modal';
import {Images} from "@root/App/Themes";
import {GetHelpActions} from "@root/App/Reducers/GetHelpReducers";

interface IProps {
}

interface IStateProps {
  infoMessage: InfoMessage | undefined;
  openInfoModal: boolean;
}

interface IDispatchProps {
  closeInfoModalFun: () => void;
  openInfoModalFun: () => void;
  requestInfoMessage: (key: string) => void;
}

interface State {
}

type Props = IProps & IDispatchProps & IStateProps;

class NoSearchResultModal extends React.PureComponent<Props, State> {
  
  public closeModal = () => {
    this.props.closeInfoModalFun();
  }

  public componentDidMount = () => {    
    if(!this.props.openInfoModal)
    {
      this.props.openInfoModalFun();
    }    
    if(this.props.infoMessage)
    {
      if(this.props.infoMessage.key != "no-provider-found")
      {
        this.props.requestInfoMessage("no-provider-found");  
      }
    }
    else {
      this.props.requestInfoMessage("no-provider-found");
    }      
  }

  render(){
    if (this.props.infoMessage && this.props.infoMessage.key === "no-provider-found") {
      return (
        <Modal customBackdrop={<View style={{flex: 1, backgroundColor: "rgba(0,0,0,0.4)"}} />} isVisible={this.props.openInfoModal} animationIn={"zoomInDown"} animationOut="zoomOutUp">          
          <View style={styles.modalContainer}>
            <View style={styles.closeIconBackground}></View>                      
            <Button style={styles.closeButton}
              onPress={()=> this.closeModal()}
              transparent={true}
            >
              <Image source={Images.CloseCircle} style={styles.closeIcon} />
            </Button>
            <View style={styles.modalBody}>            
              <View style={styles.modalView}>
                <Text style={styles.modalHeading}>
                  {
                    this.props.infoMessage && this.props.infoMessage.title
                  }
                </Text>
              </View>
              <View style={[styles.modalView, { paddingVertical: 20 }]}>
                <Image source={Images.MailCircle} style={styles.mailIcon} />
              </View>
              <View style={styles.modalView}>
                <Text style={styles.modalMessage}>
                  {
                    this.props.infoMessage && this.props.infoMessage.message
                  }
                </Text>
              </View>
              <View style={[styles.modalView, { paddingVertical: 20, paddingBottom: 5 }]}>
                <Button style={{ backgroundColor: "#084c8d" }} onPress={()=> {
                  this.closeModal()
                  Actions.push("help")
                }}>
                  <Text style={styles.textContactUs}>Contact Us</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      )
    }
    else {
      return (<View />)
    }      
  }
};

const mapDispatchToProps = (dispatch: Function): IDispatchProps => ({
  closeInfoModalFun: () => dispatch(GetHelpActions.closeInfoModal()),
  openInfoModalFun: () => dispatch(GetHelpActions.openInfoModal()),
  requestInfoMessage: (key) => dispatch(GetHelpActions.requestInfoMessage(key)),
});

const mapStateToProps = (state: RootState): IStateProps => {
  return {
    infoMessage: state.help && state.help.infoMessage,
    openInfoModal: state.help && state.help.openInfoModal,
  };
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(NoSearchResultModal);
