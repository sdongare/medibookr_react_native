import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  modalContainer: { 
    backgroundColor: Colors.white,
    position: "absolute",
    borderRadius: 15,
    top: "15%",
    width: "100%"
  },
  closeButton: {
    position: "absolute", 
    right: "-4%",
    top: "-4%",
    width: 48,
    height: 48,
    zIndex: 2
  },
  closeIconBackground: {
    width: 40,
    height: 40,
    backgroundColor: Colors.white,
    borderRadius: 40/2,
    position: "absolute", 
    right: "-3%",
    top: "-3%",
  },
  closeIcon: {
    width: 48,
    height: 48,
    resizeMode: "contain",                
  },
  modalBody: {
    flex: 1,
    padding: 20,
    paddingTop: 30
  },
  modalView: {
    flexDirection: "row",
    justifyContent: "center"
  },
  modalHeading: { 
    textAlign: "center",
    fontSize: 16,
    fontFamily: Fonts.type.opensans,
    fontWeight: "600",
    lineHeight: 25,
    color: "#262626"
  },
  mailIcon: {
    width: 151,
    height: 95,
    resizeMode: "contain",                
  },
  modalMessage: { 
    textAlign: "center",
    fontSize: 14,
    fontFamily: Fonts.type.opensans,
    lineHeight: 22,
    color: "#595959"
  },
  textContactUs: {
    fontSize: 16,
    fontFamily: Fonts.type.opensans,
  },
  textTotal: {
    color: Colors.textSuggestion,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.emphasis,
  },
  text: {
    color: Colors.textSuggestion,
    fontFamily: Fonts.type.openSansSemiBold,
    fontSize: Fonts.size.medium,
  },
});
