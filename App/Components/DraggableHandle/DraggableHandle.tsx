import * as React from "react";
import {View} from "react-native";
import Handle from "@root/App/Components/DraggableHandle/Handle";
import {interval, Subject} from "rxjs";
import {distinctUntilChanged, skipUntil} from "rxjs/operators";
import {HANDLE_HEIGHT} from "@root/App/Components/DraggableHandle/DraggablehandleStyle";

interface Props {
  isAccess: boolean;
  maxHeight: number;
  posY: number;
  text: string;
  style?: any;
  view?: React.ReactNode;
  onHeightChanged?: (height: number) => void;
  isScrolling?: (scroll: boolean) => void;
  newDesign: boolean;
}

interface State {
  expanding: boolean;
  height: number;
}

class DraggableHandle extends React.PureComponent<Props, State> {

  private heightSubject = new Subject<number>();

  constructor(props) {
    super(props);

    this.heightSubject.pipe(
      distinctUntilChanged(),
      skipUntil(interval(1000)),
    ).subscribe((height) => {
      if (this.props.onHeightChanged) {
        this.props.onHeightChanged(height);
      }

      this.setState({
        height,
        expanding: height > HANDLE_HEIGHT,
      });
    });

    this.state = {
      expanding: false,
      height: 0,
    };
  }

  public render(): React.ReactNode {
    return (
      <View style={[this.props.style]}>
        <View style={{height: this.state.height, overflow: "hidden"}}>
          {this.props.view}
        </View>

        <Handle
          text={this.props.text}
          scrolling={this.props.isScrolling}
          expanding={this.state.expanding}
          newDesign={this.props.newDesign}
          expand={() => {
            if (this.state.height <= HANDLE_HEIGHT) {
              this.heightSubject.next(this.props.maxHeight);
            } else {
              this.heightSubject.next(0);
            }
          }}
          isAccess={this.props.isAccess}
          onMove={(dy) => {
            let height = dy - this.props.posY;
            height = height <= 0 ? 0 : height;
            height = height >= this.props.maxHeight ? this.props.maxHeight : height;
            this.heightSubject.next(height);
          }}
        />
      </View>

    );
  }
}

export default DraggableHandle;
