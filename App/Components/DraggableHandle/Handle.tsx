import * as React from "react";
import {PanResponder, Text, View} from "react-native";
import styles from "./DraggablehandleStyle";
import {Icon} from "@codler/native-base";
import {Colors, Fonts} from "@root/App/Themes";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";

interface Props {
  text: string;
  isAccess: boolean;
  onMove: (dy: number) => void;
  expanding: boolean;
  expand: () => void;
  scrolling: (scroll: boolean) => void;
  newDesign: boolean;
}

interface State {

}

class Handle extends React.PureComponent<Props, State> {

  private panResponder;

  constructor(props) {
    super(props);
    this.state = {};

    // Initialize PanResponder with move handling
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gesture) => true,
      onPanResponderRelease: (e, gestureState) => {
        if (gestureState.dy === 0) {
          this.props.expand();
        }
        if (this.props.scrolling) {
          this.props.scrolling(false);
        }
      },
      onPanResponderMove: (event, gestureState) => {
        this.props.onMove(gestureState.moveY);
        if (this.props.scrolling) {
          this.props.scrolling(true);
        }
      },
    });
  }

  public render(): React.ReactNode {    
    return (
      <View
        style={[styles.handleContainer, ApplicationStyles.shadow, {backgroundColor: this.props.isAccess ? Colors.magento : Colors.white}]}
        {...this.panResponder.panHandlers}
      >
        <Text style={{position: "absolute", left: 10, color: this.props.isAccess ? Colors.white : Colors.magento, fontSize: 16, fontFamily: Fonts.type.opensans }}>{this.props.text}</Text>
        <Icon style={[styles.arrowIcon, {color: this.props.isAccess ? Colors.white : Colors.magento}]} name={this.props.expanding ? "angle-double-up" : "angle-double-down"} type={"FontAwesome"}/>
        {
          !this.props.newDesign &&
          <Text>
            <Icon style={[styles.arrowIcon, {color: this.props.isAccess ? Colors.white : Colors.magento}]} name={this.props.expanding ? "angle-double-up" : "angle-double-down"} type={"FontAwesome"}/>
            <Icon style={[styles.arrowIcon, {color: this.props.isAccess ? Colors.white : Colors.magento}]} name={this.props.expanding ? "angle-double-up" : "angle-double-down"} type={"FontAwesome"}/>
          </Text>
        }          
      </View>
    );      
  }
}

export default Handle;
