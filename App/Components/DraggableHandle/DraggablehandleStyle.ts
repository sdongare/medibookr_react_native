import {StyleSheet} from "react-native";
import Colors from "@root/App/Themes/Colors";

export const HANDLE_HEIGHT = 40;

export default StyleSheet.create({
  handleContainer: {
    height: HANDLE_HEIGHT,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  arrowIcon: {
    fontSize: 30,
    marginLeft: 3,
    marginRight: 3,
    color: Colors.magento,
  },
});
