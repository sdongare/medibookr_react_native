import {StyleSheet} from "react-native";
import {Metrics, Colors, Fonts} from "@root/App/Themes";

const text = {
  color: Colors.white,
  fontSize: Fonts.size.medium,
};

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: Metrics.smallMargin,
    paddingVertical: Metrics.smallMargin,
  },
  text,
  textBold: {
    ...text,
    fontWeight: "bold",
  },
  svgContainer: {
    position: "absolute",
  },

});
