import * as React from "react";
import {Text, View} from "react-native";
import styles from "./AccordionHeaderStyle";
import {getBGColor} from "@root/App/Lib/DashboardHelper";
import Svg, {Path} from "react-native-svg";
import {Button} from "native-base";

interface Props {
  level: number;
  title: string;
  isOpened: boolean;
  isTopElement: boolean;
}

interface IState {
  dimension: { width: number, height: number };
}

class AccordionHeader extends React.Component<Props, IState> {
  public state = {
    dimension: undefined,
  };

  public onLayout = (event) => {
    if (this.state.dimension) {
      return;
    } // layout was already called
    const {width, height} = event.nativeEvent.layout;
    this.setState({dimension: {width, height}});
  };

  public render() {
    const {level, isTopElement, isOpened, title} = this.props;
    const bg = getBGColor(level);
    const bg2 = getBGColor(level - 1);
    const width = this.state.dimension && this.state.dimension.width;
    const height = this.state.dimension && this.state.dimension.height;

    return (
      <View style={[styles.container, !isTopElement ? {backgroundColor: bg, marginTop: 2} : null]} onLayout={this.onLayout}>
        {isTopElement && this.state.dimension &&
        <Svg width={`${width}`} height={`${height}`} viewBox={`0 0 ${width} ${height}`} style={styles.svgContainer}>
          <Path
            d={`M20 2l10 10 10-10 ${width - 40} 0 0 ${height} -${width} 0 0 -${height}z`}
            fill={bg}
          />
          <Path
            d={`M22 0l8 9 8-9z`}
            fill={bg2}
          />

        </Svg>
        }
        <Text style={[isOpened ? styles.textBold : styles.text, isTopElement ? {paddingTop: 5} : null]}>{title}</Text>
      </View>
    );
  }
}

export default AccordionHeader;
