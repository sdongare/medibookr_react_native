import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import AccordionHeader from "./AccordionHeader";

storiesOf("AccordionHeader", module)
  .add("Default", () => (
    <AccordionHeader />
  ));