import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    margin: Metrics.baseMargin,
    padding: Metrics.baseMargin,
    ...ApplicationStyles.shadow,
    position: "absolute", zIndex: 4, left: 0, right: 0, elevation: 4,
  },  
  textContactUs: {
    fontSize: 16,
    fontFamily: Fonts.type.opensans,
  },
  textTotal: {
    color: Colors.textSuggestion,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.emphasis,
  },
  text: {
    color: Colors.textSuggestion,
    fontFamily: Fonts.type.openSansSemiBold,
    fontSize: Fonts.size.medium,
  },
});
