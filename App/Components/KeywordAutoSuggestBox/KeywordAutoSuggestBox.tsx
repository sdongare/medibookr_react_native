import * as React from "react";
import {ScrollView, View, TouchableOpacity, Image} from "react-native";
import styles from "./KeywordAutoSuggestBoxStyle";
import {DashboardActions, DashboardSuccessParams} from "@root/App/Reducers/DashboardReducers";
import {Button, ListItem, Text} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";
import GeoLocationResponse from "@root/App/Services/DataModels/GeoLocation";
import {LocationActions} from "@root/App/Reducers/LocationReducers";
import * as R from "ramda";
import {SearchType} from "@root/App/Containers/SearchScreen/SearchScreen";
import {Fonts} from "@root/App/Themes";
import NoSearchResultModal from '@root/App/Components/NoSearchResultModal';

interface IProps {
  extraMargin?: number;
}

interface IStateProps {
  data?: DashboardSuccessParams | null;
  keyword?: string;
  mapData?: GeoLocationResponse | null;
}

interface IDispatchProps {
  keywordsCall?: (request: string) => void;
  cancelRequest?: () => void;
  searchForSpeciality: (name: string) => void;
  selectLocation?: (keyword, location) => void;
}

type Props = IProps & IDispatchProps & IStateProps;

const KeywordAutoSuggestBox: React.SFC<Props> = ({data, mapData, searchForSpeciality, keyword, keywordsCall, cancelRequest, selectLocation, extraMargin}: Props) => {
  if (!data && !mapData) {
    return null;
  }

  if (data) {
    const provider = R.pathOr(0, ["provider", "count"], data);
    const speciality = R.pathOr(0, ["speciality", "count"], data);
    const services = R.pathOr(0, ["service", "count"], data);

    const total = (provider + services + speciality);

    const replaceX = (text, keyword) => {
      if (R.contains(keyword, text)) {
        const splitted = R.split(keyword, text)
        return splitted.map(((value, index) => {
          return [
            index !== 0 ?
              <Text
                key={index}
                style={[styles.text, {textDecorationLine: "underline", fontFamily: Fonts.type.opensansBold,}]}
              >{keyword}
              </Text> : null,
            value
          ];
        }))
      } else {
        return text;
      }
    }

    const renderSpecialty = (item) => {
      return (
        <ListItem
          noBorder={true}
          iconRight={true}
          button={true}
          onPress={() => {
            cancelRequest();
            searchForSpeciality(item);
          }}
        >
          <Text style={styles.text}>{replaceX(item, keyword)}</Text>
        </ListItem>
      )
    };
    if(total)
    {
      return (      
        <View style={[styles.container, {top: 150 + (extraMargin || 0)}]}>
          <Text style={styles.textTotal}>{total ? `${total} results for "${keyword}"` : "No result found"}</Text>
          <ScrollView style={{maxHeight: 250}} keyboardShouldPersistTaps={"always"}>
            <ListItem
              noBorder={true}
              iconRight={true}
              button={true}
              onPress={() => {
                if (provider === 0) {
                  return;
                }
                cancelRequest();
  
                Actions.popTo("landing");
                Actions.push("search", {results: data.provider.results, searchType: SearchType.Provider, item: keyword});
              }}
            >
              <Text style={styles.text}>{replaceX(`Providers with ${keyword} in their names`, keyword)}</Text>
            </ListItem>
            {
              services !== 0 &&
              <ListItem
                noBorder={true}
                iconRight={true}
                button={true}
                onPress={() => {
                  cancelRequest();
                  Actions.popTo("landing");
                  Actions.push("picker", {list: data.service.results, listType: "service", keyword});
                }}
              >
                <Text style={styles.text}>{replaceX(`Find procedures for ${keyword}`, keyword)}</Text>
              </ListItem>
            }
            {
              !!speciality &&
              <>
                <Text style={styles.textTotal}>Specialties found</Text>
                {
                  R.compose(R.map(renderSpecialty), R.keys, R.path(["speciality", "results"]))(data)
                }
              </>
  
            }
          </ScrollView>
        </View>            
      );
    }
    else {      
      return (
        <NoSearchResultModal />
      )
    }
  } else if (mapData) {
    return (
      <View style={[styles.container, {top: 150 + (extraMargin || 0)}]}>
        {
          mapData.results.slice(0, mapData.results.length >= 5 ? 5 : mapData.results.length)
            .map((v) => (
              <ListItem
                noBorder={true}
                iconRight={true}
                button={true}
                onPress={() => {
                  selectLocation(v.formatted_address, v.geometry.location);
                  if (keyword && keyword.length > 0) {
                    keywordsCall(keyword);
                  }
                }}
              >
                <Text style={styles.text}>{v.formatted_address}</Text>
              </ListItem>
            ))
        }
      </View>
    );
  }
};

const mapDispatchToProps = (dispatch: Function): IDispatchProps => ({
  cancelRequest: () => dispatch(DashboardActions.cancel()),
  keywordsCall: (request) => dispatch(DashboardActions.request(request)),
  selectLocation: (keyword, location) => dispatch(LocationActions.selected({keyword, location})),
  searchForSpeciality: (name) => dispatch(DashboardActions.requestSpecialtyByKeyword(name)),
});

const mapStateToProps = (state: RootState): IStateProps => {
  return {
    keyword: state.dashboard.term,
    data: state.dashboard.data,
    mapData: state.location.data,
  };
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(KeywordAutoSuggestBox);
