import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import KeywordAutoSuggestBox from "./KeywordAutoSuggestBox";

storiesOf("KeywordAutoSuggestBox", module)
  .add("Default", () => (
    <KeywordAutoSuggestBox />
  ));