import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  footerText: {
    textAlign: "center",
    fontSize: Fonts.size.small,
    color: Colors.white,
    fontFamily: Fonts.type.opensans
  },
  footerBody: {
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    paddingTop: 10
  },
  footerDivider: {
    flex: 5,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  footerContainer: {
    paddingTop: 60,
    flex: 1
  },  
});
