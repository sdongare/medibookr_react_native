import * as React from "react";
import {Linking, Text, View, Platform, TouchableOpacity} from "react-native";
import styles from "./FooterStyle";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Colors } from "react-native/Libraries/NewAppScreen";
import { Button } from "@codler/native-base";
import AppConfig from "@root/App/Config/AppConfig";
import {Actions} from "react-native-router-flux";
import DeviceInfo from "react-native-device-info";

interface Props {
}

const termClick = () => {
  Linking.openURL(AppConfig.url() + "static/terms.html");
};

const privacyClick = () => {
  Linking.openURL(AppConfig.url() + "static/privacy.html");
};

const phoneClick = () => {
    var phoneNumber = "";
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:(800) 822-8936`;
    }
    else  {
      phoneNumber = `tel:(800) 822-8936`;
    }         
    Linking.openURL(phoneNumber);
};

const mailClick = () => {
  Linking.openURL('mailto:support@medibookr.com');
};

const Footer: React.SFC<Props> = ({}: Props) => (
  <View style={styles.footerContainer}>
    <View style={styles.footerBody}>
      <View style={[styles.footerDivider, { flex: 4.5 }]}> 
        <Button transparent={true} style={[styles.footerDivider, {height: 30 }]} onPress={phoneClick}>
          <Icon
            name={"phone"}
            style={{ fontSize: 18, color: Colors.white }}
          />
          <Text style={styles.footerText}>               
            {"   "}(800) 822-8936
          </Text>
        </Button>               
      </View>
      <View style={[styles.footerDivider, { flex: 5.5 }]}>
        <Button transparent={true} style={[styles.footerDivider, {height: 30 }]} onPress={mailClick}>
          <Icon
            name={"email"}
            style={{ fontSize: 18, color: Colors.white }}
          />
          <Text style={styles.footerText}>               
            {"   "}support@medibookr.com
          </Text>
        </Button>
      </View>
    </View>
    <View style={styles.footerBody}>
      <Text style={styles.footerText}>Copyright © 2021 MediBookr, Inc. All Rights Reserved.</Text>
    </View>   
    <View style={styles.footerBody}>
      <View style={styles.footerDivider}>
        <TouchableOpacity onPress={termClick}>
          <Text style={[styles.footerText, { textDecorationLine: "underline" }]}>Terms of Service</Text>
        </TouchableOpacity>        
      </View>
      <View style={styles.footerDivider}>
        <TouchableOpacity onPress={privacyClick}>
          <Text style={[styles.footerText, { textDecorationLine: "underline" }]}>Privacy Policy</Text>
        </TouchableOpacity>
      </View>
    </View>
    <View style={styles.footerBody}>
      <Text style={styles.footerText}>V {DeviceInfo.getReadableVersion()}</Text>
    </View>                 
</View>   
);

export default Footer;
