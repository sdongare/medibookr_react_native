import * as React from "react";
import {Text, View} from "react-native";
import styles from "./DividerStyle";

interface Props {
  style?: any;
}

const Divider: React.SFC<Props> = ({style}: Props) => (
  <View style={[styles.container, style]}/>
);

export default Divider;
