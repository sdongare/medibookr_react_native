import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import Divider from "./Divider";

storiesOf("Divider", module)
  .add("Default", () => (
    <Divider />
  ));