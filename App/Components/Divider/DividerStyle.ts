import { StyleSheet } from "react-native";
import {Metrics, Colors} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.border,
    flex: 1,
    marginVertical: Metrics.baseMargin,
    height: Metrics.horizontalLineHeight,
  },
});
