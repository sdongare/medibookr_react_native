import * as React from "react";
import {Image, ImageBackground, Platform, Text, TouchableOpacity, View} from "react-native";
import styles from "./SearchItemStyle";
import {Colors, Images} from "@root/App/Themes";
import {Card, CardItem, Col, Grid, Row} from "@codler/native-base";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {connect} from "react-redux";
import {ProviderActions} from "@root/App/Reducers/ProviderReducers";
import * as Redux from "redux";
import {RootState} from "@root/App/Reducers";
import {getGenderImage, notEmptyAndNull} from "@root/App/Lib/AppHelper";
import RatingStarBar from "@root/App/Components/RatingStarBar/RatingStarBar";
import CashBar from "@root/App/Components/CashBar";
import {Actions} from "react-native-router-flux";
import * as R from "ramda";
import {parseAmount} from "@root/App/Lib/CurrencyHelper";
import FastImage from "react-native-fast-image";
import {SearchType} from "@root/App/Containers/SearchScreen/SearchScreen";
import Tooltip from 'react-native-walkthrough-tooltip';

interface IProps {
  item: ProviderResultsItem;
  index: string;
  searchType: SearchType;
}


interface IDispatch {
  callProviderById?: (id: string) => void;
}

interface IStateToProps {
  term: string;
}

interface State {
  toolTipVisible: boolean;
}

type Props = IProps & IDispatch & IStateToProps ;

class SearchItem extends React.PureComponent<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      toolTipVisible: false
    }
  }

  public render() {
    const {item} = this.props;
    let name = item.providerName;
    if(name)
    {
      name = item.providerName.replace(/,/g, ', ')
    }
    return (
      <Card style={styles.container}>
        <CardItem
          style={item.inNetwork ? styles.inNetworkBorder : null}
          button={true}
          onPress={this.providerById}
        >
          <Grid>
            <Row style={{paddingHorizontal: 5}}>
              <Col style={{marginTop: 10, marginRight: 5, flex: 0}}>
                {
                  !!item.preferredFlag ?
                    <Tooltip
                      placement={"top"}
                      isVisible={this.state.toolTipVisible}
                      content={
                        <View style={{ flex: 1, padding: 10 }}>
                          <View style={{ flexDirection: "row" }}>
                              <Text style={styles.headerText}>
                                Best Value
                              </Text>
                          </View>
                          <View style={{ flexDirection: "row", paddingTop: 5 }}>
                              <Text style={styles.desriptionText}>
                                The “Best Value” label represents the top tier providers according to our analysis of quality of care and cost to you according to your benefit plan.
                              </Text>
                          </View>
                          <View style={{ flexDirection: "row", paddingTop: 15, justifyContent: "flex-end" }}>
                              <TouchableOpacity style={styles.button} onPress={() => this.setState({ toolTipVisible: false })}>
                                <Text style={styles.toolTipbuttonText}>
                                  Got it
                                </Text>
                              </TouchableOpacity>
                          </View>
                        </View>
                      }
                      backgroundColor={"rgba(0,0,0,0)"}
                      contentStyle={{ position: "relative", width: 250, elevation: 2 }}
                      backgroundStyle={{ position: "relative", top: Platform.OS == "ios" ? "7%" : "8%", zIndex: 2 }}
                      onClose={() => this.setState({ toolTipVisible: false })}
                    >
                      <Image
                        source={getGenderImage(item)}
                        style={styles.image}
                        resizeMode={"contain"}
                      />
                      <TouchableOpacity style={styles.imageBestValue} onPress={()=> this.setState({ toolTipVisible: true })}>
                        <Image source={Images.BestValue} style={styles.imageBestValue} resizeMode={"contain"}/>
                      </TouchableOpacity>                               
                    </Tooltip>
                    :
                    <Image
                      source={getGenderImage(item)}
                      style={styles.image}
                      resizeMode={"contain"}
                    />
                }                
              </Col>
              <Col size={2}>
                <Text
                  style={styles.txtDoctorName}
                  numberOfLines={2}
                >{`${name}${item.credentials ? ", " + item.credentials : ""}`}
                </Text>
                <View style={styles.spaceBetween}>
                  <Text
                      style={styles.text}
                    >{notEmptyAndNull(item.age) && parseInt(item.age, 10) > 0 ? "Age: " + item.age : ""}
                  </Text>
                  <Text
                    lineBreakMode={"middle"}
                    style={[styles.text, {flexWrap: "wrap", flex: 1, textAlign: "right"}]}
                  >{item.specialty}
                  </Text>
                </View>
                <View style={styles.spaceBetween}> 
                  <Text style={styles.text}></Text>                 
                  <Text style={styles.text}>{item.providerCity && item.providerCity.trim()}, {item.providerState}</Text>
                </View>
              </Col>
            </Row>
            <Row style={{marginTop: 5}}>
              {
                /*this.props.term !== "telehealth" &&
                <Col pointerEvents={"none"} style={{aspectRatio: 1}}>
                  <FastImage
                    source={notEmptyAndNull(item.mapTileUrl) ? {uri: item.mapTileUrl} : Images.MapUnavailable}
                    resizeMode={"cover"}
                    style={{width: "100%", height: "100%"}}
                  /> 

                  <View style={[styles.milesContainer]}>
                    <Text style={[styles.milesValue]}>{item.distance && item.distance.toFixed(1)}</Text>
                    <Text style={[styles.milesText]}>miles</Text>
                  </View>
                </Col>*/
              }
              <Col style={styles.requestContainer}>
                {this.renderAppointmentButton()}
              </Col>
              <Col
                style={[styles.estimateContainer, {backgroundColor: item.inNetwork ? Colors.estimateContainerBg : Colors.requestBg}]}
              >
                <View style={{flex: 1, padding: 5}}>
                  {!!item.inNetwork && <Text style={styles.inNetworkText}>In Network</Text>}
                  {this.costEstimateLogic()}
                </View>
              </Col>
            </Row>

          </Grid>
        </CardItem>
      </Card>
    );
  }

  private renderAppointmentButton = () => {
    return (
      <TouchableOpacity style={styles.buttonRequest} onPress={this.onRequestPress}>
        <Text style={styles.buttonText}>{"Request\nAppointment"}</Text>
      </TouchableOpacity>
    );
  };

  private renderCopay = () => {
    const textColor = !!this.props.item.inNetwork ? Colors.white : Colors.black;
    return (
      <View>
        <Text style={[styles.priceText, {color: textColor}]}>{parseAmount(parseFloat(this.props.item.copay))}</Text>
        <Text style={[styles.copayText, {color: textColor}]}>Copay</Text>
      </View>
    )
  };

  private renderOOP = () => {

    const textColor = !!this.props.item.inNetwork ? Colors.white : Colors.black;

    return (
      <React.Fragment>
        <Text style={[styles.inNetworkText, {marginTop: 10, color: textColor}]}>Out of Pocket
          Estimate</Text>
        <Text style={[styles.priceRangeText, {color: textColor}]}>{this.props.item.oopPriceText}</Text>
        <CashBar
          style={{alignSelf: "flex-end", marginTop: 4}}
          color={Colors.requestBg}
          colorDisabled={Colors.white}
          size={14}
          rating={this.props.item.insDollarSigns}
        />
      </React.Fragment>
    )
  };

  private renderCashPrice = () => {
    return (
      <ImageBackground resizeMode={"center"} source={Images.CashPriceBG} style={styles.cashPriceBg}>
        <Text
          style={[styles.copayText, {textAlign: "center"}]}
        >{parseAmount(parseFloat(this.props.item.cashPrice))}
        </Text>
        <Text style={[styles.inNetworkText, {textAlign: "center"}]}>Cash Price</Text>
      </ImageBackground>
    )
  };

  private renderCostEstimateButton = () => {
    return (
      <View style={styles.buttonEstimate}>
        <TouchableOpacity style={styles.buttonEstimateButton} onPress={this.onEstimatePress}>
          <Text style={styles.buttonText}>{"Get Cost\nEstimate"}</Text>
        </TouchableOpacity>
      </View>
    )
  };

  private costEstimateLogic = () => {
    if (this.props.item.cashPrice) {
      return this.renderCashPrice();
    } else if (parseFloat(this.props.item.oopPrice) >= 0) {
      return this.renderOOP();
    } else if (R.compose(R.not, R.either(R.isEmpty, R.isNil))(this.props.item.copay)) {
      return this.renderCopay();
    } else {
      return this.renderCostEstimateButton();
    }
  };

  private onRequestPress = () => {
    Actions.push("request-appointment", {provider: this.props.item});
  };

  private onEstimatePress = () => {
    Actions.push("cost-estimate", {
      provider: this.props.item,
      serviceId: this.props.searchType === SearchType.Procedure ? this.props.item.serviceId : 5074,
    });
  };

  private providerById = () => {
    this.props.callProviderById(this.props.item.providerId);
  };
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<RootState>): IDispatch => ({
  callProviderById: (id) => dispatch(ProviderActions.requestById(id)),
});

const mapStateToProps = (root: RootState): IStateToProps => ({
  term: root.dashboard.term,
});

export default connect<IStateToProps, IDispatch, IProps>(mapStateToProps, mapDispatchToProps)(SearchItem);
