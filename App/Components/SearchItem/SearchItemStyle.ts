import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.shadow,
  },
  inNetworkBorder: {
    borderWidth: 3,
    borderColor: Colors.primaryLight,
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 0
  },
  inNetworkTextContainer: {
    position: "absolute",
    zIndex: 10,
    borderBottomRightRadius: 10,
    borderWidth: 5,
    top: 0,
    borderColor: Colors.orange
  },
  // inNetworkText: {backgroundColor: Colors.orange, color: Colors.white, fontFamily: Fonts.type.opensans, fontSize: Fonts.size.small},
  image: {
    height: Metrics.images.large,
    width: Metrics.images.large,
  },
  spaceBetween: {
    justifyContent: "space-between",
    flexDirection: "row",
  },
  imageBestValue: {
    height: Metrics.images.large,
    zIndex: 1,
    width: Metrics.images.large,
    position: "absolute",
  },
  preferredBackground: {
    borderRadius: 100, borderWidth: 2, width: 20, height: 20, overflow: "hidden",
    position: "absolute", top: 2, left: 2, justifyContent: "center", zIndex: 100,
  },
  text: {
    fontSize: Fonts.size.medium,
    color: Colors.textSuggestion,
    fontFamily: Fonts.type.opensans,
  },
  textCash: {
    fontFamily: Fonts.type.openSansSemiBold,
    fontSize: Fonts.size.h5,
    color: Colors.textSuggestion,
    textAlign: "center",
  },
  textEstimate: {
    fontFamily: Fonts.type.latoRegular,
    fontSize: Fonts.size.small,
    textAlign: "center",
    color: Colors.textSuggestion,
  },
  textEstimateHeader: {
    textAlign: "center",
    fontFamily: Fonts.type.latoBold,
    fontSize: Fonts.size.medium,
    color: Colors.textSuggestion,
  },
  milesContainer: {
    flexDirection: "row",
    marginTop: Metrics.smallMargin,
    alignItems: "baseline",
    width: "100%",
    paddingLeft: 5,
    backgroundColor: "#00000039",
    position: "absolute",
  },
  milesValue: {
    fontFamily: Fonts.type.opensansBold,
    color: Colors.white,
    fontSize: Fonts.size.regular,
  },
  milesText: {
    marginLeft: 2,
    fontFamily: Fonts.type.opensans,
    color: Colors.white,
    fontSize: Fonts.size.regular,
  },
  rowTwo: {
    paddingTop: Metrics.smallMargin,
  },
  txtDoctorName: {
    backgroundColor: "transparent",
    fontFamily: Fonts.type.latoBold,
    height: 40,
    fontSize: 16,
  },
  txtStatus: {
    alignSelf: "center",
    fontFamily: Fonts.type.opensansBold,
    marginTop: Metrics.marginVertical,
    color: Colors.green,
  },

  cashPriceBg: {flex: 1, aspectRatio: 1, justifyContent: "center", alignSelf: "center"},
  requestContainer: {backgroundColor: Colors.requestContainerBg, justifyContent: "center",height: 112},
  estimateContainer: {justifyContent: "center",height: 112},
  buttonRequest: {
    backgroundColor: Colors.requestBg,
    position: "absolute",
    alignSelf: "center",
    right: 0,
    left: 0,
    padding: 5,
  },
  buttonEstimate: {position: "absolute", top: 0, bottom: 0, left: 0, right: 0, justifyContent: "center"},
  buttonEstimateButton: {backgroundColor: Colors.estimateBg, padding: 5, alignSelf: "stretch"},
  buttonText: {fontFamily: Fonts.type.latoBold, fontSize: Fonts.size.regular, color: Colors.white, textAlign: "center"},
  timeText: {
    backgroundColor: Colors.requestBg, fontFamily: Fonts.type.latoBold, fontSize: Fonts.size.medium,
    color: Colors.white, padding: 5, flex: 0.75, textAlign: "center", height: 40,
  },
  timeTextAlt: {
    backgroundColor: Colors.requestDarkBg, fontFamily: Fonts.type.latoBold, fontSize: Fonts.size.medium,
    color: Colors.white, padding: 5, flex: 0.75, textAlign: "center", height: 40,
  },
  dateText: {fontFamily: Fonts.type.latoBold, fontSize: Fonts.size.small, color: Colors.white, padding: 5},
  priceRangeText: {
    fontFamily: Fonts.type.opensansBold,
    textAlign: "right",
    fontSize: Fonts.size.small,
    color: Colors.white
  },
  priceText: {fontFamily: Fonts.type.latoBold, fontSize: Fonts.size.h4, textAlign: "right", color: Colors.white},
  copayText: {fontFamily: Fonts.type.latoBold, fontSize: 14, textAlign: "right", color: Colors.white},
  inNetworkText: {fontFamily: Fonts.type.latoRegular, fontSize: 10, color: Colors.white, textAlign: "right"},
  headerText: { 
    fontSize: 14,
    fontFamily: Fonts.type.opensans,
    color: "#595959",
    fontWeight: "bold",
    lineHeight: 23
  },
  desriptionText: { 
    fontSize: 14, 
    fontFamily: Fonts.type.opensans,
    color: "#595959",
    lineHeight: 23
  },
  button: {
    backgroundColor: "#084c8d",
    paddingVertical: 3,
    paddingHorizontal: 10
  },
  toolTipbuttonText: {
    color: Colors.white,
    fontFamily: Fonts.type.opensans,
    fontSize: 14,
  }
});
