import * as React from "react";
import styles from "./HeaderStyle";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import {Body, Button, Header as NBHeader, Left, Right} from "@codler/native-base";
import {Actions} from "react-native-router-flux";
import {Image, TouchableOpacity} from "react-native";
import SafeAreaView from 'react-native-safe-area-view';
import {Colors, Images} from "@root/App/Themes";
import { View, Text } from "react-native";

interface Props {
  style?: any;
  hideBack?: boolean;
  hideMenu?: boolean;
}

const back = () => {
  Actions.pop();
};

const menu = () => {
  Actions.drawerOpen();
};

const Header: React.SFC<Props> = ({style, hideBack, hideMenu}: Props) => (
  <SafeAreaView style={{backgroundColor: Colors.primary}}>
    <NBHeader style={[styles.container, style]}>
      <Left>
        {
          !hideBack &&
          <Button
            transparent={true}
            onPress={back}
          >
            <Icon style={styles.icon} name="arrow-left"/>
            <Text style={styles.text}>Back</Text>
          </Button>
        }
      </Left>
      <Body>
        <Image source={Images.Logo}/>
      </Body>
      <Right>
        {
          !hideMenu &&
          <Button
            transparent={true}
            onPress={menu}
          >
            <Icon style={styles.icon} name="menu"/>
          </Button>
        }
      </Right>
    </NBHeader>
  </SafeAreaView>
);

export default Header;