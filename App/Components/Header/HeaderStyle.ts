import {StyleSheet,Platform} from "react-native";
import {Colors, Fonts, Metrics} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.primary,
  },
  text: {
    color: Colors.white,
    fontFamily: Fonts.type.opensans
  },
  textButton: {
    color: Colors.primary,
  },
  icon: {
    color: Colors.white,
    fontSize: Fonts.size.h5
  },
  backIcon: {
    color: Colors.white,
    fontSize: Platform.OS === "ios" ? 22 : 15
  }
});
