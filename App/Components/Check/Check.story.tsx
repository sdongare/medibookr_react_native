import { storiesOf } from "@storybook/react-native";
import * as React from "react";

import CheckBox from "./CheckBox";

storiesOf("CheckBox", module)
  .add("Default", () => (
    <CheckBox />
  ));