import { StyleSheet } from "react-native";
import Metrics from "../../Themes/Metrics";
import Colors from "../../Themes/Colors";

export default StyleSheet.create({
  checkContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  check: {
    marginLeft: 0,
    marginRight: 0,
    color: Colors.white,
  },
  checkLabel: {
    color: Colors.white,
    marginTop: 2,
    marginHorizontal: Metrics.smallMargin,
  },
});
