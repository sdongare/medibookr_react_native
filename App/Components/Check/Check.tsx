import * as React from "react";
import {Text} from "react-native";
import styles from "./CheckStyle";
import {Button, Icon} from "@codler/native-base";

interface Props {
  text: string;
  isCheckProp: boolean;
  onClick: () => void;
  color?: string;
  style?: any;
  textStyle?: any;
}

const Check: React.SFC<Props> = ({text, isCheckProp, onClick, style, color, textStyle}) =>
  (<Button transparent={true} style={[styles.checkContainer, style]} onPress={onClick}>
    <Icon name={isCheckProp ? "check-box" : "check-box-outline-blank"} type="MaterialIcons" style={[styles.check, color ? {color} : null]}/>
    <Text style={[styles.checkLabel, color ? {color} : null, textStyle]}>{text}</Text>
  </Button>);

export default Check;
