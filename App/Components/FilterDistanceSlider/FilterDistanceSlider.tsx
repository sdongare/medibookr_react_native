import * as React from "react";
import {Text, View} from "react-native";
import styles from "./FilterDistanceSliderStyle";
import MultiSlider from "@ptomasroos/react-native-multi-slider/MultiSlider.js";
import {Colors} from "@root/App/Themes";
import Svg, {Circle} from "react-native-svg";
import * as R from "ramda";

interface Props {
  values: number[];
  isAge?: boolean;
  onValueChange?: (values: number[]) => void;
}

interface IState {
  width: number;
}

class FilterDistanceSlider extends React.PureComponent<Props, IState> {

  constructor(props) {
    super(props);

    this.state = {
      width: 0,
    };
  }

  public render() {
    const labels = this.props.isAge ? ["<\n30", "40", "50", "60", ">\n70"] : ["0", "10", "20", "30", "40", "50", "60"];
    return (
      <View style={[styles.container]} onLayout={(event) => this.setState({width: event.nativeEvent.layout.width})}>
        <MultiSlider
          sliderLength={this.state.width}
          isMarkersSeparated={true}
          customMarkerLeft={this.svgGenerator}
          customMarkerRight={this.svgGenerator}
          selectedStyle={{backgroundColor: Colors.primary}}
          unselectedStyle={{backgroundColor: Colors.white}}
          values={this.props.values}
          min={this.props.isAge ? 26 : 0}
          max={this.props.isAge ? 65 : 100}
          onValuesChange={this.props.onValueChange}
          // step={10}
          snapped={true}
          containerStyle={styles.sliderContainer}
          trackStyle={{height: 5, backgroundColor: "green"}}
          touchDimensions={{height: 40, width: 40, borderRadius: 10, slipDisplacement: 10}}
        />
        {/* <View style={styles.labelContainer}>
          {labels.map((value, index) => (<Text style={styles.labelText} key={index}>{value + " -> "+[this.props.values]}</Text>))}
        </View>         */}
      </View>
    );
  }

  private svgGenerator = (e) => {
    return svgCircle();
  }

}


const svgCircle = () => {
  return (
    <Svg width={30} height={30}>
      <Circle
        r={6}
        fill={Colors.primary}
        strokeWidth={1}
        stroke={Colors.black}
        strokeOpacity={0.15}
        cx={15}
        cy={18}
      />
    </Svg>
  );
};


export default FilterDistanceSlider;
