import {StyleSheet} from "react-native";
import {Colors} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";


export default StyleSheet.create({
  container: {
    marginLeft: 5,
    flex: 1,
    flexDirection: "column",
    width: "100%"
  },
  slider: {
    marginLeft: 10,
  },
  sliderContainer: {
    height: 20,
  },
  labelContainer: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  labelText: {
    maxWidth: 20,
    textAlign: "center",
    fontSize: Fonts.size.small - 2,
  }
});
