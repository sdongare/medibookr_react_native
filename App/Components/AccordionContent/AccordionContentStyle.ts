import {StyleSheet} from "react-native";
import Colors from "../../Themes/Colors";
import Metrics from "../../Themes/Metrics";
import Fonts from "../../Themes/Fonts";


const text = {
  color: Colors.white,
  fontSize: Fonts.size.medium,
};
export default StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    borderRadius: 0,
    paddingHorizontal: Metrics.smallMargin,
    paddingVertical: Metrics.smallMargin,
  },
  text,
  textBold: {
    ...text,
    fontWeight: "bold",
  },
  svgContainer: {
    position: "absolute",
  },
});
