import * as React from "react";
import {Text, TouchableOpacity} from "react-native";
import styles from "./AccordionContentStyle";
import {getBGColor} from "../../Lib/DashboardHelper";
import {Button} from "native-base";
import {connect} from "react-redux";
import {ProviderActions} from "@root/App/Reducers/ProviderReducers";
import Svg, {Path} from "react-native-svg";
import * as R from "ramda";

interface IProps {
  level: number;
  isTopElement: boolean;
  item: any;
}

interface IState {
  dimension: { width: number, height: number };
}

interface DispatchProps {
  callService?: (item: any) => void;
}


type Props = IProps & DispatchProps;


class AccordionContent extends React.Component<Props, IState> {
  public state = {
    dimension: undefined,
  };

  public onLayout = (event) => {
    if (this.state.dimension) {
      return;
    } // layout was already called
    const {width, height} = event.nativeEvent.layout;
    this.setState({dimension: {width, height}});
  };

  public click = () => {
    const {item, callService} = this.props;
    callService(item);
  };

  public render() {
    const {level, isTopElement, item} = this.props;

    if (!item) {
      return null;
    }

    const bg = getBGColor(level);
    const bg2 = getBGColor(level - 1);
    const width = this.state.dimension && this.state.dimension.width;
    const height = this.state.dimension && this.state.dimension.height;

    const content = item[`level_${level}`];
    const text = !!(content && content.length) ? content : item[`level_${level - 1}`];

    return (
      <TouchableOpacity onPress={this.click}
                        style={[styles.container, !isTopElement ? {backgroundColor: bg, marginTop: 2} : null]}
                        onLayout={this.onLayout}>
        {isTopElement && this.state.dimension &&
        <Svg width={`${width}`} height={`${height}`} viewBox={`0 0 ${width} ${height}`} style={styles.svgContainer}>
          <Path
            d={`M20 2l10 10 10-10 ${width - 40} 0 0 ${height} -${width} 0 0 -${height}z`}
            fill={bg}
          />
          <Path
            d={`M22 0l8 9 8-9z`}
            fill={bg2}
          />

        </Svg>
        }
        <Text
          style={[styles.text, isTopElement ? {paddingTop: 5} : null]}
        >{text}</Text>
      </TouchableOpacity>

    );
  }
}


const mapDispatchToProps = (dispatch: Function): DispatchProps => (
  {
    callService: (item) => dispatch(ProviderActions.request({item})),
  }
);

export default connect(null, mapDispatchToProps)(AccordionContent);
