import * as React from "react";
import {Text, TextInput, ScrollView, View, TouchableOpacity} from "react-native";
import styles from "./FilterStyle";
import {Button, Col, Grid, Icon, Row} from "@codler/native-base";
import NumericInputBox from "@root/App/Components/NumericInputBox/NumericInputBox";
import FilterDistanceSlider from "@root/App/Components/FilterDistanceSlider/FilterDistanceSlider";
import * as SI from "seamless-immutable";
import CustomCheck from "@root/App/Components/CustomCheck";
// import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Colors, Fonts, Metrics} from "@root/App/Themes";
import CashBar from "@root/App/Components/CashBar/CashBar";
import RatingStarBar from "@root/App/Components/RatingStarBar/RatingStarBar";
import CustomPicker from "@root/App/Components/CustomPicker";
import SafeAreaView from 'react-native-safe-area-view';

interface Props {
  style: any;
  state: IFilterState;
  enableDollar: boolean;
  specialties: string[];
  onClose?: () => void;
  onDone?: (IFilterState) => void;
  onReset?: () => void;
}

const sortBy = [
  "Lowest total out-of-pocket cost",
  "Best Value",
  "Nearest to my location",
  "Full name alphabetically (A-Z)",
  "Full name alphabetically (Z-A)",
  "Insurance - low to high",
  "Insurance - high to low",
];

export type IFilterState = SI.Immutable<ISate>;

interface ISate {
  sort: number;
  bestValue: boolean;
  cashPrice: number;
  insurancePrice: number;
  rating: number;
  specialty: number;
  outOfNetwork: boolean;
  distance: { selected };
  male: boolean;
  female: boolean;
  age: { selectedMin: number, selectedMax: number };
  ageInput: { selectedMin: number, selectedMax: number };
  facilityName: string;
  unrated: boolean;
}

export const INITIAL_STATE: IFilterState = SI.from({
  age: {selectedMax: 65, selectedMin: 0},
  ageInput: {selectedMax: 65, selectedMin: 26},
  distance: {selected: 100},
  cashPrice: 0,
  insurancePrice: 0,
  rating: 0,
  bestValue: false,
  facilityName: "",
  male: false,
  specialty: 0,
  female: false,
  outOfNetwork: false,
  sort: 0,
  unrated: true,
});

class Filter extends React.Component<Props, ISate> {

  private inputText;

  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  public componentWillReceiveProps(next) {
    const {facilityName, ...obj} = next.state;
    this.setState(obj);
  }

  public done = () => {
    this.props.onDone(this.state);
  };

  public networkCheck = () => {
    this.setState({outOfNetwork: !this.state.outOfNetwork});
  };

  public bestValueCheck = () => {
    this.setState({bestValue: !this.state.bestValue});
  };

  public unratedCheck = () => {
    this.setState({unrated: !this.state.unrated});
  };

  public maleCheck = () => {
    this.setState({male: !this.state.male});
  };

  public femaleCheck = () => {
    this.setState({female: !this.state.female});
  };

  public onCashPrice = (price) => {
    this.setState({cashPrice: this.state.cashPrice === price ? 0 : price});
  };

  public onInsurancePrice = (price) => {
    this.setState({insurancePrice: this.state.insurancePrice === price ? 0 : price});
  };

  public onRating = (rating) => {
    this.setState({rating: this.state.rating === rating ? 0 : rating});
  };


  public render() {
    const {style, onReset, onClose} = this.props;

    return (
      <View style={[styles.container, style]}>
        <ScrollView
          contentContainerStyle={{zIndex: 1, elevation: 1}}
          // @ts-ignore
          getTextInputRefs={this.getInputRefs}
        >
          <View style={{ flex: 1, padding: 20, paddingTop: 15 }}>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.header}>Sorts</Text>
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <Text style={styles.label}>Sort by:</Text>             
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <CustomPicker
                selectedValue={this.state.sort}
                onValueChange={this.pickerValueChanged}
                list={sortBy}
              />
            </View>
            <View style={{ flexDirection: "row", paddingTop: 15 }}>
              <Text style={styles.header}>Advanced Filters</Text>
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <Text style={styles.label}>Specialties</Text>             
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <CustomPicker
                textStyle={styles.pickerText}
                selectedValue={this.state.specialty}
                onValueChange={this.pickerSpecialtyChange}
                list={this.props.specialties}
              />
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <CustomCheck
                style={styles.row}
                text="Show only best value"
                onClick={this.bestValueCheck}
                isCheckProp={this.state.bestValue}
                color={"#595959"}
                checkBoxColor={"#084c8d"}
              />                        
            </View>            
            <View style={{ flexDirection: "row" }}>             
              <CustomCheck
                style={styles.row}
                text="Show out-network options"
                onClick={this.networkCheck}
                isCheckProp={this.state.outOfNetwork}
                color={"#595959"}
                checkBoxColor={"#084c8d"}
              />           
            </View>
            <View style={{ flexDirection: "row" }}>             
              <CustomCheck
                style={styles.row}
                text="Show only male providers"
                isCheckProp={this.state.male}
                onClick={this.maleCheck}
                color={"#595959"}
                checkBoxColor={"#084c8d"}
              />           
            </View>
            <View style={{ flexDirection: "row" }}>             
              <CustomCheck
                style={styles.row}
                text="Show only female providers"
                isCheckProp={this.state.female}
                onClick={this.femaleCheck}
                color={"#595959"}
                checkBoxColor={"#084c8d"}
              />           
            </View>
            <View style={{ flexDirection: "row", paddingTop: 10 }}>
              <Text style={styles.header}>Ranges</Text>
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <Text style={styles.label}>Distance (mi.)</Text>             
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <View style={{ flex: 8 }}>
                <FilterDistanceSlider
                  values={[this.state.distance.selected]}
                  onValueChange={this.distanceSliderValueChanged}
                /> 
              </View>                           
              <View style={{ flex: 2 }}></View>
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>             
              <View style={{ flex: 8, justifyContent: "flex-end", alignItems: "flex-end" }}>
                <NumericInputBox
                  min={0}
                  max={100}
                  type={""}
                  values={[this.state.distance.selected]}
                  onValueChange={this.distanceSliderValueChanged}
                />
              </View>                           
              <View style={{ flex: 2 }}></View>              
            </View>
            <View style={{ flexDirection: "row", paddingTop: 5 }}>
              <Text style={styles.label}>Age (yrs.)</Text>             
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <View style={{ flex: 8 }}>
                <FilterDistanceSlider
                  isAge={true}
                  values={[this.state.age.selectedMin, this.state.age.selectedMax]}
                  onValueChange={this.ageSliderValueChanged}
                />  
              </View>                           
              <View style={{ flex: 2 }}></View>                       
            </View>
            <View style={{ flexDirection: "row", paddingTop: 8 }}>
              <View style={{ flex: 5 }}>
                <NumericInputBox
                  min={26}
                  max={65}
                  values={[this.state.ageInput.selectedMin]}
                  type={"min"}
                  onValueChange={this.ageSliderValueChangedInput}
                />   
              </View>
              <View style={{ flex: 5 }}>
                <NumericInputBox
                  min={this.state.ageInput.selectedMin}
                  max={65}
                  values={[this.state.ageInput.selectedMax]}
                  type={"max"}
                  onValueChange={this.ageSliderValueChangedInput}
                />   
              </View>         
            </View>
          </View>                       
        </ScrollView>
        <View style={styles.footerContainer}>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.footerBody}>
              <TouchableOpacity onPress={onReset}>
                <Text style={{ ...styles.buttonText, textDecorationLine: "underline" }}>Reset All</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.footerBody}>
              <TouchableOpacity style={{ ...styles.button, borderWidth: 1, borderColor: "#084c8d", backgroundColor: "transparent" }} onPress={onClose}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.footerBody}>
              <TouchableOpacity style={styles.button} onPress={this.done}>
                <Text style={{ ...styles.buttonText, color: Colors.white }}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View> 
      </View>             
    );
  }

  private onFacilityTextChange = (text) => this.setState({facilityName: text});

  private pickerValueChanged = (value) => this.setState({sort: value});

  private pickerSpecialtyChange = (itemValue) => this.setState({specialty: itemValue});

  private distanceSliderValueChanged = (values) => this.setState({distance: {selected: values[0] >= 99 ? 100 : values[0]}});

  private ageSliderValueChanged = (values) => {
    if (values[0] <= 26) {
      values[0] = 0;
    }

    if (values[1] >= 65) {
      values[1] = 100;
    }
    this.setState({
      age: {selectedMin: values[0], selectedMax: values[1]},
      ageInput: {selectedMin: values[0] === 0 ? 26 : values[0], selectedMax: values[1] === 100 ? 65 : values[1] }
    });
    // ageInput
  };

  private ageSliderValueChangedInput = (values, type) => {
    if(type == "min")
    {
      this.setState({
        age: {selectedMin: values[0], selectedMax: this.state.age.selectedMax },
        ageInput: {selectedMin: values[0], selectedMax: this.state.ageInput.selectedMax }
      });
    }
    else {
      this.setState({
        age: {selectedMin: this.state.age.selectedMin, selectedMax: values[0] },
        ageInput: {selectedMin: this.state.ageInput.selectedMin, selectedMax: values[0] }
      });
    }
  };

  private getInputRefs = () => [this.inputText];
}

export default Filter;
