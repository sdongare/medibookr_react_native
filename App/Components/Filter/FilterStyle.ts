import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";
import ApplicationStyles from "@root/App/Themes/ApplicationStyles";

const text = {
  color: "#000000",
};

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.shadow,
    elevation: 5,
    zIndex: 5,
    position: "absolute",
    right: 0,
    bottom: 0,
    width: Metrics.screenWidth * 0.8,
    paddingBottom: Metrics.marginVertical+5,
    backgroundColor: "#f5f5f6",
  },
  filterButton: {
    paddingHorizontal: 5,
  },
  filterIcon: {
    marginLeft: 0,
    marginRight: 0,
    color: Colors.white,
    fontSize: 38,
  },
  pickerText: {
    color: Colors.textPicker,
    alignItems: "center",
    marginVertical: 20,
    paddingVertical: Metrics.smallMargin,
    backgroundColor: Colors.white,
    borderColor: Colors.textPicker,
    borderWidth: 1,
    width: "100%",
  },
  header: {
    ...text,
    fontWeight: "600",
    fontFamily: Fonts.type.openSansSemiBold,
    fontSize: Fonts.size.h5,
  },
  spinnerPlaceholder: {
    ...text,
    marginTop: Metrics.marginVertical,
    fontSize: Fonts.size.medium - 1,
  },
  row: {},
  labelContainer: {
    marginTop: 10,
  },
  label: {
    ...text,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.opensans,
    color: "#262626"
  },
  button: {
    backgroundColor: "#084c8d",
    width: 80,
    height: 26,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: "#084c8d",
    fontFamily: Fonts.type.opensans,
    fontSize: 14
  },
  footerBody: {
    flex: 3.33,
    justifyContent: "center",
    alignItems: "center"
  },
  footerContainer: {
    position: "relative",
    bottom: 0,
    borderTopWidth: 1,
    borderTopColor: "#989898",
    paddingVertical: 15
  }
});
