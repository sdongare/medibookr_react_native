import * as React from "react";
import Svg, {Line, Path} from "react-native-svg";
import {View} from "react-native";

interface Props {
  size: number;
  fillColor: string;
  style: any
}

const AccordionIcon: React.SFC<Props> = ({size, fillColor, style}: Props) => {
  const halfSize = size / 2;
  return (
    <View style={style}>
      <Svg height={size} width={size} viewBox={`0 0 ${size} ${size}`}>
        <Path
          d={`M0 0l${halfSize} ${halfSize} ${halfSize}-${halfSize}z`}
          fill={fillColor}
        />
        <Line
          x1="0"
          y1="0"
          x2={`${halfSize}`}
          y2={`${halfSize}`}
          stroke="transparent"
          strokeWidth="1"
        />
        <Line
          x2={`${size}`}
          y2="0"
          x1={`${halfSize}`}
          y1={`${halfSize}`}
          stroke="black"
          strokeWidth="1"
        />
      </Svg>
    </View>
  );
};

export default AccordionIcon;
