import * as React from "react";
import {Text, View, TouchableOpacity, Image} from 'react-native';
import styles from "./TosterStyle";
import Images from "@root/App/Themes/Images";

export const toastConfig = {
  'custom_error': ({ text1, props, ...rest }) => (
    <View style={styles.errorCustomContainer}>    
        <View style={{ flex: 0.7 }} ></View>
        <View style={styles.errorCustomViewContainer}>
            <Text style={styles.errorCustomText}>
                {text1}
            </Text>            
        </View>  
        <View style={styles.errorCustomCloseContainer} >
            <TouchableOpacity onPress={() => props.onPress()} style={{ justifyContent: "center", alignItems: "center"}}>
                <Image source={Images.CloseWhite} style={styles.errorCustomCloseImage}/>
            </TouchableOpacity>
        </View>
    </View>  
  )
}