import {StyleSheet} from "react-native";
import {Colors, Metrics} from "@root/App/Themes";
import Fonts from "@root/App/Themes/Fonts";

export default StyleSheet.create({
  errorCustomContainer: {
    flexDirection: "row",
    width: '100%',
    backgroundColor: '#a8071a',
    padding: 10,
  },
  errorCustomViewContainer: {
    flex: 8.3,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  errorCustomText: {
    color: Colors.white,
    fontFamily: Fonts.type.opensans,
    fontSize: 14,
    lineHeight: 22,
    textAlign: "center"
  },
  errorCustomCloseContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  errorCustomCloseImage: {
    width: 18,
    height: 18
  }
});
