import {StyleSheet} from "react-native";
import Colors from "@root/App/Themes/Colors";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  arrowIcon: {
    fontSize: 30,
    marginLeft: 3,
    marginRight: 3,
    color: Colors.primary,
  },
  buttonGps: {
    position: "absolute",
    bottom: 8,
    right: 8,
    backgroundColor: Colors.searchBg,
  },
  gpsIcon: {
    color: Colors.primary,
  },
  navContainer: {
    backgroundColor: Colors.transparent,
    position: "absolute",
    alignContent: "center",
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    bottom: 0,
  },
});
