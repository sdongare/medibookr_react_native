import {StyleSheet} from "react-native";
import {Colors, Fonts} from "@root/App/Themes";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
  },
  icon: {fontSize: 45, color: Colors.primary},
  circle: {
    width: 20,
    height: 20,
    top: 5,
    position: "absolute",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderColor: Colors.searchBg,
    borderWidth: 10,
    borderRadius: 10,
  },
  text: {
    width: 20,
    height: 20,
    alignSelf: "center",
    textAlign: "center",
    color: Colors.primary,
    fontSize: Fonts.size.small,
  },
});
