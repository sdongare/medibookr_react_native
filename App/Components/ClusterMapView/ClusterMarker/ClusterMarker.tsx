import * as React from "react";
import {View} from "react-native";
import styles from "./ClusterMarkerStyle";
import {LatLng, Marker} from "react-native-maps";
import {ClusterProperties} from "supercluster";
import {Badge, Icon, Text} from "@codler/native-base";
import {Colors} from "@root/App/Themes";

interface Props {
  clusterItem: ClusterProperties;
  onPress: () => void;
  coordinate: LatLng;
}

const ClusterMarker: React.SFC<Props> = ({clusterItem, coordinate, onPress}: Props) => {
  return (
    <Marker
      coordinate={coordinate}
      tracksViewChanges={false}
      onPress={onPress}
    >
      <View style={styles.container}>
        <Icon name={"map-marker"} type={"FontAwesome"} style={styles.icon}/>
        <Badge style={{top: 6, left: -25, width: 22, height: 22, backgroundColor: Colors.searchBg}}>
          <Text style={styles.text}>{clusterItem.point_count}</Text>
        </Badge>
      </View>
    </Marker>
  );
};

export default ClusterMarker;
