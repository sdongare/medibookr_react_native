import * as React from "react";
import MapView, {LatLng, Marker, Region} from "react-native-maps";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {
  createCluster,
  createRegions,
  extractLatLng,
  filterFromCluster,
  getDataInRegion,
  getRegion,
  getZoomLevel,
} from "@root/App/Lib/MapHelper";
import {Supercluster} from "supercluster";
import ClusterMarker from "@root/App/Components/ClusterMapView/ClusterMarker/ClusterMarker";
import {from, onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, filter, mergeMap} from "rxjs/operators";
import {Images} from "@root/App/Themes";
import {View} from "react-native";
import {Button, Icon} from "@codler/native-base";
import {connect} from "react-redux";
import {RootState} from "@root/App/Reducers";
import styles from "./ClusterMapViewStyle";

interface OwnProps {
  data: ProviderResultsItem[];
  onGetNewData: (newData: ProviderResultsItem[]) => void;
  setLoader: () => void;
  style?: any;
  suggestedHeight: number;
  onMarkerPress: (item) => void;
}

interface IStateProps {
  gps?: LatLng;
}

interface IState {
  region: Region;
  features: any[];
  cluster: Supercluster;
  expanded: boolean;
  zoomLevel: number;
}

type Props = OwnProps & IStateProps;

export const EDGE_PADDING = {top: 20, bottom: 10, left: 10, right: 10};

class ClusterMapView extends React.Component<Props, IState> {

  private map: MapView;
  private regionListener = new Subject<Region>();

  constructor(props: Props) {
    super(props);
    this.state = {
      zoomLevel: 0,
      cluster: null,
      region: null,
      expanded: false,
      features: null,
    };

    this._getState(props.data).then((state) => this.setState(state));

    onErrorResumeNext(
      this.regionListener
        .pipe(
          debounceTime(500),
          filter((value) => value !== null),
          mergeMap((value) => from(getDataInRegion(this.props.data, value))),
          filter((value) => value !== null && value.length > 0),

          mergeMap((next) => {
            console.log(next);
            // this.props.onGetNewData(next);
            return from(this._getState(next));
          }),
        ),
    ).subscribe((next) => {
      this.setState(next);
    });
  }

  public async _getState(data: ProviderResultsItem[]) {
    const region = getRegion(data);
    const zoomLevel = getZoomLevel(region);

    const cluster = await createCluster(data, zoomLevel);
    const features = createRegions(cluster, region, zoomLevel);

    return {
      region, cluster, features, zoomLevel,
    };
  }

  public async setThisState(data: ProviderResultsItem[]) {
    const state = await this._getState(data);
    this.setState(state);

    const latlong = extractLatLng(data);
    if (latlong) {
      this.map.fitToCoordinates(latlong);
    }
  }

  public componentWillReceiveProps(next: Props) {
    if (next.data !== this.props.data) {
      // this.setThisState(next.data);
    }
  }

  public renderMarker(value: any, index) {
    const coordinate = {
      latitude: parseFloat(value.geometry.coordinates[1]) || 0,
      longitude: parseFloat(value.geometry.coordinates[0]) || 0
    };

    if (!value.properties.cluster) {
      return (
        <Marker
          coordinate={coordinate}
          key={index}
          image={Images.Marker}
          onPress={() => this.props.onMarkerPress(value.properties.item)}>
          {/* <Callout >
            <Text style={{flex: 1}}>Provider name: {value.properties.item.providerName}</Text>
          </Callout>*/}
        </Marker>
      );

    } else {
      return (
        <ClusterMarker
          onPress={() => {
            const clusters = this.state.cluster.getLeaves(value.properties.cluster_id, getZoomLevel(this.state.region));
            const data = filterFromCluster(clusters);
            this.setThisState(data);
          }}
          key={index}
          clusterItem={value.properties}
          coordinate={coordinate}
        />
      );
    }
  }

  public onRegionComplete = (region) => {
    this.regionListener.next(region);
  };

  public locate = () => {
    this.map.animateToCoordinate(this.props.gps, 500);
  };

  public render() {
    if (!this.state.features || isNaN(this.state.region.latitude)) {
      return null;
    }
    return (
      <View
        style={{height: this.props.suggestedHeight}}
      >

        <MapView
          style={this.props.style}
          mapPadding={EDGE_PADDING}
          initialRegion={this.state.region}
          onRegionChange={this.onRegionComplete}
          ref={(component) => this.map = component}
        >
          {this.state.features.filter(val => !isNaN(val.geometry.coordinates[0])).map((value, index) => this.renderMarker(value, index))}
          {!!this.props.gps && <Marker coordinate={this.props.gps} image={Images.UserMarker}/>}

        </MapView>

        {
          !!this.props.gps &&
          <Button style={styles.buttonGps} warning={true} onPress={this.locate}>
            <Icon
              style={styles.gpsIcon}
              name={"locate"}
            />
          </Button>
        }
      </View>
    );
  }
}

const mapStateToProps = (state: RootState): IStateProps => ({
  gps: state.location.gps,
});

export default connect<IStateProps, null, Props>(mapStateToProps, null, null, {forwardRef: true})(ClusterMapView);
