import * as React from "react";
import MapView, {Marker, Region} from "react-native-maps";
import {
  getDataInRegionPharmacy,
  getRegionPharmacy,
} from "@root/App/Lib/MapHelper";
import {from, onErrorResumeNext, Subject} from "rxjs";
import {debounceTime, filter, mergeMap} from "rxjs/operators";
import {Images} from "@root/App/Themes";

interface OwnProps {
  data: PriceResponse[];
}

interface IStateProps {
}

interface IState {
  region: Region;
}

type Props = OwnProps & IStateProps;

export const EDGE_PADDING = {top: 20, bottom: 10, left: 10, right: 10};

class PharmacyMapView extends React.Component<Props, IState> {

  private map: MapView;
  private regionListener = new Subject<Region>();

  constructor(props: Props) {
    super(props);
    this.state = {
      region: null,
    };

    this._getState(props.data).then((state) => this.setState(state));

    onErrorResumeNext(
      this.regionListener
        .pipe(
          debounceTime(500),
          filter((value) => value !== null),
          mergeMap((value) => from(getDataInRegionPharmacy(this.props.data, value))),
          filter((value) => value !== null && value.length > 0),

          mergeMap((next) => {
            console.log(next);
            // this.props.onGetNewData(next);
            return from(this._getState(next));
          }),
        ),
    ).subscribe((next) => {
      this.setState(next);
    });
  }

  public async _getState(data: PriceResponse[]) {
    const region = getRegionPharmacy(data);
    return {
      region,
    };
  }

  public renderMarker(value: any, index) {
    const coordinate = {
      latitude: parseFloat(value.Address.Latitude.toString()) || 0,
      longitude: parseFloat(value.Address.Longitude.toString()) || 0
    };
    return (
        <Marker
            coordinate={coordinate}
            key={index}
            image={Images.Marker}
            title={value.Name}
        >
        </Marker>
    );
  }

  public onRegionComplete = (region) => {
    this.regionListener.next(region);
  };

  public render() {
    if (this.state.region && isNaN(this.state.region.latitude)) {
      return null;
    }
    return (
        <MapView
          style={{width: "100%", height: 350}}
          mapPadding={EDGE_PADDING}
          initialRegion={this.state.region}
          onRegionChange={this.onRegionComplete}
          ref={(component) => this.map = component}
        >
          {this.props.data.map((value, index) => this.renderMarker(value, index))}
        </MapView>
    );
  }
}
export default PharmacyMapView;