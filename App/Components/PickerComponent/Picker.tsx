import * as React from "react";
import {PickerItem, View, ViewStyle} from "react-native";
import styles from "./PickerStyle";
import {Icon, Picker} from "@codler/native-base";

interface Props {
  items: string[];
  selected?: number;
  style?: ViewStyle;
  onItemSelected: (item: number) => void;
}

interface State {
  selected
}

export default class PickerComponent extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected || 0,
    }
  }

  public render() {
    return <View style={this.props.style}>
      <Picker
        style={styles.pickerStyle}
        textStyle={{flex: 1, flexWrap: "wrap"}}
        mode={"dropdown"}
        selectedValue={this.state.selected}
        onValueChange={this.onSelected}
      >
        {this.props.items.map((value, index) => (<PickerItem label={value.toString()} key={index} value={index}/>))}
      </Picker>
      <Icon name={"caretdown"} type={"AntDesign"} style={styles.pickerCarrot}/>
    </View>;
  }

  private onSelected = (value) => {
    this.setState({selected: value});
    this.props.onItemSelected(value);
  };
}


