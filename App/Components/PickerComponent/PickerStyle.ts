import {StyleSheet} from "react-native";
import Colors from "../../Themes/Colors";

export default StyleSheet.create({
  pickerCarrot: {
    position: "absolute",
    right: 8,
    top: 15,
    fontSize: 14,
    color: Colors.textDark,
    alignSelf: "center",
  },
  pickerStyle: {
    width: "100%",
    borderRadius: 3,
    backgroundColor: Colors.searchBg,
    borderColor: Colors.border,
    borderWidth: 1,
  },
});
