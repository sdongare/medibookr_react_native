import Reactotron from "reactotron-react-native";

// gets the current screen from navigation state
const getCurrentRouteName = (navigationState: any): any => {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
};

const screenTracking = ({getState}: any) => (next: any) => (action: any) => {


  const currentScreen = getCurrentRouteName(getState().nav);
  const result = next(action);
  const nextScreen = getCurrentRouteName(getState().nav);
  if (nextScreen !== currentScreen) {
    try {
      Reactotron.log(`NAVIGATING ${currentScreen} to ${nextScreen}`);
      // Example: Analytics.trackEvent('user_navigation', {currentScreen, nextScreen})
    } catch (e) {
      Reactotron.log(e);
    }
  }
  return result;
};

export default screenTracking;
