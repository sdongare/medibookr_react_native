import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import {AnyAction, Reducer} from "redux";
import {createReducer} from "reduxsauce";
import {networkFailure} from "@root/App/Epics/AppEpics";


export const RESET = "RESET";

/* ------------- Types and Action Creators ------------- */

export interface SendFeedbackParams {
  message: string;
  name: string;
  email: string;
  onSuccess: () => void;
}


const actionCreators = {
  actionVoid: createAction("TYPE_VOID"),
  actionSendFeedback: createStandardAction("SEND_FEEDBACK_REQUEST")<SendFeedbackParams>(),
  actionSendFeedbackSuccess: createAction("SEND_FEEDBACK_SUCCESS"),
  actionLoader: createStandardAction("TYPE_LODAER")<boolean>(),
  actionReset: createAction(RESET),
  actionError: networkFailure,
  actionRetry: createStandardAction("TYPE_RETRY")<AnyAction>(),
};

export const AppActions = actionCreators;

export interface AppState {
  action: AnyAction;
  fetching: boolean;
}

export type ImmutableAppState = SI.Immutable<AppState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableAppState = SI.from({
  action: null,
  fetching: false,
});

/* ------------- Reducers ------------- */

const actionRetry: Reducer<ImmutableAppState> = (state, {payload}) => state.merge({action: payload});

const actionLoader: Reducer<ImmutableAppState> = (state, {payload}) => state.merge({fetching: payload});

const actionSendFeedback: Reducer<ImmutableAppState> = (state) => state.merge({fetching: true});

const actionSendFeedbackSuccess: Reducer<ImmutableAppState> = (state) => state.merge({fetching: false});


/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.actionRetry)]: actionRetry,
  [getType(actionCreators.actionLoader)]: actionLoader,
  [getType(actionCreators.actionSendFeedback)]: actionSendFeedback,
  [getType(actionCreators.actionSendFeedbackSuccess)]: actionSendFeedbackSuccess,
  "NETWORK_ERROR": actionSendFeedbackSuccess,
};

export const AppReducer = createReducer(INITIAL_STATE, reducerMap);

export default AppReducer;
