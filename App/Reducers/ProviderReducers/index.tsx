import {AnyAction, Reducer} from "redux";
import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import {ErrorResponse} from "@root/App/Services/DataModels/ErrorResponse";
import {networkFailure} from "@root/App/Epics/AppEpics";
import {RESET} from "@root/App/Reducers/AppReducers";
import {createReducer} from "reduxsauce";

/* ------------- Types and Action Creators ------------- */

export interface SearchRequestParam {
  item: Service | Specialty | string;
}

const TYPE_REQUEST = "PROVIDER_REQUEST";
const TYPE_REQUEST_ID = "PROVIDER_BY_ID";
const TYPE_SUCCESS_ID = "PROVIDER_BY_ID_SUCCESS";
const TYPE_SUCCESS = "PROVIDER_SUCCESS";

const actionCreators = {
  request: createStandardAction(TYPE_REQUEST)<SearchRequestParam>(),
  requestById: createStandardAction(TYPE_REQUEST_ID)<string>(),
  successById: createAction(TYPE_SUCCESS_ID),
  success: createAction(TYPE_SUCCESS),
  failure: networkFailure,
  reset: createAction(RESET),
};

export const ProviderActions = actionCreators;

export interface ProviderState {
  error?: ErrorResponse | null;
  fetching?: boolean | null;
}

export type ImmutableProviderState = SI.Immutable<ProviderState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableProviderState = SI.from({
  error: null,
  fetching: null,
});

/* ------------- Reducers ------------- */

export const request: Reducer<ImmutableProviderState> =
  (state: ImmutableProviderState) =>
    state.merge({fetching: true});

export const requestById: Reducer<ImmutableProviderState> =
  (state: ImmutableProviderState) => state.merge({fetching: true});

export const success: Reducer<ImmutableProviderState> =
  (state: ImmutableProviderState) => state.merge({fetching: false, error: null});

export const successById: Reducer<ImmutableProviderState> = (state) => state.merge({fetching: false});

export const failure: Reducer<ImmutableProviderState> = (state: ImmutableProviderState, action: AnyAction) =>
  state.merge({fetching: false, error: action.payload});

export const reset: Reducer<ImmutableProviderState> = (state) => INITIAL_STATE;

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.request)]: request,
  [getType(actionCreators.requestById)]: requestById,
  [getType(actionCreators.failure)]: failure,
  [getType(actionCreators.success)]: success,
  [getType(actionCreators.successById)]: successById,
  [getType(actionCreators.reset)]: reset,
};

export const ProviderReducer = createReducer(INITIAL_STATE, reducerMap);

export default ProviderReducer;
