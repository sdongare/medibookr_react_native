import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import {ErrorResponse} from "@root/App/Services/DataModels/ErrorResponse";
import {networkFailure} from "@root/App/Epics/AppEpics";
import {RESET} from "@root/App/Reducers/AppReducers";
import {createReducer} from "reduxsauce";
import {RequestAppointmentResponse} from "@root/App/Services/DataModels/Patient/RequestAppointmentResponse";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {Buffer} from "safe-buffer";
import * as R from "ramda";

/* ------------- Types and Action Creators ------------- */
const TYPE_INFO_REQUEST = "PATIENT_INFO_REQUEST";
const TYPE_INFO_SUCCESS = "PATIENT_INFO_SUCCESS";

export interface CostEstimateRequestParam {
  note: string,
  method: string,
  serviceId: string,
  provider: ProviderResultsItem,
  onSuccess: () => void,
}

export interface RequestAppointmentParam {
  appointmentType: string,
  provider: ProviderResultsItem,
  reasonForVisit: string,
  onSuccess: () => void,
}

export const getPatientEmailBase64 = (state) => Buffer.from(R.path(["patient", "email"], state)).toString("base64");

const actions = {
  setPatientEmail: createStandardAction("SET_EMAIL_PATIENT")<string>(),
  requestInfo: createStandardAction(TYPE_INFO_REQUEST)<boolean>(),
  requestDashboard: createAction("TYPE_PATIENT_DASHBOARD"),
  benefitsSuccess: createStandardAction("TYPE_BENEFITS_SUCCESS")<BenefitsResponse>(),
  appointmentsSuccess: createStandardAction("TYPE_APPOINTMENT_SUCCESS")<RequestAppointmentResponse>(),
  patientInfoSuccess: createStandardAction(TYPE_INFO_SUCCESS)<PatientsInfo, boolean>(),
  infoError: networkFailure,
  pcpRequest: createAction("TYPE_PCP_REQUEST"),
  pcpSuccess: createStandardAction("TYPE_PCP_SUCCESS")<{}>(),
  pcpFailure: createAction("TYPE_PCP_FAILURE"),
  reset: createAction(RESET),
  requestPatientInsurance: createAction("REQUEST_PATIENT_INSURANCE"),
  patientInsuranceSuccess: createStandardAction("PATIENT_INSURANCE_SUCCESS")<Insurance>(),
  resetPasswordRequest: createStandardAction("RESET_PASSWORD")<{ currentPassword: string, newPassword: string, onSuccess: () => void }>(),
  resetPasswordSuccess: createAction("RESET_PASSWORD_SUCCESS"),
  patientUpdateRequest: createStandardAction("PATIENT_UPDATE_REQUEST")<{}>(),
  patientUpdateSuccess: createAction("PATIENT_UPDATE_SUCCESS"),
  costEstimateRequest: createStandardAction("PATIENT_COST_ESTIMATE")<CostEstimateRequestParam>(),
  costEstimateSuccess: createAction("PATIENT_COST_ESTIMATE_SUCCESS"),
  appointmentRequest: createStandardAction("PATIENT_REQUEST_APPOINTMENT")<RequestAppointmentParam>(),
  appointmentRequestSuccess: createAction("PATIENT_REQUEST_APPOINTMENT_SUCCESS"),
};

export const PatientAPIActions = actions;

interface PatientState {
  info: PatientsInfo;
  email: string;
  insurance: Insurance;
  benefits: BenefitsResponse;
  pcp: PCPResponse;
  appointments: RequestAppointmentResponse;
  fetching: boolean;
  errorInfo: ErrorResponse;
}

export type PatientInitialState = SI.Immutable<PatientState>;

/* ------------- Initial RootState ------------- */

export const INITIAL_STATE: PatientInitialState = SI.from({
  info: null,
  email: "",
  insurance: null,
  benefits: null,
  pcp: null,
  appointments: null,
  fetching: false,
  errorInfo: null,
});

/* ------------- Reducers ------------- */


/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actions.setPatientEmail)]: (state, {payload}) => state.merge({email: payload}),
  [getType(actions.requestInfo)]: (state) => state.merge({fetching: true, errorInfo: null}),
  [getType(actions.requestDashboard)]: (state) => state.merge({fetching: true, benefits: null, appointments: null}),
  [getType(actions.patientInfoSuccess)]: (state, action) => action.meta ? state.merge({fetching: false, info: action.payload}) : state.merge({info: action.payload}),
  [getType(actions.pcpSuccess)]: (state, action) => state.merge({pcp: action.payload, fetching: false}),
  [getType(actions.resetPasswordRequest)]: (state) => state.merge({fetching: true}),
  [getType(actions.resetPasswordSuccess)]: (state) => state.merge({fetching: false}),
  [getType(actions.patientUpdateRequest)]: (state) => state.merge({fetching: true}),
  [getType(actions.patientUpdateSuccess)]: (state) => state.merge({fetching: false}),
  [getType(actions.costEstimateRequest)]: (state) => state.merge({fetching: true}),
  [getType(actions.costEstimateSuccess)]: (state) => state.merge({fetching: false}),
  [getType(actions.appointmentRequest)]: (state) => state.merge({fetching: true}),
  [getType(actions.patientInsuranceSuccess)]: (state, action) => state.merge({insurance: action.payload}),
  [getType(actions.appointmentRequestSuccess)]: (state) => state.merge({fetching: false}),
  [getType(actions.infoError)]: (state, action) => state.merge({fetching: false, errorInfo: action.payload}),
  [getType(actions.benefitsSuccess)]: (state, action) => state.merge({fetching: false, benefits: action.payload}),
  [getType(actions.appointmentsSuccess)]: (state, action) => state.merge({fetching: false, appointments: action.payload}),
  [getType(actions.reset)]: (_) => INITIAL_STATE,
};

export const PatientReducers = createReducer(INITIAL_STATE, reducerMap);

export default PatientReducers;
