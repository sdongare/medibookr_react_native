import {Reducer} from "redux";
import * as SI from "seamless-immutable";
import {mapReducers, ReducerMap} from "../../Lib/ReduxHelpers";
import {networkFailure} from "@root/App/Epics/AppEpics";
import {createAction, createStandardAction} from "typesafe-actions";

/* ------------- Types and Action Creators ------------- */

const actionCreators = {
  failure: networkFailure,
  toggleRegistration: createAction("TOGGLE_REGISTRATION_PROCESS"),
  updatePCP: createStandardAction("UPDATE_PCP")<string, { onSuccess: () => void } | undefined>(),
  updatePCPSuccess: createAction("UPDATE_PCP_SUCCESS"),
};

export const RegistrationProcessActions = actionCreators;

export interface RegistrationProcessState {
  fetching?: boolean | null;
  isProcessing: boolean;
}

export type ImmutableRegistrationProcessState = SI.Immutable<RegistrationProcessState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableRegistrationProcessState = SI.from({
  fetching: null,
  isProcessing: false,
});

/* ------------- Reducers ------------- */


export const failure: Reducer<ImmutableRegistrationProcessState> = (state) => state.merge({fetching: false});

export const requestPcpUpdate: Reducer<ImmutableRegistrationProcessState> = (state) => state.merge({fetching: true});

export const pcpUpdateSuccess: Reducer<ImmutableRegistrationProcessState> = (state) => state.merge({fetching: false});

export const toggleRegistration: Reducer<ImmutableRegistrationProcessState> = (state) => state.merge({
  isProcessing: !state.isProcessing,
});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap: ReducerMap<typeof actionCreators, ImmutableRegistrationProcessState> = {
  failure,
  updatePCP: requestPcpUpdate,
  updatePCPSuccess: pcpUpdateSuccess,
  toggleRegistration,
};

export const RegistrationProcessReducer = mapReducers(INITIAL_STATE, reducerMap, actionCreators);

export default RegistrationProcessReducer;
