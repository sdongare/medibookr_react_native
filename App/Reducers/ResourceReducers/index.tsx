import {Reducer} from "redux";
import * as SI from "seamless-immutable";
import {createAction, createStandardAction} from "typesafe-actions";
import {mapReducers, ReducerMap} from "../../Lib/ReduxHelpers";
import {PayloadAction} from "typesafe-actions/dist/types";
import {networkFailure} from "@root/App/Epics/AppEpics";

/* ------------- Types and Action Creators ------------- */
export interface ResourceResponse {
  data: string;
}

const actionCreators = {
  request: createAction("RESOURCE_REDUCERS_REQUEST"),
  success: createStandardAction("RESOURCE_REDUCERS_SUCCESS")<ResourceResponse>(),
  failure: networkFailure,
};

export const ResourceReducersActions = actionCreators;

export interface ResourceReducersState {
  data?: ResourceResponse | null;
  error?: boolean | null;
  fetching?: boolean | null;
}

export type ResourceReducersAction = PayloadAction<string, ResourceReducersState>;

export type ImmutableResourceReducersState = SI.Immutable<ResourceReducersState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableResourceReducersState = SI.from({
  data: null,
  fetching: null,
});

/* ------------- Reducers ------------- */

export const request: Reducer<ImmutableResourceReducersState> =
  (state: ImmutableResourceReducersState) => state.merge({fetching: true});

export const success: Reducer<ImmutableResourceReducersState> = (state, {payload}) =>
  state.merge({data: payload, fetching: false});

export const failure: Reducer<ImmutableResourceReducersState> = (state: ImmutableResourceReducersState) =>
  state.merge({fetching: false});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap: ReducerMap<typeof actionCreators, ImmutableResourceReducersState> = {
  request,
  failure,
  success,
};

export const ResourceReducersReducer = mapReducers(INITIAL_STATE, reducerMap, actionCreators);

export default ResourceReducersReducer;
