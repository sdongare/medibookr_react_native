import {AnyAction, Reducer} from "redux";
import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import SearchByNameResponse from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {ErrorResponse} from "@root/App/Services/DataModels/ErrorResponse";
import {networkFailure} from "@root/App/Epics/AppEpics";
import {RESET} from "@root/App/Reducers/AppReducers";
import {createReducer} from "reduxsauce";

/* ------------- Types and Action Creators ------------- */
export interface DashboardSuccessParams {
  term: string;
  provider: SearchByNameResponse | null;
  service: ServicesSearchResponse | null;
  speciality: SpecialtySearchResponse | null;
}

const REQUEST = "DASHBOARD_REQUEST";
const SUCCESS = "DASHBOARD_SUCCESS";
const CANCEL = "DASHBOARD_CANCEL";

const actionCreators = {
  cancel: createAction(CANCEL),
  request: createStandardAction(REQUEST)<string>(),
  success: createStandardAction(SUCCESS)<DashboardSuccessParams>(),
  requestByName: createStandardAction("REQUEST_BY_NAME")<string>(),
  byNameSuccess: createStandardAction("REQUEST_BY_NAME_RESPONSE")<SearchByNameResponse>(),
  requestSpecialties: createStandardAction("DASHBOARD_REQUEST_SPECIALTIES")<string[]>(),
  requestSpecialtyByKeyword: createStandardAction("DASHBOARD_REQUEST_SPECIALTY_BY_KEYWORD")<string>(),
  specialitiesSuccess: createAction("DASHBOARD_SPECIALTIES_SUCCESS"),
  requestSpecialtyByName: createStandardAction("SPECIALTY_BY_NAME")<string>(),
  clearByName: createAction("CLEAR_BY_NAME_REQUEST"),
  failure: networkFailure,
  searchFailure: createAction("SEARCH_FAILURE"),
  reset: createAction(RESET),
};

export const DashboardActions = actionCreators;

export interface DashboardState {
  term?: string;
  nameResult?: SearchByNameResponse;
  nameFetching?: boolean;
  data?: DashboardSuccessParams;
  error?: ErrorResponse | null;
  fetchingSpecialty?: boolean | null;
  fetching?: boolean | null;
}

export type ImmutableDashboardState = SI.Immutable<DashboardState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableDashboardState = SI.from({
  data: null,
  error: null,
  nameResult: null,
  term: null,
  fetching: null,
  nameFetching: null,
});

/* ------------- Reducers ------------- */
export const cancel: Reducer<ImmutableDashboardState> = (state) => state.merge({
  data: null,
  error: null,
  fetching: false
});

export const request: Reducer<ImmutableDashboardState> = (state: ImmutableDashboardState, action) => state.merge({
  fetching: true,
  term: action.payload
});

export const success: Reducer<ImmutableDashboardState> =
  (state: ImmutableDashboardState, action: AnyAction & { payload?: DashboardSuccessParams }) => {
    if (!action.payload) {
      return failure(state, action);
    }

    return state.merge({fetching: false, error: null, data: action.payload});
  };

export const failure: Reducer<ImmutableDashboardState> = (state: ImmutableDashboardState, action) =>
  state.merge({fetching: false, error: action.payload, fetchingSpecialty: false, nameFetching: false});

export const searchFailure: Reducer<ImmutableDashboardState> = (state: ImmutableDashboardState, action) =>
  state.merge({fetching: false, error: action.payload, fetchingSpecialty: false, nameFetching: false});

export const reset: Reducer<ImmutableDashboardState> = (state) => INITIAL_STATE;

export const byNameRequest: Reducer<ImmutableDashboardState> = (state) => state.merge({nameFetching: true});

export const byNameResults: Reducer<ImmutableDashboardState> = (state, {payload}) => state.merge({
  nameResult: payload,
  nameFetching: false,
});

export const cancelNames: Reducer<ImmutableDashboardState> = (state) => state.merge({
  nameResult: null,
  nameFetching: false,
});


export const specialtySuccess: Reducer<ImmutableDashboardState> = (state) => state.merge({fetchingSpecialty: false});

export const specialtyByNameRequest: Reducer<ImmutableDashboardState> = (state, {payload}) => state.merge({
  fetchingSpecialty: true,
  term: payload,
});

export const requestSpecialties: Reducer<ImmutableDashboardState> = (state) => state.merge({fetchingSpecialty: true});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.cancel)]: cancel,
  [getType(actionCreators.request)]: request,
  [getType(actionCreators.failure)]: failure,
  [getType(actionCreators.searchFailure)]: searchFailure,  
  [getType(actionCreators.success)]: success,
  [getType(actionCreators.reset)]: reset,
  [getType(actionCreators.specialitiesSuccess)]: specialtySuccess,
  [getType(actionCreators.requestSpecialtyByName)]: specialtyByNameRequest,
  [getType(actionCreators.requestSpecialtyByKeyword)]: specialtyByNameRequest,
  [getType(actionCreators.requestSpecialties)]: requestSpecialties,
  [getType(actionCreators.requestByName)]: byNameRequest,
  [getType(actionCreators.byNameSuccess)]: byNameResults,
  [getType(actionCreators.clearByName)]: cancelNames,
};

export const DashboardReducer = createReducer(INITIAL_STATE, reducerMap);

export default DashboardReducer;
