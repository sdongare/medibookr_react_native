import rootEpics from "@root/App/Epics";
import {combineReducers} from "redux";
import configureStore from "./CreateStore";
import {PatientInitialState, PatientReducers} from "./PatientsReducers";
import DebugConfig from "@root/App/Config/DebugConfig";
import FixtureApi from "@root/App/Services/FixtureApi";
import Api from "@root/App/Services/Api";
import LoginReducers, {LoginState} from "@root/App/Reducers/LoginReducers";
import DashboardReducers, {DashboardState} from "@root/App/Reducers/DashboardReducers";
import ProviderReducers, {ProviderState} from "@root/App/Reducers/ProviderReducers";
import LocationReducers, {LocationState} from "@root/App/Reducers/LocationReducers";
import {NavigationState} from "react-navigation";
import GetHelpReducer, {GetHelpState} from "@root/App/Reducers/GetHelpReducers";
import AppReducer, {AppState} from "@root/App/Reducers/AppReducers";
import ResourceReducersReducer, {ResourceReducersState} from "@root/App/Reducers/ResourceReducers";
import RegistrationProcessReducer, {RegistrationProcessState} from "@root/App/Reducers/RegistrationProcessReducers";
import BiometricReducer, {BiometricState} from "@root/App/Reducers/BiometricReducers";

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  app: AppReducer,
  patient: PatientReducers,
  location: LocationReducers,
  resources: ResourceReducersReducer,
  registration: RegistrationProcessReducer,
  provider: ProviderReducers,
  help: GetHelpReducer,
  dashboard: DashboardReducers,
  login: LoginReducers,
  biometric: BiometricReducer,
});

export interface RootState {
  app: AppState;
  biometric: BiometricState;
  resources: ResourceReducersState;
  registration: RegistrationProcessState;
  patient: PatientInitialState;
  location: LocationState;
  provider: ProviderState;
  help: GetHelpState;
  dashboard: DashboardState;
  nav: NavigationState;
  login: LoginState;
}

const API_CALL = DebugConfig.useFixtures ? FixtureApi() : Api;

export default () => {
  const {store} = configureStore(reducers, rootEpics, {
    api: API_CALL,
  });

  return {store, api: API_CALL};
};
