import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import {Reducer} from "redux";
import {createReducer} from "reduxsauce";

/* ------------- Types and Action Creators ------------- */


const actionCreators = {
  toggleBiometric: createAction("BIOMETRIC_TOGGLE"),
  checkBiometric: createAction("BIOMETRIC_CHECK"),
  setBiometricSupport: createStandardAction("BIOMETRIC_SUPPORT_RESULT")<string | undefined>(),
  setBiometricEnableStatus: createStandardAction("BIOMETRIC_ENABLE_RESULT")<boolean>(),
  setUserNamePassword: createStandardAction("BIOMETRIC_USERNAME_PASSWORD_SET")<{ userName: string, password: string }>(),
};

export const BiometricActions = actionCreators;

export interface BiometricState {
  isBiometricEnable: boolean;
  biometricSupport?: string;
  userName: string;
  password: string;
}

export type ImmutableBiometricState = SI.Immutable<BiometricState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableBiometricState = SI.from({
  biometricSupport: undefined,
  isBiometricEnable: false,
  userName: "",
  password: "",
});

/* ------------- Reducers ------------- */

const setBiometricSupport: Reducer<ImmutableBiometricState> = (state, {payload}) => state.merge({biometricSupport: payload});

const setUserNamePassword: Reducer<ImmutableBiometricState> = (state, {payload}) => state.merge({
  userName: payload.userName,
  password: payload.password,
});

const isBiometricEnable: Reducer<ImmutableBiometricState> = (state, {payload}) => state.merge({isBiometricEnable: payload});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.setBiometricEnableStatus)]: isBiometricEnable,
  [getType(actionCreators.setBiometricSupport)]: setBiometricSupport,
  [getType(actionCreators.setUserNamePassword)]: setUserNamePassword,
};

export const BiometricReducer = createReducer(INITIAL_STATE, reducerMap);

export default BiometricReducer;
