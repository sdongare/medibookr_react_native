import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import {LoginResponse} from "@root/App/Services/DataModels/LoginResponse";
import {RESET} from "@root/App/Reducers/AppReducers";
import {createReducer} from "reduxsauce";
import {networkFailure} from "@root/App/Epics/AppEpics";


/* ------------- Types and Action Creators ------------- */

export interface LoginRequestParams {
  email: string;
  password: string;
  remember: boolean;
}

interface RegisterParams {
  groupNumber: string;
  memberId: string;
  email: string;
  password: string;
}

const TYPE_LOGIN_REQUEST = "LOGIN_REQUEST";
const TYPE_LOGIN_SUCCESS = "LOGIN_SUCCESS";
const TYPE_FORGOT_PASSWORD = "FORGOT_PASSWORD";
const TYPE_REGISTER = "REGISTER_USER";
const TYPE_FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";

const actionCreators = {
  request: createStandardAction(TYPE_LOGIN_REQUEST)<LoginRequestParams, boolean>(),
  success: createStandardAction(TYPE_LOGIN_SUCCESS)<LoginResponse>(),
  register: createStandardAction(TYPE_REGISTER)<RegisterParams>(),
  registerSuccess: createAction("REGISTER_SUCCESS"),
  forgotPassword: createStandardAction(TYPE_FORGOT_PASSWORD)<string>(),
  forgotPasswordSuccess: createAction(TYPE_FORGOT_PASSWORD_SUCCESS),
  logout: createAction("TYPE_LOGOUT"),
  loginFailure: createAction("LOGIN_FAILURE"),
  registerFailure: createAction("REGISTER_FAILURE"),
  failure: networkFailure,
  resetLogin: createAction(RESET),
  toggleLoader: createStandardAction("LOGIN_TOGGLE_LOADER")<boolean>(),
  oneTimeOpenLocationModal: createAction("ONE_TIME_OPEN_LOCATION_MODAL"),
};

export const LoginActions = actionCreators;

export interface LoginState {
  data?: LoginResponse | null;
  fetching?: boolean | null;
  openLocationModal: boolean;
}

export type ImmutableLoginState = SI.Immutable<LoginState>;

/* ------------- Initial RootState ------------- */

const INITIAL_STATE: ImmutableLoginState = SI.from({
  data: null,
  fetching: null,
  openLocationModal: false,
});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.request)]: (state) => state.merge({fetching: true}),
  [getType(actionCreators.register)]: (state) => state.merge({fetching: true}),
  [getType(actionCreators.registerSuccess)]: (state) => state.merge({fetching: false}),
  [getType(actionCreators.forgotPassword)]: (state) => state.merge({fetching: true}),
  [getType(actionCreators.logout)]: (state) => state.merge({fetching: true}),
  [getType(actionCreators.forgotPasswordSuccess)]: (state) => state.merge({fetching: false}),
  [getType(actionCreators.resetLogin)]: (_) => INITIAL_STATE,
  [getType(actionCreators.failure)]: (state) => state.merge({fetching: false}),
  [getType(actionCreators.loginFailure)]: (state) => state.merge({fetching: false}),
  [getType(actionCreators.registerFailure)]: (state) => state.merge({fetching: false}),    
  [getType(actionCreators.success)]: (state, action) => state.merge({fetching: false, data: action.payload, openLocationModal: true}),
  [getType(actionCreators.toggleLoader)]: (state) => state.merge({fetching: false}),
  [getType(actionCreators.oneTimeOpenLocationModal)]: (state) => state.merge({openLocationModal: false}),
};

export const LoginReducer = createReducer(INITIAL_STATE, reducerMap);

export default LoginReducer;
