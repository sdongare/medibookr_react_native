import {Reducer} from "redux";
import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import {networkFailure} from "@root/App/Epics/AppEpics";
import {createReducer} from "reduxsauce";

/* ------------- Types and Action Creators ------------- */
export interface GetHelpRequestParams {
  question: string;
  contactMethod: string;
  onSuccess: () => void;
}

export interface PharmacyCompareParam {
  drugName: string;
  dosage?: string;
  quantity?: string;
  comments?: string;
  contactMethod: string;
  onSuccess?: () => void;
}

export interface DrugInfoRequest {
  NDCItem: any;
  DrugItem: DrugResponse;
  PriceResponse: PriceResponse;
  Dosage: string;
  ConsumptionType: string;
}

const actionCreators = {
  success: createAction("GET_HELP_SUCCESS"),
  request: createStandardAction("GET_HELP_REQUEST")<GetHelpRequestParams>(),
  clearDrugName: createAction("CLEAR_DRUG_NAME"),

  requestDrugName: createStandardAction("GET_DRUGS_NAME")<string>(),
  drugNameSuccess: createStandardAction("DRUG_NAME_SUCCESS")<DrugResponse[]>(),

  requestDrugPrice: createStandardAction("GET_DRUGS_PRICE")<{ NDC: string, ZipCode: string, Quantity: string }>(),
  drugPriceSuccess: createStandardAction("DRUG_PRICE_SUCCESS")<PriceResponse[]>(),

  requestDrugStructure: createStandardAction("GET_DRUGS_STRUCTURE")<string>(),
  drugStructureSuccess: createStandardAction("DRUG_STRUCTURE_SUCCESS")<{}>(),

  requestDrugCompare: createStandardAction("GET_DRUG_COMPARE")<PharmacyCompareParam>(),
  failure: networkFailure,

  requestDrugInfo: createStandardAction("REQUEST_DRUG_INFO_SCREEN_3")<DrugInfoRequest>(),
  drugInfoSuccess: createAction("REQUEST_DRUG_INFO_SUCCESS"),

  requestInfoMessage: createStandardAction("TYPE_INFO_MESSAGE")<string>(),
  closeInfoModal: createStandardAction("TYPE_INFO_MODAL_CLOSE")(),
  openInfoModal: createStandardAction("TYPE_INFO_MODAL_OPEN")(),
  infoMessageSuccess: createStandardAction("TYPE_INFO_MESSAGE_SUCCESS")<InfoMessage>(),
};

export const GetHelpActions = actionCreators;

export interface GetHelpState {
  fetching?: boolean;
  fetchingDrugName: boolean;
  contactMethods: Array<{ [key: string]: string }>;
  drugNames: DrugResponse[] | undefined;
  drugPrices: PriceResponse[] | undefined;
  drugStructureResponse: any;
  infoMessage: InfoMessage;
  openInfoModal: boolean;
}

export type ImmutableGetHelpState = SI.Immutable<GetHelpState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableGetHelpState = SI.from({
  fetching: false,
  fetchingDrugName: false,
  contactMethods: [{email: "Email"}, {phone: "Phone"}],
  drugNames: undefined,
  drugPrice: undefined,
  drugStructureResponse: undefined,
  infoMessage: null,
  openInfoModal: false
});

/* ------------- Reducers ------------- */

export const request: Reducer<ImmutableGetHelpState> = (
  state: ImmutableGetHelpState,
) => state.merge({fetching: true});


export const requestDrugName: Reducer<ImmutableGetHelpState> = (state: ImmutableGetHelpState) => state.merge({fetchingDrugName: true});

export const drugNameSuccess: Reducer<ImmutableGetHelpState> = (state: ImmutableGetHelpState, action) =>
  state.merge({fetchingDrugName: false, drugNames: action.payload});

export const drugStructureSuccess: Reducer<ImmutableGetHelpState> = (state: ImmutableGetHelpState, action) =>
  state.merge({fetching: false, drugStructureResponse: action.payload});

export const drugPriceSuccess: Reducer<ImmutableGetHelpState> = (state: ImmutableGetHelpState, action) =>
  state.merge({fetching: false, drugPrices: action.payload});

export const clearDrugName: Reducer<ImmutableGetHelpState> = (
  state: ImmutableGetHelpState,
) => state.merge({fetchingDrugName: false, drugNames: null});

export const success: Reducer<ImmutableGetHelpState> = (
  state: ImmutableGetHelpState,
) => state.merge({fetching: false});

export const failure: Reducer<ImmutableGetHelpState> = (
  state: ImmutableGetHelpState,
) => state.merge({fetching: false, fetchingDrugName: false});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.request)]: request,
  [getType(actionCreators.requestDrugInfo)]: request,
  [getType(actionCreators.drugInfoSuccess)]: success,
  [getType(actionCreators.requestDrugCompare)]: request,
  [getType(actionCreators.requestDrugName)]: requestDrugName,
  [getType(actionCreators.drugNameSuccess)]: drugNameSuccess,
  [getType(actionCreators.clearDrugName)]: clearDrugName,
  [getType(actionCreators.failure)]: failure,
  [getType(actionCreators.success)]: success,
  [getType(actionCreators.requestDrugPrice)]: request,
  [getType(actionCreators.drugPriceSuccess)]: drugPriceSuccess,
  [getType(actionCreators.requestDrugStructure)]: request,
  [getType(actionCreators.drugStructureSuccess)]: drugStructureSuccess,
  [getType(actionCreators.requestInfoMessage)]: (state, {payload}) => state.merge({infoMessage: null }),
  [getType(actionCreators.infoMessageSuccess)]: (state, action) => state.merge({infoMessage: action.payload}),  
  [getType(actionCreators.closeInfoModal)]: (state) => state.merge({openInfoModal: false}),  
  [getType(actionCreators.openInfoModal)]: (state) => state.merge({openInfoModal: true}),  
};

export const GetHelpReducer = createReducer(INITIAL_STATE, reducerMap);

export default GetHelpReducer;
