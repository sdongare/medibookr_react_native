import {Reducer} from "redux";
import * as SI from "seamless-immutable";
import {createAction, createStandardAction, getType} from "typesafe-actions";
import GeoLocationResponse from "@root/App/Services/DataModels/GeoLocation";
import {LatLng, Region} from "react-native-maps";
import {ErrorResponse} from "@root/App/Services/DataModels/ErrorResponse";
import {networkFailure} from "@root/App/Epics/AppEpics";
import {createReducer} from "reduxsauce";
import {ProviderResultsItem} from "@root/App/Services/DataModels/Providers/SearchByNameResponse";
import {Supercluster} from "supercluster";

/* ------------- Types and Action Creators ------------- */

const LOCATION_REQUEST = "LOCATION_REQUEST";
const LOCATION_SUCCESS = "LOCATION_SUCCESS";
const LOCATION_FETCH_CANCEL = "LOCATION_CANCEL";
const LOCATION_SELECTED = "LOCATION_SELECTED";
const LOCATION_GPS = "LOCATION_GPS";
const LOCATION_MODAL = "LOCATION_MODAL";
const LOCATION_MODAL_CLOSE = "LOCATION_MODAL_CLOSE";

export interface LocationSuccessParams {
  keyword: string;
  data: GeoLocationResponse;
}

export interface LocationSelectedParams {
  keyword: string;
  location: LatLng | string;
}

export interface ILocationClusterResponse {
  region: Region;
  features: any[];
  cluster: Supercluster;
  zoomLevel: number;
}

const actionCreators = {
  request: createStandardAction(LOCATION_REQUEST)<string, boolean>(),
  checkLocationSetThroughGPS: createAction("LOCATION_CHECK_GPS"),
  requestZipCode: createAction("LOCATION_ZIP_CODE_REQUEST"),
  successZipCode: createStandardAction("LOCATION_ZIP_CODE_SUCCESS")<string>(),
  requestCluster: createStandardAction("LOCATION_CREATE_CLUSTER_REQUEST")<ProviderResultsItem[]>(),
  successCluster: createStandardAction("LOCATION_CREATE_CLUSTER_SUCCESS")<ILocationClusterResponse>(),
  success: createStandardAction(LOCATION_SUCCESS)<LocationSuccessParams>(),
  selected: createStandardAction(LOCATION_SELECTED)<LocationSelectedParams>(),
  gpsLocation: createStandardAction(LOCATION_GPS)<LatLng>(),
  failure: networkFailure,
  cancel: createAction(LOCATION_FETCH_CANCEL),

  clearDrugLocation: createAction("LOCATION_DRUG_CLEAR"),
  locationModal: createStandardAction(LOCATION_MODAL)<string>(),
  locationModalClose: createAction(LOCATION_MODAL_CLOSE),
};

export const LocationActions = actionCreators;

export interface LocationState {
  selected?: LatLng;
  zipCodeForDrugs?: string;
  cluster?: ILocationClusterResponse | null;
  gps?: LatLng | null;
  data?: GeoLocationResponse | null;
  input?: string;
  error?: ErrorResponse | null;
  fetching: boolean;
  fetchingZipCode: boolean;
  fetchingCluster: boolean;
  locationModalWindow: boolean;
  locationResult: string;
}

export type ImmutableLocationState = SI.Immutable<LocationState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableLocationState = SI.from({
  data: null,
  selected: undefined,
  cluster: null,
  gps: null,
  input: null,
  error: null,
  fetchingZipCode: false,
  fetching: false,
  fetchingCluster: false,
  locationModalWindow: false,
  locationResult: ""
});

/* ------------- Reducers ------------- */

const request: Reducer<ImmutableLocationState> = (state, action) => {
  const input = action.payload;
  return state.merge({fetching: input ? !action.meta : false, input, selected: input ? state.selected : undefined});
};

const requestDrugLocation: Reducer<ImmutableLocationState> = (state, action) => {
  return state.merge({fetching: true});
};
const requestCluster: Reducer<ImmutableLocationState> = (state) => state.merge({fetchingCluster: true, cluster: null});

const successCluster: Reducer<ImmutableLocationState> = (state, action) => state.merge({fetchingCluster: false, cluster: action.payload});

const gpsLocation: Reducer<ImmutableLocationState> = (state, action) => state.merge({gps: action.payload});

const success: Reducer<ImmutableLocationState> = (state, action) => {
  if (!action.payload) {
    return failure(state, action);
  }

  return state.merge({fetching: false, error: null, data: action.payload.data});
};

const cancel: Reducer<ImmutableLocationState> = (state) => state.merge({data: null, fetching: false});

const locationModal: Reducer<ImmutableLocationState> = (state, action) =>
  state.merge({locationModalWindow: true, locationResult: action.payload});

const locationModalClose: Reducer<ImmutableLocationState> = (state) => state.merge({ locationModalWindow: false });

const selected: Reducer<ImmutableLocationState> = (state, action) => {
  return state.merge({
    input: action.payload.keyword,
    selected: {
      latitude: action.payload.location.lat,
      longitude: action.payload.location.lng,
    },
    data: null,
  });
};

const failure: Reducer<ImmutableLocationState> = (state, action) =>
  state.merge({fetching: false, fetchingZipCode: false, error: action.payload});

const zipCodeSuccess: Reducer<ImmutableLocationState> = (state, action) =>
  state.merge({fetchingZipCode: false, zipCodeForDrugs: action.payload});

const zipCodeRequest: Reducer<ImmutableLocationState> = (state, action) =>
  state.merge({fetchingZipCode: true});

/* ------------- Hookup Reducers To Types ------------- */

const reducerMap = {
  [getType(actionCreators.requestZipCode)]: zipCodeRequest,
  [getType(actionCreators.successZipCode)]: zipCodeSuccess,
  [getType(actionCreators.request)]: request,
  [getType(actionCreators.selected)]: selected,
  [getType(actionCreators.requestCluster)]: requestCluster,
  [getType(actionCreators.successCluster)]: successCluster,
  [getType(actionCreators.cancel)]: cancel,
  [getType(actionCreators.gpsLocation)]: gpsLocation,
  [getType(actionCreators.failure)]: failure,
  [getType(actionCreators.success)]: success,
  [getType(actionCreators.locationModal)]: locationModal,
  [getType(actionCreators.locationModalClose)]: locationModalClose,
};

export const LocationReducer = createReducer(INITIAL_STATE, reducerMap);

export default LocationReducer;
