import {NavigationAction, NavigationState} from "react-navigation";
import AppNavigation from "@root/App/Navigation/AppNavigation";


export const NavigationReducer = (state: NavigationState, action: NavigationAction) => {
  // @ts-ignore
  const newState = AppNavigation.router.getStateForAction(action, state);
  return newState || state;
};
